package uk.ac.hw.macs.ilab.nlg;

/**************************************************************
 * @(#)XMLUtils.java
 *                                                         
 *     Copyright(C) 2010 iLab, Dept. of CS, School of MACS
 *              Heriot-Watt University
 *
 *                  All rights reserved
 *
 *   Version: V0.1   Created by XKL,  14/10/2010
 *                   Tidied/Modified: 15/11/2012
 *            
 *   Author: Xingkun Liu
 *
 *   Projects: FP7 SpaceBook, FP7 JAMES, EPSRC ABC-Project, FP7 Parlance.
 *   
 **************************************************************/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

/**
 * Convenient common methods handling XML.
 * 
 * @author XKL
 */
public class XMLUtils {
    
    static Logger logger = Logger.getLogger(XMLUtils.class.getName());    
    public static final String fsep = System.getProperty("file.separator");
    
   /**
     * Method1:
     * Using org.w3c.dom.ls.LSOutput, LSSerializer to write XML to a file
     * with specified encoding.
     * 
     * The methods using org.apache.xml.serialize.XMLSerializer of Xerces
     * are deprecated, and that is XML parser specific which may not work well
     * with other parsers.
     * 
     * XKL: 14 Oct. 2011
     * 
     * @param dom the document to save
     * @param fileURI saved file URI.
     * 
     */
   public static void writeXMLToFile1(Document dom, String dirURI, String fileName, String encoding) {

       /*
        *  another way:
       DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
       DOMImplementationLS impl = (DOMImplementationLS)registry.getDOMImplementation("LS");
        */
       logger.info("method: writeXMLToFile1, dirURI:" + dirURI);
       logger.info("method: writeXMLToFile1, fileName:" + fileName);
        // if dirURI does not exist, create it
        if (!(new File(dirURI)).exists()) {
            // create the directory
            (new File(dirURI)).mkdirs();
        }
        DOMImplementation impl = dom.getImplementation();
        DOMImplementationLS implLS = (DOMImplementationLS) impl.getFeature("LS", "3.0");
        LSSerializer lsSerializer = implLS.createLSSerializer();
        lsSerializer.getDomConfig().setParameter("format-pretty-print", true);
        LSOutput lsOutput = implLS.createLSOutput();
        // lsOutput.setEncoding("UTF-8"); // or GB2312
        lsOutput.setEncoding(encoding);
               //Writer stringWriter = new StringWriter();
        //lsOutput.setCharacterStream(stringWriter);
        OutputStream out = null;
        try {
            out = new FileOutputStream(dirURI + fsep + fileName);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        lsOutput.setByteStream(out);
        lsSerializer.write(dom, lsOutput);
 
        //String result = stringWriter.toString();       
   }
    
   /**
    *  Method2:
     * Using javax.xml.transform.Transformer to write XML to a file
     * with specified encoding.
     * 
     * The methods using org.apache.xml.serialize.XMLSerializer of Xerces
     * are deprecated, and that is XML parser specific which may not work well
     * with other parsers.
     * 
     * XKL: 14 Oct. 2011
     * 
     * @param dom the document to save
     * @param fileURI saved file URI.
     * 
     */
   public static void writeXMLToFile2(Document dom, String dirURI, String fileName, String encoding) {

       logger.info("method: writeXMLToFile2, dirURI:" + dirURI);
       logger.info("method: writeXMLToFile2, fileName:" + fileName);
        // if dirURI does not exist, create it
        if (!(new File(dirURI)).exists()) {
            // create the directory
            (new File(dirURI)).mkdirs();
        }
        
       Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException ex) {
            ex.printStackTrace();
        }
        //transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        DOMSource source = new DOMSource(dom);
        //Writer stringWriter = new StringWriter();
        //StreamResult streamResult = new StreamResult(stringWriter);
        OutputStream out = null;
        try {
            out = new FileOutputStream(dirURI + fsep + fileName);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        StreamResult streamResult = new StreamResult(out);
        try {
            transformer.transform(source, streamResult);
            //String result = stringWriter.toString();
        } catch (TransformerException ex) {
            ex.printStackTrace();
        }
   }

   /**
     * Using JAXP in implementation independent manner create a document object
     * using which we create a xml tree in memory
     */
    public static Document createDocument() {
        Document dom = null;
        //get an instance of factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            //get an instance of builder
           // dbf.setAttribute("encoding", "GB2312");
            DocumentBuilder db = dbf.newDocumentBuilder();
              
            //create an instance of DOM
            dom = db.newDocument();
            

        } catch (ParserConfigurationException pce) {
            //dump it
            logger.debug("Error while trying to instantiate DocumentBuilder " + pce);
            System.exit(1);
        }
        return dom;
    }
    
    public static Document parseXmlFile(String xmlFileURI) {
        Document dom = null;
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            //parse using builder to get DOM representation of the XML file
            dom = db.parse(xmlFileURI);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return dom;
    }
    
    /**
     * check and create a directory if it does not exist
     * @param dirURI the directory URI
     * @since 15/11/2012
     */
    public static void checkAndCreateDirectory(String dirURI) {
        if ((new File(dirURI)).exists()) {
            logger.debug("the directory already exists.");
        } else {
            // create the directory
            (new File(dirURI)).mkdirs();
            logger.info("Directory " + dirURI + " created!");
        }
    }

    /**
     * To get the JAVA VM default encoding name.
     * 
     * @return String of default encoding name
     */
   public static String getDefaultCharSet() {
    	OutputStreamWriter writer = new OutputStreamWriter(new ByteArrayOutputStream());
    	String enc = writer.getEncoding();
    	return enc;
    }    
    /**
     * 
     * Write a list of sentences to a file
     * 
     * @param fileURI a string of the file URI
     * @param language a string of language of sentences
     * @param contents ArrayList of sentences
     * @param encoding, example for "Canonical Name for java.nio API" ("Canonical Name for java.io and java.lang API")
     *                  UTF-8 (UTF8), UTF-16 (UTF-16), GB2312 (EUC_CN ), Big5(Big5), ISO-8859-1 (ISO8859_1) (Description: ISO 8859-1, Latin Alphabet No. 1 ).
     *                  refer to: http://docs.oracle.com/javase/1.5.0/docs/guide/intl/encoding.doc.html
     *                  Should prefer the first to the second as the former is standard e.g. UTF-8
     * @author XKL
     * @since  28/09/2012
     * 
     */
    public static void writeToTextFile(String fileURI, String language, ArrayList<String> contents, String commentPrefix, String encoding) {

        BufferedWriter out = null;
        try {
            // Create an OutputStreamWriter that uses the default character encoding.
            if(encoding == null) {
                out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileURI)));
                String defaultEncoding = getDefaultCharSet();
                logger.info("Using JAVA VM default encoding " + defaultEncoding);
                encoding = defaultEncoding;
                
            } else {
               out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileURI), encoding));
            }

        } catch (UnsupportedEncodingException ue) {

            //System.out.println("Not supported : " + ue.getMessage());
        } catch (IOException e) {
            //e.printStackTrace();
            //System.out.println(e.getMessage());
        }

        Calendar sysDate = Calendar.getInstance();
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        String timeStamp = df.format(sysDate.getTime());
        String headLine1 = "";
        String headLine2 = commentPrefix + " Generated at " + timeStamp + ";  Language: " + language + ";  Encoding: " + encoding;
        //String headLine3 = "-- Number of Sentences:" + contents.size() / 2;
        // String headLine3 = "-- Number of Sentences:" + contents.size();
        String headLine4 = "";

        try {
            out.write(headLine1);
            out.newLine();
            out.write(headLine2);
            out.newLine();
            //   out.write(headLine3);
            //  out.newLine();
            out.write(headLine4);
            out.newLine();

            for (int i = 0; i < contents.size(); i++) {
                String strToSave = contents.get(i);
                out.write(strToSave);
                out.newLine(); // Write system dependent end of line.
                // add extra empty line if it is even line
                // if ((i + 1) % 2 == 0) {
                out.newLine();
                //}
            }
            out.close();
        } catch (IOException e) {
            //e.printStackTrace();
            //System.out.println(e.getMessage());
        }

    }
    

    /**
     * Read line by line from a text file, return a array list of lines.
     * 
     * @param fileURI the file uri.
     * @param commentPrefix the prefix for commented lines, e.g. #
     * @return array of lines
     * 
     * @throws FileNotFoundException
     * @throws IOException 
     * @since  16/11/2012, by XKL
     */
    public static ArrayList<String> readFromTextFile(String fileURI, String commentPrefix, String encoding, String skipDuplicatedLines) 
            throws FileNotFoundException, IOException, UnsupportedEncodingException {
    
        DataInputStream inStream = null;

        inStream = new DataInputStream(new FileInputStream(fileURI));
        BufferedReader inBr = null;
        if(encoding == null)         {
            // use default encoding to read out the file
        inBr = new BufferedReader(new InputStreamReader(inStream));
        } else {
            inBr = new BufferedReader(new InputStreamReader(inStream, encoding));
        }
        //new OutputStreamWriter(new FileOutputStream(fileURI), encoding)
        ArrayList<String> lines = new ArrayList<String>();
        String line;
        int count = 0;        
        while ((line = inBr.readLine()) != null) {
            line = line.trim();
            // skip the comment line
           if(line.equals("") || line.startsWith(commentPrefix) ) continue;
           
            // consider inline comments
            if (line.indexOf(commentPrefix) >= 0) {
                line = line.substring(0, line.indexOf(commentPrefix));
            }
            
            if(skipDuplicatedLines.toLowerCase().equals("true")) {
            if (!lines.contains(line)) {
                lines.add(line);
            }
                
            } else {
           lines.add(line);                
            }
            
        }

        inBr.close();
        inStream.close();
        return lines;
    }
    
    /**
     * Generated unique string for file name based on current system time
     * 
     *  by XKL.
     * 
     * @return a string
     * 
     */
    public static String generateTimeStamp() {

        Calendar sysDate = Calendar.getInstance();
        int iYear = sysDate.get(Calendar.YEAR);
        int iMonth = sysDate.get(Calendar.MONTH) + 1;
        int iDayOfMonth = sysDate.get(Calendar.DAY_OF_MONTH);
        int iDayOfWeek = sysDate.get(Calendar.DAY_OF_WEEK);
        // use HOUR_OF_DAY or use AM_PM: 0 -- AM, 1 -- PM
        //int iHour = sysDate.get(Calendar.HOUR);
        int iHour = sysDate.get(Calendar.HOUR_OF_DAY);
        int iMinute = sysDate.get(Calendar.MINUTE);
        int iSecond = sysDate.get(Calendar.SECOND);
        int iMilliSec = sysDate.get(Calendar.MILLISECOND);
        String[] sWeek = {"Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"};
        String sYear = String.valueOf(iYear);
        String sMonth = String.valueOf(iMonth);
        String sDayOfMonth = String.valueOf(iDayOfMonth);
        String sDayOfWeek = String.valueOf(iDayOfWeek);
        String sHour = String.valueOf(iHour);
        String sMinute = String.valueOf(iMinute);
        String sSecond = String.valueOf(iSecond);
        if (iMonth <= 9) {
            sMonth = "0" + sMonth;
        }
        if (iDayOfMonth <= 9) {
            sDayOfMonth = "0" + sDayOfMonth;
        //if(iDayOfWeek <=9) sDayOfWeek = "0"+sDayOfWeek;
        }
        if (iHour <= 9) {
            sHour = "0" + sHour;
        }
        if (iMinute <= 9) {
            sMinute = "0" + sMinute;        //String sStr = "Y"+iYear + "_M"+ iMonth+"_D"+iDayOfMonth+"_"+sWeek[iDayOfWeek-1]+"_H"
        //	+iHour+"_Min"+iMinute+"_"+iMilliSec;

        //String sStr = "Y"+sYear + "_M"+ sMonth+"_D"+sDayOfMonth+"_"+sWeek[iDayOfWeek-1]+"_H"
        //	+sHour+"_Min"+sMinute+"_"+iMilliSec;
        }
        if (iSecond <= 9) {
            sSecond = "0"+sSecond;
        }

        String sStr = sDayOfMonth + sMonth + sYear + sWeek[iDayOfWeek - 1] + sHour + sMinute +sSecond+ "_" + iMilliSec;

        return sStr;
    } // end of method generateUniqueStringBySysTime()
    
    /**
     * 
     * To configure the log4j to log the debugging info into file or console
     * 
     * @param propURI the log4j property file uri.
     */
    public static boolean configLog4jByPropertyFile(String sAppHome, String propURI) {

        // load the log4f property file
        Properties log4jProps = new Properties();
        try {
            InputStream inStream = new FileInputStream(new File(propURI));
            log4jProps.load(inStream);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        // get the log file URI
        String logFileURI = null;
        if (log4jProps.getProperty("log4j.appender.logfile.File") != null) {
            String logFile = log4jProps.getProperty("log4j.appender.logfile.File");
            try {
                logFileURI = (new File(sAppHome, logFile)).getCanonicalPath();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            System.err.println("log4j.appender.logfile.File is not defined in your log4j property file.");
            System.err.println("APP_HOME dir:" + sAppHome);
            System.exit(1);
            return false;
        }

        // check if the directory exists, create it if not.
        String logDirURI = null;
        try {
            logDirURI = (new File(logFileURI)).getParentFile().getCanonicalPath();
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        // if dirURI does not exist, create it
        if (!(new File(logDirURI)).exists()) {
            // create the directory
            (new File(logDirURI)).mkdirs();
            //System.out.println("Debug log directory created: " + logDirURI);
        } else {
            //System.out.println("Debug log directory: " + logDirURI + " already existed. It is expected.");
        }

        // overwritten the relative log file with the absolute URI
        log4jProps.put("log4j.appender.logfile.File", logFileURI);

        // configure the log4j
        PropertyConfigurator.configure(log4jProps);
        logger.debug("Log4j is configured successfully!");
        return true;

    }
    
}
