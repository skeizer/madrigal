package uk.ac.hw.macs.ilab.nlg;

/**************************************************************
 * @(#)TemplateBasedNLG2 -- A simple template based generator for testing
 *
 *                                                         
 *     Copyright(C) 2012 iLab, Dept. of CS, School of MACS
 *              Heriot-Watt University
 *
 *                  All rights reserved
 *
 *   Author: Xingkun Liu
 * 
 *   Version: V0.1,   28 May 2013
 *            V1.0    12 Feb 2014 (for both System1 and System2 templates )
 *
 *   Project: Parlance 
 *            https://sites.google.com/site/parlanceprojectofficial/
 *                                                           
 **************************************************************/

//import hw.macs.ilab.utils.Pair;
//import hw.macs.ilab.utils.XMLUtils;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.apache.log4j.Logger;

import uk.ac.hw.madrigal.utils.MyLogger;

import java.io.Console;

/**
 * To create a simple template based generator.
 * The template used is from UCAM with some bug fixed.
 * 
 * History:
 *  12/02/2014, v1.0 for both sys1 and sys2
 *  14/07/2014, fixed bug in parseAllVarVals(DialogueAct daIn)
 *       for confreq(...) to consider value of dontcare
 *       because the templates didn't define the cases for dontcare value 
 *       while other templates have defined these cases and the codes had 
 *       processings for this special value.
 * 
 * TODO: 
 *    1. consider multiple definitions in the template. e.g. hello(): "xxxxx"; # "aaaaaa";
 *    2. consider multiple matching templates (currently only take the first matched one)
 * 
 * @author Xingkun Liu
 */
//public class TemplateBasedNLG2 implements TemplateBasedNLGFace {
public class TemplateBasedNLG2 {

	public static MyLogger logger1;
	
    //public final static Logger logger = Logger.getRootLogger();
    public static final String fsep = System.getProperty("file.separator");
    List<Template> templates;
    List<SubTemplate> subTemplates;
    // KnownDASlotNames is used for parsing Template or DialugeAct for confirm and select
    //public final static String[] KnownDASlotNames = {"food", "area", "pricerange", "drinks", "name", "type", "addr"};

    public TemplateBasedNLG2() {
    }

    public TemplateBasedNLG2(String templateFileURI, String crntLanguage, String crntEncoding) {
        readTemplatesFromFile(templateFileURI, crntEncoding);
    }

    public TemplateBasedNLG2(String templateFileURI) {
        readTemplatesFromFile(templateFileURI, null);
    }
    
    /**
     * the interface method called the caller class.
     * 
     * @param daIn a string of Dialogue Act input
     * @return a string of generated sentence.
     */
    public String generate(String daIn) {
        logger1.log( Level.FINE,"generate method receives:" + daIn);
        Template matchedTempl = null;

        // loop to find the matching template
        DialogueAct oDAIn = new DialogueAct(daIn);
        LinkedList<Template> allMatched = new LinkedList();
        for (int i = 0; i < templates.size(); i++) {
            Template aTmpl = templates.get(i);
            if (aTmpl.isTemplateMatching(oDAIn)) {
                logger1.log( Level.FINE,"A matched template:" + aTmpl.toString());
                allMatched.add(aTmpl);
            }
        }


        Template matchedTmpl = null;
        String out = "";

        // input: inform(name=none+other="true"+name="DOJO NOODLE BAR"+type="RESTAURANT"+near="Police Station"
        // will match:
        // inform(name=none,other=true,type=$U,$Y=$O,$Z=$P)
        // inform(name=none,other=true,name=$X,type=$U,$Y=$O)

        // For now, take the template with maximum non var slot as the matched one.
        // and maxinum non-var value e.g. confirm(addr=dontcare)
        if (allMatched.size() > 0) {
            int maxNumNonVar = allMatched.get(0).getNumOfNonVarSlots() + allMatched.get(0).getNumOfNonVarValues();
            int maxIdx = 0;
            for (int i = 0; i < allMatched.size(); i++) {
                Template aTmpl = allMatched.get(i);
                int numNonVar = aTmpl.getNumOfNonVarSlots() + aTmpl.getNumOfNonVarValues();
                if (numNonVar > maxNumNonVar) {
                    maxNumNonVar = numNonVar;
                    maxIdx = i;
                }
            }
            
            matchedTmpl = allMatched.get(maxIdx);
        }

        if (matchedTmpl == null) {
            logger1.log( Level.WARNING, "Sorry, no template matches the input:" + daIn + "\n" );
        } else {
            logger1.log( Level.FINE,"\n Final used matching template:" + matchedTmpl.toString() + "\n");
            out = matchedTmpl.realize(oDAIn);
            logger1.log( Level.FINE,"realized sentence:" + out);
        }
        //remove double quote in the sensence e.g. in the values
        out = out.replaceAll("\"", "");
        // replace double spaces to one space
        out = out.replaceAll("  ", " ");
        out = out.trim();
        if (out.endsWith(";")) {
            out = out.substring(0, out.length() - 1).trim();
        }
        // add missed punctuations
        if (!out.endsWith("?") && !out.endsWith("!") && !out.endsWith(".")) {
            out = out + ".";
        }

        logger1.log( Level.FINE,"Final output:" + out);
        return out;
    }
    
    public void readTemplatesFromFile( String fileURI, String encoding ) {
        logger1.log( Level.FINEST, "Trying to read template from file :" + fileURI );
        try {
            DataInputStream inStream = new DataInputStream( new FileInputStream(fileURI) );
            readTemplates( inStream, encoding );
        } catch ( IOException e ) {
            logger1.log( Level.SEVERE,"IOException when reading template file:" + fileURI);
            e.printStackTrace();
        }
    }

    public void readTemplates( InputStream is ) throws IOException {
    	readTemplates( is, null );
    }

    private void readTemplates( InputStream inStream, String encoding ) throws IOException {

            BufferedReader reader = null;
            if (encoding == null) {
                // use default encoding to read out the file
                reader = new BufferedReader(new InputStreamReader(inStream));
            } else {
                reader = new BufferedReader(new InputStreamReader(inStream, encoding));
            }

            this.subTemplates = new ArrayList<SubTemplate>();
            this.templates = new ArrayList<Template>();

            String line = "";
            String subName = "";
            String subParams = "";
            boolean startSub = false;
            List<String> subsubList = null;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                // ignore comments
                if (line.startsWith("#") || line.trim().equals("")) {
                    continue;
                }

                if (line.indexOf("#") > 0) {
                    logger1.log( Level.FINE,"There are more definitions in the template. Only use the first one for the moment.");
                    logger1.log( Level.FINE,"The template is " + line);
                    line = line.substring(0, line.indexOf("#")).trim();
                }

                if (line.trim().startsWith("%")) {
                    subName = line.substring(line.indexOf("%") + 1, line.indexOf("(")).trim();
                    subParams = line.substring(line.indexOf("(") + 1, line.indexOf(")")).trim();
                    startSub = true;
                    subsubList = new ArrayList<String>();
                    continue;

                } else if (line.trim().startsWith("}")) {
                    startSub = false;
                    logger1.log( Level.FINEST,"create subtemplate for " + subName + "--" + subParams + "--" + subsubList.toString());
                    SubTemplate aSub = new SubTemplate(subName, subParams, subsubList);
                    subTemplates.add(aSub);
                    continue;

                } else if (startSub) {
                    logger1.log( Level.FINEST,"added to subsublist:" + line);
                    subsubList.add(line);
                    continue;
                }

                // now process normal templates
                Template tmp = new Template(line);
                this.templates.add(tmp);
            }
            reader.close();

    }

    /**
     * To use the unique name of the subtemplate to get it.
     * Could consider the parameters later on.
     * 
     * @param subName
     * @return 
     */
    private SubTemplate findSubTemplate(String subName) {
        //List<String> subTmplParams = getSubTemplateParams(slotsValues);
        logger1.log( Level.FINE,"Trying to find a subTemplate for " + subName);
        //e.g. input: inform(name=none+type="COFFEE SHOP"+near="Museum")
        //     matched template: inform(name=none,type=$U,$Y=$O) : "I am sorry but there is no $U %$Y_str($O)."
        // Trying to find a subTemplate for $Y_str
        for (int i = 0; i < subTemplates.size(); i++) {
            SubTemplate st = subTemplates.get(i);
            //logger1.log( Level.FINEST,"subtempl name:" + st.subTemplateName);
            if (st.subTemplateName.equals(subName)) {
                logger1.log( Level.FINE,"Found it.");
                return st;
            }
        }
        logger1.log( Level.FINE,"Could not find a subtemplate for " + subName);
        return null;
    }

    class DialogueAct {

        String daName; // e.g. confirm
        // Pair < Slot, Value>, Value could be empty string.
        List<Pair<String, String>> daItems; // e.g. type= restaurant,addr=dontcare
        List<String> slotList;  // used for finding the matching template
        //String daItemSeparator = "\\+";
        String daItemSeparator = "\\s*,\\s*";
        String daString;

        public DialogueAct(String daActs) {
            logger1.log( Level.FINE,"Create DialogueAct from DM input:" + daActs);
            daString = daActs;
            daName = daActs.substring(0, daActs.indexOf("(")).trim();
            daItems = parseDAItems(daActs, daItemSeparator);
            slotList = getSlotList(daItems);
        }

        private List<String> getSlotList(List<Pair<String, String>> items) {
            ArrayList<String> slots = new ArrayList<String>();
            for (int i = 0; i < items.size(); i++) {
                Pair<String, String> apair = items.get(i);
                slots.add(apair.getElement0());
            }
            return slots;
        }

        public String getValue(String slot) {
            for (int i = 0; i < daItems.size(); i++) {
                String slot0 = daItems.get(i).getElement0();
                String value0 = daItems.get(i).getElement1();
                if (slot0.equals(slot)) {
                    return value0;
                }
            }
            return "";

        }

        public List<Pair<String, String>> cloneDaItems() {
            List newItems = new LinkedList<Pair<String, String>>();
            for (int i = 0; i < daItems.size(); i++) {
                String slot = daItems.get(i).getElement0();
                String value = daItems.get(i).getElement1();
                Pair<String, String> aPair = Pair.createPair(slot, value);
                newItems.add(aPair);
            }
            return newItems;
        }

        public int getNumOfDuplicatedSlot(String slot) {
            int num = 0;
            for (int i = 0; i < slotList.size(); i++) {
                String aslot = slotList.get(i).trim();
                if (aslot.equals(slot)) {
                    num++;
                }
            }
            return num;
        }
        
        public String toString() {
            return daString;
        }
    }

    /**
     * Parse the items of Dialogue Acts for Template left hand or for input Dialogue Acts.
     * One item is a slot-value pair. A value could be empty, e.g. for request(area)
     * 
     * Template left hand example: inform(name=none,other=true,name=$X,type=$U,$Y=$O)
     *                             select($X, $Y) in System1 version
     * An input DA example: inform(name=none+type="COFFEE SHOP"+near="Museum")
     * 
     * @param daActs the DA string
     * @param daItemSeparator the character to separate the items.
     * @return  a List of slot-value pair
     */
    protected List<Pair<String, String>> parseDAItems(String daActs, String daItemSeparator) {
       
        String daName = daActs.substring(0, daActs.indexOf("("));
        logger1.log( Level.FINEST,"daName:" + daName);
        String items = daActs.substring(daActs.indexOf("(") + 1, daActs.indexOf(")"));
        logger1.log( Level.FINEST,"items=" + items);
        String[] itemsArr = items.split(daItemSeparator);
        List daItems = new LinkedList<Pair<String, String>>();
        for (int i = 0; i < itemsArr.length; i++) {
            String item = itemsArr[i].trim();
            String slot = "";
            String value = "";
            if (item.indexOf("=") < 0) {
                slot = item;
            } else {
                slot = item.substring(0, item.indexOf("=")).trim();
                value = item.substring(item.indexOf("=") + 1).trim();
                // remove quote sign for single word value. e.g. for other="true"
                if (value.indexOf(" ") < 0) {
                    if (value.startsWith("\"")) {
                        value = value.substring(1);
                    }
                    if (value.endsWith("\"")) {
                        value = value.substring(0, value.length() - 1);
                    }
                }
            }
            //logger1.log( Level.FINEST,"slot-value:" + slot + "--" + value);
            // following conditions cover cases like:
            //confirm($A=$X) : "confirmdefault"; 
            // confirm($A=$X) : "Let me confirm, You are looking for a $X $A right?";
            //   request($A) : "What kind of $A would you like?"; 
            //   reqmore($A=dontcare) : "Would you like me to look for venues with any kind of $A?";
            // select($A,$B) : "Would you like a $A or a $B?";
            // select($A=$X,$A=$Y) : "Would you like $X or $Y $A?";
             /*
            if (item.startsWith("$") || !isItKnownDASlot(slot)) {
            // first condition is for parsing Template, the second is for parsing DialogueAct
            // special case for select(...)
            if (value.equals("")) {
            // select($A,$B) : "Would you like a $A or a $B?";
            // internally convert to select(ASlot = $A, BSlot= $B);
            if (i == 0) { // first slot $A or aValue for DialogueAct
            slot = "ASlot";
            value = item;
            } else {
            slot = "BSlot";
            value = item;
            }
            } else {
            // select($A=$X,$A=$Y) : "Would you like $X or $Y $A?";
            if (i == 0) {
            String value1 = slot; // i.e. the $A or its value in DialogueAct
            String slot1 = "TSlot";
            Pair<String, String> aPair = Pair.createPair(slot1, value1);
            daItems.add(aPair);
            slot = "XSlot";
            } else { // the second item: $A=$Y
            slot = "YSlot";
            }
            }
            } */

            Pair<String, String> aPair = Pair.createPair(slot, value);
            daItems.add(aPair);
        }

        return daItems;
    }

    /*
    private boolean isItKnownDASlot(String slot) {
    for (int i = 0; i < KnownDASlotNames.length; i++) {
    String slot0 = KnownDASlotNames[i];
    if (slot.equals(slot0)) {
    logger1.log( Level.FINEST,"This is known slot:" + slot);
    return true;
    }
    }
    logger1.log( Level.FINEST,"This is unknown slot:" + slot);
    return false;
    }*/
    /**
     * Examples of subtemplate
     * 
     * Exam1:
     *   %food_str($food) {
     *       dontcare : "if you don't care about the food";
     *       $food : "serving $food food";
     *    }
     * 
     * Exam2:
     * %area_near_str($area, $near) {
     *   dontcare, dontcare : "if you don't care about the area or where it is near to";
     *   dontcare, $near : "near $near";
     *   $area, dontcare : "in the area of $area if you don't care where it is near to";
     *   $area, $near : "near $near and in the area of $area";
     * }
     * 
     * 
     * 
     */
    class SubTemplate {

        String subTemplateName; // without the %, e.g. food_str, area_near_str
        String paramsStr; // ($area,$price)
        List<String> params; // <$area,$price>
        List<SubSubTemplate> subSubTempls = null;
        //HashMap<String, String> subTemplateLR;

        public String toString() {
            String str = subTemplateName + "(" + paramsStr + ")";
            String ssub = "";
            for (int i = 0; i < subSubTempls.size(); i++) {
                ssub += subSubTempls.get(i).toString() + "\n";
            }
            str = str + "{" + ssub + "}";
            return str;
        }

        public SubTemplate(String name, String varStr) {
            this.subTemplateName = name;
            this.paramsStr = varStr;

            this.params = parseVarList(varStr);
        }

        public SubTemplate(String name, String varStr, List<String> subsubList) { // called when reading from file
            this.subTemplateName = name;
            this.paramsStr = varStr;
            logger1.log( Level.FINEST,"subtemplate name and string:" + name + "--" + varStr);
            logger1.log( Level.FINEST,"subsub list:" + subsubList.toString());
            this.params = parseVarList(varStr);
            subSubTempls = new ArrayList<SubSubTemplate>();
            for (int i = 0; i < subsubList.size(); i++) {
                String sub = subsubList.get(i);
                SubSubTemplate ssub = new SubSubTemplate(sub);
                subSubTempls.add(ssub);
            }
        }

        private List<String> parseVarList(String varStr) {
            List varList = new ArrayList<String>();
            if (varStr.indexOf(",") < 0) { // only one var
                varList.add(varStr.trim());
            } else {
                String[] varArr = varStr.split(",");
                for (int i = 0; i < varArr.length; i++) {
                    varList.add(varArr[i].trim());
                }
            }
            return varList;
        }

        // assume input has same order as definitions e.g. $area, $food, $price
        // need to consider  $food, dontcare : "serving $food in any price range";
        protected SubSubTemplate findMatchedSubsubTemplate(List<String> inValues) {
            // loop subTempls;
            logger1.log( Level.FINEST,"trying to find matching subsubtemplate");
            for (int i = 0; i < subSubTempls.size(); i++) {
                SubSubTemplate subsub = subSubTempls.get(i);
                logger1.log( Level.FINEST,"a subsub:" + subsub.toString());
                boolean match = true;
                for (int j = 0; j < subsub.leftVars.size(); j++) {
                    // assume vars are in the same sequence 
                    String ssubVar = subsub.leftVars.get(j);
                    logger1.log( Level.FINEST,"ssubVar:" + ssubVar);
                    if (!ssubVar.startsWith("$") && !ssubVar.equals(inValues.get(j))) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    logger1.log( Level.FINEST,"Matched subsubtmpl:" + subsub.toString());
                    return subsub;
                } else {
                    continue;
                }
            }

            return null; // not found matched subsubtemplate
        }

        private String realizeSubTemplate(List<String> inValues) {
            // 
            SubSubTemplate ssubTmpl = findMatchedSubsubTemplate(inValues);
            if (ssubTmpl == null) {
                logger1.log( Level.SEVERE,"ERROR: not find a matched subsubtemplate!");
                return "";
            }

            String res = ssubTmpl.realize(inValues);

            return res;
        }

        class SubSubTemplate {

            String leftHand;
            String rightHand;
            List<String> leftVars; // include dontcare etc. It is actaully the List representation of the leftHand 

            // $food, dontcare : "serving $food in any price range";
            //$food, $price : "serving $food in the $price price range";
            public SubSubTemplate(String sub) {
                leftHand = sub.substring(0, sub.indexOf(":")).trim();
                rightHand = sub.substring(sub.indexOf(":") + 1).trim();
                if (rightHand.endsWith(";")) {
                    rightHand = rightHand.substring(0, rightHand.length() - 1).trim();
                }
                if (rightHand.endsWith("\"")) {
                    rightHand = rightHand.substring(0, rightHand.length() - 1);
                }
                if (rightHand.startsWith("\"")) {
                    rightHand = rightHand.substring(1);
                }

                leftVars = parseLeftHand(leftHand);
            }

            private List<String> parseLeftHand(String varStr) {
                List varList = new ArrayList<String>();
                if (varStr.indexOf(",") < 0) { // only one var
                    varList.add(varStr.trim());
                } else {
                    String[] varArr = varStr.split(",");
                    for (int i = 0; i < varArr.length; i++) {
                        varList.add(varArr[i].trim());
                    }
                }
                return varList;
            }

            /**
             * To realize the subsubtemplate, e.g. a subsubtemplate:
             *     $food, $price : "serving $food in the $price price range";
             * @param inValues a list of values
             * @return the surface form of the subsubtemplate.
             */
            public String realize(List<String> inValues) {
                String res = rightHand;
                //logger1.log( Level.FINEST,"Trying to realize subsubtemplate" + this.toString());
                logger1.log( Level.FINEST,"invalues size:" + inValues.size());
                logger1.log( Level.FINEST,"leftVar size:" + leftVars.size());
                for (int i = 0; i < leftVars.size(); i++) {
                    String var = leftVars.get(i);
                    logger1.log( Level.FINEST,"bbb:" + res + "--" + var);
                    if (var.startsWith("$")) {
                        // inValue has same order as vars in the subsubstemplate
                        logger1.log( Level.FINEST,"Before replaceAll: " + res);
                        logger1.log( Level.FINEST,"Before replaceAll:value " + inValues.get(i) + "--" + i);
                        res = res.replaceAll("\\" + var, inValues.get(i));
                        // res = res.replaceAll( var, inValues.get(i));
                        logger1.log( Level.FINEST,"after replaceAll: " + res);
                    }
                }
                return res;
            }

            public String toString() {
                return leftHand + " : " + rightHand;
            }
        }
    }

    class Template {

        String daItemSeparator = ","; // in Template it is comma ","
        //confirm(type=$U,addr=dontcare) : "Ok, a $U and you dont care about the address is that right?";
        String daName; // e.g. confirm
        List<Pair<String, String>> daItems; // e.g. type=$U,addr=dontcare
        String leftHand; // inform(name=$X,addr=none,area=$Z)
        String rightHand; //"$X is in the $Z of town but I don't know the exact address";
        // subTemplates here are referring templates which share the Class SubTemplate with SubTemplates definitions from file.
        List<SubTemplate> subTemplates;
        // allSlots records all slots in the template left hand. Used for multiple matching template
        List<String> allSlots;

        /**
         * The Template constructor
         * 
         *  examples:
         *  inform(name=$X,addr=none,area=$Z) : "$X is in the $Z of town but I don't know the exact address";
         *  inform(name=$X,pricerange=$price, phone=$phone) : "$X is a great restaurant %price_str($price) . %phone_str($phone) . ";
         * 
         * @param templ 
         */
        public Template(String tmpl) {
            // populate the template
            daName = tmpl.substring(0, tmpl.indexOf("(")).trim();
            leftHand = tmpl.substring(0, tmpl.indexOf(":")).trim();
            rightHand = tmpl.substring(tmpl.indexOf(":") + 1).trim();
            daItems = parseDAItems(leftHand, daItemSeparator);
            allSlots = parseSlotsOfItems(daItems);
            subTemplates = parseSubTemplates(rightHand);

        }

        public String toString() {
            return leftHand + " : " + rightHand;
        }

        public int getNumOfNonVarSlots() {
            int num = 0;
            for (int i = 0; i < allSlots.size(); i++) {
                String aslot = allSlots.get(i).trim();
                if (!aslot.startsWith("$")) {
                    num++;
                }
            }
            return num;
        }

        public int getNumOfNonVarValues() {
            int num = 0;
            
            for (int i = 0; i < this.daItems.size(); i++) {
                String tmplSlot = this.daItems.get(i).getElement0();
                String tmplValue = this.daItems.get(i).getElement1();
                if (!tmplValue.startsWith("$")) {
                    num++;
                }
            }
            
            return num;
        }
        
        public int getNumOfDuplicatedSlot(String slot) {
            int num = 0;
            for (int i = 0; i < allSlots.size(); i++) {
                String aslot = allSlots.get(i).trim();
                if (aslot.equals(slot)) {
                    num++;
                }
            }
            return num;
        }
        
        /**
         * Parse all slots in the template. Used for multiple matching template
         */
        private List<String> parseSlotsOfItems(List<Pair<String, String>> items) {
            List<String> slots = new ArrayList();
            for (int i = 0; i < items.size(); i++) {
                String slot = items.get(i).getElement0().trim();
                slots.add(slot);
            }
            return slots;
        }

        /*
        private List<Pair<String, String>> parseDAVarItems(List<Pair<String, String>> items) {
        List<Pair<String, String>> daVars = new ArrayList<Pair<String, String>>();
        for (int i = 0; i < items.size(); i++) {
        String slot = items.get(i).getElement0().trim();
        String value = items.get(i).getElement1().trim();
        if (value.startsWith("$")) {
        Pair<String, String> apair = Pair.createPair(value, slot);
        daVars.add(apair);
        }
        }
        return daVars;
        }
         */
        private List<SubTemplate> parseSubTemplates(String rightHand) {

            List<SubTemplate> subs = new LinkedList<SubTemplate>();

            if (rightHand.indexOf("%") < 0) { // so this template does not contain subtemplate
                return subs;
            }

            String remain = rightHand.substring(rightHand.indexOf("%"));
            while (remain.indexOf("%") >= 0) {
                String sub = remain.substring(remain.indexOf("%") + 1, remain.indexOf(")") + 1).trim();

                String subName = sub.substring(0, sub.indexOf("(")).trim();
                String varStr = sub.substring(sub.indexOf("(") + 1, sub.indexOf(")")).trim();
                //logger1.log( Level.FINEST,"subTemplate name:" + subName);
                //logger1.log( Level.FINEST,"subTemplate varStr:" + varStr);

                SubTemplate subTmpl = new SubTemplate(subName, varStr);
                subs.add(subTmpl);

                if (remain.length() <= (remain.indexOf(")") + 1)) {
                    break;
                }

                remain = remain.substring(remain.indexOf(")") + 1).trim();
            }
            return subs;
        }

        /**
         * To check if this template matches the input.
         * 
         * Matching conditions:
         *   1. daName is same.
         *   2. has same number of items
         *   3. non-variable slots are same.
         *   4. consistent values: either value or empty.
         *   5. same number of non-var slot (for duplicated slots)
         *         e.g. inform(name=none,other=true,name=$X,name=$E,$Y=$O)
         *   6. same number of special values. 
         *   
         * Template left hand examples:
         * inform(name=none,other=true,name=$X,name=$E,$Y=$O)
         * inform(name=none,other=true,name=$X,name=$E,$Y=$O,$Z=$P)
         * 
         * @param daIn DialogueAct object of the input
         * @return true if matching, false otherwise.
         */
        private boolean isTemplateMatching(DialogueAct daIn) {
            //logger1.log( Level.FINEST,"checking a match:" +this.daName + "--" + daIn.daName);
            //logger1.log( Level.FINEST,"checking a match: template:" +this.toString());
            //logger1.log( Level.FINEST,"checking a match: DA In:" +daIn.toString());

            // first check for the non-variable slots.
            boolean nonVarSlotMeet = true;
            for (int i = 0; i < this.daItems.size(); i++) {
                String tmplSlot = this.daItems.get(i).getElement0();
                String tmplValue = this.daItems.get(i).getElement1();
                //logger1.log( Level.FINEST,"tmpl slot value:" + tmplSlot +"--" + tmplValue);
                if (tmplSlot.startsWith("$")) {
                    continue;
                }
                
                // only get the first matching slot value. Assume non duplicated slot
                // in template where slot is non-var and value is non-var, e.g. name=none,other=true
                List<String> inValues = getValueFromPairListBySlot(daIn.daItems, tmplSlot);
                if (inValues.size() == 0) { // can not find slot in inItems.
                    nonVarSlotMeet = false;
                    break;
                }

                if (!tmplValue.startsWith("$")) { // template value is non-var
                    // then it should be found in inItem values for that slot
                    if (!inValues.contains(tmplValue)) {
                        nonVarSlotMeet = false;
                        break;
                    }
                }
            }

            // check if value is consistent
            int numTmplVal = 0;
            int numInputVal = 0;
            for (int i = 0; i < this.daItems.size(); i++) {
                String tmplValue = this.daItems.get(i).getElement1();
                if (tmplValue.equals("")) { // should be NotEquals?
                    numTmplVal++;
                }
            }

            for (int i = 0; i < daIn.daItems.size(); i++) {
                String inValue = daIn.daItems.get(i).getElement1();
                if (inValue.equals("")) { // should be NotEquals?
                    numInputVal++;
                }
            }
            
            //inform(name=none,other=true,name=$X,name=$E,$Y=$O)
            boolean sameDupSlots = true;
            for (int i = 0; i < this.daItems.size(); i++) {
                String tmplSlot = this.daItems.get(i).getElement0();
                if(!tmplSlot.startsWith("$")) {
                    int tmplNum = getNumOfDuplicatedSlot(tmplSlot);
                    int daNum = daIn.getNumOfDuplicatedSlot(tmplSlot);
                    if(tmplNum != daNum) {
                       // logger1.log( Level.FINE,"number of duplicated slots not match for slot:" + tmplSlot);
                        sameDupSlots = false;
                        break;
                    }
                }
            }
            
            //  same number of non-var values. (e.g. in select($X=$Y,$X=dontcare) )
            // now only consider special case for select(...)
            int numSpecialValofTmpl = 0;
            int numSpecialValofInput = 0;
            for (int i = 0; i < this.daItems.size(); i++) {
                String tmplVal = this.daItems.get(i).getElement1();
                if(tmplVal.equals("dontcare"))
                    numSpecialValofTmpl++;
            }
            
            for (int i = 0; i < daIn.daItems.size(); i++) {
                String inValue = daIn.daItems.get(i).getElement1();
                if(inValue.equals("dontcare"))
                    numSpecialValofInput++;

            }            
            
            boolean specialValMeet = true;
            if(this.daName.equals("select") && (numSpecialValofTmpl != numSpecialValofInput) ) {
                specialValMeet = false;
            }
                    
            if (this.daName.equals(daIn.daName) && (this.daItems.size() == daIn.daItems.size())
                    && (numTmplVal == numInputVal) && nonVarSlotMeet && sameDupSlots
                    && specialValMeet ) {
                return true;
            } else {
                return false;
            }

        }

       
        private List<Pair<String, String>> removePairBySlotValue(List<Pair<String, String>> itemPairs, String slot, String value) {
            
            for (int i = 0; i < itemPairs.size(); i++) {
                String pairSlot = itemPairs.get(i).getElement0();
                String pairValue = itemPairs.get(i).getElement1();
                //logger1.log( Level.FINEST,"sss:pair slot--value:" + pairSlot + "--" + pairValue);
                if (slot.equals(pairSlot) && value.equals(pairValue)) {
                    //logger1.log( Level.FINEST,("sss:pair slot--value: removed " + pairSlot + "--" + pairValue);
                    itemPairs.remove(i);
                    break;
                }
            }

            return itemPairs;

        }

        private LinkedList<String> getValueFromPairListBySlot(List<Pair<String, String>> itemPairs, String slot) {
            LinkedList<String> valList = new LinkedList();
            for (int i = 0; i < itemPairs.size(); i++) {
                String pairSlot = itemPairs.get(i).getElement0();
                if (slot.equals(pairSlot)) {
                    String val = itemPairs.get(i).getElement1();
                    valList.add(val);
                }
            }

            return valList;

        }

        /**
         * Get the values of template variables according to the input DAs.
         * To be used when doing realization.
         * 
         * E.g.
         *  Template: inform(name=none,type=$U,$Y=$O) : "I am sorry but there is no $U %$Y_str($O).";
         *  Input:    inform(name=none+type="COFFEE SHOP"+near="Museum")
         * 
         * The items in the input may not necessarily the same order as the template,
         * e.g. it could be inform(type="COFFEE SHOP"+name=none+near="Museum")
         * 
         * 
         */
        private LinkedHashMap<String, String> parseAllVarVals(DialogueAct daIn) {
            // daIn.daItems s1=v1, s2=v2, s3=v3
            List<Pair<String, String>> inItems4Vars = daIn.cloneDaItems();
            // first remove the non-varibles from input
            for (int i = 0; i < this.daItems.size(); i++) {
                String slot = this.daItems.get(i).getElement0();
                String value = this.daItems.get(i).getElement1();
                if (!slot.startsWith("$") && (value.equals("") || value.equals("none")
                        || value.equals("dontcare") || value.equals("true") || value.equals("false"))) {
                    inItems4Vars = removePairBySlotValue(inItems4Vars, slot, value);
                }
            }

            // get items which do not have value part. e.g. request("coffe shop").
            // The slot name item has been removed about e.g. request(area)
            List<Pair<String, String>> inItems4TwoVars = new LinkedList();
            LinkedList<String> inItems4OneVar = new LinkedList();
            for (int i = 0; i < inItems4Vars.size(); i++) {

                String slot = inItems4Vars.get(i).getElement0();
                String value = inItems4Vars.get(i).getElement1();
                //XKL 14-July-2014: to fix bug in confreq(..). The templates didn't define
                // cases in confreq(...) for value of dontcare. 
                // e.g. a template: confreq(count=$X,$Y=$P,$Q) : "There %count_rest($X) %$Y_str($P). %q($Q) ?";
                // an input: confreq(count=239+food=dontcare+goodformeal)
                //if (value.equals("") || value.equals("dontcare")) { // ToDo: consider all special value like empty, none, dontcare true or false etc.
                if (value.equals("") || (value.equals("dontcare") && !this.daName.equals("confreq")) ) { 
                    // e.g. reqmore($A=dontcare) === reqmore(PUB=dontcare)
                    inItems4OneVar.add(slot);
                } else {
                    Pair<String, String> aPair = Pair.createPair(slot, value);
                    inItems4TwoVars.add(aPair);
                }
            }
            
            LinkedHashMap varVals = new LinkedHashMap();

            // process only var of value: type=$U.  var include its $, and could be of Slot, or of Value
            for (int i = 0; i < daItems.size(); i++) {
                String slot = this.daItems.get(i).getElement0();
                String value = this.daItems.get(i).getElement1();
                if (!slot.startsWith("$") && value.startsWith("$")) {
                    List<String> vals = getValueFromPairListBySlot(inItems4TwoVars, slot);
                    if (vals.size() == 0) {
                        // inItems do not contain template slot. It is Error, need to check. possible template matching error.
                        logger1.log( Level.SEVERE,"The input do not contain template slot. Need to check. Possible template matching error!");
                    } else {

                        //if(!vals.get(0).equals("dontcare")) {
                        // e.g. for case: select(food=$X, food=dontcare). do not use dontcare as the value.
                        // ToDo: consider to use List rather than Map for duplicated keys.
                        varVals.put(value, vals.get(0)); // value is a var now
                        // remove the pair which is just used.
                        inItems4TwoVars = removePairBySlotValue(inItems4TwoVars, slot, vals.get(0));
                        //}
                    }
                }
            }

            // then process slot and value are both vars.
            int daInIdx = 0;
            for (int i = 0; i < daItems.size(); i++) {
                String slot = this.daItems.get(i).getElement0();
                String value = this.daItems.get(i).getElement1();
                //XKL 14-July-2014: to fix bug in confreq(..). see notes above.
               // if (slot.startsWith("$") && (value.equals("") || value.equals("dontcare")) ) { //ToDo: consider all special value. see above.
                if (slot.startsWith("$") && (value.equals("") || (value.equals("dontcare") && !this.daName.equals("confreq"))) ) {
                
                    // for cases: request($A), select($A,$B) etc. 
                    String slotVal = inItems4OneVar.remove();//retrieve and remove the head. The list should be made sure there are required items before 
                    varVals.put(slot, slotVal);
                } else if (slot.startsWith("$") && value.startsWith("$")) {
                    // just use the index as inItems4Var are now left only with variables for both slot and value
                    // e.g. $Y=$O,$Z=$P,$W=$R, input: s1=v1, s2=v2, s3=v3
                    // this is the matched template, so the input and template have the same number of items.
                    String slotVal = inItems4TwoVars.get(daInIdx).getElement0();
                    String valueVal = inItems4TwoVars.get(daInIdx).getElement1();
                    daInIdx++;
                    varVals.put(slot, slotVal);
                    varVals.put(value, valueVal);
                }

            }
            // assume there is no case in template where slot is variable but value is non-variable: $Y = aValue, or $area = aValue
            return varVals;
        }

        /**
         * To realize this matched template. First it needs to check if it matches the input.
         * 
         * @param daIn  string of DA input
         * @return string of realized sentence.
         * 
         */
        private String realize(DialogueAct daIn) {
            // use values in daIn to replace vars in rightHand
            // first replace subtemplates
            // loop subTemplates 
            LinkedHashMap<String, String> allVarVals = parseAllVarVals(daIn);

            //SubTemplate subTempl;
            String rh = rightHand;
            for (int i = 0; i < subTemplates.size(); i++) { // subtemplates referred in the right hand side of this template
                SubTemplate refTmpl = subTemplates.get(i); //the refTmpl.subTemplateName is wihtout %, in new version: %$Z_str($P), the name is $Z_str
                String realSubName = "";
                if (refTmpl.subTemplateName.startsWith("$")) { // for System2 new version of Template
                    String subNameVar = refTmpl.subTemplateName.substring(0, refTmpl.subTemplateName.indexOf("_")); // e.g. $Z
                    String subNameSuffix = refTmpl.subTemplateName.substring(refTmpl.subTemplateName.indexOf("_")); // e.g. "_str", "_inf"
                    // find the real definition for the refering template here: subTempl
                    logger1.log( Level.FINEST, "subNameVar:" + subNameVar);
                    String subNameVal = allVarVals.get(subNameVar); //$Z to "area"
                    logger1.log( Level.FINEST, "subNameVaule:" + subNameVal);
                    realSubName = subNameVal + subNameSuffix;
                } else {
                    realSubName = refTmpl.subTemplateName;
                }
                logger1.log( Level.FINEST, "template name referred in matching template:" + realSubName);
                //SubTemplate defTmpl = findSubTemplate(refTmpl.subTemplateName);
                SubTemplate defTmpl = findSubTemplate(realSubName);
                logger1.log( Level.FINEST, "Matched subTemplate:" + defTmpl.toString());
                // get value list for subtemplate
                ArrayList<String> values = new ArrayList<String>();


                for (int j = 0; j < refTmpl.params.size(); j++) { // system2 version only has one params
                    String paramVar = refTmpl.params.get(j);
                    // all parameters in defTmpl startsWith $
                    logger1.log( Level.FINEST,"paramSlot:" + paramVar);
                    // String val = daIn.getValue(paramSlot);
                    String paramVal = allVarVals.get(paramVar);
                    logger1.log( Level.FINEST,"paramSlot:Val:" + paramVal);
                    values.add(paramVal);
                }
                //logger1.log( Level.FINEST,"input to subtemplate values:" + values.toString());
                String subStr = defTmpl.realizeSubTemplate(values);
                //logger1.log( Level.FINEST,"realised value:" + subStr);
                // now replace the subtemplate with the subStr
                //$X is a great restaurant %area_price_str($area,$price) . %phone_str($phone) .
                String subInRightHand1 = rh.substring(rh.indexOf(refTmpl.subTemplateName) - 1).trim();
                //logger1.log( Level.FINEST,"RH:" + rh);
                //logger1.log( Level.FINEST,"a sub1 in RH:" + subInRightHand1);
                String subInRH = subInRightHand1.substring(0, subInRightHand1.indexOf(")") + 1).trim();
                //logger1.log( Level.FINEST,"a sub in RH:" + subInRH);
                // rh = rh.replaceAll(subInRH, subStr);
                rh = rh.replace(subInRH, subStr);
                logger1.log( Level.FINEST,"RH after replace" + rh);
            }

            // now realizing for normal variables

            for (Map.Entry<String, String> entry : allVarVals.entrySet()) {

                String var = entry.getKey();
                String value = entry.getValue();
                if (rh.indexOf(var) >= 0) {
                    rh = rh.replaceAll("\\" + var, value); // a var startsWith $
                    logger1.log( Level.FINEST,"rh after replace:" + rh);
                }

            }

            return rh;
        }
    }

    /**
     * XKL added notes
     * This is from Regime Phase1. C++ output to terminal via stdout
     * then java NLG receives from here.
     */
    private void test() {
        ArrayList<String> daIn = new ArrayList<String>();
        
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String str;          
            while ( (str = br.readLine()) != null )
            {
                    daIn.add(str);
            }
            
        }catch(Exception ex){
         
            // if any error occurs
            ex.printStackTrace();      
        }

        for (int i = 0; i < daIn.size(); i++) {
            singleTest(daIn.get(i));
        }
    }

    private void singleTest() {
        //String daIn = "inform(type=restaurant+pricerange=cheap);";
        String daIn = "inform(type=restaurant, pricerange=cheap);";
        String utt = generate(daIn);
        logger1.log( Level.FINEST,"\n\nGenerated sentence:" + utt + "\n\n");
    }

    private void singleTest_madrigal() {
        String daIn = "inform(name=All Bar One,pricerange=cheap,area=riverside);";
        String utt = generate(daIn);
        logger1.log( Level.FINEST,"\n\nGenerated sentence: " + utt + "\n\n");
    }

    private void singleTest(String daIn) {
        String utt = generate(daIn);
        System.out.println(utt);
    }
    
    private void testInmission() {
        String rpt = "ReportMission(PercentComplete=50%)";
        String utt = generate(rpt);
        logger1.log( Level.FINEST,"Generated sentence:" + utt);        
    }

    public String generateReport(List<String> rptSemantics) {
        String utt="";    
        for (int i = 0; i < rptSemantics.size(); i++) {
           utt += generate(rptSemantics.get(i)) + "\n";
        }
        return utt;
    }
    
    public static void main(String[] args) {
       
        String appHome = ".";
        String log4jConfig = appHome + fsep + "resources"+ fsep + "log4j" + fsep + "log4j.properties";
        XMLUtils.configLog4jByPropertyFile(appHome, log4jConfig);
      
        //String templateFile = appHome + fsep + "resources"+ fsep + "nlg_templates" + fsep + "TopTableMessages_Sys2.txt";
        String templateFile = appHome + fsep + "resources"+ fsep + "nlg_templates" + fsep + "CamInfo_Madrigal_templates.txt";
        
        TemplateBasedNLG2 tnlg2 = new TemplateBasedNLG2(templateFile);
        //tnlg2.singleTest();
        tnlg2.singleTest_madrigal();
        //tnlg2.test();
    }
}
