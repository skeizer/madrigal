package uk.ac.hw.macs.ilab.nlg;

/**************************************************************
 * @(#)TemplateBasedNLGFace -- A interface for TemplatedBasedNLG
 *
 *                                                         
 *     Copyright(C) 2012 iLab, Dept. of CS, School of MACS
 *              Heriot-Watt University
 *
 *                  All rights reserved
 *
 *   Author: Xingkun Liu
 * 
 *   Version:  V1.0    12 Feb 2014 
 *
 *   Project: Parlance 
 *            https://sites.google.com/site/parlanceprojectofficial/
 *                                                           
 **************************************************************/
//package hw.macs.ilab.parlance.nlg;

/**
 * A interface for template-based NLG.
 * 
 * @author XKL
 */
public interface TemplateBasedNLGFace {
    public String generate(String dacts);
}
