/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created March 2017                                                              *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import uk.ac.hw.madrigal.utils.LoggingManager;

/**
 * Simple GUI to show messages about the dialogue system server and the connections it makes.
 * The window closing event is used to properly close the log files.
 */
public class TextDialSystemServerGUI extends JFrame {
	
	private static final long serialVersionUID = 1L;

	private JTextArea logArea;
	
	/** Constructor */
	public TextDialSystemServerGUI() {
		super( "Text-based dialogue system server" );

		Container container = getContentPane();
		container.setLayout( new BorderLayout() );
		logArea = new JTextArea( 15, 50 );
		logArea.setEditable( false );
		container.add( logArea, BorderLayout.CENTER );
		
        addWindowListener( new WindowAdapter() {
        	public void windowClosing( WindowEvent we ) {
        		LoggingManager.close_loggers();
        		System.out.println( "Log files closed" );
        		System.exit( 0 );
        	}
        } );
        
        pack();
        setVisible( true );
	}

	public void message( String msg ) {
		logArea.append( msg + "\n" );
	}
	
}
