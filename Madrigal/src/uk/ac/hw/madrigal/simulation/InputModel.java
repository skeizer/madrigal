package uk.ac.hw.madrigal.simulation;

import java.util.Properties;
import java.util.logging.Level;

import resources.Configuration;
import cc.mallet.util.Randoms;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.utils.MyLogger;

/**
 * Simulates the top level input processing, including determining which
 * of the lower processing levels is achieved.  In case of interpretation
 * level processing, the error model is consulted to produce n-best lists
 * of dialogue acts.
 */
public class InputModel {
	
	public static MyLogger logger;

	public static Randoms rand_num_gen;

	public static double perception_prob = 0.1d;
	public static double interpretation_prob = 0.1d;
	
	public static void computeNBest( DialogueTurn turn ) {
		if ( rand_num_gen.nextDouble() < perception_prob )
			return;		
		if ( rand_num_gen.nextDouble() < interpretation_prob ) {
			turn.setProcLevel( ProcLevel.PERCEPTION );
			return;
		}
		turn.setProcLevel( ProcLevel.INTERPRETATION );
		turn.dact_nbest_all = ErrorModel.getNBest( turn.dact_all );
	}
	
	public static void loadConfig( Properties config ) {
		perception_prob = Double.parseDouble( Configuration.PERC_PROBLEM_PROB );
		interpretation_prob = Double.parseDouble( Configuration.INT_PROBLEM_PROB );

		StringBuffer logStr = new StringBuffer( "Input model configuration:\n" );
		logStr.append( String.format( "   perception_prob: %.2f\n", perception_prob ) );
		logStr.append( String.format( "   interpretation_prob: %.2f\n", interpretation_prob ) );
		logger.logf( Level.WARNING, "%s\n", logStr );
		
		ErrorModel.loadConfig( config );
	}
	
}
