/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Simulation of user behaviour and ASR/SLU noise.                                 *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.simulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;
import java.util.logging.Level;

import cc.mallet.types.Dirichlet;
import cc.mallet.types.Multinomial;
import cc.mallet.util.Randoms;
import resources.Configuration;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.SemanticContent;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.utils.MyLogger;

public class ErrorModel {
	
	public static MyLogger logger;

	public static Randoms rand_num_gen; // = TestProgram.rand_num_gen;

	/** Domain ontology used for creating confusions in semantic content of a dialogue act */
	public static Ontology domain; // = TestProgram.domain;
	
	private static int MAX_NUM_ATTEMPTS = 5; // number of attempts to confuse a dialogue act
	
	/** Number of dialogue acts in nbest list of confused dialogue acts */
	private static int nbest_size = 1;

	/** Distribution for sampling confidence scores */
	private static Dirichlet conf_distr;
	
	/** Probability of confusing a dialogue act */
	private static double confusion_rate = 0d;

	/** Parameter for generating parameters of the Dirichlet distribution for generating confidence scores */
	private static double oracle_exp_param = 1d;
	
	/** Parameter for controlling variance of confidence scores */
	private static double variability = 32d; // default value adopted from (Thomson et al, SLT 2012)
	
	/** Probability of confusing the communicative function of a dialogue act */
	private static double cf_confusion_rate = 0d;
	
//	private static CommFunction selectedCFs[] = { CommFunction.inform, CommFunction.initialGoodbye, CommFunction.setQuestion };
	private static CommFunction selectedCFs[] = { CommFunction.inform, CommFunction.confirm, CommFunction.disconfirm, CommFunction.propQuestion, CommFunction.initialGoodbye, CommFunction.setQuestion };
	
	/** Probability of removing a slot value pair */
	private static double item_deletion_rate = 0d;
	
	/** Probability of inserting a slot value pair */
	private static double item_insertion_rate = 0d;
	
	/** Probability of confusing a slot value pair in the semantic content of a dialogue act */
	private static double slot_confusion_rate = 0d;
	
	/** Probability of confusing the value of a slot in the semantic content of a dialogue act */
	private static double value_confusion_rate = 0d;
	
	/** 
	 * Returns an n-best list of <code>nbest_size</code> dialogue act hypotheses, 
	 * potentially containing one or more confusions, based on the given true dialogue act.
	 */
	public static ArrayList<DialogueAct> getNBest( DialogueAct dact ) {
		logger.logf( Level.INFO, "Generating n-best list for user act: %s\n", dact.toShortString() );
		ArrayList<DialogueAct> nbest;
		if ( confusion_rate == 0 || nbest_size == 0 ) { //TODO remove the nbest_size bit?
			logger.log( Level.INFO, "No confusions to generate: return list containing only the true act\n" );
			nbest = new ArrayList<DialogueAct>();
			nbest.add( new DialogueAct(dact) );
			return nbest;
		}
		
		Dirichlet conf_dirichlet = conf_distr;
		CommFunction cf = dact.getCommFunction();
		
		// lower confusion rate for yes/no/goodbye
		boolean cfOnly = dact.getSemContent().isEmpty();
		if ( cfOnly && (cf == CommFunction.confirm || cf == CommFunction.disconfirm || cf == CommFunction.initialGoodbye) )
			conf_dirichlet = getConfScoring( 0.5 * confusion_rate );
		
		// generate confidence scores and position of true user act
		double[] conf_scores = conf_dirichlet.randomVector( rand_num_gen );
		Multinomial distr = new Multinomial( conf_scores );
		int dact_pos = distr.randomIndex( rand_num_gen );
		logger.logf( Level.INFO, "Position of true user act: %d\n", dact_pos );
		DialogueAct[] nbest_arr = new DialogueAct[ nbest_size ];
		if ( dact_pos < nbest_size )
			nbest_arr[ dact_pos ] = new DialogueAct( dact ); // correct dial act hypothesis

		// create confused dialogue act hypotheses to complete the n-best list
		DialogueAct da_hyp = null;
		boolean duplicate;
		for ( int i = 0; i < nbest_size; i++ ) {
			logger.logf( Level.INFO, "N-best position %d\n", i );
			duplicate = true;
			if ( i != dact_pos ) {
				int num_attempts = 0;
				while ( duplicate && num_attempts < MAX_NUM_ATTEMPTS ) { //TODO introduce config for max num attempts?
					da_hyp = getDialActHyp( dact );
					logger.logf( Level.FINE, "DA hyp (%d): %s\n", num_attempts, da_hyp.toShortString() );
					if ( !dact.equals(da_hyp) ) { // for some reason, the true act has not been changed
						int j = 0;
						while ( j < i ) {
							if ( nbest_arr[j].equals(da_hyp) )
								break;
							j++;
						}
						if ( j == i )
							duplicate = false;
					}
					num_attempts++;
				}
				if ( da_hyp == null )
					System.out.println( "ERROR: null dial act hypothesis created!" );
				//else if ( num_attempts < MAX_NUM_ATTEMPTS ) {
				else {
					logger.logf( Level.FINE, "==> Adding DA hyp to list: %s\n", da_hyp.toShortString() );
					nbest_arr[i] = da_hyp; // incorrect dial act hypothesis
				}
			}
			logger.logf( Level.INFO, "Adding act hypothesis: %s (%.3f)\n", nbest_arr[i].toShortString(), distr.probability(i) );
			nbest_arr[i].setConf( conf_scores[i] );
		}
		nbest = new ArrayList<DialogueAct>( Arrays.asList(nbest_arr) );
		DialogueAct.mergeDuplicates( nbest );
		Collections.sort( nbest );
        StringBuffer logStr = new StringBuffer( "N-best list:\n" );
		for ( DialogueAct dact_hyp: nbest )
			logStr.append( String.format( "  %s (%.3f)\n", dact_hyp.toShortString(), dact_hyp.getConf() ) );
		logger.logf( Level.INFO, "%s\n", logStr );

		return nbest;
	}
	
	/**
	 * Returns a dialogue act hypothesis with one or more confusions.
	 */
	private static DialogueAct getDialActHyp( DialogueAct dact ) {
		DialogueAct dact_hyp = new DialogueAct();
		dact_hyp.setSpeaker( dact.getSpeaker() );
		dact_hyp.setAddressee( dact.getAddressee() );
		
		// confuse the CF with probability given by cf_confusion_rate
		if ( rand_num_gen.nextDouble() < cf_confusion_rate ) {
			CommFunction cfHyp = getCFHyp( dact.getCommFunction() );
			dact_hyp.setCommFunction( cfHyp );
		} else
			dact_hyp.setCommFunction( dact.getCommFunction() );
		
		CommFunction cf_hyp = dact_hyp.getCommFunction();
		if ( cf_hyp == CommFunction.initialGoodbye || cf_hyp == CommFunction.thanking ) { //NOTE there might be other CFs we don't want to combine with any content
			logger.log( Level.FINE, "Prevent goodbye with non-empty semantic content\n" );
			return dact_hyp;
		}
		
		// confuse the semantic content
		SemanticItem itemHyp = getSemItemHyp( dact_hyp.getCommFunction(), dact.getSemContent() );
		dact_hyp.addItem( itemHyp );

		return dact_hyp;
	}
	
	/**
	 * Returns a confused version of the given semantic item, conditional on the communicative function of the dialogue act
	 */
	private static SemanticItem getSemItemHyp( CommFunction cf, SemanticContent sem_content ) {
		logger.logf( Level.FINE, "Confusing semantic content %s\n", sem_content.toString() );
		
		SemanticItem item_hyp = new SemanticItem();
		SlotValuePair svp_hyp;
		boolean notFound;
		boolean reqSlotRequired = ( cf == CommFunction.setQuestion ); // flag to ensure there is a requested slot in the created DA hyp
		boolean noReqSlots = ( cf != CommFunction.setQuestion ); // to avoid creating a slot without value (used to retrieve slots from ontology)
		
		if ( !sem_content.isEmpty() ) {
			SemanticItem item = sem_content.getFirstItem();
			for ( SlotValuePair svp: item.svps ) {
				
				// handle requested slot
				if ( svp.value.isEmpty() && reqSlotRequired ) {
					svp_hyp = getSVPHyp( false, true, svp );
					item_hyp.addSlotValuePair( svp_hyp );
					reqSlotRequired = false;
					continue;
				}
				// decide whether to delete current svp
				if ( rand_num_gen.nextDouble() < item_deletion_rate )
					continue;
				
				// decide whether to confuse this svp
				if ( rand_num_gen.nextDouble() < slot_confusion_rate )
					svp_hyp = getSVPHyp( noReqSlots, false, svp );
				else
					svp_hyp = new SlotValuePair( svp ); // leave this slot value pair unchanged

				// ensure new svp hyp is not duplicate
				notFound = true;
				for ( SlotValuePair prev_svp_hyp: item_hyp.svps ) {
					if ( svp_hyp.equals(prev_svp_hyp) ) {
						notFound = false;
						break;
					}
				}
				// if not duplicate, add to item hyp
				if ( notFound ) {
					item_hyp.addSlotValuePair( svp_hyp );
					if ( svp_hyp.value.isEmpty() )
						reqSlotRequired = false;
				}
			}
		}
		// insert an additional slot value pair
		if ( rand_num_gen.nextDouble() < item_insertion_rate || reqSlotRequired ) { //|| item_hyp.svps.isEmpty() ) {
			svp_hyp = getRandomSVP( noReqSlots, reqSlotRequired, (sem_content.isEmpty()?new ArrayList<SlotValuePair>():sem_content.getFirstItem().svps) );
			notFound = true;
			if ( !sem_content.isEmpty() ) {
				for ( SlotValuePair svp: sem_content.getFirstItem().svps ) {
					if ( svp_hyp.equals(svp) ) {
						notFound = false;
						break;
					}
				}
			}
			if ( notFound ) {
				if ( reqSlotRequired ) {
					svp_hyp.value = "";
					item_hyp.svps.add( 0, svp_hyp );
				} else if ( item_hyp.svps.isEmpty() )
					item_hyp.addSlotValuePair( svp_hyp );
				else {
					int randInd = rand_num_gen.nextInt( item_hyp.svps.size() );
					item_hyp.svps.add( randInd, svp_hyp );
				}
				if ( svp_hyp.value.isEmpty() )
					reqSlotRequired = false;
			}
		}
		return item_hyp;
	}
	
	/**
	 * Returns a confused version of the given slot value pair.
	 */
	private static SlotValuePair getSVPHyp( boolean noReqSlots, boolean reqOnly, SlotValuePair svp ) {
		logger.logf( Level.FINE, "Confusing slot value pair %s\n", svp.toString() );
		SlotValuePair confSVP = new SlotValuePair( svp );
		if ( domain.isInformable(svp.slot) && rand_num_gen.nextDouble() < value_confusion_rate ) {
			String randVal = getRandomValueForSlot( svp );
			if ( !randVal.isEmpty() )
				confSVP.value = randVal;
		} else
			// confuse the slot name
			confSVP = getRandomSVP( noReqSlots, reqOnly, svp );
		//}
		return confSVP;
	}
	
	private static SlotValuePair getRandomSVP( boolean noReqSlots, boolean reqOnly, ArrayList<SlotValuePair> svps ) {
		ArrayList<String> slots = ( reqOnly ? domain.getReqOnlySlotNames() : domain.getSlotNames( noReqSlots ) );
		slots.removeAll( svps ); //TODO does not work?
		int randSlotInd = rand_num_gen.nextInt( slots.size() );
		String randSlot = slots.get( randSlotInd );
		logger.logf( Level.FINE, "Random slot %s\n", randSlot );
		SlotValuePair randSVP = new SlotValuePair( randSlot );
		if ( domain.isInformable(randSlot) ) {
			randSVP.value = getRandomValueForSlot( randSVP );
			logger.logf( Level.FINE, "Random value of random slot %s: %s\n", randSlot, randSVP.value );
		}
		return randSVP;
	}
	
	private static SlotValuePair getRandomSVP( boolean noReqSlots, boolean reqOnly, SlotValuePair svp ) {
		ArrayList<SlotValuePair> svps = new ArrayList<SlotValuePair>();
		svps.add( svp );
		return getRandomSVP( noReqSlots, reqOnly, svps );
	}
	
	private static String getRandomValueForSlot( SlotValuePair svp ) {
		logger.logf( Level.FINE, "Random value of slot %s\n", svp );
		ArrayList<String> values = domain.getValuesForSlot( svp.slot );
		values.remove( svp.value );
		if ( !values.isEmpty() ) {
			int randValInd = rand_num_gen.nextInt( values.size() ); 
			return values.get( randValInd );
		}
		return "";
	}
	
	private static CommFunction getCFHyp( CommFunction cf ) {
		// confuse this communicative function
		logger.logf( Level.FINE, "Confusing communicative function %s\n", cf.name() );
		int randInd = rand_num_gen.nextInt( selectedCFs.length - 1 );
		if ( randInd == 0 )
			return selectedCFs[ 0 ];
		int i = 0;
		while ( i < selectedCFs.length ) {
			if ( selectedCFs[i] == cf )
				return selectedCFs[ randInd + 1 ]; // cf to be excluded found: shift random index forward and return corresponding CF
			if ( i == randInd )
				return selectedCFs[ i ]; // random index found (but not the cf to be excluded): return CF at random index
			i++;
		}
		return cf; //NOTE should not happen
	}
	
	public static void setConfRate( Double confrate ) {
		confusion_rate = confrate;
		setupConfScoring();
		logger.logf( Level.INFO, "   NEW CONFUSION_RATE: %.2f\n", confusion_rate );
	}
	
	public static void loadConfig( Properties config ) {
		
		// load error rates and n-best size from given configuration
		MAX_NUM_ATTEMPTS = Integer.parseInt( Configuration.MAX_NUM_CONF_ATTEMPTS );
		nbest_size = Integer.parseInt( Configuration.nbest_size );
		confusion_rate = Double.parseDouble( Configuration.confusion_rate );
		oracle_exp_param = Double.parseDouble( Configuration.oracle_exp_param );
		variability = Double.parseDouble( Configuration.variability );
		// confusion model parameters
		cf_confusion_rate = Double.parseDouble( Configuration.cf_confusion_rate );
		item_deletion_rate = Double.parseDouble( Configuration.item_deletion_rate );
		item_insertion_rate = Double.parseDouble( Configuration.item_insertion_rate );
		slot_confusion_rate = Double.parseDouble( Configuration.slot_confusion_rate );
		value_confusion_rate = Double.parseDouble( Configuration.value_confusion_rate );
		
        StringBuffer logStr = new StringBuffer( "Error model configuration:\n" );
		logStr.append( String.format( "   MAX_NUM_ATTEMPTS: %d\n", MAX_NUM_ATTEMPTS ) );
		logStr.append( String.format( "   NBEST_SIZE: %d\n", nbest_size ) );
		logStr.append( String.format( "   CONFUSION_RATE: %.2f\n", confusion_rate ) );
		logStr.append( String.format( "   ORACLE_EXP_PARAM: %.2f\n", oracle_exp_param ) );
		logStr.append( String.format( "   VARIABILITY: %.2f\n", variability ) );
		logStr.append( String.format( "   CF_CONFUSION_RATE: %.2f\n", cf_confusion_rate ) );
		logStr.append( String.format( "   ITEM_DELETION_RATE: %.2f\n", item_deletion_rate ) );
		logStr.append( String.format( "   ITEM_INSERTION_RATE: %.2f\n", item_insertion_rate ) );
		logStr.append( String.format( "   SLOT_CONFUSION_RATE: %.2f\n", slot_confusion_rate ) );
		logStr.append( String.format( "   VALUE_CONFUSION_RATE: %.2f\n", value_confusion_rate ) );
		logger.logf( Level.WARNING, "%s\n", logStr );

		// construct Dirichlet distribution for confidence scoring
		setupConfScoring();
	}
	
	private static void setupConfScoring() {
		// construct Dirichlet distribution for sampling the confidence scores for n-best lists
		double[] alphas = new double[ nbest_size + 1 ];
		double lastOacc = 0d;
		double newOacc;
		for ( int i = 0; i < nbest_size; i++ ) {
			newOacc = getOracleAccuracy( confusion_rate, i+1 );
			alphas[i] = variability * ( newOacc - lastOacc );
			lastOacc = newOacc;
		}
		alphas[nbest_size] = variability * ( 1d - lastOacc );
		conf_distr = new Dirichlet( alphas );

		// print alpha's
        StringBuffer logStr = new StringBuffer( "   Alphas:" );
		for ( int j = 0; j < nbest_size + 1; j++ )
			logStr.append( String.format( "  %.3f", alphas[j] ) );
		logStr.append( "\n" );
		logger.logf( Level.WARNING, "%s\n", logStr );
	}
	
	private static Dirichlet getConfScoring( double conf_rate ) {
		// construct Dirichlet distribution for sampling the confidence scores for n-best lists
		double[] alphas = new double[ nbest_size + 1 ];
		double lastOacc = 0d;
		double newOacc;
		for ( int i = 0; i < nbest_size; i++ ) {
			newOacc = getOracleAccuracy( conf_rate, i+1 );
			alphas[i] = variability * ( newOacc - lastOacc );
			lastOacc = newOacc;
		}
		alphas[nbest_size] = variability * ( 1d - lastOacc );

		// print alpha's
        StringBuffer logStr = new StringBuffer( "   Alphas (conf_rate " + conf_rate + "): " );
		for ( int j = 0; j < nbest_size + 1; j++ )
			logStr.append( String.format( "  %.3f", alphas[j] ) );
		logStr.append( "\n" );
		logger.logf( Level.WARNING, "%s\n", logStr );
		
		return new Dirichlet( alphas );
		
	}
	
	private static double getOracleAccuracy( double r, int n ) {
		return 1 - r * oracle_exp_param * Math.exp( -(n-1) * (1-r) );
	}

}
