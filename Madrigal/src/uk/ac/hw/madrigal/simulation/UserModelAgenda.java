/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Simulation of user behaviour and ASR/SLU noise.                                 *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.simulation;

import java.util.ArrayDeque;
import java.util.ArrayList;

import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;
import uk.ac.hw.madrigal.dialogue_acts.SemanticContent;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoal;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.domain.UserGoalSVP;

/**
 * Stack-like structure for managing planned dialogue acts by a simulated user.
 */
public class UserModelAgenda {
	
	private ArrayDeque<DialogueAct> dial_act_stack;
	
	public UserModelAgenda() {
		dial_act_stack = new ArrayDeque<DialogueAct>();
	}
	
	public void pushAct( DialogueAct dact ) {
		dact.setDimension( Dimension.all ); //NOTE for now, assume single dimension
		dial_act_stack.push( dact );
	}
	
	public DialogueAct popAct() {
		return dial_act_stack.poll();
	}
	
	public SlotValuePair getConstraintFromFirst() {
		DialogueAct first = dial_act_stack.peekFirst();
		if ( first == null )
			return null;
		SemanticContent content = first.getSemContent(); 
		SemanticItem sem_item = content.getFirstItem();
		SlotValuePair svp;
		if ( sem_item == null )
			return null;
		if ( first.getCommFunction() == CommFunction.inform && !sem_item.svps.isEmpty() ) {
			svp = sem_item.getFirstItem();
			sem_item.svps.remove( svp );
			if ( sem_item.svps.isEmpty() )
				first.getSemContent().removeItem( sem_item );
			if ( content.isEmpty() )
				popAct();
			return svp;
		}
		return null;
	}

	/** Initialises the agenda by pushing dialogue acts conveying 
	 * the requests and constraints of the given user goal */
	public void init( UserGoal user_goal ) {
		dial_act_stack.clear();
		DialogueAct dact = DialActUtils.newUserAct( CommFunction.initialGoodbye );
		pushAct( dact );
		SlotValuePair svp;
		if ( !user_goal.current_items.isEmpty() ) {
			UserGoalItem ug_item = user_goal.current_items.get( 0 );
			for ( UserGoalSVP ug_svp: ug_item.requests ) {
				svp = ug_svp.getSlotValuePair();
				dact = DialActUtils.newUserAct( CommFunction.setQuestion, svp );
				pushAct( dact );
			}
			for ( UserGoalSVP ug_svp: ug_item.constraints ) {
				svp = ug_svp.getSlotValuePair();
				dact = DialActUtils.newUserAct( CommFunction.inform, svp );
				pushAct( dact );
			}
		}
	}
	
	public void clear() {
		dial_act_stack.clear();
	}

	public void update( SlotValuePair svp ) {
		ArrayList<DialogueAct> toRemove = new ArrayList<DialogueAct>();
		SemanticItem sem_item;
		SlotValuePair req_svp;
		for ( DialogueAct dact: dial_act_stack ) {
			sem_item = dact.getSemContent().getFirstItem();
			if ( sem_item != null && !sem_item.svps.isEmpty() ) {
				req_svp = sem_item.getFirstItem();
				if ( dact.getCommFunction() == CommFunction.setQuestion ) {
					if ( svp.slot.equals(req_svp.slot) )
						toRemove.add( dact );
				} else if ( dact.getCommFunction() == CommFunction.inform ) {
					if ( svp.equals(req_svp) )
						toRemove.add( dact );
				}
			}
		}
		dial_act_stack.removeAll( toRemove );
	}
	
	public boolean isEmpty() {
		return dial_act_stack.isEmpty();
	}
	
	public String toString() {
		//return StringUtils.join( dial_act_stack, "\t\n" );
		String result = "Agenda:";
		for ( DialogueAct dact: dial_act_stack )
			result += "\n\t" + dact.toShortString();
		return result;
	}
	
}
