/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Simulation of user behaviour and ASR/SLU noise.                                 *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.simulation;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

import resources.Configuration;
import uk.ac.hw.madrigal.DialogueAgentInterface;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.domain.GoalGenerator;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoal;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.domain.SlotValuePair.SpecialValue;
import uk.ac.hw.madrigal.domain.UserGoalSVP.GroundStatus;
import uk.ac.hw.madrigal.utils.MyLogger;

/**
 * Hand-coded MaDrIgAL user simulator.
 */
public class UserSimulator implements DialogueAgentInterface {
	
	public static MyLogger logger;
	public static Random rand_num_gen; // = TestProgram.rand_num_gen;
	public static Ontology domain; // = TestProgram.domain;
	
	/* ----- REWARD FUNCTION PARAMETERS ------------------------- */
	private static double GOAL_COMPLETION_REWARD = 20;
	private static double SOCIAL_PENALTY = -2;
	private static double SOCIAL_REWARD = 2;
	private static double TURN_PENALTY = -1;
	private static double SYS_TURN_ITEM_PENALTY = -1; //TODO change to internal penalty?
	private static double PATIENCE_PENALTY = -1;
	
	/* ---------------------------------------------------------- */
	private static double CONSTR_ADD_PROB = 0.2d;
	private static double DONTCARE_PROB = 0.5d;
	private static double MAX_NUM_REPEATED_SYS_ACTS = 2d;
	private static double EMPTY_INFORM_PROB = 0.08d;
	private static double SETQUESTION_PROB = 0.07d;
	
	/** Goal of this simulated user */
	private UserGoal user_goal;
	
	/** Last system dialogue act received by simulator */
	//DialogueAct last_sys_act;

	/** Last user dialogue act sent to dialogue manager */
	DialogueAct last_usr_act;
	
	DialogueTurn last_sys_turn, last_usr_turn;
	
	private int num_repeated_sys_acts;
	
//	private SlotValuePair last_svp_specified;
	
//	ReactivePressure reactive_pressure;
//	ReactivePressure sys_reactive_pressure;
	
	/** Agenda of this simulated user, containing planned user dialogue acts */
	UserModelAgenda agenda;
	
	private double reward;
		
	/** Flag to remember if this user has given a goal completion reward */
    private boolean goal_reward_given;
    
    /** Dialogue number of the current session (used for logging) */
	private int dial_num;

	/** Turn number of the current dialogue (used for logging) */
	private int turn_num;

	/** Constructor */
	public UserSimulator() {
		setUserGoal(new UserGoal());
		//last_sys_act = null;
		last_usr_act = null;
		last_sys_turn = null;
		last_usr_turn = null;
		num_repeated_sys_acts = 0;
//		last_svp_specified = null;
//		reactive_pressure = ReactivePressure.NONE;
//		sys_reactive_pressure = ReactivePressure.NONE;
		agenda = new UserModelAgenda();
		reward = 0d;
		goal_reward_given = false;
		dial_num = 0;
		turn_num = 0;
	}
	
	/** Selects a new random goal and populates the agenda accordingly */
	public void start_dialogue( GoalGenerator goalGen ) {
		turn_num = 0;
		logger.logf( Level.INFO, "\n===== Dialogue %d =====\n", dial_num++ );
		//setUserGoal( domain.getRandomGoal( rand_num_gen ) );
		setUserGoal(  goalGen.generateGoal(rand_num_gen) );
//		reactive_pressure = ReactivePressure.NONE;
//		sys_reactive_pressure = ReactivePressure.NONE;
		num_repeated_sys_acts = 0;
		agenda.init( user_goal );
		if ( rand_num_gen.nextDouble() < EMPTY_INFORM_PROB )
			agenda.pushAct( DialActUtils.newUserAct(CommFunction.inform) ); // add empty inform act (I'm looking for a restaurant)
		goal_reward_given = false;
		//logger.logf( Level.INFO, "%s\n", user_goal.toFirstItemString(true) );
		logger.logf( Level.FINE, "%s\n", agenda.toString() );
		logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_num );
	}
	
	@Override
	/** Updates the simulator's internal state based on the given system dialogue act */
	public void receive( DialogueTurn sys_turn ) {
		if ( sys_turn.isUserTurn() )
			System.out.printf( "ERROR: unexpected speaker of dialogue act: %s\n", sys_turn.toString() );
		else {
			logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_num );
	    	logger.logf( Level.INFO, "Receiving system dial acts: %s\n", sys_turn.toString() );
			//updateWithSysAct( sys_turn.dact_all );
	    	updateWithSysTurn( sys_turn );
			logger.logf( Level.FINE, "---\n%s\n---\n", user_goal.toFirstItemString(true) );
			logger.logf( Level.FINE, "%s\n", agenda.toString() );
		}
	}
	
	/** Method that returns a numeric score to be used for evaluation or training of the dialogue manager */
	public double getReward() {
		double score = reward; // copy current reward
		score += TURN_PENALTY;
		reward = 0d;
		logger.logf( Level.INFO, "Reward given: %.0f\n", score );
		return score;
	}
	
    public boolean taskSuccess() {
    	return goal_reward_given;
    }

	@Override
	/** Selects a response from the simulator in the form of a dialogue act */
	public DialogueTurn respond() {
		
		// decide whether to ask setQuestion
		SlotValuePair offeredEntity = user_goal.getEntityInFocus();
		boolean entityOffered = ( offeredEntity != null && !offeredEntity.value.isEmpty() );
		ArrayList<String> slotsToAsk = user_goal.getSlotsToAsk();
		if ( !slotsToAsk.isEmpty() && entityOffered && rand_num_gen.nextDouble() < SETQUESTION_PROB ) {
			String slotToAsk = slotsToAsk.get( rand_num_gen.nextInt(slotsToAsk.size()) );
			DialogueAct usr_act_setQ = DialActUtils.newUserAct( CommFunction.setQuestion, new SlotValuePair(slotToAsk) );
			agenda.pushAct( usr_act_setQ );
		}
		
		DialogueAct usr_act = agenda.popAct();
		if ( usr_act == null ) // nothing left to do: say bye again
			usr_act = new DialogueAct( CommFunction.initialGoodbye );

		SemanticItem first_item = usr_act.getSemContent().getFirstItem();
		CommFunction cf = usr_act.getCommFunction();
		boolean emptyInform = ( cf == CommFunction.inform && usr_act.getSemContent().isEmpty() );
		if ( cf == CommFunction.inform || cf == CommFunction.confirm || cf == CommFunction.disconfirm ) {
			SlotValuePair new_svp;
			while ( !emptyInform && rand_num_gen.nextDouble() < CONSTR_ADD_PROB ) {
				new_svp = agenda.getConstraintFromFirst();
				if ( new_svp == null )
					break;
				if ( first_item == null ) {
					SemanticItem newItem = new SemanticItem();
					newItem.addSlotValuePair( new_svp );
					usr_act.addItem( newItem );
				} else
					first_item.addSlotValuePair( new_svp );
			}
		} else if ( cf == CommFunction.setQuestion || cf == CommFunction.propQuestion ) {
			// check if there is an entity in focus to ask questions about
			// if not, push back candidate response and issue a negative feedback act (evaluation level?)
			String entity_val = user_goal.getEntityInFocus().value;
			logger.logf( Level.FINE, "Entity under discussion: %s\n", entity_val );
			if ( entity_val.isEmpty() || entity_val == SpecialValue.NONE.name() ) {
				agenda.pushAct( usr_act );
				usr_act = DialActUtils.newUserAct( CommFunction.autoNegative );
			} //TODO else ( with some probability include the name of the entity in the question act? )
		}
		
    	logger.logf( Level.INFO, "Response act: %s\n", usr_act.toShortString() );
    	updateWithUsrAct( usr_act );
		logger.logf( Level.FINE, "---\n%s\n---\n", user_goal.toFirstItemString(true) );
    	turn_num++;
    	
    	usr_act.setDimension( DialogueAct.Dimension.all );
    	
    	DialogueTurn usr_turn = new DialogueTurn( AgentName.user, AgentName.system );
    	usr_turn.setDialAct( usr_act );
    	updateHistory( usr_turn );
    	return usr_turn;
	}
	
    @SuppressWarnings("unused")
	private DialogueAct specifyConstraint() {
    	DialogueAct usr_act = new DialogueAct( CommFunction.inform );
    	usr_act.setAddressee( AgentName.system );
    	usr_act.setSpeaker( AgentName.user );
    	UserGoalItem usr_goal_it = new UserGoalItem();
    	ArrayList<UserGoalSVP> ug_svps; 
    	for ( UserGoalItem ug_it: user_goal.current_items ) {
        	ug_svps = new ArrayList<UserGoalSVP>(); 
    		for ( UserGoalSVP ug_svp: ug_it.constraints ) {
    			if ( ug_svp.getGroundStatus() == GroundStatus.INIT ) {
    				//System.out.printf( "constraint to specify: %s\n", ug_svp.toString() );
    				//usr_goal_it.addUGoalSVP( ug_svp );
    				//break;
    				ug_svps.add( ug_svp );
    			}
    		}
    		if ( !ug_svps.isEmpty() ) {
    			int rand_index = rand_num_gen.nextInt( ug_svps.size() );
    			usr_goal_it.addUGoalSVP( ug_svps.get(rand_index) );
    		}
    	}
    	if ( usr_goal_it.constraints.isEmpty() ) // no further constraints to convey
    		return null;
    	
    	usr_act.addItem( new SemanticItem(usr_goal_it) );
    	return usr_act;
    }
    
    private void updateWithUsrAct( DialogueAct dact ) {
    	DialogueAct last_sys_act = ( last_sys_turn == null ? null : last_sys_turn.dact_all );
//    	if ( dact.getCommFunction() == CommFunction.initialGoodbye )
//    		sys_reactive_pressure = ReactivePressure.BYE;
    	for ( UserGoalItem ug_it: user_goal.current_items )
    		for ( UserGoalSVP ug_svp: ug_it.constraints )
    			ug_svp.updateGrndStatus( last_sys_act, dact );
    	last_usr_act = dact;
    }
    
    private void updateWithSysTurn( DialogueTurn sys_turn ) {
    	if ( sys_turn.dact_all != null )
    		updateWithSysAct( sys_turn.dact_all, last_usr_turn.dact_all );
    	//NOTE multi-dim DM currently fills in dact_all as well, hence above update is always performed
    	else {
    		if ( sys_turn.dact_task != null )
    			updateWithSysTaskAct( sys_turn.dact_task );
    		if ( sys_turn.dact_auto_fb != null )
    			updateWithSysAutoFBAct( sys_turn.dact_auto_fb );
    		if ( sys_turn.dact_som != null )
    			updateWithSysSOMAct( sys_turn.dact_som );
    	}
    	
    	updateHistory( sys_turn );
    }
    
    private void updateHistory( DialogueTurn turn ) {
    	DialogueTurn last_turn;
    	if ( turn.getSpeaker() == AgentName.system ) {
    		if ( last_sys_turn == null )
    			last_sys_turn = new DialogueTurn( AgentName.system, AgentName.user );
    		last_turn = last_sys_turn;
    	} else {
    		if ( last_usr_turn == null )
    			last_usr_turn = new DialogueTurn( AgentName.user, AgentName.system );
    		last_turn = last_usr_turn;
    	}
    	last_turn.setDialAct( turn.dact_all );
    	last_turn.setDialAct( turn.dact_task );
    	last_turn.setDialAct( turn.dact_auto_fb );
    	last_turn.setDialAct( turn.dact_som );
    }
    
    // update for the one-dimensional case
    private void updateWithSysAct( DialogueAct dact, DialogueAct last_uact ) {
    	
    	// handle repetitions
    	DialogueAct last_sys_act = ( last_sys_turn == null ? null : last_sys_turn.dact_all );
    	if ( last_sys_act != null && dact.equals(last_sys_act) ) {
    		num_repeated_sys_acts++;
    		if ( num_repeated_sys_acts > MAX_NUM_REPEATED_SYS_ACTS ) {
//    			agenda.clear();
//    			agenda.pushAct( DialActUtils.newUserAct(CommFunction.initialGoodbye) );
    			reward += PATIENCE_PENALTY;
//    			return;
    		}
    	} else
    		num_repeated_sys_acts = 0;
    	
    	// add penalty proportional to content size of system turn
    	//TODO move up to turn level
    	int num_sys_turn_items = dact.getNumItems();
    	reward += num_sys_turn_items * SYS_TURN_ITEM_PENALTY;
    	
    	// apply updates, depending on communicative function of the dialogue act
    	CommFunction cf = dact.getCommFunction();
		SemanticItem content = dact.getSemContent().getFirstItem(); //NOTE assuming there is only one item!
		String primVal = ( content==null ? "" : content.getValueForSlot( domain.primarySlot() ) ); 
		//boolean correctionNeeded = false;
		DialogueAct corrAct  = null;
		//DialogueAct noConstrInfResp = null;
		String noneStr = SlotValuePair.getString( SlotValuePair.SpecialValue.NONE );
		boolean sysInfNameNone = primVal.equals( noneStr );
		
		if ( cf == CommFunction.returnGoodbye ) {
			if ( !agenda.isEmpty() || (last_usr_act != null && last_usr_act.getCommFunction() != CommFunction.initialGoodbye ))
				reward += SOCIAL_PENALTY;
			else if ( last_usr_act != null && last_usr_act.getCommFunction() == CommFunction.initialGoodbye )
				reward += SOCIAL_REWARD;

		} else {
			if ( last_usr_act != null && last_usr_act.getCommFunction() == CommFunction.initialGoodbye 
					&& cf != CommFunction.autoNegative_int && cf != CommFunction.autoNegative_perc ) {
				logger.log( Level.FINEST, "System not responding to goodbye: penalise!" );
				reward += SOCIAL_PENALTY;
				if ( agenda.isEmpty() ) { // this should always be the case if the last user act was initialGoodbye
					agenda.pushAct( DialActUtils.newUserAct(CommFunction.initialGoodbye) );
					return; // ignore system act: no further updating of agenda
				}
			}

//			if ( last_usr_act != null && last_usr_act.getCommFunction() == CommFunction.setQuestion ) {
//				//TODO check if system answers question
//				SemanticItem first_item = last_usr_act.getSemContent().getFirstItem();
//				String reqSlot = first_item.getFirstItem().slot;
//				//TODO check sem content of system act for this slot
//				logger.log( Level.FINEST, "System not answering setQuestion: penalise!" );
//				reward += SOCIAL_PENALTY;
//			}

//			if ( cf == CommFunction.autoNegative || cf == CommFunction.autoNegative_perc || cf == CommFunction.autoNegative_int ) {
//				agenda.pushAct( new DialogueAct(last_uact) ); // user repeats last utterance
//				if ( cf == CommFunction.autoNegative ) {
//					logger.log( Level.FINEST, "System autoNegative backoff action: penalise!" );
//					reward += BACKOFF_PENALTY;
//				}

			if ( cf == CommFunction.autoNegative || cf == CommFunction.autoNegative_perc || cf == CommFunction.autoNegative_int ) {
				agenda.pushAct( new DialogueAct(last_uact) ); // user repeats last utterance
						
			} else if ( cf == CommFunction.inform ) {
	    		ArrayList<SlotValuePair> toBeCorrected = new ArrayList<SlotValuePair>();
	    		for ( SlotValuePair svp: content.svps ) {
	    			toBeCorrected.addAll( user_goal.updateInform(svp,sysInfNameNone) );
	    		}
	    				
	    		// push any correcting acts on the agenda if needed
	    		if ( !toBeCorrected.isEmpty() ) {
	    			//correctionNeeded = true;
	    			corrAct = DialActUtils.newUserAct( CommFunction.inform );
	    			SemanticItem sem_it = new SemanticItem();
	    			for ( SlotValuePair corr_svp: toBeCorrected )
	    				sem_it.addSlotValuePair( corr_svp );
	    			corrAct.addItem( sem_it );
	    			//agenda.pushAct( corrAct );
	    		} else {
	        		if ( sysInfNameNone ) {
	        			SlotValuePair modifdSVP = user_goal.modifyConstraints( content.svps, rand_num_gen, DONTCARE_PROB, domain );
	        			if ( modifdSVP != null ) {
	        				corrAct = DialActUtils.newUserAct( CommFunction.inform, modifdSVP );
	        				//agenda.pushAct( corrAct );
	        			}
	        		}
	        		for ( SlotValuePair svp: content.svps )
	        			agenda.update( svp );
	        		if ( !goal_reward_given ) {
	        			if ( user_goal.currentCompleted() ) {
	        				goal_reward_given = true;
	        				reward += GOAL_COMPLETION_REWARD;
	        			}
	        		}
	    		}
	    		// update agenda according to presented information
	    		//agenda.update( user_goal ); //NOTE currently updated directly with svp in system inform act
	    		
			} else if ( cf == CommFunction.disconfirm ) {
				if ( last_uact.getCommFunction() == CommFunction.propQuestion ) {
					DialogueAct responseAct = new DialogueAct( last_uact );
					responseAct.setCommFunction( CommFunction.inform );
					agenda.pushAct( responseAct );
				}
	    		
	    	} else if ( cf == CommFunction.propQuestion ) {
	    		SlotValuePair corr_svp;
	    		//DialogueAct answerAct;
	    		if ( !content.svps.isEmpty() ) {
	    		//for ( SlotValuePair svp: content.svps ) {
	    			SlotValuePair svp = content.svps.get( 0 ); // assuming there is only one slot-value pair in question
	    			corr_svp = user_goal.checkForItem( svp );
	    			if ( corr_svp == null )
	    				//answerAct = DialogueAct.newUserAct( CommFunction.confirm );
	    				corrAct = DialActUtils.newUserAct( CommFunction.confirm );
	    			else {
	    				//answerAct = DialogueAct.newUserAct( CommFunction.disconfirm );
	    				//answerAct.addItem( new SemanticItem(new UserGoalItem(corr_svp)) );
	    				corrAct = DialActUtils.newUserAct( CommFunction.disconfirm );
	    				corrAct.addItem( new SemanticItem(new UserGoalItem(corr_svp)) );
	    			}
					//agenda.pushAct( answerAct );
	    		}
	    	} else if ( cf == CommFunction.setQuestion ) {
	    		DialogueAct answerAct;
	    		SlotValuePair ansSVP;
	    		if ( !content.svps.isEmpty() ) {
	    			SlotValuePair svp = content.svps.get( 0 ); // assuming there is only one slot-value pair in question
	    			ansSVP = user_goal.getValue( svp );
					answerAct = DialActUtils.newUserAct( CommFunction.inform );
					answerAct.addItem( new SemanticItem( new UserGoalItem(ansSVP) ) );
					agenda.update( ansSVP ); // remove any inform acts on the agenda already stating this svp
					agenda.pushAct( answerAct );
	    		}
	    	}
	    	
	    	// update grounding status of goal items
	    	for ( UserGoalItem ug_it: user_goal.current_items ) {
	    		for ( UserGoalSVP ug_svp: ug_it.constraints ) {
	    			ug_svp.updateGrndStatus( last_uact, dact );
	//    			if ( ug_svp.getGroundStatus() == GroundStatus.SYS_EXP_CONF ) {
	//    				System.out.println( "SYS_EXP_CONF" );
	//    				agenda.pushAct( DialogueAct.newUserAct(CommFunction.confirm) ); //TODO check if this is redundant
	//    			}
	    			if ( dact.getCommFunction() == CommFunction.inform && !primVal.equals(noneStr) && corrAct == null
	    					&& !ug_svp.getSlotValuePair().value.equals(SlotValuePair.getString(SpecialValue.DONTCARE) ) 
	    					&& (ug_svp.getGroundStatus() == GroundStatus.USR_INF || ug_svp.getGroundStatus() == GroundStatus.USR_EXP_CONF)  ) {
	    				// item not confirmed by system: ask whether recommended entity matches the required constraint
	    				DialogueAct propQ = DialActUtils.newUserAct( CommFunction.propQuestion, ug_svp.getSlotValuePair() );
	    				agenda.pushAct( propQ ); 
	    			}
	    		}
	    	}
	    	
	    	// check if a task-related act should be pushed back onto the agenda
	    	//boolean repeatSetQ = false;
	//    	if ( !correctionNeeded && last_usr_act.getCommFunction() == CommFunction.setQuestion 
	//    			&& cf != CommFunction.setQuestion && cf != CommFunction.propQuestion ) {
	    	if ( last_usr_act.getCommFunction() == CommFunction.setQuestion ) {
	    		// check if this question has been answered; if not push question back onto the agenda
	    		SlotValuePair svpAsked = last_uact.getSemContent().getFirstItem().getFirstItem();
	        	for ( UserGoalItem ug_it: user_goal.current_items )
	        		for ( UserGoalSVP ug_svp: ug_it.requests ) {
	        			SlotValuePair ugSVP = ug_svp.getSlotValuePair();
	        			if ( ug_svp.slotMatches(svpAsked) && ugSVP.value.isEmpty() ) {
	        				DialogueAct respAct = new DialogueAct( last_uact );
	        				agenda.pushAct( respAct );
	        				//repeatSetQ = true;
	        			}
	        		}
	    	}
	    	
	    	if ( corrAct != null ) // && !repeatSetQ )
	    		agenda.pushAct( corrAct );
	    	
	    	//last_sys_act = dact;
		}
    	
    }
    
    // requires dact to be task act
    private void updateWithSysTaskAct( DialogueAct dact ) {
    	// apply updates, depending on communicative function of the dialogue act
    	CommFunction cf = dact.getCommFunction();
		SemanticItem content = dact.getSemContent().getFirstItem(); //NOTE assuming there is only one item!
		String primVal = ( content==null ? "" : content.getValueForSlot( domain.primarySlot() ) );
		DialogueAct corrAct  = null;
		String noneStr = SlotValuePair.getString( SlotValuePair.SpecialValue.NONE );
		boolean sysInfNameNone = primVal.equals( noneStr );
		
		if ( cf == CommFunction.inform ) {
    		
    		ArrayList<SlotValuePair> toBeCorrected = new ArrayList<SlotValuePair>();
    		for ( SlotValuePair svp: content.svps ) {
    			toBeCorrected.addAll( user_goal.updateInform(svp,sysInfNameNone) );
    		}
    				
    		// push any correcting acts on the agenda if needed
    		if ( !toBeCorrected.isEmpty() ) {
    			//correctionNeeded = true;
    			corrAct = DialActUtils.newUserAct( CommFunction.inform );
    			SemanticItem sem_it = new SemanticItem();
    			for ( SlotValuePair corr_svp: toBeCorrected )
    				sem_it.addSlotValuePair( corr_svp );
    			corrAct.addItem( sem_it );
    			//agenda.pushAct( corrAct );
    		} else {
        		if ( sysInfNameNone ) {
        			SlotValuePair modifdSVP = user_goal.modifyConstraints( content.svps, rand_num_gen, DONTCARE_PROB, domain );
        			if ( modifdSVP != null ) {
        				corrAct = DialActUtils.newUserAct( CommFunction.inform, modifdSVP );
        				//agenda.pushAct( corrAct );
        			}
        		}
        		for ( SlotValuePair svp: content.svps )
        			agenda.update( svp );
        		if ( !goal_reward_given ) {
        			if ( user_goal.currentCompleted() ) {
        				goal_reward_given = true;
        				reward += GOAL_COMPLETION_REWARD;
        			}
        		}
    		}
    		// update agenda according to presented information
    		//agenda.update( user_goal ); //NOTE currently updated directly with svp in system inform act
    		
		} else if ( cf == CommFunction.disconfirm ) {
			if ( last_usr_act.getCommFunction() == CommFunction.propQuestion ) {
				DialogueAct responseAct = new DialogueAct( last_usr_act );
				responseAct.setCommFunction( CommFunction.inform );
				agenda.pushAct( responseAct );
			}
    		
    	} else if ( cf == CommFunction.setQuestion ) {
    		DialogueAct answerAct;
    		SlotValuePair ansSVP;
    		if ( !content.svps.isEmpty() ) {
    			SlotValuePair svp = content.svps.get( 0 ); // assuming there is only one slot-value pair in question
    			ansSVP = user_goal.getValue( svp );
				answerAct = DialActUtils.newUserAct( CommFunction.inform );
				answerAct.addItem( new SemanticItem( new UserGoalItem(ansSVP) ) );
				agenda.update( ansSVP ); // remove any inform acts on the agenda already stating this svp
				agenda.pushAct( answerAct );
    		}
    	}
    	
    	// update grounding status of goal items
    	for ( UserGoalItem ug_it: user_goal.current_items ) {
    		for ( UserGoalSVP ug_svp: ug_it.constraints ) {
    			ug_svp.updateGrndStatus( last_usr_act, dact );
    			if ( dact.getCommFunction() == CommFunction.inform && !primVal.equals(noneStr) && corrAct == null
    					&& !ug_svp.getSlotValuePair().value.equals(SlotValuePair.getString(SpecialValue.DONTCARE) ) 
    					&& (ug_svp.getGroundStatus() == GroundStatus.USR_INF || ug_svp.getGroundStatus() == GroundStatus.USR_EXP_CONF)  ) {
    				// item not confirmed by system: ask whether recommended entity matches the required constraint
    				DialogueAct propQ = DialActUtils.newUserAct( CommFunction.propQuestion, ug_svp.getSlotValuePair() );
    				agenda.pushAct( propQ );
    			}
    		}
    	}
    	
    	// check if a task-related act should be pushed back onto the agenda
    	//boolean repeatSetQ = false;
//    	if ( !correctionNeeded && last_usr_act.getCommFunction() == CommFunction.setQuestion 
//    			&& cf != CommFunction.setQuestion && cf != CommFunction.propQuestion ) {
    	if ( last_usr_act.getCommFunction() == CommFunction.setQuestion ) {
    		// check if this question has been answered; if not push question back onto the agenda
    		SlotValuePair svpAsked = last_usr_act.getSemContent().getFirstItem().getFirstItem();
        	for ( UserGoalItem ug_it: user_goal.current_items )
        		for ( UserGoalSVP ug_svp: ug_it.requests ) {
        			SlotValuePair ugSVP = ug_svp.getSlotValuePair();
        			if ( ug_svp.slotMatches(svpAsked) && ugSVP.value.isEmpty() ) {
        				DialogueAct respAct = new DialogueAct( last_usr_act );
        				agenda.pushAct( respAct );
        				//repeatSetQ = true;
        			}
        		}
    	}
    	
    	if ( corrAct != null ) // && !repeatSetQ )
    		agenda.pushAct( corrAct );

    }

    private void updateWithSysAutoFBAct( DialogueAct dact ) {
    	CommFunction cf = dact.getCommFunction();

    	if ( cf == CommFunction.autoNegative || cf == CommFunction.autoNegative_perc || cf == CommFunction.autoNegative_int )
			agenda.pushAct( new DialogueAct(last_usr_turn.dact_all) ); // user repeats last utterance
    	
    	else if ( cf == CommFunction.propQuestion ) {
    		SemanticItem content = dact.getSemContent().getFirstItem(); //NOTE assuming there is only one item!
    		DialogueAct corrAct  = null;
    		SlotValuePair corr_svp;
    		if ( !content.svps.isEmpty() ) {
    			SlotValuePair svp = content.svps.get( 0 ); //NOTE assuming there is only one slot-value pair in question
				corr_svp = user_goal.checkForItem( svp );
				if ( corr_svp == null )
					corrAct = DialActUtils.newUserAct( CommFunction.confirm );
				else {
					corrAct = DialActUtils.newUserAct( CommFunction.disconfirm );
					corrAct.addItem( new SemanticItem(new UserGoalItem(corr_svp)) );
				}
    		}
		}
    }

    private void updateWithSysSOMAct( DialogueAct dact ) {
		if ( dact.getCommFunction() == CommFunction.returnGoodbye )
			if ( !agenda.isEmpty() )
				reward += SOCIAL_PENALTY;
    }

	public UserGoal getUserGoal() {
		return user_goal;
	}

	public void setUserGoal( UserGoal user_goal ) {
		this.user_goal = user_goal;
	}

	public static void loadConfig() {
		// load reward function parameters
		GOAL_COMPLETION_REWARD = Double.parseDouble( Configuration.GOAL_COMPLETION_REWARD );
		SOCIAL_PENALTY = Double.parseDouble( Configuration.SOCIAL_PENALTY );
		SOCIAL_REWARD = Double.parseDouble( Configuration.SOCIAL_REWARD );
		TURN_PENALTY = Double.parseDouble( Configuration.TURN_PENALTY );
		SYS_TURN_ITEM_PENALTY = Double.parseDouble( Configuration.SYS_TURN_ITEM_PENALTY );
		PATIENCE_PENALTY = Double.parseDouble( Configuration.PATIENCE_PENALTY );
		CONSTR_ADD_PROB = Double.parseDouble( Configuration.CONSTR_ADD_PROB );
		DONTCARE_PROB = Double.parseDouble( Configuration.DONTCARE_PROB );
		MAX_NUM_REPEATED_SYS_ACTS = Double.parseDouble( Configuration.MAX_NUM_REPEATED_SYS_ACTS );
		EMPTY_INFORM_PROB = Double.parseDouble( Configuration.EMPTY_INFORM_PROB );
		SETQUESTION_PROB = Double.parseDouble( Configuration.SETQUESTION_PROB );

        StringBuffer logStr = new StringBuffer( "User Simulator configuration:\n" );
		logStr.append( String.format( "   GOAL_COMPLETION_REWARD: %.2f\n", GOAL_COMPLETION_REWARD ) );
		logStr.append( String.format( "   SOCIAL_PENALTY: %.2f\n", SOCIAL_PENALTY ) );
		logStr.append( String.format( "   SOCIAL_REWARD: %.2f\n", SOCIAL_REWARD ) );
		logStr.append( String.format( "   TURN_PENALTY: %.2f\n", TURN_PENALTY ) );
		logStr.append( String.format( "   SYS_TURN_ITEM_PENALTY: %.2f\n", SYS_TURN_ITEM_PENALTY ) );
		logStr.append( String.format( "   PATIENCE_PENALTY: %.2f\n", PATIENCE_PENALTY ) );
		logStr.append( String.format( "   CONSTR_ADD_PROB: %.2f\n", CONSTR_ADD_PROB ) );
		logStr.append( String.format( "   DONTCARE_PROB: %.2f\n", DONTCARE_PROB ) );
		logStr.append( String.format( "   MAX_NUM_REPEATED_SYS_ACTS: %.2f\n", MAX_NUM_REPEATED_SYS_ACTS ) );
		logStr.append( String.format( "   EMPTY_INFORM_PROB: %.2f\n", EMPTY_INFORM_PROB ) );
		logStr.append( String.format( "   SETQUESTION_PROB: %.2f\n", SETQUESTION_PROB ) );

		logger.logf( Level.WARNING, "%s\n", logStr );
	}
	
}
