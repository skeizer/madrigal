/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created February 2017                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.java_websocket.WebSocketImpl;
import cc.mallet.util.Randoms;
import resources.Configuration;
import resources.Resources;
import uk.ac.hw.macs.ilab.nlg.TemplateBasedNLG2;
import uk.ac.hw.madrigal.dial_act_agents.DialogueActAgent;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.domain.Database;
import uk.ac.hw.madrigal.domain.GoalGenerator;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.learning.mdps.TabularMDP;
import uk.ac.hw.madrigal.learning.mdps.NumericMDP;
import uk.ac.hw.madrigal.learning.mdps.Policy;
import uk.ac.hw.madrigal.nlg.AbstractNLG;
import uk.ac.hw.madrigal.nlg.Template_NLG;
import uk.ac.hw.madrigal.nlu.AbstractNLU;
import uk.ac.hw.madrigal.nlu.Abstract_LUIS_NLU;
import uk.ac.hw.madrigal.nlu.LUIS_NLU;
import uk.ac.hw.madrigal.nlu.RASA_NLU;
import uk.ac.hw.madrigal.utils.LoggingManager;
import uk.ac.hw.madrigal.utils.MyLogger;

/**
 * Text-based dialogue system, which can be run in standalone mode (using TextDialSystemGUI)
 * or as a server.  In the latter case, it uses a passive GUI TextDialSystemServerGUI to show
 * messages and properly close the server.
 * 
 * The boolean configuration setting RUN_SERVER is used to specify which mode is to be used.
 * 
 * When run as a server, TextDialSystemClient can be used to connect to it and engage in 
 * a text-based natural language conversation.  In case of passing N-best lists with confidence
 * scores, the connection data type should be modified.  In the current setup, connections are
 * ended client-side using the input "HANGUP".
 */
public class TextDialSystem implements Runnable {
	
	static ArrayList<MyLogger> loggers = new ArrayList<MyLogger>();
	static String LOGGING_DIR;
	
	static String config_filename;
	static String core_settings_filename = "CoreSettings.cfg";
	static Properties core_settings, config;
	static MyLogger logger;
    public static Randoms rand_num_gen = new Randoms();
	
    static String domain_file = "ontology.json";
    public static Ontology domain;
    static String lexicon_file;
    static String dbase_file = "database.json";
    public static Database database;
    
	private static boolean RUN_SERVER = false;
	private static boolean RUN_OLD_SERVER = false;
//	private static boolean RUN_NEW_SERVER = true;
	private static boolean RUN_WEBSOCKET_SERVER = false;
	private static int PORT_NUMBER = 8080;
	private static String TOKEN_MANAGER_URL = "http://www.macs.hw.ac.uk/cgi-bin/cgiwrap/sk382/get_token.cgi";
	
	static int ASR_NBEST_SIZE = 1;
	static double ASR_CONF_THRESHOLD = 0.7d;
	static int MIN_NUM_NONTRIVIAL_TURNS = 3;
	
    static boolean use_mdp = false;
    static boolean use_deep_mdp = false;
    static boolean use_md_dm = false;
    	
//    TextDialSystemServerGUI serverGUI;
    
    private static enum NLU_System { LUIS, RASA };
    private static NLU_System nlu_system = NLU_System.RASA;
	AbstractNLU nlu;
	AbstractNLG nlg;
	DialManAgent dial_man;
	private DialogueTurn usr_turn, sys_turn;
	
	PrintWriter out;
	BufferedReader in;
	
	private static void loadConfig() {

		LOGGING_DIR = Configuration.logging_directory;
		logger = LoggingManager.setupLogging( LOGGING_DIR, "main", Configuration.logging_level );
		DialManAgent.logger = LoggingManager.setupLogging( LOGGING_DIR, "dman", Configuration.dman_logging_level );
		DialogueActAgent.logger = DialManAgent.logger;
		AbstractNLU.logger = LoggingManager.setupLogging( LOGGING_DIR, "nlu", Configuration.nlu_logging_level );
		AbstractNLG.logger = LoggingManager.setupLogging( LOGGING_DIR, "nlg", Configuration.nlg_logging_level );
		TemplateBasedNLG2.logger1 = AbstractNLG.logger;

		lexicon_file = Configuration.lexicon_file;
		domain_file = Configuration.domain_file;
		domain = new Ontology( domain_file, lexicon_file );
		DialManAgent.domain = domain;
		DialogueActAgent.domain = domain;
//		Abstract_LUIS_NLU.domain = domain;
		AbstractNLU.domain = domain;
		GoalGenerator.domain = domain;
		
		dbase_file = Configuration.dbase_file;
		database = new Database( dbase_file, domain );
		
		nlu_system = NLU_System.valueOf( Configuration.nlu_system );
		
		RUN_SERVER = Boolean.parseBoolean( Configuration.RUN_SERVER );
		RUN_WEBSOCKET_SERVER = Boolean.parseBoolean( Configuration.RUN_WEBSOCKET_SERVER );
		PORT_NUMBER = Integer.parseInt( Configuration.PORT_NUMBER );
		TOKEN_MANAGER_URL = Configuration.TOKEN_MANAGER_URL;
		
		ASR_NBEST_SIZE = Integer.parseInt( Configuration.ASR_NBEST_SIZE );
		ASR_CONF_THRESHOLD = Double.parseDouble( Configuration.ASR_CONF_THRESHOLD );
        MIN_NUM_NONTRIVIAL_TURNS = Integer.parseInt( Configuration.MIN_NUM_NONTRIVIAL_TURNS );
		
        use_mdp = Boolean.parseBoolean( Configuration.use_mdp );
        use_deep_mdp = Boolean.parseBoolean( Configuration.use_deep_mdp );
        use_md_dm = Boolean.parseBoolean( Configuration.use_md_dm );

        StringBuffer logStr = new StringBuffer( "Main configuration:\n" );
		logStr.append( String.format( "   LOGGING_DIR: %s\n", LOGGING_DIR ) );
		logStr.append( String.format( "   Domain ontology: %s\n", domain_file ) );
		logStr.append( String.format( "   Domain database: %s\n", dbase_file ) );
		logStr.append( String.format( "   Running as server: %s\n", RUN_SERVER ) );
		logStr.append( String.format( "   Port number: %d\n", PORT_NUMBER ) );
		logStr.append( String.format( "   Token manager URL: %s\n", TOKEN_MANAGER_URL ) );
		logStr.append( String.format( "   ASR N-best size: %d\n", ASR_NBEST_SIZE ) );
		logStr.append( String.format( "   ASR confidence threshold: %.3f\n", ASR_CONF_THRESHOLD ) );
		logStr.append( String.format( "   MIN_NUM_NONTRIVIAL_TURNS: %d\n", MIN_NUM_NONTRIVIAL_TURNS ) );
		logStr.append( String.format( "   Using MDP dialogue manager: %s\n", use_mdp ) );
		logStr.append( String.format( "   Using deep MDP dialogue manager: %s\n", use_deep_mdp ) );
		logStr.append( String.format( "   Using multi-dimensional dialogue manager: %s\n", use_md_dm ) );
		logger.logf( Level.WARNING, "%s\n", logStr );
		
		Abstract_LUIS_NLU.loadConfig();
		AbstractNLG.loadConfig();
		
		if ( use_mdp ) {
			TabularMDP.logger = LoggingManager.setupLogging( Configuration.logging_directory, "mdp", Configuration.mdp_logging_level );
			AbstractTabularMDPDialMan.loadConfig(); // also loads MDP config
		} else if ( use_deep_mdp ) {
			NumericMDP.logger = LoggingManager.setupLogging( Configuration.logging_directory, "mdp", Configuration.mdp_logging_level );
			AbstractMDPDialMan.loadConfig(); // also loads DeepMDP config
		} else if ( use_md_dm ) {
			NumericMDP.logger = LoggingManager.setupLogging( Configuration.logging_directory, "mdp", Configuration.mdp_logging_level );
			MultiDimDialMan.loadConfig();
		} else
			DialogueManager.loadConfig();

	}
	
    	
    private static void syncRandNumGen() {
    	DialManAgent.rand_num_gen = rand_num_gen;
    	DialogueActAgent.rand_num_gen = rand_num_gen;
    	TabularMDP.rand_num_gen = rand_num_gen;
    	Policy.rand_num_gen = rand_num_gen;
    	NumericMDP.rand_num_gen = rand_num_gen;
    	AbstractNLG.rand_num_gen = rand_num_gen;
    }

    String getResponse( String usr_utt ) {
    	if ( !usr_utt.isEmpty() ) {
    		// get user dialogue act
    		usr_turn = nlu.getSemantics( usr_utt );
        	logger.logf( Level.INFO, usr_turn.toString() );
    		// send user act(s) to dialogue manager
    		dial_man.receive( usr_turn );
    		// get system response act from dialogue manager
    		sys_turn = dial_man.respond();
    		// get system utterance
    		String sys_utt = nlg.getUtterance( sys_turn );
    		sys_turn.setUtterance( sys_utt );
        	logger.logf( Level.INFO, sys_turn.toString() );
    		return sys_utt;
    	}
    	return "";
    }
    
	@Override
	public void run() {
    	ServerSocket server;
    	Socket connection;
    	try {
    		server = new ServerSocket( PORT_NUMBER );
    		String hostName = server.getInetAddress().getHostAddress();
    		System.out.println( "Server started at host " + hostName + "; listening at port " + PORT_NUMBER );
			int dial_index = 0;
			StringBuffer logStr = new StringBuffer( "-----------------------------------\n" );
			logStr.append( "= = = = = DIALOGUE CORPUS = = = = =\n" );
			logger.logf( Level.INFO, "%s", logStr );
    		while ( true ) {
        		System.out.println( "Waiting for connection ..." );
//        		serverGUI.message( "Waiting for connection ..." );
        		
    			String dial_header = String.format( "\n===== Dialogue %d =====\n", dial_index );
    			dial_man.reset();
    			int turn_index = 0;
        		while ( true ) {
	    			connection = server.accept();
	    			System.out.println( "Connection received from: " + connection.getInetAddress().getHostName() );
//	    			serverGUI.message( "Connection received from: " + connection.getInetAddress().getHostName() );
	                out = new PrintWriter( connection.getOutputStream(), true );                   
	    			in = new BufferedReader( new InputStreamReader(connection.getInputStream()) );
	    			out.println( "SERVER>>> Connection successful" );
	    			
	    			// user turn
	    			String usr_utt = in.readLine();
	    			System.out.println( "User: " + usr_utt );
	    			if ( usr_utt.equals("HANGUP") ) {
	    				System.out.println( "CLIENT>>> HANGUP" );
	    				break;
	    			} else if ( usr_utt.startsWith("META:") ) {
	    				logger.logf( Level.INFO, usr_utt );
	    				out.println( "SERVER>>> " + usr_utt + " received" );
	    				continue;
	    			}

	    			if ( !dial_header.isEmpty() ) {
	    				logger.log( Level.INFO, dial_header );
	    				dial_header = "";
	    			}
	    			logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_index );

	    			// system turn
	    			String sys_utt = getResponse( usr_utt );
	    			System.out.println( "System: " + sys_utt );
	    			out.println( sys_utt );

	    			turn_index++;
    			
	    			in.close();
	    			out.close();
	    			connection.close();
        		}
//    			serverGUI.message( "Dialogue ended, connection closed\n" );
        		System.out.println( "Dialogue ended, connection closed\n" );
    			dial_index++;
    			
    			String token = getToken( "Madrigal" ); //TODO sort out setting system/dialogue IDs
    			if ( !token.isEmpty() ) {
    				System.out.println( "Sending token: " + token );
    				out.println( token );
    			}

    		}
    	} catch ( IOException ioe ) {
    		ioe.printStackTrace();
    	}
    }
	
    private void run_dialogue() throws IOException {
		dial_man.reset();
		String usr_utt = "";
		String sys_utt = "";
		int turn_index = 0;
		while ( usr_utt != null ) {
			logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_index );
			usr_utt = in.readLine();
			System.out.println( "User: " + usr_utt );
			if ( usr_utt.equals("HANGUP") ) {
				System.out.println( "CLIENT>>> HANGUP" );
				break;
			}
			sys_utt = getResponse( usr_utt );
			System.out.println( "System: " + sys_utt );
			out.println( sys_utt );
			turn_index++;
		}
		if ( !TOKEN_MANAGER_URL.isEmpty() ) {
			// get token and send to user/client
			String token = getToken( "Madrigal" ); //TODO sort out setting system/dialogue IDs
			if ( !token.isEmpty() ) {
				System.out.println( "Sending token: " + token );
				out.println( token );
			}
		}
    }
    
    // requests a new token from the (cgi) token manager to provide to the user for verification of task
    String getToken( String id ) {
    	
		if ( TOKEN_MANAGER_URL.equals("NONE") )
			return "1234";
		
		/*
		 * info to pass to token manager:
		 * sysName
		 * sysPort
		 * userID
		 * taskID
		 */

		// get token and send to user/client
		String url = TOKEN_MANAGER_URL + "?dialogueID=" + id;
		//System.out.println( "http request: " + url );
		URLConnection connection;
		try {
			connection = new URL( url ).openConnection();
			InputStream response = connection.getInputStream();
			BufferedReader bir = new BufferedReader( new InputStreamReader(response) );
			String resp_str = bir.readLine();
			
			/*
			 * resp_str will be json string
			 * meta : "GET_TOKEN"
			 * token : <token>
			 * message : "..." 
			 */
			
			if ( !resp_str.isEmpty() ) {
				String[] parts = resp_str.split( ":" );
				if ( parts.length == 2 && parts[0].equals("token") )
					return parts[1];
			}
		} catch ( MalformedURLException mue ) {
			mue.printStackTrace();
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		}
    	return "";
    }
	

    /**
     * requests a new token from the (php) token manager to provide to the user 
     * for verification of task.
     * 
     * All parameters will be passed from PHP server.
     * 
     * @param sysName string of system name
     * @param sysPort string of the port
     * @param userID string of the user ID, generated by browser Javascript
     * @param taskID string of taskID, passed from CrowdFlower loaded task data
     * @param userIP string of user host IP, got from PHP server. It will also be in CrowdFlower final report.
     * @param dialogueID string of a dialogue ID with format of "Dial_" + timestamp.
     * 
     * @return a string of issued token.
     */
    String getToken( String sysName, String sysPort, String userID, String taskID, String userIP, String dialogueID ) {
    	
		if ( TOKEN_MANAGER_URL.equals("NONE") )
			return "1234";
		
		// get token and send to user/client
		String url = TOKEN_MANAGER_URL + "?sysName=" + sysName + "&sysPort=" + sysPort 
                        + "&userID=" + userID + "&taskID=" + taskID + "&userIP=" + userIP + "&dialogueID=" + dialogueID;
		//System.out.println( "http request: " + url );
		URLConnection connection;
		try {
			connection = new URL( url ).openConnection();
			InputStream response = connection.getInputStream();
			BufferedReader bir = new BufferedReader( new InputStreamReader(response) );
			String resp_str = bir.readLine();
                        String msg =  "Raw Token String returned from PHP:" + resp_str;
			System.out.println(msg);
                        logger.logf( Level.INFO, msg );
			/*
			 * resp_str will be json string
			 * meta : "GET_TOKEN"
			 * token : <token>
			 * message : "..." 
			 */
			// XKL: now PHP returns "token:xxxx"
			if ( !resp_str.isEmpty() ) {
				String[] parts = resp_str.split( ":" );
				if ( parts.length == 2 && parts[0].equals("token") ) {
                                    String token = parts[1];
                                    bir.close();
                                    response.close();
                                    
                                    return token;
                                }
			}
		} catch ( MalformedURLException mue ) {
			mue.printStackTrace();
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		}
                
                System.out.println( "PHP getToken(..) returns empty token string!");
                
    	return "";
    }
    
    /** Constructor */
    public TextDialSystem() {
    	
    	setupDialogueSystem();
		
    	if ( RUN_SERVER ) {
//    	if ( RUN_NEW_SERVER ) {
			DialSysServer.logger = logger;
			DialSysServer server = new DialSysServer( this, PORT_NUMBER ); 
			server.start();
			runCmdlineInterface( server );
			System.exit( 0 );
    	
    	} else if ( RUN_OLD_SERVER ) {
//    	} else if ( RUN_SERVER ) {
			Thread server = new Thread( this );
			server.start();
			System.out.println( "Server started" );
			runCmdlineInterface( server );
			System.exit( 0 );

    	} else if ( RUN_WEBSOCKET_SERVER ) {
			try {
				WebSocketImpl.DEBUG = true;

				DialogSystemServer.logger = logger;
				DialogSystemServer server = new DialogSystemServer( this, PORT_NUMBER );
				server.start();
				System.out.println( "Dialogue system server listening on port: " + server.getPort() );
				
				// command line interface to server
				BufferedReader sysin = new BufferedReader( new InputStreamReader(System.in) );
				while ( true ) {
					String in = sysin.readLine();
					if ( in.equals( "exit" ) ) {
						// save dialogue
//						server.closeJsonDial(); //TODO only save json if last dialogue not properly ended
						server.stop();
						sysin.close();
						LoggingManager.close_loggers();
						System.out.println( "Log files closed" );
						break;
					}
				}

			} catch ( UnknownHostException e ) {
				e.printStackTrace();
			} catch ( IOException e ) {
				e.printStackTrace();
			} catch ( InterruptedException e ) {
				e.printStackTrace();
//			} catch (JSONException e) {
//				e.printStackTrace();
			}
			
		} else {
			new TextDialSystemGUI( this );
			TextDialSystemGUI.logger = logger;
		}
    	
	}
    
	/** Command line interface to server: can be used to shut down 
	 * the dialogue system server application (by typing 'exit') 
	 * and properly close the log files.
	 */
    private void runCmdlineInterface( Thread server ) {
		BufferedReader sysin = new BufferedReader( new InputStreamReader(System.in) );
		while ( true ) {
			try {
				String in = sysin.readLine();
				if ( in.equals( "exit" ) ) {
					server.interrupt();
					sysin.close();
					LoggingManager.close_loggers();
					System.out.println( "Log files closed" );
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println( "System shutdown" );
    }
    
    /** Creates NLU, NLG and DM components */
	private void setupDialogueSystem() {
		if ( nlu_system == NLU_System.LUIS )
			nlu = new LUIS_NLU();
		else if ( nlu_system == NLU_System.RASA )
			nlu = new RASA_NLU();
		else {
			System.out.println( "Fatal error: Could not create NLU system." );
			System.exit( 0 );
		}
			
		nlg = new Template_NLG();

		if ( use_mdp )
			dial_man = new NBestMDPDialMan();
		else if ( use_deep_mdp )
			dial_man = new MDPDialMan();
		else if ( use_md_dm ) {
			dial_man = new MultiDimDialMan();
			((MultiDimDialMan) dial_man).setReloadPolicy( true );
		} else
			dial_man = new DialogueManager();
	}
	    
    private static Options createCmdLnOpts() {
    	
		Options options = new Options();

		Option help = Option.builder( "h" )
				.hasArg( false )
				.longOpt( "help" )
				.desc( "print usage information" )
				.build();
		options.addOption( help );

		Option config = Option.builder( "c" )
				.hasArg()
				.longOpt( "config" )
				.desc( "configuration file" )
				.build();
		options.addOption( config );

		return options;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
    	
		Options options = createCmdLnOpts();
		CommandLineParser parser = new DefaultParser();
		core_settings = new Properties();
		try {
			core_settings = Resources.readProperties( core_settings_filename );
			Configuration.loadConfig( core_settings );
			
			// process command line arguments
			CommandLine cmd = parser.parse( options, args );
			
			if ( cmd.hasOption("h") ) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "uk.ac.hw.madrigal.TextDialSystem", options );
				System.exit( 0 );
			}
			if ( cmd.hasOption("c") ) {
				config_filename = cmd.getOptionValue( "c" );
		        InputStream config_is = new FileInputStream( new File(config_filename) );
		        config = new Properties();
		        config.load( config_is );
		        Configuration.loadConfig( config );
			}

	    	loadConfig();
	    	syncRandNumGen();

	    	new TextDialSystem();
	    	
		} catch ( ParseException pe ) {
			System.out.println( pe.getMessage() );
        } catch ( FileNotFoundException fnfe ) {
			System.out.println( fnfe.getMessage() );
        } catch ( IOException ioe ) {
			System.out.println( ioe.getMessage() );
		}
		
    }

}
