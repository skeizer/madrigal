/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.util.ArrayList;
import java.util.logging.Level;

import resources.Configuration;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.domain.DBEntity;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.domain.UserGoalSVP.GroundStatus;

/**
 * Hand-coded dialogue manager.
 */
public class DialogueManager extends DialManAgent {
	
	private static double SLU_CONF_THRESHOLD = .5;
	private static double CONF_THRESHOLD = .6;
	private static int NUM_ENTITIES_THRESHOLD = 3;
	
	@Override
	public DialogueTurn respond() {
		// select response action based on user goal and database matches ...
		// for now, always recommend a venue, unless user has not specified any constraints
		DialogueAct sys_act = DialActUtils.newSystemAct( CommFunction.inform );
		ArrayList< String > reqSlots = info_state.getReqSlots( CONF_THRESHOLD );
		DialogueAct last_uact = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		CommFunction last_sact_cf = ( info_state.last_system_act == null ? null : info_state.last_system_act.getCommFunction() ); 

		if ( info_state.getProcProblem() == ProcLevel.PERCEPTION )
			sys_act.setCommFunction( CommFunction.autoNegative_perc );
		
		else if ( info_state.getProcProblem() == ProcLevel.INTERPRETATION || last_uact.getConf() < SLU_CONF_THRESHOLD )
			sys_act.setCommFunction( CommFunction.autoNegative_int );
		
		else {
			String primDescrStr = primeSlot;
//			if ( info_state.react_pressure == ReactivePressure.BYE ) {
//				sys_act.setCommFunction( CommFunction.returnGoodbye );
			if ( last_uact != null 
					&& last_uact.getCommFunction() == CommFunction.initialGoodbye && last_uact.getSemContent().isEmpty() 
					&& info_state.current_entity != null && reqSlots.isEmpty() 
					&& last_sact_cf != null && last_sact_cf != CommFunction.propQuestion && last_sact_cf != CommFunction.setQuestion ) {
				sys_act.setCommFunction( CommFunction.returnGoodbye ); 
			//} else if ( !info_state.requested_slots.isEmpty() && info_state.current_entity != null ) {
			} else if ( !reqSlots.isEmpty() && info_state.current_entity != null ) {
				SemanticItem sem_item = new SemanticItem();
				String val = info_state.current_entity.getAttribute( primDescrStr );
				sem_item.addSlotValuePair( new SlotValuePair(primDescrStr,val) );
				for ( String req_slot: reqSlots ) {
					val = info_state.current_entity.getAttribute( req_slot );
					sem_item.addSlotValuePair( new SlotValuePair(req_slot,val) );
				}
				sys_act.addItem( sem_item );
				
			} else if ( last_uact.getCommFunction() == CommFunction.propQuestion && info_state.current_entity != null ) {
				SemanticItem sem_item = new SemanticItem();
				SlotValuePair corrSVP;
				for ( SlotValuePair svp: last_uact.getSemContent().getFirstItem().svps ) {
					if ( !info_state.current_entity.matches(svp) ) {
						corrSVP = new SlotValuePair( svp.slot, info_state.current_entity.getAttribute(svp.slot) );
						sem_item.addSlotValuePair( corrSVP );
					}
				}
				if ( sem_item.svps.isEmpty() )
					sys_act.setCommFunction( CommFunction.confirm );
				else {
					sys_act.setCommFunction( CommFunction.disconfirm );
					sys_act.addItem( sem_item );
				}
			} else {
				// uncertainty handling
	//			int num_entities = info_state.matching_entities.size();
				UserGoalSVP minConfUG = info_state.getMinConfUsrGoalHyp();
				double minConf = ( minConfUG == null ? -1 : minConfUG.getConf() );
				logger.logf( Level.FINE, "Minimum belief: %.3f (%s)\n", minConf, (minConfUG==null?"NA":minConfUG.toString()) );
				if ( minConfUG != null && minConf < CONF_THRESHOLD && minConfUG.getGroundStatus() != GroundStatus.GROUNDED ) {
					
					// construct propQuestion confirming ug_hyp_min
					sys_act.setCommFunction( CommFunction.propQuestion );
					UserGoalItem ugConfItem = new UserGoalItem();
					ugConfItem.addUGoalSVP( minConfUG );
					SemanticItem confItem = new SemanticItem( ugConfItem );
					sys_act.addItem( confItem );
					
				} else if ( !info_state.slots_to_ask.isEmpty() && info_state.matching_entities.size() > NUM_ENTITIES_THRESHOLD ) {
					
					sys_act.setCommFunction( CommFunction.setQuestion );
					SlotValuePair randSVP = info_state.slots_to_ask.get( rand_num_gen.nextInt(info_state.slots_to_ask.size()) );
					SlotValuePair newSVP = new SlotValuePair( randSVP.slot );
					SemanticItem newItem = new SemanticItem();
					newItem.addSlotValuePair( newSVP );
					sys_act.addItem( newItem );
					
				} else {
				
					DBEntity entity = info_state.getRecommendation();
					SemanticItem sem_item = new SemanticItem();
					if ( entity == null ) { // no matching entities found in database
						String noneStr = SlotValuePair.getString( SlotValuePair.SpecialValue.NONE );
						sem_item.addSlotValuePair( new SlotValuePair(primeSlot,noneStr) );
					} else {
						SlotValuePair name_svp = new SlotValuePair( primeSlot, entity.getAttribute(primeSlot) );
						sem_item.addSlotValuePair( name_svp );
					}
					// including user goal slot value pairs, which were used for the database query
					for ( UserGoalSVP ug_svp: info_state.ugoal_top_hyps ) { // info_state.user_goal_items ) {
						SlotValuePair svp = ug_svp.getSlotValuePair();
						if ( svp.isConstraint() && !svp.value.equals(SlotValuePair.getString(SlotValuePair.SpecialValue.DONTCARE)) )
							sem_item.addSlotValuePair( svp );
					}
					//TODO including attributes should be handled by feedback agent; task agent would simply say "<name> matches your constraints",
					// or possibly include new attributes to help the user in their search 
					// for ( SlotValuePair svp: entity.getAttributes() )
					// 		sem_item.addSlotValuePair( svp );
					sys_act.addItem( sem_item );
				
					info_state.current_entity = entity;
					//TODO make current_entity keep track of which information has been conveyed about this entity
	
				}
			}
		}
		
    	logger.logf( Level.INFO, "Response act: %s\n", sys_act.toShortString() );
		info_state.updateWithSysDialAct( sys_act );
		logger.logf( Level.FINE, "New info state:\n%s\n", info_state.toString() );
    	turn_num++;
    	
    	DialogueTurn sys_turn = new DialogueTurn( AgentName.system, AgentName.user );
    	sys_turn.setDialAct( sys_act );
    	sys_turn.setProcLevel( ProcLevel.INTERPRETATION );
		return sys_turn;
	}
	
	public static void loadConfig() {
		SLU_CONF_THRESHOLD = Double.parseDouble( Configuration.SLU_CONF_THRESHOLD );
		CONF_THRESHOLD = Double.parseDouble( Configuration.CONF_THRESHOLD );
		NUM_ENTITIES_THRESHOLD = Integer.parseInt( Configuration.NUM_ENTITIES_THRESHOLD );
		// show configuration:
        StringBuffer logStr = new StringBuffer( "Dialogue Manager configuration:\n" );
		logStr.append( String.format( "   SLU_CONF_THRESHOLD: %.2f\n", SLU_CONF_THRESHOLD ) );
		logStr.append( String.format( "   CONF_THRESHOLD: %.2f\n", CONF_THRESHOLD ) );
		logStr.append( String.format( "   NUM_ENTITIES_THRESHOLD: %d\n", NUM_ENTITIES_THRESHOLD ) );
		logger.logf( Level.WARNING, "%s\n", logStr );
	}

}
