/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created February 2017                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.utils.LoggingManager;
import uk.ac.hw.madrigal.utils.MyLogger;

/**
 * GUI for TextDialSystem when running standalone (i.e. not as server).
 */
public class TextDialSystemGUI extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	static MyLogger logger;

	private TextDialSystem dial_sys;
	private DialogueTurn usr_turn, sys_turn;
	
	private JTextField user_text_field;
	private JTextArea dact_text_area, system_text_area;

	/** Constructor */
	public TextDialSystemGUI( TextDialSystem ds ) {
		super( "Text-based dialogue system" );
		dial_sys = ds;
		
		Container container = getContentPane();
		container.setLayout( new BorderLayout() );
		
		JPanel usr_pnl = new JPanel();
		usr_pnl.setBorder( BorderFactory.createEmptyBorder(10,10,10,10) );
		JLabel usr_lbl = new JLabel( "user utterance:", JLabel.TRAILING );
		usr_pnl.add( usr_lbl );
		user_text_field = new JTextField( 35 );
		usr_lbl.setLabelFor( user_text_field );
		usr_pnl.add( user_text_field );
		container.add( usr_pnl, BorderLayout.NORTH );

		JPanel nlu_pnl = new JPanel();
		nlu_pnl.setLayout( new BoxLayout(nlu_pnl,BoxLayout.PAGE_AXIS) );
		JLabel nlu_lbl = new JLabel( "user act", JLabel.TRAILING );
		nlu_lbl.setBorder( BorderFactory.createEmptyBorder(10,10,10,10) );
		nlu_pnl.add( nlu_lbl );
		dact_text_area = new JTextArea( 15, 50 );
		dact_text_area.setBorder( BorderFactory.createEmptyBorder(10,10,10,10) ); 
		nlu_lbl.setLabelFor( dact_text_area );
		nlu_pnl.add( dact_text_area );
		container.add( nlu_pnl, BorderLayout.WEST );
		
		JPanel sys_pnl = new JPanel();
		sys_pnl.setBorder( BorderFactory.createEmptyBorder(10,10,10,10) ); 
		JLabel sys_lbl = new JLabel( "system response:", JLabel.TRAILING );
		sys_pnl.add( sys_lbl );
		system_text_area = new JTextArea( 5, 35 );
		sys_lbl.setLabelFor( system_text_area );
		sys_pnl.add( system_text_area );
		container.add( sys_pnl, BorderLayout.SOUTH );

        addWindowListener( new WindowAdapter() {
        	public void windowClosing( WindowEvent we ) {
        		LoggingManager.close_loggers();
        		System.out.println( "Log files closed" );
        		System.exit( 0 );
        	}
        } );
                
        user_text_field.addActionListener( this );
        
        pack();
        setVisible( true );

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if ( e.getSource() == user_text_field ) {
			String usr_utt = user_text_field.getText();
        	if ( !usr_utt.isEmpty() ) {
        		// get user dialogue act
        		usr_turn = dial_sys.nlu.getSemantics( usr_utt );
        		dact_text_area.setText( "\n========================\n" );
        		dact_text_area.append( usr_turn.toString() );
        		logger.log( Level.INFO, "========================\n" );
        		logger.log( Level.INFO, usr_turn.toString() );
        		// send user act(s) to dialogue manager
        		dial_sys.dial_man.receive( usr_turn );
        		// get system response act from dialogue manager
        		sys_turn = dial_sys.dial_man.respond();
        		if ( sys_turn.dact_all.getCommFunction() == CommFunction.returnGoodbye ) {
        			String tk = dial_sys.getToken( "skeizer" );
        			JOptionPane.showMessageDialog( null, "Token: " + tk );
        		}
        		dact_text_area.append( "\n========================\n" );
        		dact_text_area.append( sys_turn.toString() );
        		logger.log( Level.INFO, "========================\n" );
        		logger.log( Level.INFO, sys_turn.toString() );
        		String sys_utt = dial_sys.nlg.getUtterance( sys_turn );
        		system_text_area.setText( sys_utt );
        		user_text_field.selectAll();
        	}
		}
	}

}
