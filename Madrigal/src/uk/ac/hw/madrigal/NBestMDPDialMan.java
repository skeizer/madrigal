/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created October 2016                                                            *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;

import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.learning.mdps.TabularMDP;
import uk.ac.hw.madrigal.utils.Utils;

/** Modified version of MDP Dialogue Manager, with a smaller state space */
public class NBestMDPDialMan extends TabularMDPDialMan {

	@Override
	protected void initialise_MDP_model() {
		num_state_feats = 8;
		state_dims = new int[ num_state_feats ];
		state_dims[0] = 6; // see getSysActIndex
		state_dims[1] = 6; // top usr act hyp; see getUserActIndex
		state_dims[2] = 5; // SLU conf bins
		state_dims[3] = 5; // SLU normalised entropy bins
		state_dims[4] = 6; // UG hyp min belief conf bins
		state_dims[5] = 5; // UG max entropy bins
		state_dims[6] = 4; // number of matching entities: 0, 1, 2, >2
		state_dims[7] = 5; // Req Slots max belief conf bins
		num_acts = 6;
		mdp_model = new TabularMDP( "dial_man_mdp", num_state_feats, state_dims, num_acts );
	}

	@Override
	protected int[] getStateFeatures() {
		int[] features = new int[ num_state_feats ];
		int featInd = 0;
		
		DialogueAct lastSysAct = info_state.last_system_act;
		features[featInd] = getSysActIndex( lastSysAct );		
		featInd++;
		
		DialogueAct lastUserAct = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		features[featInd] = getUserActIndex( lastUserAct );
		featInd++;
		
		double SLU_conf = lastUserAct.getConf();
		features[featInd] = getConfBin( SLU_conf, state_dims[featInd] );
		featInd++;
		
		double SLU_normEntropy = ( info_state.last_uact_hyps.isEmpty() ? 1f : Utils.getEntropy( info_state.last_uact_hyps, true ) );
		features[featInd] = getConfBin( SLU_normEntropy, state_dims[featInd] );
		featInd++;
		
		if ( info_state.minConfUG == null )
			features[featInd] = state_dims[featInd] - 1; // reserve last value to indicate there is no min conf UG hyp
		else {
			double minConf = info_state.minConfUG.getConf();
			logger.logf( Level.FINE, "minConfUG: %.2f\n", minConf );
			features[featInd] = getConfBin( minConf, state_dims[featInd] - 1 );
		}
		featInd++;
		
		if ( info_state.ug_item_entropies.isEmpty() )
			features[featInd] = state_dims[featInd] - 1; // reserve last value to indicate there is no max ent UG hyp
		else {
			double maxEnt = -1d;
			for ( Double f: info_state.ug_item_entropies )
				if ( f > maxEnt )
					maxEnt = f;
			features[featInd] = getConfBin( maxEnt, state_dims[featInd] - 1 );
		}
		featInd++;
		
		int numMatchEnts = info_state.matching_entities.size();
		features[featInd] = getNumIndex( numMatchEnts, state_dims[featInd] );
		featInd++;
		
		reqSlots = info_state.requested_slots; // info_state.getReqSlots();
		logger.logf( Level.FINE, "reqSlots: %s\n", StringUtils.join(reqSlots, ",") );
		//TODO replace with info_state members
		double maxProb = 0d;
		UserGoalSVP ug_svp_max = null;
		for ( UserGoalSVP ug_svp: reqSlots ) {
			if ( ug_svp.getConf() > maxProb ) {
				maxProb = ug_svp.getConf();
				ug_svp_max = ug_svp;
			}
		}
		logger.logf( Level.FINE, "max prob reqSlot: %s (%.3f)\n", ( ug_svp_max == null ? "NONE" : ug_svp_max.toString() ), maxProb );
		if ( ug_svp_max == null )
			features[featInd] = state_dims[featInd] - 1; // reserve last value to indicate there is no evidence for any requested slots
		else
			features[featInd] = getConfBin( maxProb, state_dims[featInd] - 1 );
		//featInd++;

		String logStr = "MDP features:";
		for ( int i = 0; i < num_state_feats; i++ )
			logStr += " " + features[i];
		logger.log( Level.FINE, logStr + "\n" );

		return features;
		
	}

	//@Override
	protected double[] getNumericStateFeatures() {
		double[] features = new double[ num_state_feats ];
		int featInd = 0;
		
		DialogueAct lastSysAct = info_state.last_system_act;
		features[featInd] = getSysActIndex( lastSysAct );		
		featInd++;
		
		DialogueAct lastUserAct = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		features[featInd] = getUserActIndex( lastUserAct );
		featInd++;
		
		features[featInd] = lastUserAct.getConf();
		featInd++;
		
		features[featInd] = ( info_state.last_uact_hyps.isEmpty() ? 1f : Utils.getEntropy( info_state.last_uact_hyps, true ) );
		featInd++;
		
		features[featInd] = info_state.minConfUG.getConf();
		featInd++;
		
		double maxEnt = -1d;
		for ( Double f: info_state.ug_item_entropies )
			if ( f > maxEnt )
				maxEnt = f;
		features[featInd] = maxEnt;
		featInd++;
		
		features[featInd] = info_state.matching_entities.size();
		featInd++;
		
		reqSlots = info_state.requested_slots; // info_state.getReqSlots();
		logger.logf( Level.FINE, "reqSlots: %s\n", StringUtils.join(reqSlots, ",") );
		//TODO replace with info_state members
		double maxProb = 0d;
		for ( UserGoalSVP ug_svp: reqSlots )
			if ( ug_svp.getConf() > maxProb )
				maxProb = ug_svp.getConf();
		features[featInd] = maxProb;

		String logStr = "MDP features:";
		for ( int i = 0; i < num_state_feats; i++ )
			logStr += String.format( " %.2f", features[i] );
		logger.log( Level.FINE, logStr + "\n" );

		return features;
		
	}
	
}
