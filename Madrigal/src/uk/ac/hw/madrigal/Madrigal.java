/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import resources.Configuration;
import resources.Resources;
import cc.mallet.util.Randoms;
import uk.ac.hw.madrigal.dial_act_agents.DialogueActAgent;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.domain.Database;
import uk.ac.hw.madrigal.domain.GoalGenerator;
import uk.ac.hw.madrigal.domain.Lexicon;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.learning.mdps.TabularMDP;
import uk.ac.hw.madrigal.learning.mdps.AbstractMDP;
import uk.ac.hw.madrigal.learning.mdps.NumericMDP;
import uk.ac.hw.madrigal.learning.mdps.Policy;
import uk.ac.hw.madrigal.simulation.ErrorModel;
import uk.ac.hw.madrigal.simulation.InputModel;
import uk.ac.hw.madrigal.simulation.UserSimulator;
import uk.ac.hw.madrigal.utils.CorpusStats;
import uk.ac.hw.madrigal.utils.LoggingManager;
import uk.ac.hw.madrigal.utils.MyLevel;
import uk.ac.hw.madrigal.utils.MyLogger;
import uk.ac.hw.madrigal.utils.MyTimer;

/**
 * Program to test, train, and evaluate the MaDrIgAL dialogue manager
 * in interaction with the MaDrIgAL simulated user.
 */
public class Madrigal {
	
	static MyTimer timer = new MyTimer();
//	static ArrayList<MyLogger> loggers = new ArrayList<MyLogger>();
	static String LOGGING_DIR;// = "logs";

	static String config_filename; // = "config.cfg";
	static String core_settings_filename = "CoreSettings.cfg";
	static Properties core_settings, config;
	static MyLogger logger;
    public static Randoms rand_num_gen = new Randoms();
    
    static String domain_file = "ontology.json";
    public static Ontology domain; // = new Ontology( domain_file );
    static String lexicon_file;
    public static Lexicon lexicon;
    static String dbase_file = "database.json";
    public static Database database;
    
//    static String num_dialogues_str = "25";
//    static String start_dialogue_str = "-1";
//    static String max_num_turns_str = "15";
    static int num_dialogues;
    static int start_dialogue;
    static int max_num_turns;
    
    static boolean use_mdp = false;
    static boolean use_deep_mdp = false;
    static boolean use_md_dm = false;

	private static void loadConfig() {

		LOGGING_DIR = Configuration.logging_directory;
		logger = LoggingManager.setupLogging( LOGGING_DIR, "main", Configuration.logging_level );
		UserSimulator.logger = LoggingManager.setupLogging( LOGGING_DIR, "usim", Configuration.usim_logging_level );
		GoalGenerator.logger = UserSimulator.logger;
		InputModel.logger = LoggingManager.setupLogging( LOGGING_DIR, "emod", Configuration.emod_logging_level );
		ErrorModel.logger = InputModel.logger;
		DialManAgent.logger = LoggingManager.setupLogging( LOGGING_DIR, "dman", Configuration.dman_logging_level );
		DialogueActAgent.logger = DialManAgent.logger;

		lexicon_file = Configuration.lexicon_file;
		domain_file = Configuration.domain_file;
		domain = new Ontology( domain_file, lexicon_file );
		DialManAgent.domain = domain;
		DialogueActAgent.domain = domain;
		UserSimulator.domain = domain;
		ErrorModel.domain = domain;
		GoalGenerator.domain = domain;
		
		dbase_file = Configuration.dbase_file;
		database = new Database( dbase_file, domain );
		GoalGenerator.dbase = database;
        
        num_dialogues = Integer.parseInt( Configuration.num_dialogues );
        start_dialogue = Integer.parseInt( Configuration.start_dialogue );
        max_num_turns = Integer.parseInt( Configuration.max_num_turns );
        use_mdp = Boolean.parseBoolean( Configuration.use_mdp );
        use_deep_mdp = Boolean.parseBoolean( Configuration.use_deep_mdp );
        use_md_dm = Boolean.parseBoolean( Configuration.use_md_dm );

        StringBuffer logStr = new StringBuffer( "Main configuration:\n" );
		logStr.append( String.format( "   LOGGING_DIR: %s\n", LOGGING_DIR ) );
		logStr.append( String.format( "   Domain ontology: %s\n", domain_file ) );
		logStr.append( String.format( "   Domain database: %s\n", dbase_file ) );
		logStr.append( String.format( "   Number of dialogues: %d\n", num_dialogues ) );
		logStr.append( String.format( "   Start dialogue index: %d\n", start_dialogue ) );
		logStr.append( String.format( "   Maximum number of turns: %d\n", max_num_turns ) );
		logStr.append( String.format( "   Using MDP dialogue manager: %s\n", use_mdp ) );
		logStr.append( String.format( "   Using deep MDP dialogue manager: %s\n", use_deep_mdp ) );
		logStr.append( String.format( "   Using multi-dimensional dialogue manager: %s\n", use_md_dm ) );
		logger.logf( Level.WARNING, "%s\n", logStr );
		
		UserSimulator.loadConfig();
		GoalGenerator.loadConfig( config );
		InputModel.loadConfig( config );
		if ( use_mdp ) {
			TabularMDP.logger = LoggingManager.setupLogging( Configuration.logging_directory, "mdp", Configuration.mdp_logging_level );
			AbstractTabularMDPDialMan.loadConfig(); // also loads MDP config
		} else if ( use_deep_mdp ) {
			NumericMDP.logger = LoggingManager.setupLogging( Configuration.logging_directory, "mdp", Configuration.mdp_logging_level );
			AbstractMDPDialMan.loadConfig(); // also loads DeepMDP config
		} else if ( use_md_dm ) {
			AbstractMDP.logger = LoggingManager.setupLogging( Configuration.logging_directory, "mdp", Configuration.mdp_logging_level );
			MultiDimDialMan.loadConfig();
		} else
			DialogueManager.loadConfig();

	}
	
	/** Runs a number of dialogues between simulated user and dialogue manager */
    private static void run_simulations( int start_dial_index ) {

		UserSimulator user_sim = new UserSimulator();
		//DialManAgent dial_man = ( use_mdp ? new MDPDialMan() : new DialogueManager() );
		DialManAgent dial_man;
		if ( use_mdp )
			dial_man = new NBestMDPDialMan();
		else if ( use_deep_mdp )
			dial_man = new MDPDialMan();
		else if ( use_md_dm )
			dial_man = new MultiDimDialMan();
		else
			dial_man = new DialogueManager();
		DialogueTurn usr_turn, sys_turn;
		CorpusStats stats = new CorpusStats();
		GoalGenerator goalGen = new GoalGenerator();
		
		timer.start();
		
		// CORPUS LOOP
		StringBuffer logStr = new StringBuffer( "-----------------------------------\n" );
		logStr.append( "= = = = = DIALOGUE CORPUS = = = = =\n" );
		logger.logf( Level.INFO, "%s", logStr );
		int dial_index;
		for ( int i = 0; i < num_dialogues; i++ ) {
			if ( start_dial_index >= 0 ) {
				rand_num_gen.setSeed( 1 + (start_dial_index + i) * 10000 );
				logger.logf( Level.FINE, "Set seeding with %d\n", 1 + (start_dial_index + i) * 10000 );
				dial_index = start_dial_index + i;
			} else
				dial_index = i;
			
			// DIALOGUE LOOP
			logger.logf( MyLevel.DIALSTATS, "\n===== Dialogue %d =====\n", dial_index );
			logger.logf( MyLevel.DIALSTATS, "GoalGen: %s\n", goalGen.getStats() );
			user_sim.start_dialogue( goalGen );
			stats.startNewEpisode();
			logger.logf( Level.INFO, "%s\n", user_sim.getUserGoal().toFirstItemString(false) );
			dial_man.reset();
			int j = 0;
			boolean hangup = false;
			while ( j < max_num_turns && !hangup ) {
				logger.logf( Level.INFO, "\n----- Turn %d -----\n", j );
				// get user response and generate n-best list
				usr_turn = user_sim.respond();
				InputModel.computeNBest( usr_turn );
				logger.logf( Level.INFO, usr_turn.toString() );
				// send user dialogue turn to the dialogue manager
				dial_man.receive( usr_turn );
				
				// get system response
				sys_turn = dial_man.respond();
				logger.logf( Level.INFO, sys_turn.toString() );
				if ( sys_turn.dact_all.getCommFunction() == CommFunction.returnGoodbye && sys_turn.dact_all.getSemContent().isEmpty() )
					hangup = true;
					//break; // system hangs up
				// send system response to user
				user_sim.receive( sys_turn );
				// get reward signal and update evaluation statistics
				double reward = user_sim.getReward();
				logger.logf( Level.INFO, "Rew> %.3f\n", reward );
				dial_man.observeReward( reward );
				stats.addScore( reward );
				j++;
			}
			boolean taskSuccess = user_sim.taskSuccess();
			double cumulScore = stats.processLatestEpisode( taskSuccess );
			dial_man.update( taskSuccess );
			//dial_man.savePolicies( "mdp" ); //NOTE now part of update in above line
			dial_man.computeStatistics();
			logStr = new StringBuffer();
			logger.logf( MyLevel.DIALSTATS, "\nDialogue score: %.3f\nTask success: %s\n", cumulScore, taskSuccess );
			//flush_loggers();
		}
		stats.computeStats();
		logStr = new StringBuffer( "\n= = = = = END OF CORPUS = = = = =\n" );
		logStr.append( "-----------------------------------\n" );
		logStr.append( stats.getSummary() );
		logger.logf( MyLevel.DIALSTATS, "%s", logStr );
		logger.logf( Level.FINE, "\tGenerated goals: %s\n", goalGen.getStats() );
		
		timer.stop();
		long timeMins = timer.time( TimeUnit.MINUTES );
		long timeSecs = timer.time( TimeUnit.SECONDS );
		long timeMSecs = timer.time( TimeUnit.MILLISECONDS );
		logger.logf( Level.WARNING, "\nComputation time: %d mins %d secs (%d msecs)\n", timeMins, timeSecs, timeMSecs );

    }
    
    private static Options createCmdLnOpts() {
		Options options = new Options();

		Option help = Option.builder( "h" )
				.hasArg( false )
				.longOpt( "help" )
				.desc( "print usage information" )
				.build();
		options.addOption( help );

		Option config = Option.builder( "c" )
				.hasArg()
				.longOpt( "config" )
				.desc( "configuration file" )
				.build();
		options.addOption( config );
		
		Option numdials = Option.builder( "n" )
				.hasArg()
				.longOpt( "numdials" )
				.desc( "number of dialogues to simulate" )
				.build();
		options.addOption( numdials );

		Option logdir = Option.builder( "l" )
				.hasArg()
				.longOpt( "logdir" )
				.desc( "logging directory" )
				.build();
		options.addOption( logdir );

		Option seed = Option.builder( "s" )
				.hasArg()
				.longOpt( "seed" )
				.desc( "number of first dialogue to simulate (use -1 for random seeding)" )
				.build();
		options.addOption( seed );

		Option confrate = Option.builder( "r" )
				.hasArg()
				.longOpt( "confrate" )
				.desc( "confusion rate (e.g., -r .25" )
				.build();
		options.addOption( confrate );

		Option policy = Option.builder( "p" )
				.hasArg()
				.longOpt( "policy" )
				.desc( "MDP policy file" )
				.build();
		options.addOption( policy );
		
		Option training = Option.builder( "t" )
				.hasArg()
				.longOpt( "training" )
				.desc( "update the MDP policy and save it in the specified directory" )
				.build();
		options.addOption( training );

		return options;
    }
    
    private static void syncRandNumGen() {
    	//TODO create static methods for syncing rand num gen (only call it here for 
    	//     UserSimulator, ErrorModel and DialManAgent and then propagate where necessary)
    	UserSimulator.rand_num_gen = rand_num_gen;
    	DialManAgent.rand_num_gen = rand_num_gen;
    	DialogueActAgent.rand_num_gen = rand_num_gen;
    	InputModel.rand_num_gen = rand_num_gen;
    	ErrorModel.rand_num_gen = rand_num_gen;
    	TabularMDP.rand_num_gen = rand_num_gen;
    	Policy.rand_num_gen = rand_num_gen;
    	NumericMDP.rand_num_gen = rand_num_gen;
    }
    
    /** 
     * Main method: processes the command line arguments 
     * and launches the simulation.
     */
	public static void main( String[] args ) {
		Options options = createCmdLnOpts();
		CommandLineParser parser = new DefaultParser();
		core_settings = new Properties();
		try {
			core_settings = Resources.readProperties( core_settings_filename );
			Configuration.loadConfig( core_settings );
			
			// process command line arguments
			CommandLine cmd = parser.parse( options, args );
			if ( cmd.hasOption("h") ) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "uk.ac.hw.madrigal.Madrigal", options );
				System.exit( 0 );
			}
			if ( cmd.hasOption("c") ) {
				config_filename = cmd.getOptionValue( "c" );
		        InputStream config_is = new FileInputStream( new File(config_filename) );
		        config = new Properties();
		        config.load( config_is );
		        Configuration.loadConfig( config );
			}
	        if ( cmd.hasOption("l") ) {
	        	Configuration.logging_directory = cmd.getOptionValue( "l" );
	        }
	        loadConfig();
	        
	        // overwrite config with any command line options
			if ( cmd.hasOption("n") ) {
				num_dialogues = Integer.parseInt( cmd.getOptionValue("n") );
				logger.logf( Level.INFO, "Number of dialogues set to %d\n", num_dialogues );
			}
			if ( cmd.hasOption("s") ) {
				start_dialogue = Integer.parseInt( cmd.getOptionValue("s") );
				logger.logf( Level.INFO, "Start dialogue index set to %d\n", start_dialogue );
			}
			if ( cmd.hasOption("r") )
				ErrorModel.setConfRate( Double.parseDouble(cmd.getOptionValue("r")) );
			if ( cmd.hasOption("p") )
				if ( use_mdp )
					AbstractTabularMDPDialMan.setPolicyFileName( cmd.getOptionValue("p") );
				else if ( use_deep_mdp )
					MDPDialMan.setPolicyFileName( cmd.getOptionValue("p") );
				else if ( use_md_dm )
					MultiDimDialMan.setPolicyFilenames( cmd.getOptionValue("p") );
			if ( cmd.hasOption("t") ) {
				if ( use_mdp )
					TabularMDP.setPolOpt( cmd.getOptionValue("t") );
				else if ( use_deep_mdp || use_md_dm )
					AbstractMDP.setPolOpt( cmd.getOptionValue("t") );
			}
			
			syncRandNumGen();
	        
	        System.out.print( "Running simulations ..." );
	        run_simulations( start_dialogue ); // fixed seeding for every dialogue index (for testing)
	        LoggingManager.close_loggers();
	        System.out.println( " done!" );
	        
		} catch ( ParseException pe ) {
			System.out.println( pe.getMessage() );
        } catch ( FileNotFoundException fnfe ) {
			System.out.println( fnfe.getMessage() );
        } catch ( IOException ioe ) {
			System.out.println( ioe.getMessage() );
        }
	}

}
