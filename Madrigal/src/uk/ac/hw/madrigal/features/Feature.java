package uk.ac.hw.madrigal.features;

public class Feature extends FeatureStructureElement {

	private String attribute;
	
	private FeatureStructureElement value;
	
	public Feature( String attr ) {
		attribute = attr;
	}
	
	public Feature( String attr, FeatureStructureElement val ) {
		attribute = attr;
		value = val;
	}
	
	public String getAttribute() {
		return attribute;
	}
	
	public FeatureStructureElement getValue() {
		return value;
	}
	
	public void setValue( FeatureStructureElement val ) {
		value = val;
	}
	
	public String toString( String indent ) {
		return indent + attribute + " : " + value.toString( indent );
	}

}
