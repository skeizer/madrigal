package uk.ac.hw.madrigal.features;

public class Value extends FeatureStructureElement {
	
	private String value;
	
	public Value( String val ) {
		value = val;
	}
	
	public String getValue() {
		return value;
	}

	public String toString( String indent ) {
		return value;
	}
	
}
