package uk.ac.hw.madrigal.features;

import java.util.Vector;

public class FeatureStructure extends FeatureStructureElement {
	
	private Vector<Feature> features;
	
	public FeatureStructure() {
		features = new Vector<Feature>();
	}
	
	public Feature getFeature( String attribute ) {
		for ( Feature feat : features ) {
			if ( feat.getAttribute().equals( attribute ) )
				return feat;
		}
		return null;
	}
	
	public void addFeature( Feature feat ) {
		features.add( feat );
	}
	
	public void addFeature( String attr, String val ) {
		Value fs_value = new Value( val );
		Feature f1 = new Feature( attr, fs_value );
		features.add( f1 );
	}
	
	public String toString( String indent ) {
		String result = "[\n";
		for ( Feature feat : features ) {
			result += feat.toString( indent + FeatureStructureElement.INDENT_SIZE ) + "\n";
		}
		result += indent + " ]";
		return result;
	}
	
	public String toString() {
		return toString( "" );
	}

}
