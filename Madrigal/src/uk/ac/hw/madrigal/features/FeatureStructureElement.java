package uk.ac.hw.madrigal.features;

public abstract class FeatureStructureElement {
	
	static String INDENT_SIZE = "     ";
	
	abstract public String toString( String indent );
	
	public String toString() {
		return toString( INDENT_SIZE );
	}

}
