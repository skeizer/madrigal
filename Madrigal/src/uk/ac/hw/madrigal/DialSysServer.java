package uk.ac.hw.madrigal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.logging.Level;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.hw.madrigal.dialogue_acts.ASRHypothesis;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.dialogue_acts.SemanticContent;
import uk.ac.hw.madrigal.utils.MyLogger;
import uk.ac.hw.madrigal.utils.MyTimer;
import uk.ac.hw.madrigal.utils.Utils;

public class DialSysServer extends Thread {

	protected static MyLogger logger;
	private static MyTimer timer = new MyTimer();

	private TextDialSystem dial_system;
	private JSONObject dial_json, turn_json;
	private JSONArray turns_arr;
	
	private DialogueTurn usr_turn, sys_turn;
	private int turn_index;
	private boolean dialogue_started;
	private int num_nontrivial_turns;
	
	private int port_number;	
	private ServerSocket server;
	private Socket connection;
	private PrintWriter out;
	private BufferedReader in;
	
	private String sysName;
	private String sysPort;
	private String userID;
	private String taskID;
        private String dialogueID;
        private String userIP;
	
    public enum MetaMessage {
    	INIT,
        SYSTEMTURN,
        USERTURN,
        HANGUP,
        DIALOGUE_TIMEOUT, 
        PORT_TIMEOUT, // related to DB
        RELOAD,
        CLOSE,
        DM_SERVER_ERROR
    }
    
	/** Constructor 
	 * @throws IOException */
	public DialSysServer( TextDialSystem ds, int port ) {
		dial_system = ds;
		turn_index = 0;
		dialogue_started = false;
		num_nontrivial_turns = 0;
		port_number = port;
	}

	@Override
	public void run() {
		String dm_err_msg = "Sorry, an internal error occurred.  Please reload the page.";
    	try {
			server = new ServerSocket( port_number );
			String hostName = server.getInetAddress().getHostAddress();
			System.out.println( "Server started at host " + hostName + "; listening at port " + port_number );

			int dial_index = 0;
			StringBuffer logStr = new StringBuffer( "-----------------------------------\n" );
			logStr.append( "= = = = = DIALOGUE CORPUS = = = = =\n" );
			logger.logf( Level.INFO, "%s", logStr );
    		while ( true ) {
        		System.out.println( "Waiting for connection ..." );
        		
    			String dial_header = String.format( "\n===== Dialogue %d =====\n", dial_index );
    			dial_system.dial_man.reset();
    			//TODO enable line below to force LUIS app to start up?
    			//DialogueTurn init_turn = dial_system.nlu.getSemantics( "start" );
				dial_json = new JSONObject();
				turns_arr = new JSONArray();
				sysName = "";
				sysPort = "";
				userID = "";
				taskID = "";
                                userIP = "";
                                dialogueID = "";
				turn_index = 0;
				num_nontrivial_turns = 0;

    			while ( true ) {
	    			connection = server.accept();
	    			System.out.println( "Connection received from: " + connection.getInetAddress().getHostName() );
	                out = new PrintWriter( connection.getOutputStream(), true );                   
	    			in = new BufferedReader( new InputStreamReader(connection.getInputStream()) );
	    			
	    			if ( !dial_header.isEmpty() ) {
	    				logger.log( Level.INFO, dial_header );
	    				System.out.print( dial_header );
	    				dial_header = "";
	    			}
	    			
	    			String message = in.readLine();
	    			System.out.println( ">>> Message received from client: " + message );
	    			boolean	stop = processMessage( message );
	    			
	    			in.close();
	    			out.close();
	    			connection.close();

	    			if ( stop ) break;
    			}
        		System.out.println( "Dialogue ended, connection closed\n" );
        		
    			dial_json.put( "dialogue", turns_arr );
    			if ( dialogue_started ) {
                            // saveDialogue( TextDialSystem.LOGGING_DIR, "demosys" );
                            saveDialogue( TextDialSystem.LOGGING_DIR, "demosys", dialogueID );
                        }
                        
    			dialogue_started = false;
    			dial_index++;
    			
    		}
    	} catch ( Exception e ) {
			String json_msg = getJSONMessage( dm_err_msg, "", MetaMessage.DM_SERVER_ERROR );
			out.println( json_msg );
			e.printStackTrace();
			System.exit( -1 );
//		} catch (JSONException e) {
//			String json_msg = getJSONMessage( dm_err_msg, "", MetaMessage.DM_SERVER_ERROR );
//			out.println( json_msg );
//			e.printStackTrace();
//			System.exit( -1 );
//    	} catch ( IOException ioe ) {
//			String json_msg = getJSONMessage( dm_err_msg, "", MetaMessage.DM_SERVER_ERROR );
//			out.println( json_msg );
//    		ioe.printStackTrace();
//			System.exit( -1 );
    	}
    }
	
	private void saveDialogue( String path, String prefix, String dialogueID ) throws JSONException, IOException {
		String logfilename = new SimpleDateFormat( "'" + prefix + "_'yyyy-MM-dd-'T'HH:mm:ss:SSS'.json'" ).format( new Date() );
		
                if(!dialogueID.isEmpty()) {
                    //X.Liu: use passed dialogueID to correspond to the audio log file name.
                    // it is unique for a specific user calling to the specific DM instance.
                    logfilename = prefix + "_" + dialogueID +".json";
                }
                
                File json_file = new File( path + File.separator + logfilename );
//		try {
			BufferedWriter writer = new BufferedWriter( new PrintWriter(json_file) );
			writer.write( dial_json.toString(5) );
			writer.close();
//		} catch ( IOException  ioe ) {
//			ioe.printStackTrace();
//		}
	}

	private boolean processMessage( String msg ) throws JSONException {
		
		JSONObject root = new JSONObject( msg );
		String meta_value = root.getString( "meta" );
		String response = "";
		String token = "";
                String logmsg = "";
                String crntTurnTimeStr = "init0";
                long crntTurnTimeMillis = 0;
                
		if ( meta_value.equals(MetaMessage.USERTURN.name()) ) {
			
			logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_index );
			System.out.printf( ">>> Processing user turn (%d; non-trivial: %d)\n", turn_index, num_nontrivial_turns );

                        crntTurnTimeStr = root.getString( "crntTurnTimeStr" );
                        crntTurnTimeMillis = root.getLong( "crntTurnTimeMillis" );
                        
			JSONArray asr_hyp_arr = root.getJSONArray( "asrhyps" );
			ArrayList<ASRHypothesis> asr_hyps = getASRHyps( asr_hyp_arr );
			Collections.sort( asr_hyps );
			
			ArrayList<ASRHypothesis> asr_hyps_nbest;
			if ( asr_hyps.size() < TextDialSystem.ASR_NBEST_SIZE )
				asr_hyps_nbest = asr_hyps;
			else
				asr_hyps_nbest = new ArrayList<ASRHypothesis>( asr_hyps.subList( 0, TextDialSystem.ASR_NBEST_SIZE ) );
			
			// hack to transform asr n-best list with confidence scores into a probability distribution
			Utils.pseudo_normalise( asr_hyps_nbest );

			sys_turn = getResponseAct( asr_hyps_nbest ); // also generate global usr_turn inside the method
			response = sys_turn.getUtterance();
			if ( sys_turn.dact_all.getCommFunction() == CommFunction.returnGoodbye ) {
                            
				if ( num_nontrivial_turns >= TextDialSystem.MIN_NUM_NONTRIVIAL_TURNS ) {
//					token = dial_system.getToken( Integer.toString(port_number) );
                                 token = dial_system.getToken( sysName, sysPort, userID, taskID, userIP, dialogueID );
                                 System.out.printf("Sending token=" + token);
                                 
                                } else {
					response = "You have ended the dialogue without trying to complete the task.";
                                }
			}
                        
                        // the sys response will be output at the end of the method.
                       // System.out.println(">>> Sys response: " + response);
                        
		} else if ( meta_value.equals(MetaMessage.HANGUP.name()) ) {
                        crntTurnTimeStr = root.getString( "crntTurnTimeStr" );
                        crntTurnTimeMillis = root.getLong( "crntTurnTimeMillis" );

                        sys_turn = new DialogueTurn( AgentName.system, AgentName.user );
			sys_turn.dact_all = DialActUtils.newSystemAct( CommFunction.returnGoodbye );
			if ( num_nontrivial_turns > TextDialSystem.MIN_NUM_NONTRIVIAL_TURNS ) {
//				token = dial_system.getToken( Integer.toString(port_number) );
                                 token = dial_system.getToken( sysName, sysPort, userID, taskID, userIP, dialogueID);
                                 System.out.printf("token=" + token);

				response = "Goodbye.";
			} else
				response = "You have ended the dialogue without trying to complete the task.";

                        // X.Liu, create a new usr_turn for HANGUP
                        usr_turn = new DialogueTurn(AgentName.user, AgentName.system );
                        // usr_turn.toJson() will put utterance in transcription: obj.put( "transcription", utterance );
                        usr_turn.setUtterance(MetaMessage.HANGUP.name() + " : 'End dialogue' button clicked." );
                        
                        logmsg = ">>> User Hangup event";
                        logger.logf( Level.INFO, logmsg);
                        System.out.println(logmsg);
                        
		} else if ( meta_value.equals(MetaMessage.DIALOGUE_TIMEOUT.name()) ) {
			String json_msg = getJSONMessage( "DM Server returned", "", MetaMessage.DIALOGUE_TIMEOUT );
			out.println( json_msg );

                        logmsg = ">>> DIALOGUE_TIMEOUT event.";
                        logger.logf( Level.INFO, logmsg);
                        System.out.println(logmsg);

                        return true;
			
		} else if ( meta_value.equals(MetaMessage.PORT_TIMEOUT.name()) ) {
			String json_msg = getJSONMessage( "DM Server returned", "", MetaMessage.PORT_TIMEOUT );
			out.println( json_msg );
                        logmsg = ">>> PORT_TIMEOUT event.";
                        logger.logf( Level.INFO, logmsg);
                        System.out.println(logmsg);
                        
			return true;
			
		} else if ( meta_value.equals(MetaMessage.RELOAD.name()) ) {
			if ( !dialogue_started ) {
                            System.out.println(">>> RELOAD event, but Dialouge Not Started yet.");
				return false;
                        }
			String json_msg = getJSONMessage( "DM Server returned", "", MetaMessage.RELOAD );
			out.println( json_msg );
                        
                        logmsg = ">>> RELOAD event.";
                        logger.logf( Level.INFO, logmsg);
                        System.out.println(logmsg);
                        
			return true;
			
		} else if ( meta_value.equals(MetaMessage.INIT.name()) ) {

			sysName = root.getString( "sysName" );
			sysPort = root.getString( "sysPort" );
			userID = root.getString( "userID" );
			taskID = root.getString( "taskID" );
			userIP = root.getString( "userIP" );
			dialogueID = root.getString( "dialogueID" );
                        
                        // XKL make sure they are not empty and don't have string "0"
                        //    for the PHP Token Manager
                        String prefix = "a_";
                        if(sysName.isEmpty() || sysName.startsWith("0"))
                            sysName = prefix + sysName;

                        if(sysPort.isEmpty() || sysPort.startsWith("0"))
                            sysPort = prefix + sysPort;

                        if(userID.isEmpty() || userID.startsWith("0"))
                            userID = prefix + userID;

                        if(taskID.isEmpty() || taskID.startsWith("0"))
                            taskID = prefix + taskID;
                        
                        if(userIP.isEmpty() || userIP.startsWith("0"))
                            userIP = prefix + userIP;

                        if(dialogueID.isEmpty() || dialogueID.startsWith("0"))
                            dialogueID = prefix + dialogueID;
                        
                        dial_json.put( "sysName", sysName );
			dial_json.put( "sysPort", sysPort );
			dial_json.put( "userID", userID );
			dial_json.put( "taskID", taskID );
			dial_json.put( "userIP", userIP );                        
			dial_json.put( "dialogueID", dialogueID );
                        
			System.out.println( "Preparing json response" );
			String json_msg = getJSONMessage( "Hello, how may I help you?", "", MetaMessage.SYSTEMTURN );
                        
			out.println( json_msg );

                        dialogue_started = true;
                        
                        logmsg = "INIT event. Sys response: " + json_msg;
                        logger.logf( Level.INFO, logmsg);
                        System.out.println(logmsg);
                        
			return false;
			
		} else {
                    logmsg = "Received and Ignored message:" + msg;
                    logger.logf( Level.INFO, logmsg);
                    System.out.println(logmsg);

                    return false; // message ignored
                }
                
		String json_msg = "";
		if ( sys_turn.dact_all.getCommFunction() == CommFunction.returnGoodbye )
			json_msg = getJSONMessage( response, token, MetaMessage.CLOSE );
		else
			json_msg = getJSONMessage( response, token, MetaMessage.SYSTEMTURN );
                
		out.println( json_msg );
		sys_turn.setUtterance( response );
                
                logger.logf( Level.INFO, sys_turn.toString() );
                // XKL output
                System.out.println(sys_turn.toString());

                // NLU semantics info for usr_turn is got in method getResponseAct(...)
                // usr_turn is also logged in that method.
                System.out.println(usr_turn.toString());
                
                // X.Liu added crntTurnTimeStr and crntTurnTimeMillis which are passed in USERTURN and HANGUP
                // so the demoxx.json will carry timestamp for each user turn.
                
                // Could to extend DialogueTurn but extend here to avoid too many changes
                JSONObject usrObj = usr_turn.toJSON();
                usrObj.put( "crntTurnTimeStr", crntTurnTimeStr);
                usrObj.put( "crntTurnTimeMillis", crntTurnTimeMillis);
                
                turn_json = new JSONObject();
                //turn_json.put( "usr_turn", usr_turn.toJSON() );
                turn_json.put( "usr_turn", usrObj );
                turn_json.put( "sys_turn", sys_turn.toJSON() );
                turns_arr.put( turn_json );

                if ( !token.isEmpty() ) {
                        dial_json.put( "token", token );
                       // return true;
                }
    	
                 if ( !token.isEmpty() ||  meta_value.equals(MetaMessage.HANGUP.name()) ) {
                     // X.LIU: this will inculde the case where 'End dialogue' button clicked
                     // but no token is assigned.
                     return true;  // true: current dialogue will end. Server will listen to another dialgoue.
                 }
                        
		turn_index++;
		return false;
	}
	
	/** Generates a message containing the system response in json format */
	private String getJSONMessage( String utt, String token, MetaMessage metaMsg ) { //throws JSONException {
//		String tokenMsg = "";
//		if ( !token.isEmpty() )
//			tokenMsg = " Your token for this task is " + token + ".";
//		String json_str = "{ \"utt\": \"" + utt + tokenMsg + "\", \"meta\": \"" + metaMsg.name() + "\"";
		String json_str = "{ \"utt\": \"" + utt + "\", \"meta\": \"" + metaMsg.name() + "\"";
		if ( token.isEmpty() )
			json_str += " }";
		else
			json_str += ", \"token\": \"" + token + "\" }";
//		JSONObject json_msg = new JSONObject( json_str );
//		return json_msg.toString( 3 );
		return json_str;
	}
	
	/** Constructs a list of ASR hypotheses from a string representation in json format */
	private ArrayList<ASRHypothesis> getASRHyps( JSONArray asr_hyp_arr ) throws JSONException {
		ArrayList<ASRHypothesis> asr_hyps = new ArrayList<ASRHypothesis>();
		JSONObject asr_hyp_obj;
		String logStr = "";
		for ( int i = 0; i < asr_hyp_arr.length(); i++ ) {
			asr_hyp_obj = asr_hyp_arr.getJSONObject( i );
			String utt = asr_hyp_obj.getString( "utt" );
//			double conf = Double.parseDouble( asr_hyp_obj.getString("conf") );
			double conf = asr_hyp_obj.getDouble( "conf" );
			if ( conf > TextDialSystem.ASR_CONF_THRESHOLD ) {
				asr_hyps.add( new ASRHypothesis(utt,conf) );
				logStr += String.format( "ASR> %s [%.5f]\n", utt, conf );
			}
		}
		logger.log( Level.INFO, logStr );
		return asr_hyps;
	}
	
	/** Returns a system response dialogue turn, given the user input ASR hypotheses. */
    private DialogueTurn getResponseAct( ArrayList<ASRHypothesis> asr_hyps ) throws JSONException {
    	
    	if ( asr_hyps.isEmpty() ) {
    		usr_turn = new DialogueTurn( AgentName.user, AgentName.system );
    		usr_turn.setProcLevel( ProcLevel.ATTENTION );
    	} else {
    		// count as non-trivial turn:
    		num_nontrivial_turns++;
    		// get user dialogue act(s)
    		logger.logf( Level.INFO, "NLU started (%s)\n", new Date().toString() );
    		timer.start();
    		usr_turn = dial_system.nlu.getSemantics( asr_hyps );
    		if ( DialActUtils.isNonTrivial(usr_turn) )
        		num_nontrivial_turns++;
//    		if ( !usr_turn.dact_nbest_all.isEmpty() ) {
//    			SemanticContent sc = usr_turn.dact_nbest_all.get(0).getSemContent();
//    			if ( !sc.isEmpty() )
//    				sc.getFirstItem().getFirstItem().value.equals(anObject)
//    		}
    		timer.stop();
    		logger.logf( Level.INFO, "NLU finished (%s)\n", new Date().toString() );
    		logger.logf( Level.INFO, "NLU processing time: %s\n", timer.getTimePassed() );
		logger.logf( Level.INFO, usr_turn.toString() );
    	}
    	
		// send user turn to dialogue manager
    	dial_system.dial_man.receive( usr_turn );
    	
		// get system response act from dialogue manager
		sys_turn = dial_system.dial_man.respond();
		String response = dial_system.nlg.getUtterance( sys_turn );
		sys_turn.setUtterance( response );
		
    	return sys_turn;
    }
    
}
