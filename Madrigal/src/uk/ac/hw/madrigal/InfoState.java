/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

import uk.ac.hw.madrigal.dial_act_agents.DialogueActCandidate;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.SemanticContent;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.domain.DBEntity;
import uk.ac.hw.madrigal.domain.Database;
import uk.ac.hw.madrigal.domain.GoalGenerator;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.domain.SlotValuePair.SpecialValue;
import uk.ac.hw.madrigal.domain.UserGoalSVP.GroundStatus;
import uk.ac.hw.madrigal.utils.BeliefState;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Information State, used by the MaDrIgAL dialogue manager.
 */
public class InfoState {
	
	public enum ReactivePressure { NONE, GREET, THANK, BYE }
	
	static Random rand_num_gen = Madrigal.rand_num_gen;

	static Ontology domain = ( Madrigal.domain == null ? TextDialSystem.domain : Madrigal.domain ); //TODO ugly hack: find better solution to ensure domain is initialised

	public static Database database = ( Madrigal.database == null ? TextDialSystem.database : Madrigal.database ); //TODO ugly hack: find better solution to ensure database is initialised
	
	// ---------------------------------------------------------------
	
	/** Representation of the user goal belief state (in noisy conditions). */
	ArrayList< BeliefState<UserGoalSVP> > belief_state;
	public int num_user_goal_items;
	
	ArrayList< UserGoalSVP > non_informable_slots;
		
	/** List of top hypotheses for each concept/slot, derived from the current belief state. */
	public ArrayList<UserGoalSVP> ugoal_top_hyps;
	
	/** User goal concept/slot with the smallest top probability. */
	public UserGoalSVP minConfUG;

	/** List of entropies of belief distributions for each concept/slot. */
	public ArrayList<Double> ug_item_entropies;
	
	/** Slots in the belief state for which the top hypothesis slot has not been mentioned before. */
	public ArrayList<SlotValuePair> slots_to_ask;
	
	/** Non-imformable slots requested by the user according to their grounding status. */
	public ArrayList<UserGoalSVP> requested_slots;
	
	/** Slot most likely to have been requested by the user */
	public UserGoalSVP max_prob_req_slot;
	
	/** Probability of the slot most likely to have been requested by the user. */
	public double requested_slots_max_prob;

	//ReactivePressure react_pressure;
	
	/** Records whether a processing problem occurred, and on which level */
	private ProcLevel proc_problem;
	
	/** Last dialogue act selected by the system. */
	DialogueAct last_system_act; //TODO replace with DialogueTurn?
	
	public int num_sys_act_repeats;
	
	public ArrayList<DialogueAct> last_uact_hyps;
	
	public ArrayList<DialogueActCandidate> dial_act_candidates;
	
	public ArrayList<DBEntity> matching_entities;
	
	public DBEntity current_entity;
	
	protected String primeSlot = domain.primarySlot();
	
	// ---------------------------------------------------------------

	/** Constructor */
	public InfoState() {
		init();
	}
	
	void init() {
		ArrayList<UserGoalSVP> user_goal_items = domain.getUserGoalState();
		num_user_goal_items = user_goal_items.size();
		belief_state = domain.getBeliefState( user_goal_items );
		ugoal_top_hyps = new ArrayList<UserGoalSVP>();
		ug_item_entropies = new ArrayList<Double>();
		slots_to_ask = new ArrayList<SlotValuePair>();
		requested_slots = new ArrayList<UserGoalSVP>();
		minConfUG = null;
		non_informable_slots = domain.getNonInformables();
		//react_pressure = ReactivePressure.NONE;
		proc_problem = ProcLevel.NONE;
		last_system_act = null;
		num_sys_act_repeats = 0;
		//last_user_act = null;
		last_uact_hyps = new ArrayList<DialogueAct>();
		dial_act_candidates = new ArrayList<DialogueActCandidate>();
		matching_entities = new ArrayList<DBEntity>();
		current_entity = null;
	}
	
	public void updateWithUserDialAct( DialogueAct dial_act ) {

		CommFunction cf = dial_act.getCommFunction();
		//Dimension dim = dial_act.getDimension();
		ArrayList<SemanticItem> sem_items = dial_act.getSemContent().getSemItems();
		CommFunction sys_cf = ( last_system_act != null ? last_system_act.getCommFunction() : null );
		
		if ( cf == CommFunction.inform || cf == CommFunction.confirm || cf == CommFunction.disconfirm ) {
			// user confirms slot-value pairs in previous system act (e.g., sys> do you want chinese food? usr> yes)
			if ( sys_cf != null && sys_cf == CommFunction.propQuestion ) {
				ArrayList<SemanticItem> sys_sem_items = last_system_act.getSemContent().getSemItems();
				for ( SemanticItem si: sys_sem_items )
					for ( SlotValuePair svp: si.svps )
						if ( cf == CommFunction.confirm )
							userInform( svp, dial_act.getConf() );
						else if ( cf == CommFunction.disconfirm )
							userNegate( svp, dial_act.getConf() );
			}
			// update user goal based on semantic content of user act
			for ( SemanticItem si: sem_items )
				for ( SlotValuePair svp: si.svps )
					userInform( svp, dial_act.getConf() );
		} else if ( cf == CommFunction.setQuestion ) { //TODO to be replaced through grounding model
			for ( SemanticItem si: sem_items )
				for ( SlotValuePair svp: si.svps )
					if ( svp.value.isEmpty() )
						//requested_slots.add( svp.slot );
						userRequest( svp.slot, dial_act.getConf() );
//		} else if ( cf == CommFunction.initialGoodbye ) {
//			react_pressure = ReactivePressure.BYE;
		}
		
		// update grounding status
		updateGroundStatus( dial_act );
						
		minConfUG = getMinConfUsrGoalHyp();
		ug_item_entropies = getUGoalEntropies( true );
		slots_to_ask = getSlotsToAsk();

		//last_user_act = new DialogueAct( dial_act );
		
		//matching_entities = database.getMatches( user_goal_items );
		matching_entities = database.getMatches( ugoal_top_hyps );
		
	}
	
    private void mergeDuplicates( ArrayList<UserGoalSVP> evidence ) {
    	ArrayList<UserGoalSVP> toRemove = new ArrayList<UserGoalSVP>();
    	UserGoalSVP obs1, obs2;
    	SlotValuePair svp1, svp2;
    	//for ( UserGoalSVP obs1: evidence ) {
    	for ( int i = 0; i < evidence.size(); i++ ) {
    		obs1 = evidence.get( i );
    		svp1 = obs1.getSlotValuePair();
    		//for ( UserGoalSVP obs2: evidence ) {
    		for ( int j = 0; j < i; j++ ) {
        		obs2 = evidence.get( j );
    			svp2 = obs2.getSlotValuePair();
    			if ( svp1.equals(svp2) ) {
    				obs2.setConf( obs2.getConf() + obs1.getConf() );
    				toRemove.add( obs1 );
    				break;
    			}
    		}
    	}
    	evidence.removeAll( toRemove );
    }

    /**
	 * Collects evidence from last turn (uact and sact) for the given slot.
	 */
	private ArrayList<UserGoalSVP> getEvidenceForSlot( ArrayList<DialogueAct> da_hyps, String slot ) {
		
		ArrayList<UserGoalSVP> evidence = new ArrayList<UserGoalSVP>();
		UserGoalSVP ug_svp;
		SemanticItem sem_item;
		CommFunction cf;
		SemanticContent content;
		
		// handle system confirmation cases
		UserGoalSVP sys_ug_svp = null;
		UserGoalSVP sys_ug_svp_impl_conf = null;
		if ( last_system_act != null && domain.isInformable(slot) ) {
			CommFunction sys_cf = last_system_act.getCommFunction();
			//last_system_act
			ArrayList<SemanticItem> sys_sem_items = last_system_act.getSemContent().getSemItems();
			//if ( (sys_cf == CommFunction.propQuestion || sys_cf == CommFunction.inform) && !sys_sem_items.isEmpty() ) {
			if ( !sys_sem_items.isEmpty() ) {
				SemanticItem first_item = sys_sem_items.get( 0 );
				if ( !first_item.svps.isEmpty() ) {
					SlotValuePair first_svp = first_item.svps.get( 0 );
					if ( first_svp.slot.equals(slot) ) {
						if ( sys_cf == CommFunction.propQuestion )
							sys_ug_svp = new UserGoalSVP( first_svp ); // record slot confirmed by system
						else if ( sys_cf == CommFunction.inform ) //TODO add constraint for inform(name=none,...) ?
							sys_ug_svp_impl_conf = new UserGoalSVP( first_svp ); // record slot confirmed by system
					}
				}
			}
		}
		
		// handle user act content
		for ( DialogueAct da_hyp: da_hyps ) {
			cf = da_hyp.getCommFunction();
			content = da_hyp.getSemContent();
			if ( cf == CommFunction.inform || cf == CommFunction.confirm || cf == CommFunction.disconfirm ) {
				boolean evidenceFound = false;
				if ( !content.isEmpty() ) {
					sem_item = content.getFirstItem();
					for ( SlotValuePair svp: sem_item.svps ) {
						if ( svp.slot.equals(slot) ) {
							evidenceFound = true;
							ug_svp = new UserGoalSVP( svp );
							ug_svp.setConf( da_hyp.getConf() );
							evidence.add( ug_svp );
							break; // only include first item with matching slot (check error model?) 
						}
					}
				}
				// handle implicit confirmation
				if ( sys_ug_svp_impl_conf != null && !evidenceFound ) {
					sys_ug_svp_impl_conf.setConf( da_hyp.getConf() );
					evidence.add( sys_ug_svp_impl_conf );
				}
				// handle explicit confirmation
				if ( sys_ug_svp != null && !evidenceFound ) {
					if ( cf == CommFunction.confirm ) {
						ug_svp = new UserGoalSVP( sys_ug_svp.getSlotValuePair() );
						ug_svp.setConf( da_hyp.getConf() );
						evidence.add( ug_svp );
					} else if ( cf == CommFunction.disconfirm ) {
						// distribute conf score across values other than the one confirmed by system
						ArrayList<String> vals = domain.getValuesForSlot( slot );
						for ( String v: vals ) {
							if ( !v.equals(sys_ug_svp.getSlotValuePair().value) ) {
								ug_svp = new UserGoalSVP( slot, v );
								ug_svp.setConf( da_hyp.getConf() / (vals.size() - 1) );
								evidence.add( ug_svp );
							}
						}
					}
				}
			} else if ( cf == CommFunction.setQuestion && !content.isEmpty() ) {
				sem_item = content.getFirstItem();
				for ( SlotValuePair svp: sem_item.svps ) {
					if ( svp.slot.equals(slot) && svp.value.isEmpty() ) {
						ug_svp = new UserGoalSVP( svp );
						ug_svp.setConf( da_hyp.getConf() );
						evidence.add( ug_svp );
					}
				}
//			} else if ( cf == CommFunction.propQuestion && !content.isEmpty() ) {
//				sem_item = content.getFirstItem();
//				for ( SlotValuePair svp: sem_item.svps ) {
//					if ( svp.slot.equals(slot) ) {
//						ug_svp = new UserGoalSVP( svp );
//						ug_svp.setConf( da_hyp.getConf() );
//						ug_svp.setGroundStatus( GroundStatus.USR_EXP_CONF );
//						evidence.add( ug_svp );
//					}
//				}
			}
		}
		
		mergeDuplicates( evidence );
		
		return evidence;
		
	}
	
	private double getObsConfidence( ArrayList<UserGoalSVP> obs, SlotValuePair svp ) {
		for ( UserGoalSVP ug_svp: obs )
			if ( ug_svp.getSlotValuePair().equals(svp) )
				return ug_svp.getConf();
		return -1;
	}
	
	@SuppressWarnings("unused")
	private boolean checkUsrExpConf( ArrayList<UserGoalSVP> obs, UserGoalSVP ug_svp ) {
		for ( UserGoalSVP obs_ug_svp: obs )
			if ( obs_ug_svp.getSlotValuePair().equals(ug_svp.getSlotValuePair()) )
				if ( obs_ug_svp.getGroundStatus() == GroundStatus.USR_EXP_CONF ) {
					ug_svp.setGroundStatus( GroundStatus.USR_EXP_CONF );
					return true;
				}
		return false;
	}
	
	public void updateWithUserDialAct( ArrayList<DialogueAct> da_hyps ) {
		
		// check for dontcare with no slot name in user act hypotheses
		// for example:
		//    Sys> setQuestion(near:)
		//    Usr> inform(:dontcare) --> inform(near:dontcare)
		SemanticItem sem_item, sys_sem_item;
		SlotValuePair sys_svp;
		CommFunction sys_cf;
		for ( DialogueAct uact_hyp: da_hyps ) {
			if ( uact_hyp.getCommFunction() == CommFunction.inform && !uact_hyp.getSemContent().isEmpty() ) {
				// considering user acts with dontcare value
				sem_item = uact_hyp.getSemContent().getFirstItem();
				//svp = sem_item.getFirstItem();
				for ( SlotValuePair svp: sem_item.svps ) {
					if ( svp.slot.isEmpty() && svp.value.equals(SlotValuePair.getString(SpecialValue.DONTCARE)) ) {
						if ( last_system_act != null ) { // last_system_act may be null in the first turn in the live system
							sys_cf = last_system_act.getCommFunction(); 
							if ( sys_cf == CommFunction.propQuestion || sys_cf == CommFunction.setQuestion ) {
								// assuming propQ and setQ always has content
								sys_sem_item = last_system_act.getSemContent().getFirstItem();
								sys_svp = sys_sem_item.getFirstItem();
								svp.slot = sys_svp.slot; // fill in the slot name for the dontcare value in user act
							}
						}
					}
				}
					
			// last_system_act
			}
		}
		
		String ug_bel_nm;
		for ( BeliefState<UserGoalSVP> ug_bel: belief_state ) {
			// BELIEF STATE FOR PARTICULAR SLOT
			ug_bel_nm = ug_bel.getName();
			ArrayList<UserGoalSVP> evidence = getEvidenceForSlot( da_hyps, ug_bel_nm );
			double oth_conf = 1d; // probability mass for values not in evidence
			int num_ev_vals = 0;
			int num_vals = domain.getValuesForSlot( ug_bel_nm ).size();
			for ( UserGoalSVP ug_svp: evidence ) {
				oth_conf -= ug_svp.getConf();
				num_ev_vals++;
			}
			double oth_conf_val = 0d;
			if ( num_vals > num_ev_vals )
				oth_conf_val = oth_conf / (num_vals - num_ev_vals); // remaining probability mass distributed uniformly
			double obs_conf, bel_conf, bel_conf_new;
			for ( UserGoalSVP ug_svp_hyp: ug_bel.getHyps() ) {
				// SLOT-VALUE PAIR HYPOTHESIS WITH CONF SCORE b(s,v)
				if ( evidence.isEmpty() && ug_svp_hyp.getGroundStatus() == GroundStatus.SYS_IMP_CONF )
					ug_svp_hyp.setGroundStatus( GroundStatus.GROUNDED );
				// handle user expl confs
				//boolean usrExpConf = checkUsrExpConf( evidence, ug_svp_hyp );
				bel_conf = ug_svp_hyp.getConf();
				obs_conf = getObsConfidence( evidence, ug_svp_hyp.getSlotValuePair() );
				if ( obs_conf == -1 ) {
					bel_conf_new = oth_conf_val * bel_conf; 
					//NOTE no evidence for this slot-value pair, hence assuming but user might not have said anything about this slot at all!
				} else {
					bel_conf_new = ( bel_conf == 0 ? obs_conf : obs_conf * bel_conf ); 
					if ( ug_svp_hyp.getGroundStatus() != GroundStatus.GROUNDED ) // && !usrExpConf )
						ug_svp_hyp.setGroundStatus( GroundStatus.USR_INF );
				}
				ug_svp_hyp.setConf( bel_conf_new );
			}
			Utils.normalise( ug_bel.getHyps() );
			Collections.sort( ug_bel.getHyps() );
		}
		
		String nonInfSlot;
		double reqCurr, nonReqCurr, reqEv, nonReqEv, reqNew, nonReqNew;
		for ( UserGoalSVP nonInf: non_informable_slots ) { // now also includes informables (because they can be requested as well); perhaps rename to 'requestable_slots'?
			nonInfSlot = nonInf.getSlotValuePair().slot;
			ArrayList<UserGoalSVP> evidence = getEvidenceForSlot( da_hyps, nonInfSlot );
			if ( !evidence.isEmpty() ) {
				UserGoalSVP nonInfSvp = evidence.get( 0 );
				reqCurr = nonInf.getConf();
				nonReqCurr = 1 - reqCurr;
				reqEv = nonInfSvp.getConf();
				nonReqEv = 1 - reqEv;
				reqNew = ( reqCurr == 0 ? reqEv : reqEv * reqCurr );
				nonReqNew = ( nonReqCurr == 0 ? nonReqEv : nonReqEv * nonReqCurr );
				nonInf.setConf( reqNew / (reqNew + nonReqNew) ); // set normalised updated conf score
				// update grounding status
//				if ( nonInf.getGroundStatus() != GroundStatus.SYS_INF && nonInf.getGroundStatus() != GroundStatus.GROUNDED )
				if ( nonInfSvp.getSlotValuePair().value.isEmpty() && nonInf.getGroundStatus() != GroundStatus.GROUNDED ) //TODO remove this constraint as well?
					nonInf.setGroundStatus( GroundStatus.USR_REQ );
			} else if ( nonInf.getGroundStatus() != GroundStatus.GROUNDED && nonInf.getGroundStatus() != GroundStatus.SYS_INF ) // reset belief for this requested slot (forget any evidence in previous turns)
				nonInf.setConf( 0f ); //TODO correct under these conditions?
		}
		
//		for ( DialogueAct da_hyp: da_hyps )
//			updateGroundStatus( da_hyp );

		// update top hypotheses of user goal items 
		ugoal_top_hyps = getUGoalTopHyps();

		minConfUG = getMinConfUsrGoalHyp();
		ug_item_entropies = getUGoalEntropies( true );
		slots_to_ask = getSlotsToAsk();
		getReqSlots();

		//if ( !da_hyps.isEmpty() )
		//	last_user_act = new DialogueAct( da_hyps.get(0) );
		last_uact_hyps = new ArrayList<DialogueAct>( da_hyps );
		
		//matching_entities = database.getMatches( user_goal_items );
		matching_entities = database.getMatches( ugoal_top_hyps );
		
	}
	
	public void updateWithUserTurn( DialogueTurn user_turn ) {

		ProcLevel plvl = user_turn.getProcLevel();
		
		if ( plvl == ProcLevel.ATTENTION ) {
			setProcProblem( ProcLevel.PERCEPTION );
			last_uact_hyps.clear();
		}
		
		else if ( plvl == ProcLevel.PERCEPTION ) {
			setProcProblem( ProcLevel.INTERPRETATION );
			last_uact_hyps.clear();
		}
		
		else if ( plvl == ProcLevel.INTERPRETATION )
			updateWithUserDialAct( user_turn.dact_nbest_all );
		
		else
			setProcProblem( ProcLevel.ATTENTION );
			
	}

	public void updateWithUserDialAct_old( ArrayList<DialogueAct> da_hyps ) {

		CommFunction cf;
		//Dimension dim;
		ArrayList<SemanticItem> sem_items;
		for ( DialogueAct da_hyp: da_hyps ) {

			cf = da_hyp.getCommFunction();
			//dim = da_hyp.getDimension();
			sem_items = da_hyp.getSemContent().getSemItems();
			CommFunction sys_cf = ( last_system_act != null ? last_system_act.getCommFunction() : null );
		
			if ( cf == CommFunction.inform || cf == CommFunction.confirm || cf == CommFunction.disconfirm ) {
				// user confirms slot-value pairs in previous system act (e.g., sys> do you want chinese food? usr> yes)
				if ( sys_cf != null && sys_cf == CommFunction.propQuestion ) {
					ArrayList<SemanticItem> sys_sem_items = last_system_act.getSemContent().getSemItems();
					for ( SemanticItem si: sys_sem_items )
						for ( SlotValuePair svp: si.svps )
							if ( cf == CommFunction.confirm )
								userInform( svp, da_hyp.getConf() );
							else if ( cf == CommFunction.disconfirm )
								userNegate( svp, da_hyp.getConf() );
				}
				// update user goal based on semantic content of user act
				for ( SemanticItem si: sem_items )
					for ( SlotValuePair svp: si.svps )
						userInform( svp, da_hyp.getConf() );
			} else if ( cf == CommFunction.setQuestion ) { //TODO to be replaced through grounding model
				for ( SemanticItem si: sem_items )
					for ( SlotValuePair svp: si.svps )
						if ( svp.value.isEmpty() )
							//requested_slots.add( svp.slot );
							userRequest( svp.slot, da_hyp.getConf() );
//			} else if ( cf == CommFunction.initialGoodbye ) {
//				react_pressure = ReactivePressure.BYE;
			}
			
			// update grounding status
			updateGroundStatus( da_hyp );
			
		}
						
		minConfUG = getMinConfUsrGoalHyp();
		ug_item_entropies = getUGoalEntropies( true );
		slots_to_ask = getSlotsToAsk();

		//if ( !da_hyps.isEmpty() )
		//	last_user_act = new DialogueAct( da_hyps.get(0) );
		
		//matching_entities = database.getMatches( user_goal_items );
		matching_entities = database.getMatches( ugoal_top_hyps );
		
	}
	
	private void updateGroundStatus( DialogueAct dial_act ) {
		DialogueAct prev_act = null;
		if ( dial_act.isUserAct() )
			prev_act = last_system_act;
		else if ( !last_uact_hyps.isEmpty() )
			prev_act = last_uact_hyps.get( 0 );
		
		// update grounding status in user goal
//		for( UserGoalSVP ugi: user_goal_items )
//			ugi.updateGrndStatus( prev_act, dial_act );
		
		// update grounding status in belief state
		for ( BeliefState<UserGoalSVP> ubs: belief_state )
			for ( UserGoalSVP ugbs: ubs.getHyps() )
				ugbs.updateGrndStatus( prev_act, dial_act );
		for ( UserGoalSVP nonInf: non_informable_slots ) {
			nonInf.updateGrndStatus( prev_act, dial_act );
			if ( nonInf.getGroundStatus() == GroundStatus.SYS_INF )
				nonInf.setConf( 1f );
		}
	}
	
	public void updateWithSysTurn( DialogueTurn sys_turn ) {
		//TODO
	}
	
	public void updateWithSysDialAct( DialogueAct dial_act ) {
		CommFunction sys_cf = dial_act.getCommFunction();
		SemanticContent sys_content = dial_act.getSemContent();
		if ( sys_cf == CommFunction.inform ) {
			//TODO handle no dbase match...
//			SlotValuePair first_svp = sys_content.getFirstItem().getFirstItem();
//			if ( first_svp.slot.equals("name") && first_svp.value.equals("none") )
//				...
			ArrayList<SemanticItem> sys_sem_items = sys_content.getSemItems();
			for ( SemanticItem si: sys_sem_items )
				for ( SlotValuePair svp: si.svps ) {
					//if ( domain.isInformable(svp.slot) ) {
						systemInform( svp );
//						if ( current_entity != null ) //NOTE DB entity already has all information (these two lines messed up the training)
//							updateEntity( svp );
					//}
					//updateReqSlots( svp );
				}
		}
//		if ( react_pressure == ReactivePressure.BYE )
//			react_pressure = ReactivePressure.NONE; // reactive pressures always disappear after the next turn
		
		// update grounding status
		updateGroundStatus( dial_act );

		if ( last_system_act != null && dial_act.equals(last_system_act) )
			num_sys_act_repeats++;
		else
			num_sys_act_repeats = 0;
		last_system_act = new DialogueAct( dial_act );
	}
	
	public void setProcProblem( ProcLevel pl ) {
		proc_problem = pl;
	}
	
	public ProcLevel getProcProblem() {
		return proc_problem;
	}

	@SuppressWarnings("unused")
	private void updateEntity( SlotValuePair svp ) {
		for ( SlotValuePair ent_svp: current_entity.getAttributes() ) {
			if ( ent_svp.equals(svp) )
				return;
		}
		current_entity.addAttribute( svp );
	}
	
	/** 
	 * Returns a database entry that matches the current user goal constraints.
	 */
	public DBEntity getRecommendation() {
		if ( matching_entities.isEmpty() )
			return null;
		
		if ( current_entity != null ) {
			String currEntName = current_entity.getAttribute( primeSlot ); 
			for ( DBEntity ent: matching_entities )
				if ( ent.getAttribute(primeSlot).equals(currEntName) )
					return current_entity; // current entity still matches: stick with this
		}

		//System.out.printf( "%d matching entities\n", matching_entities.size() );
		int rand_index = rand_num_gen.nextInt( matching_entities.size() );
		return matching_entities.get( rand_index );
	}
	
	private void systemInform( SlotValuePair svp ) { //TODO modify to work with belief state!!
		// without error model:
//		UserGoalSVP ug = getUserGoalSVP( svp );
//		ug.systemInform( svp.value ); //TODO currently this overwrites the user goal value (though it should be the same)!

//		if ( !domain.isInformable(svp.slot) )
//			ug.setConf( 1f ); // system informed about slot: confidence set to 1
		// above already done when updating grounding status elsewhere
		// with error model: not necessary, only need to update ground states, which is done elsewhere
		
		for ( UserGoalSVP nonInf: non_informable_slots ) {
			if ( nonInf.getSlotValuePair().slot.equals(svp.slot) ) {
				nonInf.systemInform( svp.value );
				nonInf.setConf( 1f );
			}
		}
	}
	
	/** Collect requested slots from information state, using the given confidence threshold. */
	public ArrayList<String> getReqSlots( double conf_thr ) {
		ArrayList<String> reqSlots = new ArrayList<String>();
		for ( UserGoalSVP ug_svp: ugoal_top_hyps )
			if ( ug_svp.getGroundStatus() == GroundStatus.USR_REQ )
				reqSlots.add( ug_svp.getSlotValuePair().slot );
		for ( UserGoalSVP nonInf: non_informable_slots )
			if ( nonInf.getGroundStatus() == GroundStatus.USR_REQ && nonInf.getConf() > conf_thr )
				reqSlots.add( nonInf.getSlotValuePair().slot );
		return reqSlots;
	}
	
	private void getReqSlots() {
		requested_slots = new ArrayList<UserGoalSVP>();
		requested_slots_max_prob = 0d;
		max_prob_req_slot = null;
		for ( UserGoalSVP nonInf: non_informable_slots ) { //TODO change this to also allow informables to be requested
			if ( nonInf.getGroundStatus() == GroundStatus.USR_REQ ) {
				requested_slots.add( nonInf );
				if ( nonInf.getConf() > requested_slots_max_prob ) {
					requested_slots_max_prob = nonInf.getConf();
					max_prob_req_slot = nonInf;
				}
			}
		}
	}
			
	//private void userInform( DialogueAct prev_sact, SlotValuePair svp, double conf ) {
	private void userInform( SlotValuePair svp, double conf ) {
		// without error model:
//		UserGoalSVP ug = getUserGoalSVP( svp );
//		if ( ug != null )
//			ug.userInform( svp.value );
		// with error model:
		BeliefState<UserGoalSVP> ugb = getUserGoalBelief( svp );
		if ( ugb != null ) {
			ArrayList<UserGoalSVP> hyps = ugb.getHyps();
			for ( UserGoalSVP ug_svp: hyps ) {
				boolean sysReq = ( ug_svp.getGroundStatus() == GroundStatus.SYS_REQ );
				boolean empty = ug_svp.getSlotValuePair().value.isEmpty();
				if ( (sysReq && empty) || svp.value.equals( ug_svp.getSlotValuePair().value ) )
					ug_svp.setConf( 1 - (1 - ug_svp.getConf()) * (1 - conf) ); // (Wang & Lemon, 2013); Rule 1, 3.1
				else
					ug_svp.setConf( ug_svp.getConf() * (1 - conf) ); // (Wang & Lemon, 2013); not explicitly handled
			}
			Collections.sort( hyps );
		}
	}
	
	private void userRequest( String slot, double conf ) {
		// without error model:
		//TODO to be made redundant
		// with error model:
		if ( !domain.isInformable(slot) ) {
//			UserGoalSVP nonInf = getNonInfBelief( slot );
//			nonInf.setConf( conf );
			// belief tracking on requestable slots:
			for ( UserGoalSVP nonInf: non_informable_slots ) {
				if ( nonInf.getSlotValuePair().slot.equals(slot) )
					nonInf.setConf( 1 - (1 - nonInf.getConf()) * (1 - conf) );
			}
		} // else requested slot taken care of in grounding update
	}
	
	@SuppressWarnings("unused")
	private UserGoalSVP getNonInfBelief( String slot ) {
		for ( UserGoalSVP nonInf: non_informable_slots )
			if ( slot.equals(nonInf.getSlotValuePair().slot) )
				return nonInf;
		return null;
	}
	
	private void userNegate( SlotValuePair svp, double conf ) {
		// without error model:
//		UserGoalSVP ug = getUserGoalSVP( svp );
//		if ( ug != null )
//			ug.userNegate( svp.value );
		// with error model:
		BeliefState<UserGoalSVP> ugb = getUserGoalBelief( svp );
		if ( ugb != null ) {
			ArrayList<UserGoalSVP> hyps = ugb.getHyps(); 
			for ( UserGoalSVP ug_svp: hyps ) {
				if ( svp.value.equals( ug_svp.getSlotValuePair().value ) ) {
					ug_svp.setConf( ug_svp.getConf() * (1 - conf) ); // (Wang & Lemon, 2013); Rule 3.2
				}
			}
			Collections.sort( hyps );
		}		
	}

//	private UserGoalSVP getUserGoalSVP( SlotValuePair svp ) {
//		for ( UserGoalSVP ug_svp: user_goal_items ) {
//			if ( ug_svp.slotMatches(svp) )
//				return ug_svp;
//		}
//		for ( UserGoalSVP nonInf: non_informable_slots ) {
//			if ( nonInf.slotMatches(svp) )
//				return nonInf;
//		}
//		return null;
//	}
	
	private BeliefState<UserGoalSVP> getUserGoalBelief( SlotValuePair svp ) {
		for ( BeliefState<UserGoalSVP> ug_bel: belief_state )
			if ( svp.slot.equals(ug_bel.getName()) )
				return ug_bel;
		return null;
	}
	
	private ArrayList<UserGoalSVP> getUGoalTopHyps() {
		ArrayList<UserGoalSVP> ug_items = new ArrayList<UserGoalSVP>();
		UserGoalSVP ug_svp;
		GroundStatus gs;
		for ( BeliefState<UserGoalSVP> ug_bel: belief_state ) {
			ug_svp = ug_bel.getTopHypothesis(); //TODO what if multiple hyps have the same, maximum, confidence?
			gs = ug_svp.getGroundStatus();
			if ( gs == GroundStatus.USR_INF || gs == GroundStatus.USR_REQ || gs == GroundStatus.SYS_EXP_CONF || gs == GroundStatus.SYS_IMP_CONF || gs == GroundStatus.GROUNDED )
				ug_items.add( ug_svp );
		}
		return ug_items;
	}
		
	public ArrayList<UserGoalSVP> getNonGroundedUGoalTopHyps() {
		ArrayList<UserGoalSVP> result = new ArrayList<UserGoalSVP>();
		for ( UserGoalSVP ug_svp: ugoal_top_hyps ) {
			GroundStatus gs = ug_svp.getGroundStatus();
			if ( gs != GroundStatus.GROUNDED ) // ugoal item ground status can only be USR_INF, SYS_EXP_CONF, SYS_IMP_CONF, or GROUNDED
				result.add( ug_svp );
		}
		return result;
	}
	
	public boolean ugoal_top_hyps_grounded() {
		if ( ugoal_top_hyps.isEmpty() )
			return false;
		for ( UserGoalSVP ug_svp: ugoal_top_hyps ) {
			GroundStatus gs = ug_svp.getGroundStatus();
			SlotValuePair svp = ug_svp.getSlotValuePair();
			if ( gs != GroundStatus.GROUNDED && !svp.value.equals(SlotValuePair.getString(SpecialValue.DONTCARE)) ) // ugoal item ground status can only be USR_INF, SYS_EXP_CONF, SYS_IMP_CONF, or GROUNDED
//			if ( gs != GroundStatus.GROUNDED ) // ugoal item ground status can only be USR_INF, SYS_EXP_CONF, SYS_IMP_CONF, or GROUNDED
				return false;
		}
		return true;
	}
	
	private ArrayList<SlotValuePair> getSlotsToAsk() {
		ArrayList<SlotValuePair> slotsToAsk = new ArrayList<SlotValuePair>();
		UserGoalSVP ug_svp;
		for ( BeliefState<UserGoalSVP> ug_bel: belief_state ) {
			ug_svp = ug_bel.getTopHypothesis();
			GroundStatus gs = ug_svp.getGroundStatus(); 
			if ( gs == GroundStatus.INIT || gs == GroundStatus.SYS_REQ )
				slotsToAsk.add( ug_svp.getSlotValuePair() );
		}
		return slotsToAsk;
	}
	
	private ArrayList<Double> getUGoalEntropies( boolean normalise ) {
		ArrayList<Double> entropies = new ArrayList<Double>();
		for ( BeliefState<UserGoalSVP> ug_bel: belief_state )
			entropies.add( ug_bel.getEntropy(normalise) );
		return entropies;
	}
	
	public String getBeliefStateString() {
		String resStr = "";
		for ( BeliefState<UserGoalSVP> ug_bel: belief_state )
			resStr += ug_bel.toString();
		return resStr;
	}
	
	public String getRandomUGoalSlot() {
		ArrayList<String> slot_names = domain.getSlotNames( true );
		if ( slot_names.isEmpty() )
			return "";
		return slot_names.get( rand_num_gen.nextInt(slot_names.size()) );
	}
	
	private String getNonInformablesString() {
		String result = "";
		boolean first = true;
		for ( UserGoalSVP nonInfSVP: non_informable_slots )
			if ( first ) {
				result += String.format( "%s [%.3f]", nonInfSVP.toString(), nonInfSVP.getConf() );
				first = false;
			} else
				result += String.format( ", %s [%.3f]", nonInfSVP.toString(), nonInfSVP.getConf() );
		return result;
	}
	
	private String getLastUsrActHypsString() {
		String result = "";
		boolean first = true;
		for ( DialogueAct da_hyp: last_uact_hyps ) {
			if ( first ) {
				result += da_hyp.toShortString( true );
				first = false;
			} else
				result += "\n\t" + da_hyp.toShortString( true );
		}
		return result;
	}
	
	@Override
	public String toString() {
		String result = "==========\n";
		if ( !non_informable_slots.isEmpty() )
			result += "Non-informable slots: " + getNonInformablesString() + "\n"; // StringUtils.join( non_informable_slots, ", " ) + "\n";
//		if ( react_pressure != ReactivePressure.NONE )
//			result += "Reactive pressure: " + react_pressure.name() + "\n";
		if ( !ugoal_top_hyps.isEmpty() )
			result += "User goal top hypotheses: " + StringUtils.join( ugoal_top_hyps, ", " ) + "\n";
		if ( !requested_slots.isEmpty() ) {
			result += "Requested slots: ";
			for ( UserGoalSVP reqSlot: requested_slots )
				result += String.format( " %s [%.2f]", reqSlot.toString(true), reqSlot.getConf() );
			result += "\nMax prob req slot: " + (max_prob_req_slot==null?"NONE":max_prob_req_slot.toString(true)) + " " + requested_slots_max_prob + "\n";
		}
		result += "DB Entities:\n\t" + GoalGenerator.getEntitiesString( matching_entities ) + "\n";
		result += "Current entity: " + ( current_entity == null ? "NONE" : current_entity.toString() ) + "\n";
		result += "Belief state:\n" + getBeliefStateString();
		if ( last_system_act != null )
			result += "Last sys act: " + last_system_act.toShortString( false ) + "\n";
		//if ( last_user_act != null )
		//	result += "Last usr act: " + last_user_act.toShortString() + "\n";
		result += "Last usr act hyps:\n\t" + getLastUsrActHypsString() + "\n";
		result += "==========\n";
		return result;
	}

	public UserGoalSVP getMinConfUsrGoalHyp() {
		double conf;
		double minConf = 1d;
		UserGoalSVP ug_hyp_min = null;
		for ( UserGoalSVP ug_hyp: ugoal_top_hyps ) {
			conf = ug_hyp.getConf();
			if ( conf <= minConf ) {
				minConf = conf;
				ug_hyp_min = ug_hyp;
			}
		}
		return ug_hyp_min;
	}
	
}
