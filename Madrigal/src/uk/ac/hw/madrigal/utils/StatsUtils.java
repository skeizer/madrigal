/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package with utility classes.                                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package uk.ac.hw.madrigal.utils;

import java.util.List;

public class StatsUtils {
	
    /** returns the mean of the given list of data points. */
    public static <T extends DataPoint> double getMean( List<T> arr ) {
        if ( arr.isEmpty() )
        	return 0d;
        double sum = 0d;
        for ( T dp: arr )
            sum += dp.getValue();
        return sum / arr.size();
    }
    
    /** returns the variance of the given array of n real numbers with given mean. */
    public static <T extends DataPoint> double getVariance( List<T> arr, double mean ) {
        if ( arr.size() <= 1 )
            return 0d;
        double sum = 0d;
        for ( T dp: arr )
            sum += Math.pow( dp.getValue() - mean, 2 );
        return sum / arr.size();
    }
    
    /** returns the standard error for the given standard deviation and sample size n. */
    public static double getStdErr( double stdDev, int n ) {
        if ( n <= 0 )
            return 0d;
        double rootOfSampleSize = Math.sqrt( (double) n );
        double stdErr = stdDev / rootOfSampleSize;
        return stdErr;
    }

    /** returns the size of a 95% confidence interval for the given array of real numbers. */
    public static <T extends DataPoint> double getConfInterval( List<T> arr ) {
        int n = arr.size();
        double mean = getMean( arr );
        double var = getVariance( arr, mean );
        double stdErr = getStdErr( Math.sqrt(var), n );
        return stdErr * 1.96;
    }
    
    /** returns the size of a 95% confidence interval for the given proportion and sample size. */
    public static double getConfInterval( double prob, int n ) {
    	double f1 = prob * (1-prob);
    	double f2 = Math.sqrt( f1 / n );
        return 1.96 * f2;
    }
    
}
