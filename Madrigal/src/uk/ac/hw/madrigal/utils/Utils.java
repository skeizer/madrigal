/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created September 2016                                                          *
 *     ---------------------------------------------------------------------------     *
 *     Package with utility classes.                                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package uk.ac.hw.madrigal.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {

    public static double epsilon = 0.001;
    
    /**
     * Returns the index resulting from a random sample according 
     * to the given probability distribution.
     */
    public static int sampleFromProbs( double[] prob, int size, Random rand ) {
        
        double rf = rand.nextDouble();
        double pmass = 0d;
        int index = 0;
        while ( index < size-1 ) {
            pmass += prob[index];
            if ( pmass > 1 + epsilon ) {
                System.out.printf( "Utils::sampleFromProbs> ERROR: prob mass %.3f (>1)\n", pmass );
                return -1;
            }
            if ( rf < pmass )
                return index;
            index++;
        }
        return index;
        
    }
    
    public static boolean sampleFromBinaryProb( double prob, Random rand ) {
        double rf = rand.nextDouble();
        if ( rf < prob )
            return true;
        return false;
    }
    
    /** 'n choose k' = n! / [(n-k)! * k! ] */
    public static int choose( int n, int k ) {
    	int numerator = 1;
    	for ( int i = n; i > k; i-- )
    		numerator *= i;
    	int denominator = 1;
    	for ( int j = 1; j <= (n-k); j++ )
    		denominator *= j;
    	int rest = numerator % denominator;
    	if ( rest != 0 )
    		System.out.println( "ERROR: combinatorial result not an integer!" );
    	return numerator / denominator;
    }
    
    /**
     * returns an integer, sampled from a Poisson distribution with given mean.
     */
    public static int nextPoissonInt( int min, int max, double mean, Random rand ) {
    	double f = rand.nextDouble();
    	double p1 = Math.exp( (double) -mean );
    	double p2 = 0;
    	double p3 = 1;
    	double prob = 0;
        int i = 0;
        while ( f > prob && i < max ) {
            if ( i > 0 )
                p3 *= mean/i;
            p2 += p3;
            prob = p1 * p2;
            i++;
        }
        if ( i >= max )
        	return max;
        return i + min;
    }
    
    public static double[] getOnesVector( int num_bits ) {
    	double[] mask = new double[ num_bits ];
    	for ( int i = 0; i < num_bits; i++ )
    		mask[ i ] = 1d;
    	return mask;
    }
    
    public static int getRandInd( double[] mask, int num_bits, Random rand ) {
    	int num_ones = 0;
    	for ( int i = 0; i < num_bits; i++ )
    		if ( mask[i] == 1 )
    			num_ones++;
    	int index = rand.nextInt( num_ones );
    	int ind_cnt = 0;
    	for ( int i = 0; i < num_bits; i++ ) {
    		if ( mask[i] == 1 ) {
    			if ( ind_cnt == index )
    				return i;
    			ind_cnt++;
    		}
    	}
    	return -1; //NOTE this should not occur
    }
    
//    public static String readFile( String file ) throws IOException {
//		BufferedReader reader = new BufferedReader( new FileReader (file) );
//		return read( reader );
//	}
//    
//    private static String read( BufferedReader reader ) throws IOException  {
//	    String line = null;
//	    StringBuilder stringBuilder = new StringBuilder();
//	    String ls = System.getProperty( "line.separator" );
//
//	    while( ( line = reader.readLine() ) != null ) {
//	        stringBuilder.append( line );
//	        stringBuilder.append( ls );
//	    }
//	    reader.close();
//
//	    return stringBuilder.toString();
//    }
	
	public static <T> void sampleWithoutReplacement( List<T> list, List<T> samples, int num_samples, Random rnd ) {
		ArrayList<T> listCopy = new ArrayList<T>( list );
		for ( int i = 0; i < num_samples; i++ )
			samples.add( listCopy.remove( rnd.nextInt(listCopy.size()) ) );
	}
	
	public static <T extends Hypothesis> void normalise( List<T> hyps ) {
		normalise( hyps, 0 );
	}

	public static <T extends Hypothesis> void normalise( List<T> hyps, double rest ) {
		double sum = rest;
		for ( Hypothesis hyp: hyps )
			sum += hyp.getConf();
		if ( sum == 0 )
			return;
		for ( Hypothesis hyp: hyps )
			hyp.divConf( sum );
	}
	
	public static <T extends Hypothesis> double pseudo_normalise( List<T> hyps ) {
		// to be used for n-best lists with confidence scores that are probabilistic but not normalised
		double sum = 0;
		for ( Hypothesis hyp: hyps )
			sum += hyp.getConf();
		if ( sum == 0 )
			return 0;
		double residue = ( hyps.size() - sum ) / hyps.size(); 
		sum += residue;
		for ( Hypothesis hyp: hyps )
			hyp.divConf( sum );
		return residue;
	}
	
	private static <T extends Hypothesis> double getResidueConf( List<T> hyps ) {
		if ( hyps.isEmpty() )
			return 0d;
		double sum = 0d;
		for ( T hyp: hyps )
			sum += hyp.conf;
		return 1 - sum; // should be a positive number
	}
	
	public static <T extends Hypothesis> double merge( List<T> hyps, List<T> hyps1 ) {
		return merge( hyps, hyps1, false );
	}
	
	/** Merges new hypotheses hyps1 into hypotheses hyps;
	 * , taking the maximum confidence score when merging equal hypotheses. */
	public static <T extends Hypothesis> double merge( List<T> hyps, List<T> hyps1, boolean maxConf ) {
		double res = getResidueConf( hyps );
		double res1 = getResidueConf( hyps1 );
		if ( hyps.isEmpty() ) {
			hyps.addAll( hyps1 );
			return res1;
		}
		if ( hyps1.isEmpty() )
			return res;
		
		for ( T hyp1: hyps1 ) {
			boolean found = false;
			for ( T hyp: hyps ) {
				if ( hyp.equals(hyp1) ) {
					found = true;
					double newConf = hyp1.getConf();
					if ( maxConf ) {
						if ( newConf > hyp.getConf() )
							hyp.setConf( newConf );
					} else
						hyp.setConf( hyp.getConf() + hyp1.getConf() );
				}
			}
			if ( !found )
				hyps.add( hyp1 );
		}
		return res + res1;
	}
	
	public static <T extends Hypothesis> void merge( List<T> hyps ) {
		T hyp, hyp1;
		List<T> toremove;
		for ( int i = 0; i < hyps.size() - 1; i++ ) {
			toremove = new ArrayList<T>();
			hyp = hyps.get( i );
			for ( int j = i+1; j < hyps.size(); j++ ) {
				hyp1 = hyps.get( j );
				if ( hyp.equals( hyp1 ) ) {
					hyp.setConf( hyp.getConf() + hyp1.getConf() );
					toremove.add( hyp1 );
				}
			}
			hyps.removeAll( toremove );
		}
	}
	
	public static double log2( double f ) {
		double log_e = Math.log( f );
		double log2 = Math.log( 2 );
		return log_e / log2;
	}
	
	public static <T extends Hypothesis> double getEntropy( List<T> hyps, boolean normalised ) {
		// only one hypothesis with absolute certainty: entropy zero
		if ( hyps.size() == 1 && hyps.get(0).getConf() == 1d )
			return 0d;
		
		double result = 0d;
		double sumProb = 0d;
		int numVals = hyps.size();
		for ( T hyp: hyps ) {
			sumProb += hyp.conf;
			result += ( hyp.conf * log2( hyp.conf ) );
			if ( Double.isNaN(result) )
				System.out.println( "ERROR: entropy NaN" );
		}
		double restProb = 1 - sumProb;
		if ( restProb > 0 ) {
			result += restProb * log2( restProb );
			numVals++;
		}
		if ( !normalised )
			return -result;
		double normFact = log2( numVals );
		if ( Double.isNaN(normFact) )
			System.out.println( "ERROR: entropy normalisation factor NaN" );
		return -result / normFact;
	}
	
	public static <T extends Hypothesis> void eltMultScalar( List<T> hyps, double f ) {
		double f1;
		for ( T hyp: hyps ) {
			f1 = hyp.getConf() * f;
			hyp.setConf( f1 );
		}
	}
	
	/** Multiply each confidence score in hyps1 with the corresponding confidence score in hyps2 (element-wise multiplication) */ 
	public static <T1 extends Hypothesis, T2 extends Hypothesis> void eltMult( List<T1> hyps1, List<T2> hyps2 ) {
		T1 hyp1;
		T2 hyp2;
		for ( int i = 0; i < hyps1.size(); i++ ) {
			hyp1 = hyps1.get( i );
			hyp2 = hyps2.get( i );
			hyp1.setConf( hyp1.getConf() * hyp2.getConf() );
		}
	}
	
}
