/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package with utility classes.                                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.utils;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class representing a probability distribution over a list of hypotheses.
 */
public class BeliefState<T extends Hypothesis> {
	
	/** List of hypotheses over which a belief state is maintained */ 
	private ArrayList<T> hyps;
	
	/** Probability mass assigned to values other than the ones specified in hyps. */
	private double other_conf;
	
	/** Name of the slot/variable/node/concept for which this is the belief state. */
	private String concept_name; // e.g., info_state, or a particular slot name
	
	/** Number of possible values of this slot/variable over which this belief state keeps a distribution. */
	private int num_values;
	
	/** Constructor: creates empty list of hypotheses with all probability mass assigned to 'other' */
	public BeliefState( String nm, int nv ) {
		hyps = new ArrayList<T>();
		other_conf = 1d;
		concept_name = nm;
		num_values = nv;
	}
	
//	/** Constructor: creates belief state for the specified list of hypotheses. */
//	public BeliefState( List<T> hyps ) throws ProbabilityException {
//		for ( T hyp: hyps )
//			addHyp( hyp );
//	}
	
	//NOTE currently not used
	public void addHyp( T hyp ) throws ProbabilityException {
		if ( other_conf < hyp.conf )
			throw new ProbabilityException( String.format("Adding hyp with conf %.3f: only %.3f prob mass left\n", hyp.conf, other_conf) );
		if ( hyp.conf < 1d )
		other_conf -= hyp.conf;
		hyps.add( hyp );
		Collections.sort( hyps );
	}
	
	/* Normalises the probability distribution underlying this belief state.
	public void normalise() {
		Utils.normalise( hyps );
	}
	*/
	
	/** Returns the entropy for the probability distribution underlying this belief state. */
	public double getEntropy( boolean normalise ) {
		double entropy = 0d;
		int numVals = hyps.size();
		for ( T hyp: hyps )
			entropy += Utils.log2( hyp.conf ) * hyp.conf;
		if ( other_conf > 0 ) {
			entropy += Utils.log2( other_conf ) * other_conf;
			numVals++;
		}
		if ( !normalise )
			return -entropy;
		double normFact = Utils.log2( numVals );
		return -entropy / normFact;
	}
	
	public T getTopHypothesis() {
		if ( hyps.isEmpty() )
			return null;
		return hyps.get( 0 );
	}
	
	public ArrayList<T> getHyps() {
		return hyps;
	}
	
	public double getOtherConf() {
		return other_conf;
	}
	
	public void setOtherConf( double conf ) {
		other_conf = conf;
	}
	
	public String getName() {
		return concept_name;
	}
	
	public int getNumValues() {
		return num_values;
	}
	
	public String toString() {
		String result = concept_name + ":\n";
		for ( T hyp: hyps )
			result += "\t" + hyp.toString() + String.format(" [%.3f]\n",hyp.conf);
		result += String.format( "\t%s [%.3f]\n", "other", other_conf );
		return result;
	}
	
}
