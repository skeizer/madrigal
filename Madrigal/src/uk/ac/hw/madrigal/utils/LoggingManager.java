/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created June 2017                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;

public class LoggingManager {

	static ArrayList<MyLogger> loggers = new ArrayList<MyLogger>();

	/** Creates an instance of MyLogger, associated with the given logging directory and log file name prefix */
	public static MyLogger setupLogging( String dir, String prefix, String level_str ) {
		MyLogger logger = null;
		try {
			FileHandler fh = getFileHandler( dir, prefix );
			logger = new MyLogger( "uk.ac.hw.madrigal" + "." + prefix );
			logger.addHandler( fh );
			logger.setUseParentHandlers( false );
			if ( level_str.equals(MyLevel.DIALSTATS.getName()) )
				logger.setLevel( MyLevel.DIALSTATS );
			else {
				Level newLevel = Level.parse( level_str );
				logger.setLevel( newLevel );
			}
			loggers.add( logger );
		} catch ( SecurityException e ) {
			e.printStackTrace();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
		return logger;
	}
	
	public static MyLogger getLogger( String name ) {
		String log_path;
		String[] name_path;
		String log_name;
		for ( MyLogger log: loggers ) {
			log_path = log.getName();
			//System.out.println( "logger name: " + log_path );
			name_path = log_path.split( "\\." );
			//System.out.println( "split into " + name_path.length + " parts." );
			//System.out.println( "logger name path: " + StringUtils.join( name_path, ";" ) );
			log_name = name_path[ name_path.length - 1 ];
			if ( name.equals(log_name) )
				return log;
		}
		return null;
	}
	
	private static FileHandler getFileHandler( String dir, String prefix ) throws SecurityException, IOException {
		String logfilename = new SimpleDateFormat( "'" + prefix + "_'yyyy-MM-dd-'T'HH:mm:ss:SSS'.log'" ).format( new Date() );
		new File( dir ).mkdirs();
		FileHandler fh = new FileHandler( dir + File.separator + logfilename );
		//fh.setFormatter( new SimpleFormatter() );
		fh.setFormatter( new MyFormatter() );
		return fh;
	}
	
    @SuppressWarnings("unused")
	private static void flush_loggers() {
    	for ( MyLogger log: loggers )
    		flush_logger( log );
    }
    
    private static void flush_logger( MyLogger log ) {
    	for ( Handler h: log.getHandlers() )
    		h.flush();
    }
    
    public static void close_loggers() {
    	for ( MyLogger log: loggers )
    		close_logger( log );
    }
    
    private static void close_logger( MyLogger log ) {
    	for ( Handler h: log.getHandlers() )
    		h.close();
    }
    
}
