/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created September 2017                                                          *
 *     ---------------------------------------------------------------------------     *
 *     Package with utility classes.                                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.utils;

import java.util.concurrent.TimeUnit;

public class MyTimer {
	
    long starts;
    long ends;

    public MyTimer() {
    	starts = -1;
    	ends = -1;
    }

    public void start() {
        starts = System.currentTimeMillis();
    }
    
    public void stop() {
    	ends = System.currentTimeMillis();
    }
    
    public void reset() {
    	starts = -1;
    	ends = -1;
    }
    
    public boolean hasStarted() {
    	return starts != -1;
    }

    public boolean hasStopped() {
    	return ends != -1;
    }

    public long time() {
        return ends - starts;
    }

    public long time( TimeUnit unit ) {
    	//NOTE from milliseconds to seconds truncates (1600 msec -> 1 sec)
        return unit.convert( time(), TimeUnit.MILLISECONDS );
    }
    
    public String getTimePassed() {
    	long timeMins = time( TimeUnit.MINUTES );
    	long timeSecs = time( TimeUnit.SECONDS );
    	long timeMSecs = time( TimeUnit.MILLISECONDS );
    	return String.format( "%d mins %d secs (%d msecs)", timeMins, timeSecs, timeMSecs );
    }

}
