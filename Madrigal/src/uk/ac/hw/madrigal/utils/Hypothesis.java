/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package with utility classes.                                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.utils;

public abstract class Hypothesis implements Comparable<Hypothesis> {
	
	protected double conf;
	
	public Hypothesis() {
		conf = 1d;
	}
	
	public Hypothesis( double c ) {
		conf = c;
	}
	
	public Hypothesis( Hypothesis hyp ) {
		conf = hyp.conf;
	}
	
	public double getConf() {
		return conf;
	}
	
	public void setConf( double f ) {
		conf = f;
	}
	
	public void incrConf( double f ) {
		conf += f;
	}
	
	public void divConf( double f ) {
		conf /= f;
	}

	@Override
	public int compareTo( Hypothesis hyp ) {
		if ( conf < hyp.conf )
			return 1;
		else if ( conf == hyp.conf )
			return 0;
		return -1;
	}

	public abstract boolean equals( Hypothesis hyp );

	//public abstract String toShortString();

}
