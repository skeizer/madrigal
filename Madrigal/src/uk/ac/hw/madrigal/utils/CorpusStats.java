/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package with utility classes.                                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.utils;

import java.util.ArrayList;

public class CorpusStats {
	
	private ArrayList<EpisodeStats> corpus;
	
	private EpisodeStats current_episode;
	
	private double success_rate;
	
	private double avg_score;
	private double avg_score_stdev;
	@SuppressWarnings("unused")
	private double avg_score_stderr;
	
	private double avg_episode_length;
	private double avg_episode_length_stdev;
	@SuppressWarnings("unused")
	private double avg_episode_length_stderr;
	
	public CorpusStats() {
		corpus = new ArrayList<EpisodeStats>();
		success_rate = 0d;
		avg_score = 0d;
	}
	
	public void startNewEpisode() {
		current_episode = new EpisodeStats();
	}
	
	public double processLatestEpisode( boolean success ) {
		current_episode.task_success = success;
		corpus.add( current_episode );
		return current_episode.computeCumulativeScore();
	}
	
	public void addScore( double score ) {
		current_episode.addScore( score );
	}
	
//	public void computeStats() {
//		int n = corpus.size();
//		avg_score = StatsUtils.getMean( corpus );
//		avg_score_stdev = Math.sqrt( StatsUtils.getVariance(corpus,avg_score) );
//		avg_score_stderr = StatsUtils.getStdErr( avg_episode_length_stdev, n );
//	}
	
	public void computeAvgScore() {
		double sum = 0d;
		double eplth_sum = 0d;
		int numSuccs = 0;
		for ( EpisodeStats es : corpus ) {
			es.computeCumulativeScore();
			sum += es.cumulative_score;
			eplth_sum += es.getSize();
			if ( es.task_success )
				numSuccs++;
		}
		success_rate = (double) numSuccs / corpus.size();
		avg_score = sum / corpus.size();
		avg_episode_length = eplth_sum / corpus.size();
	}
	
	public void computeStats() {
		double sum = 0d;
		double eplth_sum = 0d;
		int numSuccs = 0;
		for ( EpisodeStats es : corpus ) {
			es.computeCumulativeScore();
			sum += es.cumulative_score;
			eplth_sum += es.getSize();
			if ( es.task_success )
				numSuccs++;
		}
		success_rate = (double) numSuccs / corpus.size();
		avg_score = sum / corpus.size();
		avg_episode_length = eplth_sum / corpus.size();
		computeStdErrs();
	}
	
	public void computeStdErrs() {
		double rootOfSampleSize = Math.sqrt( (double) corpus.size() );
		double sum_sqrs = 0;
		double sum_sqrs_el = 0;
		for ( EpisodeStats es : corpus ) {
			sum_sqrs += Math.pow( es.cumulative_score - avg_score, 2 );
			sum_sqrs_el += Math.pow( es.getSize() - avg_episode_length, 2 );
		}
		avg_score_stdev = Math.sqrt( sum_sqrs / corpus.size() );
        avg_score_stderr = avg_score_stdev / rootOfSampleSize;
        avg_episode_length_stdev = Math.sqrt( sum_sqrs_el / corpus.size() );
        avg_episode_length_stderr = avg_episode_length_stdev / rootOfSampleSize;		
	}

	public int getSize() {
		return corpus.size();
	}
	
	public void removeFirst() {
		corpus.remove( 0 );
	}
	
	public String getSummary() {
		String result = "";
		result += String.format( "\tNumber of episodes: %d\n", corpus.size() );
		result += String.format( "\tAverage length: %.2f (+-%.2f)\n", avg_episode_length, avg_episode_length_stdev );
		result += String.format( "\tAverage score: %.2f (+-%.2f)\n", avg_score, avg_score_stdev );
		//result += String.format( "\tSuccess rate: %.2f (%.0f%%)\n", success_rate, 100*success_rate );
		result += String.format( "\tSuccess rate: %.0f%%\n", 100 * success_rate );
		return result;
	}
	
	public String getCompactSummary() {
		return String.format( "%d\t%.2f\t%.2f\t%.2f\n", corpus.size(), avg_episode_length, avg_score, success_rate );
	}

}
