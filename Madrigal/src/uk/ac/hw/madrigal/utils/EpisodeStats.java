/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package with utility classes.                                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.utils;

import java.util.ArrayList;

public class EpisodeStats implements DataPoint {

	ArrayList<Double> scores;
	double cumulative_score;
	boolean task_success;
	EpisodeStats() {
		scores = new ArrayList<Double>();
		cumulative_score = 0;
		task_success = false;
	}
	void addScore( double score ) { 
		scores.add( new Double(score) );
	}
	double computeCumulativeScore() {
		cumulative_score = 0;
		for ( Double sc : scores ) {
			cumulative_score += sc.doubleValue();
		}
		return cumulative_score;
		//System.out.printf( "CorpusStats> cumulative score episode: %.0f\n", cumulative_score );
	}
	boolean isEmpty() { return scores.isEmpty(); }
	int getSize() { return scores.size(); }
	public double getValue() { return cumulative_score; }
	String getSummary() { return String.format( "Cumulative score in this episode: %.2f (in %d turns)", cumulative_score, scores.size() ); }
	
}
