/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created June 2017                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.logging.Level;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.hw.madrigal.dialogue_acts.ASRHypothesis;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.utils.MyLogger;
import uk.ac.hw.madrigal.utils.MyTimer;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * This class implements websocket server functionality for the Madrigal dialogue system.
 */
public class DialogSystemServer extends WebSocketServer {
	
	protected static MyLogger logger;
	private static MyTimer timer = new MyTimer();

	private static int MAX_NUM_ATTEMPTS = 3;
	private static int WAIT_INTERVAL = 5000;

	private DialogueTurn usr_turn, sys_turn;
	private int dial_index = 0;
	private int turn_index = 0;

	TextDialSystem dial_system;
	JSONObject dial_json, turn_json;
	JSONArray turns_arr;

	/** Constructor */
	public DialogSystemServer( TextDialSystem ds, int port ) throws UnknownHostException {
		super( new InetSocketAddress(port) );
		dial_system = ds;
	}
	
	/** Constructor */
	public DialogSystemServer( InetSocketAddress address ) {
		super( address );
	}

	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
		System.out.println( conn + " has left the room!" );
		//sendToClient( conn, conn + " has left the room!" ); // causes exception
		try {
			closeJsonDial();
//			dial_json.put( "dialogue", turns_arr );
//			saveDialogue( TextDialSystem.LOGGING_DIR, "demosys" );
		} catch ( JSONException je ) {
			je.printStackTrace();
		}
		dial_index++;
	}
	
	void closeJsonDial() throws JSONException {
		if ( turns_arr.length() > 0 ) {
			dial_json.put( "dialogue", turns_arr );
			saveDialogue( TextDialSystem.LOGGING_DIR, "demosys" );
		}
	}
	
	private void saveDialogue( String path, String prefix ) throws JSONException {
		String logfilename = new SimpleDateFormat( "'" + prefix + "_'yyyy-MM-dd-'T'HH:mm:ss:SSS'.log'" ).format( new Date() );
		File json_file = new File( path + File.separator + logfilename );
		try {
			BufferedWriter writer = new BufferedWriter( new PrintWriter(json_file) );
			writer.write( dial_json.toString(5) );
			writer.close();
		} catch ( IOException  ioe ) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void onError( WebSocket conn, Exception ex ) {
		ex.printStackTrace();
		if ( conn != null ) {
			// some errors like port binding failed may not be assignable to a specific websocket
			System.out.println( "websocket: " + conn.getResourceDescriptor() );
		}
	}

	@Override
	public void onMessage( WebSocket conn, String message ) {
		
		//TODO first message in dialogue could contain meta-info such as user id
		// if ...
		//dial_json.append( "user_id", "unknown" );
		
		// user turn
		System.out.println( "User>>> " + message );

		logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_index );

		// message encoded as json document containing n-best list of user utterances with confidence scores
		try {
			turn_json = new JSONObject();
			turn_json.append( "turn_idx", turn_index );

			JSONObject root = new JSONObject( message );
			JSONArray asr_hyp_arr = root.getJSONArray( "asrhyps" );
			JSONObject asr_hyp_obj;
			ArrayList<ASRHypothesis> asr_hyps = new ArrayList<ASRHypothesis>();
			String logStr = "";
			for ( int i = 0; i < asr_hyp_arr.length(); i++ ) {
				asr_hyp_obj = asr_hyp_arr.getJSONObject( i );
				String utt = asr_hyp_obj.getString( "utt" );
				double conf = Double.parseDouble( asr_hyp_obj.getString("conf") );
				if ( conf > TextDialSystem.ASR_CONF_THRESHOLD ) {
					asr_hyps.add( new ASRHypothesis(utt,conf) );
					logStr += String.format( "ASR> %s [%.5f]\n", utt, conf );
				}
			}
			Collections.sort( asr_hyps );
			logger.log( Level.INFO, logStr );
			
			ArrayList<ASRHypothesis> asr_hyps_nbest;
			if ( asr_hyps.size() < TextDialSystem.ASR_NBEST_SIZE )
				asr_hyps_nbest = asr_hyps;
			else
				asr_hyps_nbest = new ArrayList<ASRHypothesis>( asr_hyps.subList( 0, TextDialSystem.ASR_NBEST_SIZE ) );
			//ArrayList<ASRHypothesis> asr_hyps_nbest = new ArrayList<ASRHypothesis>( asr_hyps.subList( 0, TextDialSystem.ASR_NBEST_SIZE ) );
			
			// rescale confidence scores to add up to < 1
			Utils.pseudo_normalise( asr_hyps_nbest );
			
			String sys_utt = getResponse( asr_hyps_nbest );
			System.out.println( "System: " + sys_utt );
			sendToClient( conn, sys_utt );
	
			turn_index++;
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onOpen( WebSocket conn, ClientHandshake handshake ) {
		
		int numAttempts = 0;
		while ( occupied() && numAttempts++ < MAX_NUM_ATTEMPTS ) { // system occupied: as user to wait
			sendToClient( conn, "Please hold until the system becomes available (" + numAttempts + "/" + MAX_NUM_ATTEMPTS + ")" );
			try {
				Thread.sleep( WAIT_INTERVAL );
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if ( numAttempts == MAX_NUM_ATTEMPTS ) { // waited too long: tell user to try later
			
			sendToClient( conn, "System occupied: try again later" );
			conn.close();
			
		} else { // occupied() == false
		
			//sendToClient( conn, "new connection: " + handshake.getResourceDescriptor() );
			sendToClient( conn, "Hello, how may I help you?" );
			System.out.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " entered the room!" );
	
			logger.logf( Level.INFO, "\n===== Dialogue %d =====\n", dial_index );
			dial_system.dial_man.reset();
			turn_index = 0;
			
			try {
				dial_json = new JSONObject();
				dial_json.put( "user_id", "unknown" ); //TODO get user id through connection
				turns_arr = new JSONArray();
			} catch ( JSONException je ) {
				je.printStackTrace();
			}
		}
		
	}
	
	private boolean occupied() {
		Collection<WebSocket> conns = connections();
		if ( conns.size() >= 2 )
			return true;
		return false;
	}

	private void sendToClient( WebSocket client, String text ) {
		client.send( text );
	}
	
    private String getResponse( ArrayList<ASRHypothesis> asr_hyps ) throws JSONException {
    	
		String response = "";
    	if ( asr_hyps.isEmpty() ) {
    		usr_turn = new DialogueTurn( AgentName.user, AgentName.system );
    		usr_turn.setProcLevel( ProcLevel.ATTENTION );
    	} else {
    		// get user dialogue act(s)
    		timer.start();
    		usr_turn = dial_system.nlu.getSemantics( asr_hyps );
    		timer.stop();
    		logger.logf( Level.INFO, "NLU processing time: %s\n", timer.getTimePassed() );
			logger.logf( Level.INFO, usr_turn.toString() );
    		// send user act(s) to dialogue manager
        	dial_system.dial_man.receive( usr_turn );
    		// get system response act from dialogue manager
    		sys_turn = dial_system.dial_man.respond();
    		//TODO check if this is goodbye, closing the dialogue
    		// check if token can be issued
    		//response = dial_system.getToken( "Madrigal" ); //TODO sort out setting system/dialogue IDs
    		
    		// get system utterance
    		response = dial_system.nlg.getUtterance( sys_turn );
    		sys_turn.setUtterance( response );
        	logger.logf( Level.INFO, sys_turn.toString() );
    	}
    	
    	turn_json.put( "usr_turn", usr_turn.toJSON() );
    	turn_json.put( "sys_turn", sys_turn.toJSON() );
    	turns_arr.put( turn_json );
    	
    	return response;
    }

	@Override
	public void onStart() {
		dial_index = 0;
		StringBuffer logStr = new StringBuffer( "-----------------------------------\n" );
		logStr.append( "= = = = = DIALOGUE CORPUS = = = = =\n" );
		logger.logf( Level.INFO, "%s", logStr );
		System.out.println( "Websocket server started." );
	}

}
