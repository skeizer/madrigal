/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using task domains.                                *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import resources.Resources;

public class Database {
	
	public ArrayList<DBEntity> entities;
	
	private Ontology domain;
	
	public Database( String json_fname, Ontology dom ) {
		entities = new ArrayList<DBEntity>();
		domain = dom;
		try {
			//String inputJsonString = Utils.readFile( json_fname );
			String inputJsonString = Resources.readResourceFile( json_fname );
			JSONObject root = new JSONObject( inputJsonString );
			//System.out.println( root.toString() + "\n" );
			JSONObject db_obj = root.getJSONObject( "database" );
			JSONArray ent_arr = db_obj.getJSONArray( "entity" );
			//System.out.printf( "%d entities found\n", ent_arr.length() );
			JSONObject ent_obj;
			DBEntity entity;
			ArrayList<SlotValuePair> attrs;
			for ( int i = 0; i < ent_arr.length(); i++ ) {
				ent_obj = ent_arr.getJSONObject( i );
//				attrs = new ArrayList<SlotValuePair>();
//				attrs.add( getSVP("name",ent_obj) );
//				attrs.add( getSVP("address",ent_obj) );
//				attrs.add( getSVP("phone",ent_obj) );
//				attrs.add( getSVP("pricerange",ent_obj) );
//				attrs.add( getSVP("foodtype",ent_obj) );
				attrs = getAttributes( ent_obj );
				entity = new DBEntity( attrs );
				//System.out.printf( "New entity: %s\n", entity.toString() );
				entities.add( entity );
			}
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		} catch ( JSONException e ) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<DBEntity> getMatches( ArrayList<UserGoalSVP> ug_svps ) {
		ArrayList<DBEntity> matches = new ArrayList<DBEntity>( entities ); // copy of all entities
		ArrayList<DBEntity> toRemove = new ArrayList<DBEntity>();          // non-matching entities to be removed
		for ( UserGoalSVP ug_svp : ug_svps ) {
			if ( domain.isInformable(ug_svp.slot_value_pair.slot) ) {
				for ( DBEntity entity : matches )
					if ( !entity.matches(ug_svp) )
						toRemove.add( entity );
				matches.removeAll( toRemove );
			}
		}
		return matches;
	}
	
	@SuppressWarnings("unused")
	private SlotValuePair getSVP( String slot, JSONObject obj ) throws JSONException {
		return new SlotValuePair( slot, obj.getString(slot) );
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList< SlotValuePair > getAttributes( JSONObject obj ) throws JSONException {
		ArrayList< SlotValuePair > attrs = new ArrayList< SlotValuePair >();
		Iterator< String > slots = obj.keys();
		String slot, value;
		while ( slots.hasNext() ) {
			slot = slots.next();
			if ( domain.isMultiVal(slot) ) {
				JSONArray slot_arr = obj.getJSONArray( slot );
				for ( int i = 0; i < slot_arr.length(); i++ )
					attrs.add( new SlotValuePair(slot, slot_arr.getString(i)) );
			} else {
				value = obj.getString( slot );
				attrs.add( new SlotValuePair(slot,value) );
			}
		}
		return attrs;
	}

}
