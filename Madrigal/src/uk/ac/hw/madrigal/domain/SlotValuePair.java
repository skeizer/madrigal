/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using task domains.                                *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.domain;

public class SlotValuePair {
	
	public enum SpecialSlot { PRIMARY_DESCR };
	
	public enum SpecialValue { NONE, DONTCARE };
	
	private static String[] SPECIAL_SLOTS = { "name" };
	
	private static String[] SPECIAL_VALUES = { "none", "dontcare" };

	public String slot;
	
	public String value;
	
	public SlotValuePair( String s ) {
		slot = s;
		value = "";
	}
	
	public SlotValuePair( String s, String v ) {
		slot = s;
		value = v;
	}

	public SlotValuePair( SlotValuePair svp ) {
		slot = svp.slot;
		value = svp.value;
	}
	
	public boolean equals( SlotValuePair svp ) {
		return ( slot.equals(svp.slot) && value.equals(svp.value) );
	}
	
	public boolean conflictsWith( SlotValuePair svp ) {
		if ( slot.equals(svp.slot) && !value.equals(svp.value) )
			return true;
		return false;
	}
	
	public String toString() {
		return slot + ( value.isEmpty() ? "" : ":" + value );
	}
	
	public static String getString( SpecialSlot ss ) {
		return SPECIAL_SLOTS[ ss.ordinal() ];
	}

	public static String getString( SpecialValue sv ) {
		return SPECIAL_VALUES[ sv.ordinal() ];
	}

	//TODO replace this with ontology based info
	public boolean isConstraint() {
		if ( slot.equals(getPrimDescrStr()) || value.isEmpty() || value == SpecialValue.NONE.name() || value == SpecialValue.DONTCARE.name() )
			return false;
		return true;
	}
	
	public boolean isPrimDescr() {
		return slot.equals( getPrimDescrStr() );
	}
	
	public static String getPrimDescrStr() {
		return SPECIAL_SLOTS[ SpecialSlot.PRIMARY_DESCR.ordinal() ];
	}

	public static int getPrimDescrInd() {
		return SpecialSlot.PRIMARY_DESCR.ordinal();
	}

}
