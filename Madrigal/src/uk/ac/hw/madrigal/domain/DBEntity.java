/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using task domains.                                *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.domain;

import java.util.ArrayList;

public class DBEntity {
	
	/** Attributes describing this database entity */
	private ArrayList<SlotValuePair> attributes;
	
	/** Default constructor (primarily for representing the current topic by user simulator) */
	public DBEntity() {
		attributes = new ArrayList<SlotValuePair>();
	}

	/** Constructs object of this class based on its attributes in an associated database */
	public DBEntity( ArrayList<SlotValuePair> attrs ) {
		attributes = new ArrayList<SlotValuePair>();
		for ( SlotValuePair svp : attrs )
			attributes.add( new SlotValuePair(svp) );
	}
	
	/** Returns the attributes describing this database entity */
	public ArrayList<SlotValuePair> getAttributes() {
		return attributes;
	}
	
	/** Returns the attribute value of the given slot */
	public String getAttribute( String slot ) {
		for ( SlotValuePair svp : attributes )
			if ( svp.slot.equals(slot) )
				return svp.value; //NOTE returns the value of the first svp for which the slot matches
		return "";
	}
	
	public ArrayList<String> getSlotValues( String slot ) {
		ArrayList<String> values = new ArrayList<String>();
		for ( SlotValuePair svp : attributes )
			if ( svp.slot.equals(slot) )
				values.add( svp.value );
		return values;
	}
	
	public boolean equals( SlotValuePair svp ) {
		for ( SlotValuePair attr : attributes )
			if ( attr.equals(svp) )
				return true;
		return false;
	}
	
	public boolean matches( SlotValuePair svp ) {
		if ( svp.value.equals( SlotValuePair.getString(SlotValuePair.SpecialValue.DONTCARE) ) )
			return true;
		for ( SlotValuePair attr : attributes )
			if ( attr.equals(svp) )
				return true;
		return false;
	}
	
	public boolean matches( UserGoalSVP ug_svp ) {
		return matches( ug_svp.slot_value_pair );
//		if ( ug_svp.slot_value_pair.value.equals( SlotValuePair.getString(SlotValuePair.SpecialValue.DONTCARE)) )
//			return true;
//		for ( SlotValuePair attr : attributes )
//			if ( attr.equals(ug_svp.slot_value_pair) )
//				return true;
//		return false;
	}
	
	public String toString() {
		String result = "";
		for ( SlotValuePair svp: attributes )
			result += svp.toString() + "; ";
		return result;
	}

	public void addAttribute( SlotValuePair svp ) {
		attributes.add( svp );
	}

}
