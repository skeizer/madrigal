/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using task domains.                                *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import resources.Resources;
import uk.ac.hw.madrigal.utils.BeliefState;
import uk.ac.hw.madrigal.utils.Utils;

public class Ontology {
	
    /** 
     * Represents the goal item definition as part of the task-domain;
     * a task consists of a list of goal items, and each goal item is given 
     * by a list of slot-value pairs.
     */
    private static class GoalItemDef {
        
    	//String name;
    	
        ArrayList<LexDef> lex_defs;
        
        public GoalItemDef( String nm ) {
        	//name = nm;
            lex_defs = new ArrayList<LexDef>();
        }
        
        public void addLexDef( LexDef ld ) {
            lex_defs.add( ld );
        }
        
        public String toString() {
        	String res = "";
        	for ( LexDef ld: lex_defs ) {
        		res += ld.toString() + "\n";
        	}
        	return res;
        }
        
    }

    /** Represents a slot and its possible values as part of the task-domain */
    private static class LexDef {
        
        String slotname;
        
        boolean informable;
        
        boolean requestable;
        
        boolean primary;
        
        boolean multival;
        
        ArrayList<String> values;

        public LexDef( String s, boolean inf, boolean req, boolean prim, boolean mval ) {
            slotname = s;
            informable = inf;
            requestable = req;
            primary = prim;
            multival = mval;
            values = new ArrayList<String>();
        }
        
        public void addValue( String value ) {
            values.add( value );
        }
        
        public String toString() {
        	return slotname + ": " + StringUtils.join( values, ", " );
        }

    }
    
    /** List of goal items describing the ontology/task/user goal */
    private ArrayList<GoalItemDef> goal_item_defs;
    
    private String primeSlot;
    
    private Lexicon lexicon;
    
    public Ontology( String fname, String lex_fname ) {
        loadFromFile( fname );
        lexicon = new Lexicon( lex_fname );
        primeSlot = getPrimarySlot();
    }
    
    public ArrayList<String> getValuesForSlot( String slot ) {
        
        ArrayList<String> values = new ArrayList<String>();
        if ( goal_item_defs.isEmpty() )
            return values;
        
        GoalItemDef first_goal_item_def = goal_item_defs.get( 0 );
        for ( LexDef ld : first_goal_item_def.lex_defs )
            if ( ld.slotname.equals(slot) ) {
            	values.addAll( ld.values );
                return values;
            }
        
        return values;
    }
    
    public String getDomainValue( String slot, String value ) {
    	String ont_val = getCorrectValue( slot, value );
    	if ( !ont_val.isEmpty() )
    		return ont_val;
    	String lex_val = value.toLowerCase();
//    	return lexicon.getValue( slot, value );
    	return lexicon.getValue( slot, lex_val );
    }

    public String getDomainSlot( String slot ) {
    	String ont_slot = getCorrectSlot( slot );
    	if ( !ont_slot.isEmpty() )
    		return ont_slot;
    	return lexicon.getValue( slot );
    }
    
    public String getCorrectValue( String slot, String value ) {
    	ArrayList<String> values = getValuesForSlot( slot );
    	for ( String val: values ) {
    		if ( val.equalsIgnoreCase(value) )
    			return val; // ensure captioning conforms to ontology
    	}
    	return ""; // value not found in ontology
    }
    
    /** Returns a valid slot name for the given natural language expression;
     * For example: "phone number" -> "phone"
     */
    public String getCorrectSlot( String entity ) {
    	for ( String slot: getSlotNames(false) )
    		if ( entity.equalsIgnoreCase(slot) )
    			return slot;
    	return "";
    }
    
    public ArrayList<String> getReqOnlySlotNames() {
    	
        ArrayList<String> slots = new ArrayList<String>();
        if ( goal_item_defs.isEmpty() )
            return slots;
        
        GoalItemDef first_goal_item_def = goal_item_defs.get( 0 );
        for ( LexDef ld : first_goal_item_def.lex_defs )
        	if ( ld.requestable && !ld.informable )
        		slots.add( ld.slotname );
    	
        return slots;
    }
    
    public ArrayList<String> getSlotNames( boolean noReqSlots ) {
    	
        ArrayList<String> slots = new ArrayList<String>();
        if ( goal_item_defs.isEmpty() )
            return slots;
        
        GoalItemDef first_goal_item_def = goal_item_defs.get( 0 );
        for ( LexDef ld : first_goal_item_def.lex_defs )
        	if ( !noReqSlots || ld.informable )
        		slots.add( ld.slotname );
    	
        return slots;
    }
    
    /** Returns a randomly initialised user goal based on this ontology */
    public UserGoal getRandomGoal( int num_reqs_mn, int num_infs_mn, Random rnd ) {
        
        UserGoal goal = new UserGoal();
        UserGoalItem newItem = new UserGoalItem();

        int item_def_rand_index = rnd.nextInt( goal_item_defs.size() ); // currently, always index 0: ontology has only one goal item (restaurants)
        GoalItemDef item_def = goal_item_defs.get( item_def_rand_index );
        ArrayList<LexDef> inf_slots = getInformableSlots( item_def );
        ArrayList<LexDef> req_slots = getRequestableOnlySlots( item_def );
        if ( req_slots.isEmpty() )
        	System.out.println( "ERROR: no requestable slots found" ); // should never happen
        if ( inf_slots.isEmpty() )
        	System.out.println( "ERROR: no informable slots found" ); // should never happen
        
        LexDef first_req_ldef = req_slots.remove( 0 );
    	newItem.addUGoalSVP( new UserGoalSVP(first_req_ldef.slotname),first_req_ldef.requestable );
        
        // random selection of requests
        //int min_num_reqs = 1;
        //int numReqs = ( min_num_reqs < req_slots.size() ? min_num_reqs + rnd.nextInt(req_slots.size()-min_num_reqs+1) : min_num_reqs );
        // sample numReqs from Poisson instead of uniform distr
        int numReqs = Utils.nextPoissonInt( 1, req_slots.size(), num_reqs_mn, rnd );
        ArrayList<LexDef> rand_reqs = new ArrayList<LexDef>();
        Utils.sampleWithoutReplacement( req_slots, rand_reqs, numReqs, rnd );
        //TODO add a few informable slots as requests?
        for ( LexDef ldef: rand_reqs ) 
        	newItem.addUGoalSVP( new UserGoalSVP(ldef.slotname),ldef.requestable );
        //System.out.printf( "New user goal item with requests: %s\n", newItem.toString() );
        
        // remove selected requests from informables
        inf_slots.removeAll( rand_reqs );
        
        // random selection of constraints
        //int min_num_infs = 1;
        //int numInfs = ( min_num_infs < inf_slots.size() ? min_num_infs + rnd.nextInt(inf_slots.size()-min_num_infs+1) : min_num_infs );
        // sample numInfs from Poisson instead of uniform distr
        int numInfs = Utils.nextPoissonInt( 1, inf_slots.size(), num_infs_mn, rnd );
        ArrayList<LexDef> rand_infs = new ArrayList<LexDef>();
        Utils.sampleWithoutReplacement( inf_slots, rand_infs, numInfs, rnd );
        int rand_val_index;
        for ( LexDef ldef: rand_infs ) {
        	rand_val_index = rnd.nextInt( ldef.values.size() - 1 ); // size-1 to exclude DONTCARE value
        	newItem.addUGoalSVP( new UserGoalSVP( ldef.slotname, ldef.values.get(rand_val_index) ) );
        }
        goal.addItem( newItem );
        
        goal.popNewItems();
        
        //logger.logf( LogLevel.Trace, "Created random goal: %s", goal.toString() );
        //System.out.printf( "Created random goal: %s\n", goal.toString() );
        
        return goal;
        
    }
    
    private ArrayList<LexDef> getInformableSlots( GoalItemDef item_def ) {
    	ArrayList<LexDef> infs = new ArrayList<LexDef>();
        for ( LexDef lex_def: item_def.lex_defs )
        	if ( lex_def.informable )
        		infs.add( lex_def );
        return infs;
    }
    
    private ArrayList<LexDef> getRequestableOnlySlots( GoalItemDef item_def ) {
    	ArrayList<LexDef> infs = new ArrayList<LexDef>();
        for ( LexDef lex_def: item_def.lex_defs )
        	if ( lex_def.requestable && !lex_def.informable )
        		infs.add( lex_def );
        return infs;
    }
        
    public ArrayList< UserGoalSVP > getNonInformables() {
    	ArrayList< UserGoalSVP > nonInfs = new ArrayList< UserGoalSVP >();
    	
        if ( goal_item_defs.isEmpty() )
        	return nonInfs;
        GoalItemDef item_def = goal_item_defs.get( 0 );
        UserGoalSVP nonInf;
        //for ( GoalItemDef item_def: goal_item_defs ) {
            //System.out.printf( "item def: %s\n", item_def.toString() );
            for ( LexDef lex_def: item_def.lex_defs )
//            	if ( !lex_def.informable ) {
            	if ( lex_def.requestable ) {
            		nonInf = new UserGoalSVP( lex_def.slotname,lex_def.values.size() );
            		//nonInf.setGroundStatus( GroundStatus.USR_REQ ); //TODO check relation with requested slots in info state
            		nonInf.setConf( 0 );
            		nonInfs.add( nonInf );
            	}
        //}
        
    	return nonInfs;
    }
    
    /** Returns the list of user goal items for the system's information state. */
    public ArrayList<UserGoalSVP> getUserGoalState() {
    	ArrayList<UserGoalSVP> goal_items = new ArrayList<UserGoalSVP>();

        if ( goal_item_defs.isEmpty() )
        	return goal_items;
        GoalItemDef item_def = goal_item_defs.get( 0 );
        //for ( GoalItemDef item_def: goal_item_defs ) {
            //System.out.printf( "item def: %s\n", item_def.toString() );
            for ( LexDef lex_def: item_def.lex_defs )
            	if ( lex_def.informable ) // isConstraintSlot(lex_def.slotname) ) // skip name slot
            		goal_items.add( new UserGoalSVP(lex_def.slotname,lex_def.values.size()) );
        //}
        
        return goal_items;
    }
    
    /** Returns an initial belief state, containing belief states for each slot, based on this ontology. */
    public ArrayList< BeliefState<UserGoalSVP> > getBeliefState( ArrayList<UserGoalSVP> goal_items ) {
    	ArrayList< BeliefState<UserGoalSVP> > beliefs = new ArrayList< BeliefState<UserGoalSVP> >();
    	BeliefState<UserGoalSVP> ug_svp_bel;
    	SlotValuePair svp;
    	ArrayList<String> values;
    	for ( UserGoalSVP ug_svp: goal_items ) {
    		svp = ug_svp.getSlotValuePair();
    		values = getValuesForSlot( svp.slot );
    		ug_svp_bel = new BeliefState<UserGoalSVP>( svp.slot, ug_svp.getNumValues() );
    		ug_svp_bel.setOtherConf( 0f ); // keeping belief for every individual value
    		ArrayList<UserGoalSVP> hyps = ug_svp_bel.getHyps();
    		for ( String val: values )
    			hyps.add( new UserGoalSVP(svp.slot,val) );
    		Utils.normalise( hyps );
    		beliefs.add( ug_svp_bel );
    	}
    	return beliefs;    	
    }
    
    public boolean isRequestable( String slot ) {
    	LexDef lex_def = getLexDef( slot );
    	return lex_def.requestable;
    }
    
    public boolean isInformable( String slot ) {
    	LexDef lex_def = getLexDef( slot );
    	return lex_def.informable;
    }
    
    public boolean isPrimary( String slot ) {
    	LexDef lex_def = getLexDef( slot );
    	return lex_def.primary;
    }
    
    public String primarySlot() {
    	return primeSlot;
    }
    
    /** Returns first (and usually only) primary slot for this ontology */
    public String getPrimarySlot() {
        if ( goal_item_defs.isEmpty() )
        	return "";
        GoalItemDef item_def = goal_item_defs.get( 0 );
        //for ( GoalItemDef item_def: goal_item_defs ) {
            //System.out.printf( "item def: %s\n", item_def.toString() );
            for ( LexDef lex_def: item_def.lex_defs )
            	if ( lex_def.primary )
            		return lex_def.slotname;
            return"";
    }
    
    public boolean isMultiVal( String slot ) {
    	LexDef lex_def = getLexDef( slot );
    	if ( lex_def == null ) // in CamInfo domain, this happens with the slot 'id', which is currently not specified in the ontology
    		return false;
    	return lex_def.multival;    	
    }
    
    private LexDef getLexDef( String slot ) {
        GoalItemDef item_def = goal_item_defs.get( 0 );
        for ( LexDef lex_def: item_def.lex_defs ) {
        	if ( lex_def.slotname.equals(slot) )
        		return lex_def;
        }
        return null;
    }
    
    /** Loads the ontology from a json file with given name */
    private void loadFromFile( String json_fname ) {
 
        goal_item_defs = new ArrayList<GoalItemDef>();

		try {
			//String inputJsonString = Utils.readFile( json_fname );
			String inputJsonString = Resources.readResourceFile( json_fname );
			JSONObject root = new JSONObject( inputJsonString );
			//System.out.println( root.toString() + "\n" );
			JSONObject ont_obj = root.getJSONObject( "ontology" );

			String[] items = JSONObject.getNames( ont_obj );
			if ( items.length == 0 ) {
				System.out.println( "ERROR: no items found in ontology" );
				return;
			}
	        GoalItemDef gi_def = new GoalItemDef( items[0] );
			JSONArray item_arr = ont_obj.getJSONArray( items[0] ); // currently only restaurants in ontology
			LexDef lx_def;
			String attr_nm, attr_inf, attr_req, attr_prim, attr_mval;
			JSONObject attr_obj;
			JSONArray vals_arr;
			for ( int i = 0; i < item_arr.length(); i++ ) {
				attr_obj = item_arr.getJSONObject( i );
				attr_nm = attr_obj.getString( "slotname" );
				attr_inf = attr_obj.getString( "informable" );
				attr_req = attr_obj.getString( "requestable" );
				attr_prim = attr_obj.getString( "primary" );
				attr_mval = attr_obj.getString( "multival" );
				lx_def = new LexDef( attr_nm, attr_inf.equals("true"), attr_req.equals("true"), attr_prim.equals("true"), attr_mval.equals("true") );
				if ( attr_inf.equals("true") ) {
					vals_arr = attr_obj.getJSONArray( "values" );
					for ( int k = 0; k < vals_arr.length(); k++ )
						lx_def.addValue( vals_arr.getString(k) );
					lx_def.addValue( SlotValuePair.getString( SlotValuePair.SpecialValue.DONTCARE ) );
				}
				gi_def.addLexDef( lx_def );
			}
			goal_item_defs.add( gi_def );
			
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		} catch ( JSONException e ) {
			e.printStackTrace();
		}

    }

}
