/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created July 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using task domains.                                *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.domain;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;

import resources.Configuration;
import uk.ac.hw.madrigal.utils.MyLogger;

public class GoalGenerator {
	
	public static MyLogger logger;
	public static Ontology domain;
	public static Database dbase;

	private static int START_NUM_GOALS;
	private static int START_NUM_ZERO_SOL_GOALS;

	private static int MIN_VENUES_PER_GOAL = 1;
	private static int MAX_VENUES_PER_GOAL = 3;
	private static double ZERO_SOLUTION_TASK_RATIO = 0d;
	
	private static int NUM_REQS_MEAN = 1;
	private static int NUM_INFS_MEAN = 2;
	
	private static int MAX_NUM_ATTEMPTS = 15;
	private static int NUM_ATTEMPTS_BEFORE_RELAX = 5;
	
	private static int MAX_ENTS_DISPL = 5;
	
	private int num_goals;
	private int num_zero_solution_goals;
	
	public GoalGenerator() {
		num_goals = START_NUM_GOALS;
		num_zero_solution_goals = START_NUM_ZERO_SOL_GOALS;
	}
	
	public UserGoal generateGoal( Random rnd ) {
		UserGoal ug = null;
		UserGoalItem ugi;
		ArrayList<DBEntity> ents = null;
		
		boolean nonZeroSolutionNeeded = false;
		double zeroRatio = ( num_goals == 0 ? 0d : (double) num_zero_solution_goals / num_goals );
		if ( ZERO_SOLUTION_TASK_RATIO == 0 || zeroRatio > ZERO_SOLUTION_TASK_RATIO ) // usually too many zero solution goals generated
			nonZeroSolutionNeeded = true;
		logger.logf( Level.FINE, "Ratio of zero solution goals: %.2f%s\n", zeroRatio, (nonZeroSolutionNeeded?" [non zero solution needed]":"") );
		
		boolean resample = true;
		int num_attempts = 0;
		while ( resample && num_attempts < MAX_NUM_ATTEMPTS ) {
			num_attempts++;
			if ( !nonZeroSolutionNeeded || num_goals == 0 )
				resample = false;
			ug = domain.getRandomGoal( NUM_REQS_MEAN, NUM_INFS_MEAN, rnd );
			if ( ug.current_items.isEmpty() ) {
				logger.log( Level.SEVERE, "ERROR: No current items in goal!" );
				return null;
			}
			ugi = ug.current_items.get( 0 );
			ents = dbase.getMatches( ugi.constraints );
			// in certain cases, also try to relax one constraint to make a non-zero-solution goal more likely
			if ( ugi.constraints.size() > 1 && nonZeroSolutionNeeded && ents.isEmpty() && num_attempts > NUM_ATTEMPTS_BEFORE_RELAX ) {
				int randInd = rnd.nextInt( ugi.constraints.size() );
				UserGoalSVP remUgi = ugi.constraints.remove( randInd ); //NOTE change value of constraint to dontcare instead?
				logger.logf( Level.FINE, "Removed constraint: %s\n", remUgi.toString(false) );
				ents = dbase.getMatches( ugi.constraints );
			}
			if ( !nonZeroSolutionNeeded || !ents.isEmpty() )
				resample = false;
			
			// check whether the number of matching venues is outside the interval [ MIN_VENUES_PER_GOAL, MAX_VENUES_PER_GOAL ]
			if ( ents.size() < MIN_VENUES_PER_GOAL || ents.size() > MAX_VENUES_PER_GOAL )
				resample = true;
		}
		// update goal stats
		if ( ents.isEmpty() )
			num_zero_solution_goals++;
		num_goals++;
		
		logger.logf( Level.INFO, "GoalGen> goal:\n%s\n", ug.toFirstItemString(false) );
		String entsStr = getEntitiesString( ents );
		logger.logf( Level.INFO, "GoalGen> matching entities: %s\n", entsStr );
		return ug;
	}
	
	public static String getEntitiesString( ArrayList<DBEntity> ents ) {
		if ( ents.isEmpty() )
			return "NONE";
		if ( ents.size() > MAX_ENTS_DISPL )
			return String.format( "%d matching entities", ents.size() ); 
		ArrayList<String> entPrims = new ArrayList<String>();
		String primSlot = domain.primarySlot(); //.getPrimarySlot();
		for ( DBEntity ent: ents )
			entPrims.add( ent.getAttribute(primSlot) );
		return StringUtils.join( entPrims, "," );
	}
	
	public String getStats() {
		double zeroSolRatio = (double) num_zero_solution_goals / num_goals;
		return String.format( "%d goals; %d zero solution goals (%.2f%%)\n", num_goals, num_zero_solution_goals, 100 * zeroSolRatio );
	}
	
	public static void loadConfig( Properties config ) {
		START_NUM_GOALS = Integer.parseInt( Configuration.START_NUM_GOALS );
		START_NUM_ZERO_SOL_GOALS = Integer.parseInt( Configuration.START_NUM_ZERO_SOL_GOALS );
		MIN_VENUES_PER_GOAL = Integer.parseInt( Configuration.MIN_VENUES_PER_GOAL );
		MAX_VENUES_PER_GOAL = Integer.parseInt( Configuration.MAX_VENUES_PER_GOAL );
		ZERO_SOLUTION_TASK_RATIO = Double.parseDouble( Configuration.ZERO_SOLUTION_TASK_RATIO );
		NUM_REQS_MEAN = Integer.parseInt( Configuration.NUM_REQS_MEAN );
		NUM_INFS_MEAN = Integer.parseInt( Configuration.NUM_INFS_MEAN );
		MAX_NUM_ATTEMPTS = Integer.parseInt( Configuration.MAX_NUM_ATTEMPTS );
		NUM_ATTEMPTS_BEFORE_RELAX = Integer.parseInt( Configuration.NUM_ATTEMPTS_BEFORE_RELAX );
		
        StringBuffer logStr = new StringBuffer( "Goal generator configuration:\n" );
		logStr.append( String.format( "   START_NUM_GOALS: %d\n", START_NUM_GOALS ) );
		logStr.append( String.format( "   START_NUM_ZERO_SOL_GOALS: %d\n", START_NUM_ZERO_SOL_GOALS ) );
		logStr.append( String.format( "   MIN_VENUES_PER_GOAL: %d\n", MIN_VENUES_PER_GOAL ) );
		logStr.append( String.format( "   MAX_VENUES_PER_GOAL: %d\n", MAX_VENUES_PER_GOAL ) );
		logStr.append( String.format( "   ZERO_SOLUTION_TASK_RATIO: %.2f\n", ZERO_SOLUTION_TASK_RATIO ) );
		logStr.append( String.format( "   NUM_REQS_MEAN: %d\n", NUM_REQS_MEAN ) );
		logStr.append( String.format( "   NUM_INFS_MEAN: %d\n", NUM_INFS_MEAN ) );
		logStr.append( String.format( "   MAX_NUM_ATTEMPTS: %d\n", MAX_NUM_ATTEMPTS ) );
		logStr.append( String.format( "   NUM_ATTEMPTS_BEFORE_RELAX: %d\n", NUM_ATTEMPTS_BEFORE_RELAX ) );
		logger.logf( Level.WARNING, "%s\n", logStr );
	}

}
