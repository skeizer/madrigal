/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using task domains.                                *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

import uk.ac.hw.madrigal.domain.SlotValuePair.SpecialValue;
import uk.ac.hw.madrigal.domain.UserGoalSVP.GroundStatus;
import uk.ac.hw.madrigal.simulation.UserSimulator;

public class UserGoalItem {

    public ArrayList<UserGoalSVP> constraints;

    public ArrayList<UserGoalSVP> requests;
    
    public UserGoalItem() {
        constraints = new ArrayList<UserGoalSVP>();
        requests = new ArrayList<UserGoalSVP>();
    }
    
    public UserGoalItem( SlotValuePair svp ) {
    	constraints = new ArrayList<UserGoalSVP>();
    	constraints.add( new UserGoalSVP(svp) );
    	requests = new ArrayList<UserGoalSVP>();
    }
    
    @Override
    public String toString() {
    	String reqs = StringUtils.join( requests, ", " );
    	String cons = StringUtils.join( constraints, ", " ); 
        return reqs + "; " + cons;
    }
    
    public String toExtString( boolean showGrndStat ) {
    	String result = "";
    	result += "Requests: " + toUserGoalString( requests );
    	result += "\n";
    	result += "Constraints: " + toUserGoalString( constraints, showGrndStat ); 
        return result;
    }

    private String toUserGoalString( ArrayList<UserGoalSVP> ug_svps ) {
    	return toUserGoalString( ug_svps, false );
    }

    private String toUserGoalString( ArrayList<UserGoalSVP> ug_svps, boolean showGrndStat ) {
    	String ugStr = "";
    	boolean first = true;
    	for ( UserGoalSVP ug_svp: ug_svps ) {
    		if ( first )
    			ugStr += ug_svp.toString( showGrndStat );
    		else
    			ugStr += ", " + ug_svp.toString( showGrndStat );
    		first = false;
    	}
    	return ugStr;
    }

    public void addSVP( String slot, String value ) {
        constraints.add( new UserGoalSVP(slot,value) );
    }
    
    public void addUGoalSVP( UserGoalSVP ugsvp ) {
    	constraints.add( ugsvp );
    }
    
    public void addUGoalSVP( UserGoalSVP ugsvp, boolean request ) {
    	if ( request )
    		requests.add( ugsvp );
    	else
    		constraints.add( ugsvp );
    }

//    void updateStatus( DialogueAct prev_sys_act, DialogueAct sys_act ) {
//        for ( UserGoalSVP svp : svps )
//            svp.updateGrndStatus( prev_sys_act, sys_act );
//    }
    
    protected boolean informed() {
        for ( UserGoalSVP ug_svp : constraints ) {
            if ( !(ug_svp.ground_status == UserGoalSVP.GroundStatus.USR_INF || ug_svp.ground_status == UserGoalSVP.GroundStatus.GROUNDED) )
                return false;
        }
        return true;
    }

    protected boolean grounded() {
        for ( UserGoalSVP ug_svp : constraints ) {
            if ( ug_svp.ground_status != UserGoalSVP.GroundStatus.GROUNDED )
                return false;
        }
        return true;
    }

    public boolean matches( String dt_val, String qy_val ) {
        Iterator<UserGoalSVP> ug_svp_it = constraints.iterator();
        SlotValuePair svp = new SlotValuePair( dt_val, qy_val );
        while ( ug_svp_it.hasNext() ) {
            if ( ug_svp_it.next().equivalentTo(svp) )
                return true;
        }
        return false;
    }

    protected boolean isQueried() {
        for ( UserGoalSVP ug_svp : constraints ) {
            if ( ug_svp.ground_status == UserGoalSVP.GroundStatus.SYS_EXP_CONF )
                return true;
        }
        return false;
    }

    protected boolean isRequested() {
        for ( UserGoalSVP ug_svp : constraints ) {
            if ( ug_svp.ground_status == UserGoalSVP.GroundStatus.SYS_REQ )
                return true;
        }
        return false;
    }

	public ArrayList<SlotValuePair> updateInform( SlotValuePair svp, boolean sysInfNameNone ) {
		ArrayList<SlotValuePair> toBeCorrected = new ArrayList<SlotValuePair>();
		boolean svpInConstraints = false;
	    for ( UserGoalSVP con_svp: constraints ) {
	    	if ( con_svp.slotMatches(svp) )
	    		svpInConstraints = true;
	    	if ( con_svp.conflictsWith(svp) )
	    		toBeCorrected.add( con_svp.slot_value_pair );
	    }
    	// add svp to corrections if sysInfNameNone and svp.value is not in constraints
	    if ( sysInfNameNone && !svpInConstraints && !svp.slot.equals(UserSimulator.domain.primarySlot()) ) { // .getPrimarySlot()
			String dontcareStr = SlotValuePair.getString( SlotValuePair.SpecialValue.DONTCARE );
	    	toBeCorrected.add( new SlotValuePair(svp.slot,dontcareStr) );
	    }
	    	
	    for ( UserGoalSVP req_svp: requests ) {
	    	if ( req_svp.slotMatches(svp) ) {
	    		if ( toBeCorrected.isEmpty() && !sysInfNameNone )
	    			req_svp.setValue( svp.value );
	    		else
	    			req_svp.resetValue();
	    	}
	    }
	    
	    return toBeCorrected;
	}
	
	public SlotValuePair checkForItem( SlotValuePair svp ) {
		boolean slotPresent = false;
	    for ( UserGoalSVP con_svp: constraints )
	    	if ( con_svp.slotMatches(svp) )
	    		if ( con_svp.conflictsWith(svp) )
	    			return con_svp.slot_value_pair;
	    		else
	    			slotPresent = true;
	    if ( !slotPresent && !svp.value.equals(SlotValuePair.getString(SpecialValue.DONTCARE)) )
	    	return new SlotValuePair( svp.slot, SlotValuePair.getString(SpecialValue.DONTCARE) );
	    
	    return null;
	}
	
	public SlotValuePair getValue( SlotValuePair svp ) {
	    for ( UserGoalSVP con_svp: constraints )
	    	if ( svp.slot.equals( con_svp.slot_value_pair.slot ) )
	    		return con_svp.slot_value_pair;
	    for ( UserGoalSVP req_svp: requests )
	    	if ( svp.slot.equals( req_svp.slot_value_pair.slot) )
	    		return req_svp.slot_value_pair;
	    return new SlotValuePair( svp.slot, SlotValuePair.getString( SlotValuePair.SpecialValue.DONTCARE ) );
	}
	
	public SlotValuePair modify( SlotValuePair svp, Random rnd, double dontcare_prob, Ontology dom ) {
		ArrayList<String> vals;
		String newVal = null;
		SlotValuePair newSVP;
		
		if ( rnd.nextDouble() < dontcare_prob ) {
			newVal = SlotValuePair.getString( SlotValuePair.SpecialValue.DONTCARE );
			newSVP = new SlotValuePair( svp.slot, newVal );
		} else {
			if ( svp != null ) {
				vals = dom.getValuesForSlot( svp.slot );
				vals.remove( svp.value );
			} else {
				UserGoalSVP rndConstr = constraints.get( rnd.nextInt(constraints.size()) );
				vals = dom.getValuesForSlot( rndConstr.slot_value_pair.slot );
				vals.remove( rndConstr.slot_value_pair.value );
			}
			newVal = vals.get( rnd.nextInt(vals.size()) );
			newSVP = new SlotValuePair( svp.slot, newVal );
		}
		// update user goal item with new value
		for ( UserGoalSVP con_svp: constraints ) {
			if ( con_svp.slotMatches(newSVP) ) {
	    		con_svp.setValue( newVal );
	    		con_svp.setGroundStatus( GroundStatus.INIT );
			}
		}
		return newSVP;
	}
	
	boolean completed() {
	    for ( UserGoalSVP req_svp: requests )
	    	if ( !req_svp.isFilled() )
	    		return false;
		return true;
	}

//	public boolean updateInform( SlotValuePair svp ) {
//	    for ( UserGoalSVP con_svp: constraints )
//	    	if ( con_svp.conflictsWith(svp) )
//	    		return false;
//	    	
//	    for ( UserGoalSVP req_svp: requests )
//	    	if ( req_svp.slotMatches(svp) )
//	    		req_svp.setValue( svp.value );
//	    return true;
//	}

}
