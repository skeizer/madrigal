/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using task domains.                                *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.domain;

import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.utils.Hypothesis;


public class UserGoalSVP extends Hypothesis {
	
    /**
     * All possible grounding states of a single component of the user goal (drink type or quantity)
     */
    public enum GroundStatus {
        /* initial state */
        INIT,
        /* system has asked item */
        SYS_REQ,
        /* user has asked item */
        USR_REQ,
        /* user has informed about item */
        USR_INF,
        /* user explicitly confirmed item */
        USR_EXP_CONF,
        /* system has informed about item */
        SYS_INF,
        /* system explicitly confirmed item */
        SYS_EXP_CONF,
        /* system implicitly confirmed item */
        SYS_IMP_CONF,
        /* user has denied the item */
        USR_DENY,
        /* item grounded */
        GROUNDED
    }

    // - - - - - - - - - - - - - - - - - - - - - - -

    SlotValuePair slot_value_pair;
    
    int num_values;
    
    GroundStatus ground_status;
    
    // - - - - - - - - - - - - - - - - - - - - - - -

    public UserGoalSVP( String slot, int nv ) {
    	num_values = nv;
        slot_value_pair = new SlotValuePair( slot, "" );
        ground_status = GroundStatus.INIT;
    }
    
    public UserGoalSVP( String slot ) {
    	num_values = 0;
    	slot_value_pair = new SlotValuePair( slot, "" );
    	ground_status = GroundStatus.INIT;
    }

    public UserGoalSVP( String slot, String value ) {
        slot_value_pair = new SlotValuePair( slot, value );
        ground_status = GroundStatus.INIT;
    }
    
    public UserGoalSVP( SlotValuePair svp ) {
        slot_value_pair = new SlotValuePair( svp.slot, svp.value );
        ground_status = GroundStatus.INIT;    	
    }

    // - - - - - - - - - - - - - - - - - - - - - - -

    @Override
    public String toString() {
        //return slot_value_pair.toString() + " [" + ground_status + "]";
        return toString( true );
    }
    
    public String toString( boolean showGrndStat ) {
    	return slot_value_pair.toString() + ( showGrndStat ? " [" + ground_status + "]" : "" );
    }
    
    public GroundStatus getGroundStatus() {
    	return ground_status;
    }
    
    public void setGroundStatus( GroundStatus gs ) {
    	ground_status = gs;
    }
    
    boolean equivalentTo( SlotValuePair svp_sys ) {
        return slot_value_pair.equals( svp_sys );
    }
    
    public SlotValuePair getSlotValuePair() {
        return slot_value_pair;
    }
    
    public int getNumValues() {
    	return num_values;
    }

	public boolean slotMatches( SlotValuePair svp ) {
		return slot_value_pair.slot.equals( svp.slot );
	}
	
	public boolean isConstraint() {
		return slot_value_pair.isConstraint();
	}
	
	/** update this user goal slot with the given value and update the grounding status */
	public void userInform( String val ) {
		slot_value_pair.value = val;
	}
	
	public void userNegate( String val ) {
		if ( val.equals(slot_value_pair.value) )
			slot_value_pair.value = "";
	}

	/** update this user goal slot with the given value and update the grounding status */
	public void systemInform( String val ) {
		slot_value_pair.value = val;
	}

    /**
     * Updates the grounding state of this user goal item with
     * the given dialogue act.
     * 
     * @param dact dialogue act
     */
    public void updateGrndStatus( DialogueAct prev_dact, DialogueAct dact ) {

        // updating with SYSTEM acts
        
        if ( dact.isSystemAct() ) {
            
            if ( ground_status == GroundStatus.INIT ) {
                if ( dact.requestsAbout(slot_value_pair) )
                    ground_status = GroundStatus.SYS_REQ;
                else if ( dact.informsAbout(slot_value_pair) )
                	ground_status = GroundStatus.SYS_INF;
            } else if ( ground_status == GroundStatus.USR_INF ) {
                
                if ( prev_dact != null && prev_dact.informsAbout(slot_value_pair) && dact.getCommFunction()==DialogueAct.CommFunction.autoPositive )
                    ground_status = GroundStatus.GROUNDED;
                else if ( dact.explConfirms(slot_value_pair) )
                    ground_status = GroundStatus.SYS_EXP_CONF;
                else if ( dact.informsAbout(slot_value_pair) )
                    ground_status = GroundStatus.SYS_IMP_CONF; //SYS_INF?
                else if ( dact.requestsAbout(slot_value_pair) )
                    // weird for system to ask for this piece of information when the user has already informed about it;
                    // go back to initial state and then to SYS_REQ
                    ground_status = GroundStatus.SYS_REQ;
            } else if ( ground_status == GroundStatus.USR_REQ ) {
            	if ( dact.informsAbout(slot_value_pair) )
            		ground_status = GroundStatus.SYS_INF;
            } else if ( ground_status == GroundStatus.USR_EXP_CONF ) {
            	if ( dact.informsAbout(slot_value_pair) || dact.getCommFunction() == CommFunction.confirm || dact.getCommFunction() == CommFunction.disconfirm )
            		ground_status = GroundStatus.SYS_INF;            	
            }
            //TODO what if system act in conflict with this item??
        } else {

        // updating with USER acts
            
            if ( dact.explConfirms(slot_value_pair) )
            	ground_status = GroundStatus.USR_EXP_CONF;
            
            if ( ground_status==GroundStatus.INIT || ground_status==GroundStatus.SYS_REQ
                    || ground_status==GroundStatus.USR_DENY ) {
                if ( dact.informsAbout(slot_value_pair) )
                    ground_status = GroundStatus.USR_INF;
                else if ( dact.requestsAbout(slot_value_pair) )
                	ground_status = GroundStatus.USR_REQ;
                
            } else if ( ground_status==GroundStatus.SYS_EXP_CONF ) {
                if ( dact.confirms(slot_value_pair) || dact.informsAbout(slot_value_pair) )
                    // second part of above condition debatable
                    ground_status = GroundStatus.GROUNDED;
                else if ( dact.denies(slot_value_pair) )
                    ground_status = GroundStatus.USR_DENY;
                else
                    ground_status = GroundStatus.USR_INF;
            } else if ( ground_status==GroundStatus.SYS_IMP_CONF ) {
                if ( dact.denies(slot_value_pair) )
                    ground_status = GroundStatus.USR_DENY;
                else
                    ground_status = GroundStatus.GROUNDED;
            }
        }
    }

	public void setValue( String value ) {
		slot_value_pair.value = value;
	}

	public boolean conflictsWith( SlotValuePair svp ) {
		return ( slot_value_pair.conflictsWith(svp) );
	}

	boolean isFilled() {
		if( slot_value_pair.value.isEmpty() )
			return false;
		return true;
	}

	void resetValue() {
		slot_value_pair.value = "";
	}

	@Override
	public boolean equals(Hypothesis hyp) {
		// TODO Auto-generated method stub
		return false;
	}

}
