package uk.ac.hw.madrigal.domain;

import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import resources.Resources;

public class Lexicon {
	
	// define list of slot values and possible phrasings, loaded from lexicon file (json)
	ArrayList<Phrasing> phrasings;
	
	public class Phrasing {
		String slot;
		String value;
		ArrayList<String> phrases;
		public Phrasing( String s, String v ) {
			slot = s;
			value = v;
			phrases = new ArrayList<String>();
		}
		public void addPhrase( String phrase ) {
			phrases.add( phrase );
		}
	}
	
	public Lexicon( String fname ) {
        loadFromFile( fname );
	}
	
	public String getValue( String slot ) {
		Phrasing phr = null;
		for ( Phrasing p: phrasings ) {
			if ( p.value.isEmpty() && p.phrases.contains(slot) ) {
				phr = p;
				break;
			}
		}
		if ( phr == null )
			return ""; //ERROR unknown slot
		return phr.slot;
	}

	public String getValue( String slot, String phrase ) {
		Phrasing phr = null;
		for ( Phrasing p: phrasings ) {
			if ( slot.equals(p.slot) && p.phrases.contains(phrase) ) {
				phr = p;
				break;
			}
		}
		if ( phr == null )
			return ""; //ERROR unknown slot
		return phr.value;
	}

    /** Loads the ontology from a json file with given name */
    private void loadFromFile( String json_fname ) {
 
    	phrasings = new ArrayList<Phrasing>();

		try {
			String inputJsonString = Resources.readResourceFile( json_fname );
			JSONObject root = new JSONObject( inputJsonString );
			JSONArray item_arr = root.getJSONArray( "lexicon" );
			Phrasing phras;
			String slot_nm, val_nm;
			JSONObject attr_obj;
			JSONArray phras_arr;
			for ( int i = 0; i < item_arr.length(); i++ ) {
				attr_obj = item_arr.getJSONObject( i );
				slot_nm = attr_obj.getString( "slotname" );
				val_nm = attr_obj.getString( "valuename" );
				phras = new Phrasing( slot_nm, val_nm );
				phras_arr = attr_obj.getJSONArray( "phrases" );
				for ( int k = 0; k < phras_arr.length(); k++ )
					phras.addPhrase( phras_arr.getString(k) );
				phrasings.add( phras );
			}
			
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		} catch ( JSONException e ) {
			e.printStackTrace();
		}

    }
    
}
