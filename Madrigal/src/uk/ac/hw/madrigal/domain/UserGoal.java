/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using task domains.                                *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.domain;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

import uk.ac.hw.madrigal.domain.SlotValuePair.SpecialSlot;
import uk.ac.hw.madrigal.domain.SlotValuePair.SpecialValue;
import uk.ac.hw.madrigal.domain.UserGoalSVP.GroundStatus;

public class UserGoal {

    public ArrayDeque<UserGoalItem> item_stack;

    public ArrayList<UserGoalItem> current_items;
    
    public UserGoal() {
        item_stack = new ArrayDeque<UserGoalItem>();
        current_items = new ArrayList<UserGoalItem>();
    }
    
    public boolean currentCompleted() {
    	for ( UserGoalItem ugi: current_items ) {
    		if ( !ugi.completed() )
    			return false;
    	}
    	return true;
    }
    
    @Override
    public String toString() {
        String result = "Current items: " + StringUtils.join( current_items, "; " );
        result += ( item_stack.isEmpty() ? "" : "\nFurther items: " + StringUtils.join( item_stack, ";" ) );
        return result;
    }
    
    public String toFirstItemString( boolean showGrndStat ) {
    	return current_items.get( 0 ).toExtString( showGrndStat );
    }
    
    public void addItem( UserGoalItem item ) {
        item_stack.push( item );
    }
    
    public boolean hasNoGoal() {
        return current_items.isEmpty() && item_stack.isEmpty();
    }
    
    public boolean informed() {
        for ( UserGoalItem ugi : current_items ) {
            if ( !ugi.informed() )
                return false;
        }
        return true;
    }
    
    public boolean grounded() {
        for ( UserGoalItem ugi : current_items ) {
            if ( !ugi.grounded() )
                return false;
        }
        return true;
    }
    
    public UserGoalItem getQueriedItem() {
        for ( UserGoalItem ugi : current_items ) {
            if ( ugi.isQueried() )
                return ugi;
        }
        return null;
    }
    
    public UserGoalItem getRequestedItem() {
        for ( UserGoalItem ugi : current_items ) {
            if ( ugi.isRequested() )
                return ugi;
        }
        return null;        
    }

    void popNewItems() {
        if ( !item_stack.isEmpty() )
            current_items.add( item_stack.pop() ); // only popping one item
    }

	public ArrayList<SlotValuePair> updateInform( SlotValuePair svp, boolean sysInfNameNone ) {
		ArrayList<SlotValuePair> toBeCorrected = new ArrayList<SlotValuePair>(); 
		for ( UserGoalItem ug_it: current_items )
			toBeCorrected.addAll( ug_it.updateInform(svp,sysInfNameNone) );
		return toBeCorrected;
	}

	public SlotValuePair checkForItem( SlotValuePair svp ) {
		SlotValuePair corr_svp;
		for ( UserGoalItem ug_it: current_items ) {
			corr_svp = ug_it.checkForItem( svp ); 
			if ( corr_svp != null )
				return corr_svp;
		}
		return null;
	}
	
	public SlotValuePair getValue( SlotValuePair svp ) {
		SlotValuePair ansSVP;
		for ( UserGoalItem ug_it: current_items ) {
			ansSVP = ug_it.getValue( svp );
			if ( ansSVP != null )
				return ansSVP;
		}
		return null;
	}

	public SlotValuePair getEntityInFocus() {
		SlotValuePair entSVP = new SlotValuePair( SlotValuePair.getPrimDescrStr() ); 
		return getValue( entSVP );
	}
	
	public ArrayList<String> getSlotsToAsk() {
		// go through constraints and return random slot corresponding with grounding state INIT
		UserGoalItem first_item = current_items.get( 0 );
		ArrayList<String> slotsToAskSVP = new ArrayList<String>();
		for ( UserGoalSVP ug_svp : first_item.constraints ) {
			if ( ug_svp.getGroundStatus() == GroundStatus.INIT )
				slotsToAskSVP.add( ug_svp.getSlotValuePair().slot );
		}
		return slotsToAskSVP;
	}
	
	public SlotValuePair modifyConstraints( ArrayList<SlotValuePair> svps, Random rnd, double dontcare_prob, Ontology dom ) {
		UserGoalItem first_item = current_items.get( 0 );
		SlotValuePair svpToModify = null;
		// filter out slots with value DONTCARE in user goal
		ArrayList<SlotValuePair> toRemove = new ArrayList<SlotValuePair>();
		String dcVal = SlotValuePair.getString( SpecialValue.DONTCARE );
		String primeDesr = SlotValuePair.getString( SpecialSlot.PRIMARY_DESCR );
		for ( SlotValuePair svp: svps ) {
			if ( !svp.slot.equals(primeDesr) ) {
				String goalVal = first_item.getValue( svp ).value;
				if ( goalVal.equals(dcVal) )
					toRemove.add( svp );
			}
		}
		svps.removeAll( toRemove );
		if ( svps.size() < 2 )
			return null;
		svpToModify = svps.get( rnd.nextInt(svps.size()-1) + 1 ); // skip first item (primary slot, e.g. name=none)
		return first_item.modify( svpToModify, rnd, dontcare_prob, dom );
	}

}
