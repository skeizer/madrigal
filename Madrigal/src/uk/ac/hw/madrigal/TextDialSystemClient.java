/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created February 2017                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.io.*;
import java.net.*;

/**
 * Client application to connect to the TextDialSystemServer.
 */
public class TextDialSystemClient {
	
    public static void main( String[] args ) throws IOException {
    	
    	if ( args.length != 2 ) {
    		System.err.println( "Usage: java -cp .:../../lib/*.jar uk.ac.hw.madrigal.TextDialSystemClient <host name> <port number>" );
    		System.err.println( "For testing locally on same machine:" );
    		System.err.println( "       java -cp .:../../lib/*.jar uk.ac.hw.madrigal.TextDialSystemClient localhost 8000" );
    		System.exit( 1 );
    	}
    	
    	String host_name = args[0];
    	int port_number = Integer.parseInt( args[1] );
    	
    	Socket client;
    	PrintWriter out;
    	BufferedReader in;
    	BufferedReader stdIn;
    	
    	try {
    		System.out.println( "Attempting connection" );
    		client = new Socket( host_name, port_number );
    		System.out.println( "Connected to: " + client.getInetAddress().getHostName() );
    		out = new PrintWriter( client.getOutputStream(), true );
    		in = new BufferedReader( new InputStreamReader( client.getInputStream()) );
    		stdIn = new BufferedReader( new InputStreamReader(System.in) );
            System.out.println( in.readLine() );

            String usr_utt, sys_utt;
            while ( true ) {
    			System.out.print( "User: " );
            	usr_utt = stdIn.readLine();
    			System.out.println( usr_utt );
                out.println( usr_utt );
                if ( usr_utt.equals("HANGUP") )
                	break;
                sys_utt = in.readLine();
    			System.out.println( "System: " + sys_utt );
            }
            System.out.println( "End of dialogue." );
            String token_str = in.readLine();
            System.out.println( "token received: " + token_str );
            
            in.close();
            out.close();

    	} catch ( UnknownHostException uhe ) {
    		System.err.println( "Don't know about host " + host_name );
    		System.exit( 1 );
    	} catch ( IOException e ) {
            System.err.println( "Couldn't get I/O for the connection to " + host_name );
            System.exit( 1 );
        } 
    }
    
}
