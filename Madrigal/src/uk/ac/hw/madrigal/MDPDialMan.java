/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.util.logging.Level;

import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.dialogue_acts.SemanticContent;
import uk.ac.hw.madrigal.domain.DBEntity;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.learning.mdps.NumericMDP;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Represents MDP model using linear value function approximation to map input states to Q-values
 */
public class MDPDialMan extends AbstractMDPDialMan {
	
	protected void initialise_MDP_model() {
		num_usr_acts = 6;
		num_sys_acts = 0; //7;
		num_state_feats = num_sys_acts + num_usr_acts + 6;
		num_acts = 7;
		mdp_model = new NumericMDP( "dial_man_mdp", num_state_feats, num_acts );
	}

	protected double[] getNumericStateFeatures() {
		int state_feat_ind = 0;
		double[] features = new double[ num_state_feats ];
		//getSysActIndex( info_state.last_system_act, features, 0 );
		state_feat_ind += num_sys_acts;
		
		DialogueAct lastUserAct = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		getUserActIndex( lastUserAct, features, state_feat_ind );
		state_feat_ind += num_usr_acts;
		
		features[ state_feat_ind++ ] = ( lastUserAct == null ? 0f : lastUserAct.getConf() );
		
		features[state_feat_ind++] = ( info_state.last_uact_hyps.isEmpty() ? 1f : Utils.getEntropy( info_state.last_uact_hyps, true ) );

		if ( info_state.minConfUG == null )
			features[ state_feat_ind++ ] = 0d;
		else
			features[ state_feat_ind++ ] = info_state.minConfUG.getConf();
		
		double maxEnt = 0d;
		for ( Double f: info_state.ug_item_entropies )
			if ( f > maxEnt )
				maxEnt = f;
		features[state_feat_ind++] = maxEnt;
		
		features[ state_feat_ind++ ] = info_state.matching_entities.size() / InfoState.database.entities.size();
		
		String logStr = "reqSlots:";
		for ( UserGoalSVP ug_svp: info_state.requested_slots )
			logStr += String.format( " %s [%.3f]", ug_svp.toString(), ug_svp.getConf() ); 
		logger.logf( Level.FINE, logStr + "\n" );
		features[ state_feat_ind ] = info_state.requested_slots_max_prob;// maxProb;
		
		return features;
	}
	
	protected void getSysActIndex( DialogueAct sys_act, double[] result, int start_index ) {
		if ( sys_act == null )
			result[ start_index ] = 1d;
		else {
			CommFunction cf = sys_act.getCommFunction();
			if ( cf == CommFunction.inform ) //NOTE split into the two kinds of inform generated?
				result[ start_index + 1 ] = 1d;
			else if ( cf == CommFunction.propQuestion )
				result[ start_index + 2 ] = 1d;
			else if ( cf == CommFunction.setQuestion )
				result[ start_index + 3 ] = 1d;
			else if ( cf == CommFunction.autoNegative )
				result[ start_index + 4 ] = 1d;
			else if ( cf == CommFunction.confirm || cf == CommFunction.disconfirm )
				result[ start_index + 5 ] = 1d;
			else
				result[ start_index + 6 ] = 1d; //NOTE currently does not occur
		}
	}

	protected void getUserActIndex( DialogueAct usr_act, double[] result, int start_index ) {
		if ( usr_act == null )
			result[ start_index ] = 1d;
		else {
			CommFunction cf = usr_act.getCommFunction();
			if ( cf == CommFunction.inform )
				result[ start_index + 1 ] = 1d;
			else if ( cf == CommFunction.propQuestion )
				result[ start_index + 2 ] = 1d;
			else if ( cf == CommFunction.setQuestion )
				result[ start_index + 3 ] = 1d;
			else if ( cf == CommFunction.autoNegative )
				result[ start_index + 4 ] = 1d;
			else if ( cf == CommFunction.initialGoodbye )
				result[ start_index + 5 ] = 1d;
			else
				result[ start_index ] = 1d;
		}
	}

	protected DialogueAct getDialogueAct( int action_index ) {
		DialogueAct sysAct;
		
		if ( action_index == 0 ) {
			if ( info_state.getProcProblem() == ProcLevel.PERCEPTION )
				return DialActUtils.newSystemAct( CommFunction.autoNegative_perc );
			if ( info_state.getProcProblem() == ProcLevel.INTERPRETATION )
				return DialActUtils.newSystemAct( CommFunction.autoNegative_int );
			return DialActUtils.newSystemAct( CommFunction.autoNegative );
		}
		
		if ( action_index == 1 ) {
			sysAct = DialActUtils.newSystemAct( CommFunction.propQuestion );
			UserGoalSVP minConfUG = info_state.minConfUG; //.getMinConfUsrGoalHyp();
			UserGoalItem ugConfItem = new UserGoalItem();
			//if ( info_state.ugoal_top_hyps.isEmpty() )
			if ( minConfUG == null ) {
				logger.log( Level.FINE, "No minConfUG in info state: backing off to autoNegative" );
				return DialActUtils.newSystemAct( CommFunction.autoNegative ); // back off
			} else {
				ugConfItem.addUGoalSVP( minConfUG );
				SemanticItem confItem = new SemanticItem( ugConfItem );
				sysAct.addItem( confItem );
				return sysAct;
			}
		}
		
		if ( action_index == 2 ) { // answer question about current entity (setQ)
			if ( info_state.current_entity == null || info_state.requested_slots.isEmpty() //) { 
					|| info_state.max_prob_req_slot == null ) {
				logger.log( Level.FINE, "No current entity or known requested slots: backing off to autoNegative" );
				return DialActUtils.newSystemAct( CommFunction.autoNegative );
			}
			sysAct = DialActUtils.newSystemAct( CommFunction.inform );
			//String primDescrStr = domain.getPrimarySlot();
			String val = info_state.current_entity.getAttribute( primeSlot );
			SemanticItem sem_item = new SemanticItem();
			sem_item.addSlotValuePair( new SlotValuePair(primeSlot,val) );
			// select slots believed to be requested
			if ( info_state.max_prob_req_slot != null ) {
				SlotValuePair reqSVP = info_state.max_prob_req_slot.getSlotValuePair();
				val = info_state.current_entity.getAttribute( reqSVP.slot );
				sem_item.addSlotValuePair( new SlotValuePair(reqSVP.slot,val) );
			}
			sysAct.addItem( sem_item );
			return sysAct;
		}
		
		if ( action_index == 3 ) { // answer question about current entity (propQ)
			if ( !info_state.last_uact_hyps.isEmpty() && info_state.current_entity != null ) {
				DialogueAct uactTopHyp = info_state.last_uact_hyps.get( 0 );
				SemanticContent sem_content = uactTopHyp.getSemContent();
				if ( uactTopHyp.getCommFunction() == CommFunction.propQuestion && !sem_content.isEmpty() ) {
					SemanticItem item = sem_content.getFirstItem();
					String val;
					for ( SlotValuePair svp: item.svps ) {
						if ( !svp.value.isEmpty() ) {
							val = info_state.current_entity.getAttribute( svp.slot );
							if ( svp.value.equals(val) )
								return DialActUtils.newSystemAct( CommFunction.confirm );
							else
								return DialActUtils.newSystemAct( CommFunction.disconfirm );
						}
					}
					
				}
			}
			return DialActUtils.newSystemAct( CommFunction.autoNegative );
		}
		
		if ( action_index == 4 ) { // give recommendation based on current user goal belief
			sysAct = DialActUtils.newSystemAct( CommFunction.inform );
			DBEntity entity = info_state.getRecommendation();
			SemanticItem sem_item = new SemanticItem();
			if ( entity == null ) { // no matching entities found in database
				String noneStr = SlotValuePair.getString( SlotValuePair.SpecialValue.NONE );
				sem_item.addSlotValuePair( new SlotValuePair(primeSlot,noneStr) );
			} else {
				SlotValuePair name_svp = new SlotValuePair( primeSlot, entity.getAttribute(primeSlot) );
				sem_item.addSlotValuePair( name_svp );
			}
				// including user goal slot value pairs, which were used for the database query
				// TODO only include those which have an 'actual' value ...
				for ( UserGoalSVP ug_svp: info_state.ugoal_top_hyps ) { // info_state.user_goal_items ) {
					//if ( entity == null || ug_svp.getGroundStatus() != GroundStatus.GROUNDED ) {
						SlotValuePair svp = ug_svp.getSlotValuePair();
						if ( svp.isConstraint() && !svp.value.equals(SlotValuePair.getString(SlotValuePair.SpecialValue.DONTCARE)) )
							sem_item.addSlotValuePair( new SlotValuePair(svp) );
					//}
				}
				//TODO including attributes should be handled by feedback agent; task agent would simply say "<name> matches your constraints",
				// or possibly include new attributes to help the user in their search 
	//			for ( SlotValuePair svp: entity.getAttributes() )
	//				sem_item.addSlotValuePair( svp );
			//}
			sysAct.addItem( sem_item );
		
			info_state.current_entity = entity;
			//TODO make current_entity keep track of which information has been conveyed about this entity	
			return sysAct;
		}

		if ( action_index == 5 )
			return DialActUtils.newSystemAct( CommFunction.returnGoodbye );
		
		if ( action_index == 6 ) {
			sysAct = DialActUtils.newSystemAct( CommFunction.setQuestion );
			SlotValuePair randSVP;
			if ( info_state.slots_to_ask.isEmpty() ) // no slots that have not been discussed yet: back off to requesting uncertain slot
				if ( info_state.minConfUG == null ) {
					int bound = info_state.ugoal_top_hyps.size();
					if ( bound == 0 ) {
						String randSlot = info_state.getRandomUGoalSlot();
						randSVP = new SlotValuePair( randSlot, "" );
					} else
						randSVP = new SlotValuePair( info_state.ugoal_top_hyps.get(bound).getSlotValuePair() );
				} else
					randSVP = new SlotValuePair( info_state.minConfUG.getSlotValuePair() );
			else {
				SlotValuePair rand_svp = info_state.slots_to_ask.get( rand_num_gen.nextInt(info_state.slots_to_ask.size()) ); 
				randSVP = new SlotValuePair( rand_svp ); // copy this svp
			}
			randSVP.value = ""; // not necessary in case only items with grounding state INIT are selected
			SemanticItem newItem = new SemanticItem();
			newItem.addSlotValuePair( randSVP );
			sysAct.addItem( newItem );
			return sysAct;
		}
		
		return null;
	}
	
}
