/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created March 2017                                                              *
 *     ---------------------------------------------------------------------------     *
 *     Natural Language Generation (NLG) package.                                      *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.nlg;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import resources.Resources;
import uk.ac.hw.macs.ilab.nlg.TemplateBasedNLG2;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;

public class Template_NLG extends AbstractNLG {
	
	private TemplateBasedNLG2 tnlg2;
	
	public Template_NLG() {
        tnlg2 = new TemplateBasedNLG2();
		load_templates();
	}
	
	@Override
	public String getUtterance( DialogueTurn turn ) {
		logger.logf( Level.INFO, "dialogue turn to realise: %s\n", turn.toString() );
		
		if ( turn.dact_all == null )
			return "";
		
		String dial_act_str = DialActUtils.getStringForNLG( turn.dact_all );
		logger.logf( Level.INFO, "dialogue act for NLG template matching: %s\n", dial_act_str );
        String utt = tnlg2.generate( dial_act_str );
        logger.log( Level.INFO, "Generated sentence: " + utt + "\n" );
        
        return utt;
	}
	
	private void load_templates() {
		if ( templates_filename != null && !templates_filename.isEmpty() ) {
			try {
				logger.logf( Level.INFO, "Loading templates from file %s\n", templates_filename );
				if ( LOCAL_TEMPLATES )
					loadTemplates( templates_filename );
				else
					loadTemplates( Resources.getResourceFileInputStream(templates_filename) );
			} catch ( IOException ioe ) {
				logger.logf( Level.SEVERE, "Could not load templates file %s\n", templates_filename );
				ioe.printStackTrace();
			}
			logger.logf( Level.INFO, "Successfully loaded templates file %s\n", templates_filename );
		}
	}

	private void loadTemplates( InputStream resourceFileInputStream ) throws IOException {
        tnlg2.readTemplates( resourceFileInputStream );
	}

	private void loadTemplates( String templates_filename ) {
		tnlg2.readTemplatesFromFile( templates_filename, null);
	}
	
}
