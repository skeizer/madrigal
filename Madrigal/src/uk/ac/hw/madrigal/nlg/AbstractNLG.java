/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created March 2017                                                              *
 *     ---------------------------------------------------------------------------     *
 *     Natural Language Generation (NLG) package.                                      *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.nlg;

import java.util.Random;

import resources.Configuration;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.utils.MyLogger;

/**
 * Natural Language Generation component.
 * 
 * @author skeizer
 */
public abstract class AbstractNLG {

	public static MyLogger logger;

	public static Random rand_num_gen;
	
	protected static String templates_filename;
	
	protected boolean LOCAL_TEMPLATES = false;

    public abstract String getUtterance( DialogueTurn turn );

	public static void loadConfig() {
		templates_filename = Configuration.NLG_TEMPLATES;
	}

}
