/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using dialogue acts from the multi-dimensional     *
 *     taxonomy underlying the ISO standard for semantic annotation ISO/DIS 24617-2.   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dialogue_acts;

import java.util.ArrayList;
import java.util.List;

import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.utils.Hypothesis;

/**
 * Main class for representing dialogue acts; contains enumerations of possible
 * communicative functions and dimensions taken from the ISO standard.
 */
public class DialogueAct extends Hypothesis {
	
    /** Enumeration of all DIT communicative functions of interest. */
    public enum CommFunction {
        // general-purpose CFs:
        inform,
        setQuestion,
        propQuestion,
        choiceQuestion,
        confirm,
        disconfirm,
        instruct,
        request,
        acceptRequest,
        declineRequest,
        // dimension-specific CFs:
        // - autoFeedback:
        autoPositive,
        autoNegative,
        autoNegative_perc,
        autoNegative_int,
        attentionAutoPositive,
        // - alloFeedback:
        alloPositive,
        alloNegative,
        feedbackElicit,
        attentionFeedbackElicit,
        // - timeManagement
        stalling,
        pausing,
        // - socialObligationsManagement:
        initialGreeting,
        returnGreeting,
        apology,
        acceptApology,
        thanking,
        acceptThanking,
        initialGoodbye,
        returnGoodbye,
        // value for error handling:
        other
    }

    /** Enumeration of all DIT dimensions of interest. */
    public enum Dimension {
    	all,
        task,
        autoFeedback,
        alloFeedback,
        timeManagement,
        socialObligationsManagement,
        eval
    }
    
    /** communicative function of this dialogue act */
    private CommFunction comm_funct;

    /** semantic content of this dialogue act */
    private SemanticContent sem_content;

    /** speaker of this dialogue act */
    private AgentName speaker;

    /** addressee of this dialogue act */
    private AgentName addressee;
    
    /**
     * dimension of this dialogue act (particularly important in case
     * of general-purpose communicative function
     */
    private Dimension dimension;

    /**
     * Default constructor: creates 'empty' dialogue act inform()
     * in the task dimension.
     */
    public DialogueAct() {
    	super();
        comm_funct = CommFunction.inform;
        sem_content = new SemanticContent();
        dimension = Dimension.task;
        speaker = AgentName.unknown;
        addressee = AgentName.unknown;
    }
    
    public DialogueAct( CommFunction cf ) {
    	super();
        speaker = AgentName.unknown;
        addressee = AgentName.unknown;
        comm_funct = cf;
        sem_content = new SemanticContent();
        inferDimensionFromCF();
    }

    public DialogueAct( CommFunction cf, String slot, String value ) {
    	super();
        speaker = AgentName.unknown;
        addressee = AgentName.unknown;
        comm_funct = cf;
        sem_content = new SemanticContent();
        addItem( new SemanticItem( new UserGoalItem(new SlotValuePair(slot,value)) ) );
        inferDimensionFromCF();
    }

    public DialogueAct( CommFunction cf, SlotValuePair svp ) {
    	super();
        speaker = AgentName.unknown;
        addressee = AgentName.unknown;
        comm_funct = cf;
        sem_content = new SemanticContent();
        addItem( new SemanticItem( new UserGoalItem(svp) ) );
        inferDimensionFromCF();
    }

    /**
     * Constructor for creating dialogue act with specific communicative function.
     * @param cf_str string representation of communicative function
     */
    public DialogueAct( String cf_str ) {
    	super();
        for ( CommFunction cf : CommFunction.values() ) {
            if ( cf_str.equals( cf.toString() ) ) {
                comm_funct = cf;
                break;
            }
        }
        if ( comm_funct == null ) {
            System.out.printf( "ERROR: unknown communicative function: %s\n", cf_str );
            System.exit( 0 );
        } //TODO throw exception instead?

        speaker = AgentName.unknown;
        addressee = AgentName.unknown;
        sem_content = new SemanticContent();
        inferDimensionFromCF();
    }

    /**
     * Copy constructor.
     */
    public DialogueAct( DialogueAct dact ) {
    	super( dact );
        comm_funct = dact.getCommFunction();
        sem_content = new SemanticContent( dact.getSemContent() );
        speaker = dact.getSpeaker();
        addressee = dact.getAddressee();
        dimension = dact.getDimension();
    }
    
    /**
     * Sets the dimension of this dialogue act by inferring it from the communicative function.
     * Dimension-specific CFs are associated with their corresponding dimension by definition;
     * General-purpose CFs are associated with the task dimension by default and need
     * to be set to a specific dimension separately.
     */
    public final void inferDimensionFromCF() {
        if ( comm_funct.compareTo(CommFunction.autoPositive) < 0 )
            // for GP-CFs, task dimension by default
            dimension = Dimension.task;
        else if ( comm_funct.compareTo(CommFunction.alloPositive) < 0 )
            dimension = Dimension.autoFeedback;
        else if ( comm_funct.compareTo(CommFunction.stalling) < 0 )
            dimension = Dimension.alloFeedback;
        else if ( comm_funct.compareTo(CommFunction.initialGreeting) < 0 )
            dimension = Dimension.timeManagement;
        else
            dimension = Dimension.socialObligationsManagement;
    }

    /** returns the speaker of this dialogue act. */
    public AgentName getSpeaker() {
        return speaker;
    }

    /** sets the speaker of this dialogue act */
    public void setSpeaker( AgentName sp ) {
        speaker = sp;
    }

    /** returns the addressee of this dialogue act */
    public AgentName getAddressee() {
        return addressee;
    }

    /** sets the addressee of this dialogue act */
    public void setAddressee( AgentName addr ) {
        addressee = addr;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension( Dimension dim ) {
        dimension = dim;
    }
    
    public boolean isSystemAct() {
        return speaker == AgentName.system;
    }

    public boolean isUserAct() {
        return speaker == AgentName.user;
    }

    /** returns the communicative function of this dialogue act. */
    public CommFunction getCommFunction() {
        return comm_funct;
    }

    public void setCommFunction( CommFunction cf ) {
        comm_funct = cf;
        inferDimensionFromCF();
    }

    /** returns the semantic content of this dialogue act. */
    public SemanticContent getSemContent() {
        return sem_content;
    }

    /**
     * Adds a semantic item to the semantic content of this dialogue act.
     */
    public void addItem( SemanticItem sem_it ) {
    	//if ( !sem_content.contains(sem_it) )
    	sem_content.addItem( sem_it );
    }
    
    /**
     * Implementation of grounding action: user informing about item.
     */
    public boolean informsAbout( SlotValuePair svp ) {
        return ( comm_funct==CommFunction.inform && sem_content.containsSVP(svp) );
    }
    
    public void stateGoal( ArrayList<UserGoalItem> ug_items ) {
        setCommFunction( CommFunction.inform );
        sem_content.addGoalItems( ug_items );
    }

    /**
     * Implementation of grounding action: system requests the value for the given item.
     */
    public boolean requestsAbout( SlotValuePair svp ) {
        SlotValuePair svp1 = new SlotValuePair( svp.slot );
        return ( comm_funct==CommFunction.setQuestion && sem_content.containsSVP(svp1) );
    }

    /**
     * Implementation of grounding action: system explicitly confirms the given item.
     */
    public boolean explConfirms( SlotValuePair svp ) {
        return ( comm_funct==CommFunction.propQuestion && sem_content.containsSVP(svp) );
    }

    /**
     * Implementation of grounding action: user positively responds to 
     * system's explicit confirmation of the given item.
     */
    public boolean confirms( SlotValuePair svp ) {
        return ( comm_funct == CommFunction.confirm && sem_content.isConsistent(svp) );
    }
    
    public boolean denies( SlotValuePair svp ) {
        return ( comm_funct == CommFunction.disconfirm );
    }
    
	@Override
	public boolean equals( Hypothesis hyp ) {
		DialogueAct dact = (DialogueAct) hyp;
		return equals( dact );
    }
	
	public boolean equals( DialogueAct dact ) {
    	// check if CF and SC are equal to the given dact
    	if ( comm_funct != dact.getCommFunction() )
    		return false;
    	return sem_content.equals( dact.getSemContent() );
	}

    /**
     * Prints contents of this dialogue act to standard out.
     */
    public String toString() {
        String str = "";
        str += String.format( "Dialogue act: %s(%s)\n", comm_funct, sem_content.toString() );
        str += String.format( "   speaker: %s\n", speaker );
        str += String.format( "   addressee: %s\n", addressee );
        str += String.format( "   dimension: %s\n", dimension );
        str += String.format( "   confidence: %.2f\n", conf );
        return str;
    }

    /** Returns compact string showing CF and SC of this dialogue act */
    public String toShortString() {
        return toShortString( false );
    }
    
    public String toShortString( boolean withConf ) {
    	if ( withConf )
    		return String.format( "%s(%s) [%.3f]", comm_funct, sem_content.toString(), conf );
        return String.format( "%s(%s)", comm_funct, sem_content.toString() );
    }
    
    public static void mergeDuplicates( List<DialogueAct> nbest ) {
    	ArrayList<DialogueAct> toRemove = new ArrayList<DialogueAct>();
		for ( int i = 0; i < nbest.size(); i++ ) {
			DialogueAct hyp1 = nbest.get( i );
			for ( int j = 0; j < i; j++ ) {
				DialogueAct hyp2 = nbest.get( j );
				if ( hyp1.equals(hyp2) ) {
					hyp2.conf += hyp1.conf;
					toRemove.add( hyp1 );
					break;
				}
			}
		}
		nbest.removeAll( toRemove );
    }

    public int getNumItems() {
		int num_items = 0;
		for ( SemanticItem item: sem_content.getSemItems() )
			num_items += item.getNumItems();
		return num_items;
	}

}
