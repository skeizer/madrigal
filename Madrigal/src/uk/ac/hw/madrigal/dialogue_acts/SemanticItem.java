/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using dialogue acts from the multi-dimensional     *
 *     taxonomy underlying the ISO standard for semantic annotation ISO/DIS 24617-2.   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dialogue_acts;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.domain.UserGoalSVP;

/**
 * Class representing a semantic item, in the form of a list of slot-value pairs,
 * e.g. (drink=coke,quantity=2) == "two cokes"
 *
 * @author Simon Keizer
 */
public class SemanticItem {

    public ArrayList<SlotValuePair> svps;
    
    public SemanticItem() {
        svps = new ArrayList<SlotValuePair>();
    }

    public SemanticItem( SemanticItem sem_item ) {
//        svps = new ArrayList<SlotValuePair>( sem_item.svps ); // not deep copy
        svps = new ArrayList<SlotValuePair>();
        for ( SlotValuePair svp: sem_item.svps )
        	svps.add( new SlotValuePair(svp) );
    }
    
    public SemanticItem( UserGoalItem goal_item ) {
        svps = new ArrayList<SlotValuePair>();
        for ( UserGoalSVP ug_svp: goal_item.constraints )
            svps.add( ug_svp.getSlotValuePair() );
    }
    
    public void addSlotValuePair( SlotValuePair svp ) {
    	if ( !containsSVP(svp) )
    		svps.add( svp );
    }
    
    private boolean containsSVP( SlotValuePair svp ) {
    	for ( SlotValuePair p: svps ) {
    		if ( p.equals(svp) )
    			return true;
    	}
    	return false;
    }
    
    public SlotValuePair getFirstItem() {
        if ( svps.isEmpty() )
            return null;
        return svps.get( 0 );
    }
    
    public String getValueForSlot( String slot ) {
    	for ( SlotValuePair p: svps )
    		if ( p.slot.equals(slot) )
    			return p.value;
    	return "";
    }
    
    public boolean equals( SemanticItem other ) {
    	if ( svps.size() != other.svps.size() )
    		return false;
        for ( SlotValuePair oth_svp: other.svps ) {
        	if ( !contains(oth_svp) )
        		return false;
        }
    	return true;
    }
    
    public boolean contains( SlotValuePair other_svp ) {
    	for ( SlotValuePair svp: svps )
    		if ( other_svp.equals(svp) )
    			return true;
    	return false;
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public String toString() {
    	return StringUtils.join( svps );
//        String result = "";
//        boolean start = true;
//        for ( SlotValuePair svp: svps ) {
//            if ( start ) {
//                result = svp.toString();
//                start = false;
//            } else
//                result += "," + svp.toString();
//        }
//
//        return result;
    }

	public int getNumItems() {
		return svps.size();
	}
	
	public boolean isEmpty() {
		return svps.isEmpty();
	}
        
}
