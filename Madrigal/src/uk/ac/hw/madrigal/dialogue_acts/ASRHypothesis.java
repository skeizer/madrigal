package uk.ac.hw.madrigal.dialogue_acts;

import uk.ac.hw.madrigal.utils.Hypothesis;

public class ASRHypothesis extends Hypothesis {
	
	/** Recognised utterance */
	public String utterance;
	
	/** Constructor */
	public ASRHypothesis( String utt, double c ) {
		utterance = utt;
		conf = c;
	}

	@Override
	public boolean equals( Hypothesis hyp ) {
		ASRHypothesis asr_hyp = (ASRHypothesis) hyp;
		return equals( asr_hyp );
	}
	
	public boolean equals( ASRHypothesis asr_hyp ) {
		return utterance.equals( asr_hyp.utterance );
	}
	
	public String toString() {
		return String.format( "%s [%.3f]", utterance, conf );
	}

}
