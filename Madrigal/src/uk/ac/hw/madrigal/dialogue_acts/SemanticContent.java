/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using dialogue acts from the multi-dimensional     *
 *     taxonomy underlying the ISO standard for semantic annotation ISO/DIS 24617-2.   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dialogue_acts;

import java.util.ArrayList;

import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalItem;

/**
 * Class for representing the semantic content of a dialogue act.
 * It consists of a list of semantic items, each of which is given by 
 * a set of slot-value pairs.  This structure is also reflected in 
 * the user goal representation in the package domain.
 */
public class SemanticContent {
	
    /** Semantic items */
    private ArrayList<SemanticItem> semItems;

    /**
     * Default constructor: creates empty list of semantic items.
     */
    public SemanticContent() {
        semItems = new ArrayList<SemanticItem>();
    }

    /**
     * Copy constructor.
     */
    public SemanticContent( SemanticContent sc ) {
        semItems = new ArrayList<SemanticItem>();
        for ( SemanticItem sem_item: sc.semItems )
            semItems.add( new SemanticItem(sem_item) );
    }
    
    public ArrayList<SemanticItem> getSemItems() {
    	return semItems;
    }

    public SemanticItem getFirstItem() {
        if ( semItems.isEmpty() )
            return null;
        return semItems.get( 0 );
    }
    
    public SemanticItem getItemAt( int index ) {
        if ( semItems.size() >= index+1 )
            return semItems.get( index );
        return null;
    }
    
    public boolean isEmpty() {
        return semItems.isEmpty();
    }
    
    public int getNumItems() {
        return semItems.size();
    }

    /**
     * Checks if this semantic contains contains the given slot-value pair.
     *
     * @param svp slot-value pair
     * @return true if this semantic content contains the given slot-value pair.
     */
    public Boolean containsSVP( SlotValuePair svp ) {
    	for ( SemanticItem sem_item: semItems ) {
            for ( SlotValuePair sc_svp : sem_item.svps ) {
                if ( sc_svp.equals(svp) )
                    return true;
            }
        }
        return false;
    }
    
    /**
     * @param svp slot-value pair
     * @return true if all slot-value pairs in this semantic content are
     *              consistent with the given slot-value pair.
     */
    public Boolean isConsistent( SlotValuePair svp ) {
        return containsSVP( svp );
//        SemanticItem sem_item;
//        for ( Iterator sc_it = getIterator(); sc_it.hasNext(); ) {
//            sem_item = (SemanticItem) sc_it.next();
//            for ( SlotValuePair sc_svp : sem_item.svps ) {
//                if ( sc_svp.getSlot().equals(svp.getSlot()) && !sc_svp.getValue().equals(svp.getValue()) )
//                    return false;
//            }
//            
//        }
//        return true;
    }
    
    public void addGoalItems( ArrayList<UserGoalItem> ug_items ) {
        for( UserGoalItem ugi: ug_items )
            semItems.add( new SemanticItem(ugi) );
    }
    
    public boolean equals( SemanticContent other ) {
    	if ( semItems.size() != other.semItems.size() )
    		return false;
    	if ( semItems.isEmpty() )
    		return true;
    	//NOTE assuming one semantic item only!
    	SemanticItem sem_item = semItems.get( 0 );
    	SemanticItem other_sem_item = other.semItems.get( 0 );
    	return sem_item.equals( other_sem_item );
    }

    /**
     * @return string representation of semantic content.
     */
    @Override
    public String toString() {
        String result = "";
        Boolean start = true;
        for ( SemanticItem sem_item: semItems ) {
            if ( start ) {
                result = sem_item.toString();
                start = false;
            } else
                result += ";" + sem_item.toString();
        }
        return result;
    }

    public void clear() {
        semItems.clear();
    }

    void addItem( SemanticItem sem_it ) {
    	if ( !semItems.contains(sem_it) )
    		semItems.add( sem_it );
    }

	public void removeItem( SemanticItem sem_item ) {
		semItems.remove( sem_item );
	}

//	public boolean contains( SemanticItem sem_it ) {
//		return semItems.contains( sem_it );
//	}
    
}
