package uk.ac.hw.madrigal.dialogue_acts;

/**
 * Class for representing communicative functions.  Note that this class is 
 * not used in the current system; an enumeration in class DialogueAct is sufficient.
 */
public class CommFunction {
	
	private String name;
	
	private String dimension;
	
	private String parent;
	
	public CommFunction( String n, String p, String d ) {
		name = n;
		parent = p;
		dimension = d;
	}

	public CommFunction( String n, String d ) {
		name = n;
		dimension = d;
		parent = "";
	}
	
	public String getName() {
		return name;
	}
	
	public String getDimension() {
		return dimension;
	}
	
	public String getParent() {
		return parent;
	}

}
