/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using dialogue acts from the multi-dimensional     *
 *     taxonomy underlying the ISO standard for semantic annotation ISO/DIS 24617-2.   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dialogue_acts;

import org.apache.commons.lang3.StringUtils;

import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.SlotValuePair.SpecialValue;

/**
 * Class containing static convenience methods related to dialogue acts.
 */
public class DialActUtils {

	public static DialogueAct newUserAct( DialogueAct.CommFunction cf ) {
		DialogueAct dact = new DialogueAct( cf );
		dact.inferDimensionFromCF();
	    dact.setSpeaker( AgentName.user );
	    dact.setAddressee( AgentName.system );
	    return dact;
	}

	public static DialogueAct newUserAct( DialogueAct.CommFunction cf, SlotValuePair svp ) {
		DialogueAct dact = new DialogueAct( cf, svp );
		dact.inferDimensionFromCF();
	    dact.setSpeaker( AgentName.user );
	    dact.setAddressee( AgentName.system );
	    return dact;
	}

	public static DialogueAct newSystemAct( DialogueAct.CommFunction cf ) {
		return newSystemAct( cf, null );
	}

	public static DialogueAct newSystemAct( DialogueAct.CommFunction cf, SlotValuePair svp ) {
		DialogueAct dact;
		if ( svp == null )
			dact = new DialogueAct( cf );
		else
			dact = new DialogueAct( cf, svp );
		dact.inferDimensionFromCF();
		dact.setSpeaker( AgentName.system );
		dact.setAddressee( AgentName.user );
		return dact;
	}
	
	/**
	 * Returns a DialogueAct object corresponding to the given string representation.
	 * For example inform(area:centre) returns a DialogueAct with communicative function CommFunct.inform
	 * and a semantic content containing one semantic item with a single SlotValuePair (area,center).
	 */
	public static DialogueAct newDialogueActFromString( String dial_act_str ) {
		String[] da_elts = StringUtils.split( dial_act_str, "(,)" );
		String cf_str = da_elts[0];
		DialogueAct dial_act = new DialogueAct( newCommFunctFromString(cf_str) );
		SlotValuePair svp;
		SemanticItem sem_item = new SemanticItem();
		for ( int i = 1; i < da_elts.length; i++ ) {
			svp = newSlotValuePairFromString( da_elts[i] );
			sem_item.addSlotValuePair( svp );
		}
		dial_act.addItem( sem_item );
		return dial_act;
	}
	
	/**
	 * Returns a SlotValuePair object corresponding to the given string representation.
	 */
	public static SlotValuePair newSlotValuePairFromString( String svp_str ) {
		String[] svp_elts = StringUtils.split( svp_str, ":" );
		if ( svp_elts.length == 1 )
			return new SlotValuePair( svp_elts[0] );
		return new SlotValuePair( svp_elts[0], svp_elts[1] );
	}
	
	/**
	 * Returns a CommFunction corresponding to the given string representation.
	 */
	public static CommFunction newCommFunctFromString( String cf_str ) {
		CommFunction cf;
		try {
			cf = CommFunction.valueOf( cf_str );
		} catch ( IllegalArgumentException iae ) {
			return CommFunction.other;			
		}
		return cf;
	}

	/**
	 * Generates a string representation of the given dialogue act,
	 * to be used by the TemplateNLG natural language generator.
	 * Only works for dialogue acts where the semantic content has
	 * at most one semantic item (consisting of a list of slot value pairs). 
	 */
	public static String getStringForNLG( DialogueAct dact ) {
		
		DialogueAct nlg_act = new DialogueAct( dact );
		// NLG[ disconfirm(name=X,area=Y) ] --> "No, " + NLG[ inform(name=X,area=Y) ]
//		if ( nlg_act.getCommFunction() == CommFunction.disconfirm && !nlg_act.getSemContent().isEmpty() )
//			nlg_act.setCommFunction( CommFunction.inform );
		
		for( SemanticItem sem_item : nlg_act.getSemContent().getSemItems() )
			for ( SlotValuePair svp : sem_item.svps ) {
//				if ( svp.slot.equals("name") && svp.value.contains(",") )
//					svp.value = "\"" + svp.value + "\"";
				svp.value = StringUtils.remove( svp.value, ',' );
				if ( svp.slot.equals("area") ) {
					if ( svp.value.equals("citycentre") )
						svp.value = "city centre";
					else if ( svp.value.equals("newchesterton") )
						svp.value = "new chesterton";
					else if ( svp.value.equals("kingshedges") )
						svp.value = "kings hedges";
					else if ( svp.value.equals("fenditton") )
						svp.value = "fen ditton";
				} else if ( svp.slot.equals("food") ) {
					if ( svp.value.equals("Middleeastern") )
						svp.value = "Middle Eastern";
					else if ( svp.value.endsWith("food") )
						svp.value = svp.value.substring( 0, svp.value.length()-4 ).trim();
				}
					
			}
		
		String dact_str = nlg_act.toShortString();
		dact_str = StringUtils.remove( dact_str, '[' );
		dact_str = StringUtils.remove( dact_str, ']' );
		dact_str = StringUtils.replaceChars( dact_str, ':', '=' );
		
		return dact_str;
	}

	public static boolean isNonTrivial( DialogueTurn turn ) {
		if ( !turn.dact_nbest_all.isEmpty() ) {
			SemanticContent sc = turn.dact_nbest_all.get(0).getSemContent();
			if ( !sc.isEmpty() ) { // semantic content has at least one semantic item with at least one slot-value pair
				String dontcareStr = SlotValuePair.getString( SpecialValue.DONTCARE );
				boolean hasEmptySlot = sc.getFirstItem().getFirstItem().slot.isEmpty();
				boolean hasDontcare = sc.getFirstItem().getFirstItem().value.equals( dontcareStr );
				if ( !hasEmptySlot || !hasDontcare ) // inform( =dontcare ) excluded
					return true;
			}
		}
		return false;
	}
}
