/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created November 2016                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using dialogue acts from the multi-dimensional     *
 *     taxonomy underlying the ISO standard for semantic annotation ISO/DIS 24617-2.   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dialogue_acts;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Class representing a turn in a dialogue, associated with a speaker and addressee.
 * For each dimension of the taxonomy, a turn typically contains zero or one dialogue act;
 * any additional dialogue acts must be realised sequentially.
 */
public class DialogueTurn {
	
    /** Enumeration of speaker roles. */
    public enum AgentName {
    	user,
    	system,
    	unknown
    }

    /** Enumeration of possible processing levels reached */
    public enum ProcLevel {
    	NONE,             // 
    	ATTENTION,        // speaker has attention of addressee
    	PERCEPTION,       // successful speech recognition
    	INTERPRETATION,   // successful semantic parsing
    	EVALUATION,       // successful integration into context
    	EXECUTION         // successful acting upon this turn (executing instruction, answering question, etc.)
    }
    
    /** speaker of this dialogue act */
    private AgentName speaker;

    /** addressee of this dialogue act */
    private AgentName addressee;
    
    /** Processing level reached for this turn */
    private ProcLevel proc_level;
    
    /** textual utterance for this turn */
    private String utterance;
    
    public ArrayList<ASRHypothesis> asr_hyps;
    
    public DialogueAct dact_task;
    public DialogueAct dact_auto_fb;
    public DialogueAct dact_som;
    public DialogueAct dact_all;
    
    public ArrayList<DialogueAct> dact_nbest_task;     // task dimension dialogue act hypotheses
	public ArrayList<DialogueAct> dact_nbest_auto_fb;  // auto-feedback
	public ArrayList<DialogueAct> dact_nbest_som;      // social obligations management
    public ArrayList<DialogueAct> dact_nbest_all;      // one-dimensional version
	
	public DialogueTurn() {
		this( AgentName.unknown, AgentName.unknown );
	}

	public DialogueTurn( AgentName sp, AgentName ad ) {
        speaker = sp;
        addressee = ad;
        proc_level = ProcLevel.ATTENTION;
        utterance = "";
        asr_hyps = new ArrayList<ASRHypothesis>();
		dact_nbest_task = new ArrayList<DialogueAct>();
		dact_nbest_auto_fb = new ArrayList<DialogueAct>();
		dact_nbest_som = new ArrayList<DialogueAct>();
		dact_nbest_all = new ArrayList<DialogueAct>();
	}

    public boolean isSystemTurn() {
        return speaker == AgentName.system;
    }

    public boolean isUserTurn() {
        return speaker == AgentName.user;
    }

    public void setSpeaker( AgentName an ) { speaker = an; }
    public AgentName getSpeaker() { return speaker; }
    public void setAddressee( AgentName an ) { addressee = an; }
    public AgentName getAddressee() { return addressee; }
    public void setUtterance( String utt ) { utterance = utt; } 
    public String getUtterance() { return utterance; }
    public void setProcLevel( ProcLevel lvl ) { proc_level = lvl; }
    public ProcLevel getProcLevel() { return proc_level; }
    
    public void addDialActHypTurn( DialogueTurn dact_turn, double asr_conf ) {
		if ( !dact_turn.dact_nbest_all.isEmpty() ) {
			Utils.eltMultScalar( dact_turn.dact_nbest_all, asr_conf );
			//return
			Utils.merge( dact_nbest_all, dact_turn.dact_nbest_all, true );
		} //else { //TODO sort this out when moving to fully multi-dim system!!!
//			Utils.eltMult( dact_turn.dact_nbest_task, asr_conf );
//	    	Utils.merge( dact_nbest_task, dact_turn.dact_nbest_task );
//			Utils.eltMult( dact_turn.dact_nbest_auto_fb, asr_conf );
//	    	Utils.merge( dact_nbest_auto_fb, dact_turn.dact_nbest_auto_fb );
//			Utils.eltMult( dact_turn.dact_nbest_som, asr_conf );
//	    	Utils.merge( dact_nbest_som, dact_turn.dact_nbest_som );
		//}
//		return 0d;
    }
    
    /**
     * Incorporates the given dialogue act into this (multidimensional) dialogue turn,
     * currently using a small series of combination rules.
     * TODO move to DialActUtils ?
     */
    public void setDialAct( DialogueAct dact ) {
    	
    	if ( dact == null )
    		return; // nothing to do
    	
    	if ( dact.getDimension() == Dimension.all )
    		dact_all = dact; // one-dimensional version; only one dialogue act to incorporate (one call to this method per turn)
    	
    	else { // handle specific dimensions
        	
        	if ( dact.getDimension() == Dimension.task ) {
	    		dact_task = dact;
	        	dact_all = dact; // first dimension checked: store task act if any
	        	
	    	} else if ( dact.getDimension() == Dimension.socialObligationsManagement ) {
	    		dact_som = dact;
	    		if ( dact_all == null )
	    			dact_all = dact; // only keep SOM act if no other candidates available
	    		
	    	} else if ( dact.getDimension() == Dimension.autoFeedback ) {
	    		dact_auto_fb = dact;
	    		if ( dact != null )
	    			dact_all = dact; // if auto-feedback act not empty, overwrite any task act
	    		
	    	}
    	}
    }
    
    public String toString() {
    	String prompt = ( speaker == AgentName.system ? "SysTurn> " : "UsrTurn> " );
    	String procLvl = ( speaker == AgentName.system ? "" : "" + proc_level );
    	String utt = "\n" + ( speaker == AgentName.system ? "Sys> " : "Usr> ") + utterance;
    	String uact = ( speaker == AgentName.system ? "\nSact> " : "\nUact> " ) + ( dact_all == null ? "<unknown>" : dact_all.toShortString() );
    	String str = prompt + procLvl + utt + uact + "\n";
    	if ( proc_level != ProcLevel.ATTENTION && proc_level != ProcLevel.PERCEPTION )
    		for ( DialogueAct dact: dact_nbest_all )
    			str += String.format( "Sem> %s\n", dact.toShortString( true ) );
    	//TODO print da-hyps for multi-dim case
    	return str;
    }
    
    public JSONObject toJSON() throws JSONException {
    	
    	JSONObject obj = new JSONObject();

    	if ( speaker == AgentName.system ) {
    		obj.put( "sys_act", dact_all.toShortString() );
    		obj.put( "utterance", utterance );

    	} else if ( speaker == AgentName.user ) {
        	obj.put( "proc_level", proc_level );
	    	JSONArray asr_hyps_arr = new JSONArray();
	    	JSONObject asr_hyp_obj;
	    	for ( ASRHypothesis hyp: asr_hyps ) {
	    		asr_hyp_obj = new JSONObject();
	    		asr_hyp_obj.put( "utterance", hyp.utterance );
	    		asr_hyp_obj.put( "confidence", hyp.getConf() );
	    		asr_hyps_arr.put( asr_hyp_obj );
	    	}
	    	obj.put( "asr_hyps", asr_hyps_arr );
	    	
	    	obj.put( "transcription", utterance );
	    	
	    	JSONArray sem_hyps_arr = new JSONArray();
	    	JSONObject sem_hyp_obj;
	    	for ( DialogueAct hyp: dact_nbest_all ) {
	    		sem_hyp_obj = new JSONObject();
	    		sem_hyp_obj.append( "dial_act", hyp.toShortString() );
	    		//TODO above line should be: sem_hyp_obj.put( "dial_act", hyp.toShortString() );
	    		sem_hyp_obj.append( "confidence", hyp.getConf() );
	    		//TODO see above
	    		sem_hyps_arr.put( sem_hyp_obj );
	    	}
	    	obj.put( "sem_hyps", sem_hyps_arr );
	    	
    	}
    	
    	return obj;
    }
    
}
