/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for representing and using dialogue acts from the multi-dimensional     *
 *     taxonomy underlying the ISO standard for semantic annotation ISO/DIS 24617-2.   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dialogue_acts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import resources.Resources;

/**
 * Class for loading a dialogue act taxonomy from a file.  The taxonomy consists
 * of a set of (general-purpose and dimension-specific) communicative functions 
 * and a set of dimensions.  Note that this class is not used in the current system.
 */
public class Taxonomy {
	
	private ArrayList<CommFunction> comm_functions;
	
	private HashSet<String> dimensions;
	
	public Taxonomy( String json_fname ) {
		comm_functions = new ArrayList<CommFunction>();
		dimensions = new HashSet<String>();
		try {
			//String inputJsonString = Utils.readFile( json_fname );
			String inputJsonString = Resources.readResourceFile( json_fname );
			JSONObject root = new JSONObject( inputJsonString );
			//System.out.println( root.toString() + "\n" );

			JSONObject tax_obj = root.getJSONObject( "dialogue_act_taxonomy" );
			//System.out.println( tax_obj.toString() );

			JSONArray cf_arr = tax_obj.getJSONArray( "communicative_function" );
			JSONObject cf_obj;
			String cf_str, par_str, dim_str;
			for ( int i = 0; i < cf_arr.length(); i++ ) {
				cf_obj = cf_arr.getJSONObject( i );
				cf_str = cf_obj.getString( "name" );
				par_str = cf_obj.getString( "broader_concept" );
				dim_str = cf_obj.getString( "dimension" );
				comm_functions.add( new CommFunction(cf_str,par_str,dim_str) );
				dimensions.add( dim_str );
			}

		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		} catch ( JSONException e ) {
			e.printStackTrace();
		}
		
	}
	
	public String getDimension( String cf_name ) {
		for ( CommFunction cf : comm_functions ) {
			if ( cf.getName().equals(cf_name) )
				return cf.getDimension();
		}
		return null; // ERROR: cf_name does not exist! 
	}

}
