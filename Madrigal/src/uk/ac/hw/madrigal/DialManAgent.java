/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created July 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.utils.MyLogger;

public abstract class DialManAgent implements DialogueAgentInterface {

	protected static MyLogger logger;
	
	public static Random rand_num_gen;
	
	public static Ontology domain;
	protected String primeSlot = domain.primarySlot(); //.getPrimarySlot();
	
	//protected int num_dialogues; //TODO check if made redundant by new member: dial_num

	protected InfoState info_state;

    /** Dialogue number of the current session (used for logging) */
	int dial_num;

	/** Turn number of the current dialogue (used for logging) */
	int turn_num;

	/** Constructor: initialises the info state. */
	public DialManAgent() {
		info_state = new InfoState();
		//num_dialogues = 0;
		dial_num = 0;
		turn_num = 0;
	}

	public void receive( DialogueAct dial_act ) {
		logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_num );
    	logger.logf( Level.INFO, "Receiving user act: %s\n", dial_act.toShortString( true ) );
		info_state.updateWithUserDialAct( dial_act );
		logger.logf( Level.FINE, "New info state:\n%s\n", info_state.toString() );
	}

	public void receive( ArrayList<DialogueAct> da_hyps ) {
		logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_num );
    	logger.logf( Level.INFO, "Receiving user act hyps:\n" );
		for ( DialogueAct da_hyp: da_hyps )
			logger.logf( Level.INFO, "  Sem> %s\n", da_hyp.toShortString(true) );
		info_state.updateWithUserDialAct( da_hyps );
		logger.logf( Level.FINE, "New info state:\n%s\n", info_state.toString() );
	}
	
	@Override
	public void receive( DialogueTurn usr_turn ) {
		logger.logf( Level.INFO, "\n----- Turn %d -----\n", turn_num );
    	logger.logf( Level.INFO, "Receiving user turn:\n%s\n", usr_turn.toString() );
		info_state.updateWithUserTurn( usr_turn );
		logger.logf( Level.FINE, "New info state:\n%s\n", info_state.toString() );
	}
	
	public void reset() {
		turn_num = 0;
		logger.logf( Level.INFO, "\n===== Dialogue %d =====\n", dial_num++ );
		logger.log( Level.FINE, "Resetting info state\n" );
		info_state.init(); // = new InfoState();
	}

	@Override
	public abstract DialogueTurn respond();
	
	// to be overridden by learning DialManAgents
	public void observeReward( double reward ) {}
	
	// to be overridden by learning DialManAgents
	public void update( boolean success ) {}
	
	//public void savePolicies( String name_prefix ) {}
	
	public void computeStatistics() {}

}
