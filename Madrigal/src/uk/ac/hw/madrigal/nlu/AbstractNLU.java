/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created February 2017                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Natural Language Understanding (NLU) package.                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.nlu;

import java.util.ArrayList;

import uk.ac.hw.madrigal.dialogue_acts.ASRHypothesis;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.utils.MyLogger;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Spoken Language Understanding component.
 * 
 * @author skeizer
 */
public abstract class AbstractNLU {

	public static MyLogger logger;

	public static Ontology domain;

	/** 
	 * Returns a DialogueTurn object containing dialogue act hypotheses for the given
	 * natural language utterance.
	 */
	public abstract DialogueTurn getSemantics( String utterance );
		
	public DialogueTurn getSemantics( ArrayList<ASRHypothesis> asr_hyps ) {
		DialogueTurn dial_turn = new DialogueTurn( AgentName.user, AgentName.system );
		dial_turn.setProcLevel( ProcLevel.PERCEPTION );
		dial_turn.asr_hyps = new ArrayList<ASRHypothesis>( asr_hyps );
		DialogueTurn hypTurn;
		boolean noProcProblems = false;
		double asrConfResidue = 1;
		for ( ASRHypothesis hyp: asr_hyps ) {
			hypTurn = getSemantics( hyp.utterance );
			if ( hypTurn.getProcLevel() == ProcLevel.INTERPRETATION ) {
				//dial_turn.addDialActHypTurn( hypTurn, hyp.getConf() );
				dial_turn.dact_nbest_all.addAll( hypTurn.dact_nbest_all ); //NOTE in practice, only one SLU hypothesis is added
				asrConfResidue -= hyp.getConf();
				noProcProblems = true;
			}
		}
		if ( noProcProblems ) {
			dial_turn.setProcLevel( ProcLevel.INTERPRETATION );
			 // normalising SLU conf scores only
			double sluConfResidue = Utils.pseudo_normalise( dial_turn.dact_nbest_all );
			// incorporate ASR confidence scores (element-wise multiplication)
			Utils.eltMult( dial_turn.dact_nbest_all, asr_hyps );
			// re-normalise
			Utils.normalise( dial_turn.dact_nbest_all, asrConfResidue * sluConfResidue );
			// finally, merge identical dialogue act hypotheses
			Utils.merge( dial_turn.dact_nbest_all );
		}
		return dial_turn;
	}
	
}
