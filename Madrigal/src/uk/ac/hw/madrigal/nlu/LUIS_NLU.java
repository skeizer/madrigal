/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created February 2017                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Natural Language Understanding (NLU) package.                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.nlu;

import java.util.logging.Level;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.domain.SlotValuePair;

/**
 * Spoken Language Understanding component using LUIS application for restaurant domain.
 * 
 * @author skeizer
 */
public class LUIS_NLU extends Abstract_LUIS_NLU {
	
	protected DialogueTurn getTurnFromLUISobj( JSONObject luis_root ) throws JSONException {
		
		DialogueTurn usr_turn = new DialogueTurn( AgentName.user, AgentName.system );
	    // intent top hypothesis
		JSONObject top_intent_obj = luis_root.getJSONObject( "topScoringIntent" ); //TODO also include alternative intents
	    JSONArray ents_arr_obj = luis_root.getJSONArray( "entities" );
	    String cf_str = top_intent_obj.getString( "intent" );
	    double dial_act_conf = top_intent_obj.getDouble( "score" );
	    CommFunction cf;
	    DialogueAct usr_act;
	    
    	cf = DialActUtils.newCommFunctFromString( cf_str );
	    if ( cf == CommFunction.other )
	    	usr_turn.setProcLevel( ProcLevel.PERCEPTION );
	    else {
	    	usr_turn.setProcLevel( ProcLevel.INTERPRETATION );
	    	usr_act = DialActUtils.newUserAct( cf );
			// handle detected entities
			JSONObject attr_obj;
			String slot, value, corr_slot, corr_val;
			SlotValuePair svp;
			SemanticItem sem_item = new SemanticItem();
			for ( int i = 0; i < ents_arr_obj.length(); i++ ) {
				attr_obj = ents_arr_obj.getJSONObject( i );
				slot = attr_obj.getString( "type" );
				if ( slot.equals(domain.getPrimarySlot()) && cf == CommFunction.inform )
					continue; // skip item name=...
				dial_act_conf *= attr_obj.getDouble( "score" );
				if ( slot.equals("dontcare") )
					svp = new SlotValuePair( "", dontcare_value );
				else {
					value = attr_obj.getString( "entity" );
					if ( slot.equals("requestedSlot") ) {
						//corr_slot = domain.getCorrectSlot( value );
						corr_slot = domain.getDomainSlot( value );
						svp = new SlotValuePair( corr_slot );
					} else {
						//corr_val = domain.getCorrectValue( slot, value );
						if ( value.equals(luis_dontcare_entity_str) )
							value = dontcare_value;
						corr_val = domain.getDomainValue( slot, value );
						if ( !corr_val.isEmpty() )
							svp = new SlotValuePair( slot, corr_val );
						else
							svp = null;
					}
				}
				if ( svp != null )
					sem_item.addSlotValuePair( svp );
			}
			if ( !sem_item.isEmpty() )
				usr_act.addItem( sem_item );
			usr_act.setConf( dial_act_conf );
			logger.logf( Level.INFO, "Recognised user act: %s\n", usr_act.toShortString(true) );
			usr_turn.dact_nbest_all.add( usr_act );
	    }

		return usr_turn;
	}
	
}
