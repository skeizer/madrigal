/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2017 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created February 2017                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Natural Language Understanding (NLU) package.                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.nlu;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Scanner;
import java.util.logging.Level;

import org.json.JSONException;
import org.json.JSONObject;

import resources.Configuration;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.domain.SlotValuePair;

/**
 * Spoken Language Understanding component using a LUIS application (specific domain to be implemented in subclass).
 * 
 * @author skeizer
 */
public abstract class Abstract_LUIS_NLU extends AbstractNLU {
	
	/** http end-point for LUIS application */
	private static String LUIS_URL_PREFIX = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/";
	private static String LUIS_APP;
	private static String LUIS_KEY;
	
	protected static String charset = java.nio.charset.StandardCharsets.UTF_8.name();
	
//	public static Ontology domain;
	
	private String LUIS_URL;
	protected String dontcare_value;
	protected String luis_dontcare_entity_str;
	
	public Abstract_LUIS_NLU() {
		dontcare_value = SlotValuePair.getString( SlotValuePair.SpecialValue.DONTCARE );
		luis_dontcare_entity_str = "don ' t care"; // returned by LUIS when serving as entity
		LUIS_URL = LUIS_URL_PREFIX + LUIS_APP + "?subscription-key=" + LUIS_KEY + "&verbose=true";
	}

	public DialogueTurn getSemantics( String utterance ) {
		
		logger.logf( Level.INFO, "utterance: %s\n", utterance );
		DialogueTurn usr_turn = new DialogueTurn( AgentName.user, AgentName.system );
		usr_turn.setUtterance( utterance ); //NOTE only relevant for text input system (with speech the argument is an ASR hypothesis)

		URLConnection connection;
		try {
			String utt_url_str = "&q=" + URLEncoder.encode( utterance, charset );
			logger.logf( Level.INFO, "encoded: %s\n", utt_url_str );
			
			connection = new URL( LUIS_URL + utt_url_str ).openConnection();
			InputStream response = connection.getInputStream();
			
			try ( Scanner scanner = new Scanner( response ) ) {
				String responseBody = scanner.useDelimiter( "\\A" ).next();
				JSONObject root = new JSONObject( responseBody );
				logger.logf( Level.INFO, "LUIS output:\n%s\n", root.toString(5) );
				usr_turn = getTurnFromLUISobj( root );
				//usr_turn.setUtterance( utterance );
				logger.logf( Level.INFO, "user turn: %s\n", usr_turn.toString() );
			} catch ( JSONException je ) {
				je.printStackTrace();
			}

		} catch ( MalformedURLException mue ) {
			mue.printStackTrace();
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		}
		
		return usr_turn;

	}

	protected abstract DialogueTurn getTurnFromLUISobj( JSONObject luis_root ) throws JSONException;

	public static void loadConfig() {
		LUIS_APP = Configuration.LUIS_APP;
		LUIS_KEY = Configuration.LUIS_KEY;

        StringBuffer logStr = new StringBuffer( "NLU configuration:\n" );
        logStr.append( String.format( "   LUIS_APP: %s\n", LUIS_APP ) );
        logStr.append( String.format( "   LUIS_KEY: %s\n", LUIS_KEY ) );
        logStr.append( "\n" );
        logger.log( Level.WARNING, logStr.toString() );
	}

}
