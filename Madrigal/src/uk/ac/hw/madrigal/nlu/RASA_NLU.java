/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                           Copyright (C) 2018                                        *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Created December 2018                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Natural Language Understanding (NLU) package.                                   *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.nlu;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Scanner;
import java.util.logging.Level;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.domain.SlotValuePair;

public class RASA_NLU extends AbstractNLU {
	
	private static String RASA_NLU_HOST = "http://localhost";
	private static int RASA_NLU_PORT = 5000;
	private static String RASA_NLU_PROJECT = "current";
	private static String RASA_NLU_MODEL = "nlu";

	protected static String charset = java.nio.charset.StandardCharsets.UTF_8.name();
	
//	public static Ontology domain;
	
	protected String dontcare_value;
	protected String proj_mdl_str;

	public RASA_NLU() {
		dontcare_value = SlotValuePair.getString( SlotValuePair.SpecialValue.DONTCARE );
		proj_mdl_str = "project=" + RASA_NLU_PROJECT + ";model=" + RASA_NLU_MODEL;
	}

	@Override
	public DialogueTurn getSemantics( String utterance ) {

		logger.logf( Level.INFO, "utterance: %s\n", utterance );
		DialogueTurn usr_turn = new DialogueTurn( AgentName.user, AgentName.system );
		usr_turn.setUtterance( utterance ); //NOTE only relevant for text input system (with speech the argument is an ASR hypothesis)

		// curl -XPOST localhost:5000/parse -d '{"q":"hI want to find an Ialian restaurant", "project":"current","model":"nlu"}'
		
		URLConnection connection;
		try {
			String utt_url_str = "?q=" + URLEncoder.encode( utterance, charset );
			String url_str = RASA_NLU_HOST + ":" + RASA_NLU_PORT + "/parse" + utt_url_str + ";" + proj_mdl_str;
			logger.logf( Level.INFO, "encoded url: %s\n", url_str );
			
			connection = new URL( url_str ).openConnection();
			InputStream response = connection.getInputStream();
			
			try ( Scanner scanner = new Scanner( response ) ) {
				String responseBody = scanner.useDelimiter( "\\A" ).next();
				JSONObject root = new JSONObject( responseBody );
				logger.logf( Level.INFO, "RASA output:\n%s\n", root.toString(5) );
				usr_turn = getTurnFromRASAobj( root );
				//usr_turn.setUtterance( utterance );
				logger.logf( Level.INFO, "user turn: %s\n", usr_turn.toString() );
			} catch ( JSONException je ) {
				logger.logf( Level.INFO, "RASA JSONException:\n%s\n", je.toString() );
				je.printStackTrace();
			}

		} catch ( MalformedURLException mue ) {
			mue.printStackTrace();
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		}
		
		return usr_turn;

	}
	
	protected DialogueTurn getTurnFromRASAobj( JSONObject rasa_root ) throws JSONException {
			
		DialogueTurn usr_turn = new DialogueTurn( AgentName.user, AgentName.system );
		// intent top hypothesis
		JSONObject top_intent_obj = rasa_root.getJSONObject( "intent" ); //TODO also include alternative intents ("intent_ranking")
		JSONArray ents_arr_obj = rasa_root.getJSONArray( "entities" );
		String cf_str = top_intent_obj.getString( "name" );
		double dial_act_conf = top_intent_obj.getDouble( "confidence" );
		CommFunction cf;
		DialogueAct usr_act;
		    
    	cf = DialActUtils.newCommFunctFromString( cf_str );
	    if ( cf == CommFunction.other )
	    	usr_turn.setProcLevel( ProcLevel.PERCEPTION );
	    else {
	    	usr_turn.setProcLevel( ProcLevel.INTERPRETATION );
	    	usr_act = DialActUtils.newUserAct( cf );
			// handle detected entities
			JSONObject attr_obj;
			String slot, value, corr_slot, corr_val;
			SlotValuePair svp;
			SemanticItem sem_item = new SemanticItem();
			for ( int i = 0; i < ents_arr_obj.length(); i++ ) {
				attr_obj = ents_arr_obj.getJSONObject( i );
				slot = attr_obj.getString( "entity" );
				if ( slot.equals(domain.getPrimarySlot()) && cf == CommFunction.inform )
					continue; // skip item name=...
				dial_act_conf *= attr_obj.getDouble( "confidence" );
				if ( slot.equals("dontcare") )
					svp = new SlotValuePair( "", dontcare_value );
				else {
					value = attr_obj.getString( "value" );
					if ( slot.equals("requestedSlot") ) {
						//corr_slot = domain.getCorrectSlot( value );
						corr_slot = domain.getDomainSlot( value );
						svp = new SlotValuePair( corr_slot );
					} else {
						//corr_val = domain.getCorrectValue( slot, value );
						corr_val = domain.getDomainValue( slot, value );
						if ( !corr_val.isEmpty() )
							svp = new SlotValuePair( slot, corr_val );
						else
							svp = null;
					}
				}
				if ( svp != null )
					sem_item.addSlotValuePair( svp );
			}
			if ( !sem_item.isEmpty() )
				usr_act.addItem( sem_item );
			usr_act.setConf( dial_act_conf );
			logger.logf( Level.INFO, "Recognised user act: %s\n", usr_act.toShortString(true) );
			usr_turn.dact_nbest_all.add( usr_act );
	    }

		return usr_turn;
	}

}
