/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016 (code from 2012)                                              *
 *     ---------------------------------------------------------------------------     *
 *     MDP RL agent software package.                                                  *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.logging.Level;

import resources.Configuration;

public class LVFAPolicy extends Policy {
	
	private static boolean update_tabular_values = true;
	private static boolean full_state_exploration = true;
	//private static boolean saveTabularValues = false;
	private static boolean init_bin_feat_space = false;
	
	private static String FILE_PREFIX = "lvfa";

	/** Weights associated with each (binary) feature of the state-action space */
	double[][] value_function_weights;

	public LVFAPolicy( TabularMDP mdp, int ns, int na ) {
		super( mdp, ns, na );
		num_binary_feats = mdp_model.getNumBinaryFeatures();
		value_function_weights = new double[ num_actions ][ num_binary_feats ];
		if ( init_bin_feat_space )
			initBinaryFeatureSpace();
	}
	
	int getBestActionIndex( double[] state_feats ) {
		double max_value = 0;
		boolean first = true;
		ArrayList<Integer> max_indices = new ArrayList<Integer>();
		for ( int i = 0; i < num_actions; i++ ) {
			double val = value_function_weights[ i ][ 0 ];
			for ( int k = 1; k < num_binary_feats; k++ )
				//val += value_function_weights[i][k] * ( binary_feature_space[state_index][k] ? 1 : 0 );
				//val += value_function_weights[i][k] * binary_feature_space[state_index][k];
				val += value_function_weights[i][k] * state_feats[k];
			if ( first ) {
				first = false;
				max_value = val;
				max_indices.add( i );
			} else if ( val > max_value ) {
				max_value = val;
				max_indices.clear();
				max_indices.add( i );
			} else if ( val == max_value ) {
				max_indices.add( i );
			}
		}
		int ri = rand_num_gen.nextInt( max_indices.size() );
		Integer rio = (Integer) max_indices.get( ri );
		return rio.intValue();
	}

	int getBestActionIndex( int state_index ) {
		int[] binary_feats = getBinaryFeatures( state_index );
		double[] numeric_feats = new double[ mdp_model.num_state_features ];
		for ( int i = 0; i < mdp_model.num_state_features; i++ )
			numeric_feats[i] = binary_feats[i];
		return getBestActionIndex( numeric_feats );
	}
	
//	int getBestActionIndex( int state_index ) {
//		double max_value = 0;
//		boolean first = true;
//		ArrayList<Integer> max_indices = new ArrayList<Integer>();
//		int[] binary_feats = getBinaryFeatures( state_index );
//		for ( int i = 0; i < num_actions; i++ ) {
//			double val = value_function_weights[ i ][ 0 ];
//			for ( int k = 1; k < num_binary_feats; k++ )
//				//val += value_function_weights[i][k] * ( binary_feature_space[state_index][k] ? 1 : 0 );
//				//val += value_function_weights[i][k] * binary_feature_space[state_index][k];
//				val += value_function_weights[i][k] * binary_feats[k];
//			if ( first ) {
//				first = false;
//				max_value = val;
//				max_indices.add( i );
//			} else if ( val > max_value ) {
//				max_value = val;
//				max_indices.clear();
//				max_indices.add( i );
//			} else if ( val == max_value ) {
//				max_indices.add( i );
//			}
//		}
//		int ri = rand_num_gen.nextInt( max_indices.size() );
//		Integer rio = (Integer) max_indices.get( ri );
//		return rio.intValue();
//	}

	@Override
	public double updatePolicy() {

		double accReward = 0;
		double total_weight_change = 0;
		double lrate;

		double delta;
		double delta1;
		for ( StateActionPair sapair : state_action_history ) {
	            
			int[] binary_feats = getBinaryFeatures( sapair.state_index );

			accReward += sapair.reward;
	        
			addNumVisitsAt( sapair.state_index, sapair.action_index, 1 );
			double currVal = getValueAt( sapair.state_index, sapair.action_index ); //NOTE requires state-action values to be computed!
			delta1 = accReward - currVal;
			int numVis = getNumVisitsAt( sapair.state_index,sapair.action_index );
			if ( AbstractMDP.learning_rate > 0 )
				lrate = AbstractMDP.learning_rate;
			else
				lrate = (double) lrate_c1 / ( lrate_c2 + numVis );

			StringBuilder logStr = new StringBuilder();
			logStr.append( String.format( "POLICY> sapair: state_index: %d  action_index: %d\n", sapair.state_index, sapair.action_index ) );
			logStr.append( String.format( "POLICY> sapair: reward: %.3f\n", sapair.reward ) );
			logStr.append( String.format( "POLICY> currVal: %.3f accReward: %.3f --> delta1: %.3f\n", currVal, accReward, delta1 ) );
			logStr.append( String.format( "POLICY> numVis: %d lrate: %.3f\n", numVis, lrate ) );
	            
			for ( int i = 0; i < num_binary_feats; i++ ) {
				//logStr.append( String.format( "POLICY> binary_feats[%d]: %d\n", i, binary_feats[i] ) );
				//delta = lrate * delta1 * binary_feature_space[sapair.state_index][i];
				delta = lrate * delta1 * binary_feats[i];
				//logStr.append( String.format( "POLICY> delta: %.3f\n", delta ) );
				value_function_weights[ sapair.action_index ][ i ] += delta;
				total_weight_change += Math.abs( delta );
				if ( binary_feats[i] == 1 )
					//logStr.append( String.format( "POLICY> update (rate:%.5f;visits:%d): val_funct_weight[%d] increased with: [%.3f] %.3f\n", lrate, numVis, i, delta1, delta ) );
					logStr.append( String.format( "POLICY> update: value_function_weight[%d][%d]: %.3f --> %.3f (delta: %.3f)\n", sapair.action_index, i, currVal, value_function_weights[sapair.action_index][i], delta ) );
			}
			logger.logf( Level.FINEST, mdp_model.name, logStr.toString() );
	            
			accReward *= AbstractMDP.discount_factor;
			
			updateTabularValues( sapair.state_index );

		}

		// update value function
//		if ( update_tabular_values )
//			updateTabularValues( full_state_exploration );
	    
		logger.logf( Level.FINEST, mdp_model.name, "POLICY> total_weight_change: %.3f\n", total_weight_change );
		
		return total_weight_change;
	}
	
	private void updateTabularValues( int state_index ) {
		int[] binary_feats = getBinaryFeatures( state_index );
		for ( int j = 0; j < num_actions; j++ ) {
			double val = 0;
			for ( int k = 0; k < num_binary_feats; k++ )
				//val += value_function_weights[j][k] * ( binary_feature_space[i][k] ? 1 : 0 );
				//val += value_function_weights[j][k] * binary_feature_space[i][k];
				val += value_function_weights[j][k] * binary_feats[k];
			logger.logf( Level.FINEST, mdp_model.name, "LVFA> tab val funct update: setValueAt( %d, %d, %.3f )\n", state_index, j, val );
			setValueAt( state_index, j, val );
		}
	}
	
	// computes tabular value function based on current LVFA weights
	@SuppressWarnings("unused")
	private void updateTabularValues( boolean full_state_expl ) {
		for ( int i = 0; i < num_states; i++ ) {
			if ( full_state_expl || stateVisited(i) ) {
				int[] binary_feats = getBinaryFeatures( i );
				for ( int j = 0; j < num_actions; j++ ) {
					double val = 0;
					for ( int k = 0; k < num_binary_feats; k++ )
						//val += value_function_weights[j][k] * ( binary_feature_space[i][k] ? 1 : 0 );
						//val += value_function_weights[j][k] * binary_feature_space[i][k];
						val += value_function_weights[j][k] * binary_feats[k];
					logger.logf( Level.FINEST, mdp_model.name, "LVFA> tab val funct update: setValueAt( %d, %d, %.3f )\n", i, j, val );
					setValueAt( i, j, val );
				}
			}
		}
	}
	
	/**
	 * Writes weights for linear value function approximation in first line
	 * of given output stream, followed by the usual tabular value function.
	 */
	public void writePolicy( PrintStream out ) {
		for ( int k = 0; k < num_actions; k++ ) {
			out.print( FILE_PREFIX + "> " + k + ":" );
			for ( int i = 0; i < num_binary_feats; i++ )
				out.printf( " %.3f", value_function_weights[k][i] );
			out.println();
		}
//		if ( saveTabularValues )
//			super.writePolicy( out );
	}
	
	public void writePolicy(  BufferedWriter bw ) throws IOException {
		for ( int k = 0; k < num_actions; k++ ) {
			bw.write( FILE_PREFIX + "> " + k + ":" );
			for ( int i = 0; i < num_binary_feats; i++ )
				bw.write( String.format( " %.3f", value_function_weights[k][i] ) );
			bw.newLine();
		}
	}

	/**
	 * Reads weights for linear value function approximation from first line
	 * of given input file, followed by the usual tabular value function.
	 */
	public void loadPolicy( File file ) {
		try {
			FileInputStream fis = new FileInputStream( file );
			BufferedReader in = new BufferedReader( new InputStreamReader(fis) );
			String line = "";
			while ( line.isEmpty() )
				line = in.readLine();
			int k = 0;
			while ( line != null && line.startsWith(FILE_PREFIX) ) {
				String[] weight_strings = line.split( " " );
				for ( int i = 2; i < weight_strings.length; i++ ) // skipping prefix and action index
					value_function_weights[k][i-2] = Double.parseDouble( weight_strings[i] );
				k++;
				line = in.readLine();
			}
			in.close();
			if ( k == 0 || k > num_actions ) {
				logger.log( Level.SEVERE, mdp_model.name, "Policy file not in correct format!\n" );
				System.exit( 0 );
			}
			//updateTabularValues( full_state_exploration ); // currently leads to NullPointerExc, due to missing state-action-visit stats
		} catch ( FileNotFoundException fnfe ) {
			logger.log( Level.SEVERE, mdp_model.name, "Policy file not found: " + file.getName() + "\n" );
		} catch ( IOException ioe ) {
			logger.logf( Level.SEVERE, mdp_model.name, "Could not read file %s\n", file.getName() );
		} catch ( NumberFormatException nfe ) {
			logger.log( Level.SEVERE, mdp_model.name, "Wrong format: expected double\n" );
		}
		//super.loadPolicy( file, num_actions + 1 );
	}
	
	public static void loadConfig() {
		update_tabular_values = Boolean.parseBoolean( Configuration.update_tabular_values );
		full_state_exploration = Boolean.parseBoolean( Configuration.full_state_exploration );
		String logStr = "";
		logStr += String.format( "LVFAPolicy> update_tabular_values: %s\n", update_tabular_values );
		logStr += String.format( "LVFAPolicy> full_state_exploration: %s\n", full_state_exploration );
		logger.log( Level.INFO, logStr );
	}
	
}
