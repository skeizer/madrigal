/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016 (code from 2012)                                              *
 *     ---------------------------------------------------------------------------     *
 *     MDP RL agent software package.                                                  *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class StatePolicy {
	
	TabularMDP mdp_model;
	
	int state_index;
	
	//ArrayList<PolicyValue> values; // replace with HashMap for better performance?
	private HashMap<Integer,PolicyValue> values;

	public StatePolicy( TabularMDP mdp, int s ) {
		mdp_model = mdp;
		state_index = s;
		ArrayList<Integer> sa_set = mdp_model.getStateActionSet( s );
		//values = new ArrayList<PolicyValue>( sa_set.size() );
		values = new HashMap<Integer,PolicyValue>( sa_set.size() );
		for ( Integer a : sa_set )
			//values.add( new PolicyValue(a,0,0) );
			values.put( a, new PolicyValue(a,0,0) );
	}
	
	public StatePolicy( StatePolicy sp ) {
		mdp_model = sp.mdp_model;
		state_index = sp.state_index;
		values = new HashMap<Integer,PolicyValue>( sp.values.size() );
		for ( Map.Entry<Integer,PolicyValue> entry : sp.values.entrySet() )
			values.put( entry.getKey(), new PolicyValue(entry.getValue()) );
	}
	
	protected Collection<PolicyValue> getValues() {
		return values.values();
	}
	
	protected int getNumValues() {
		return values.size();
	}
	
	protected PolicyValue getPolicyValue( int a ) {
		return values.get( a );
	}
	
	public void addActionEntry( int a, int n, double v ) {
		//values.add( new PolicyValue(a,n,v) );
		values.put( a, new PolicyValue(a,n,v) );
	}
	
	public void addActionEntry( int a ) {
		//values.add( new PolicyValue(a,0,0) );
		values.put( a, new PolicyValue(a,0,0) );
	}
	
	public void getPolicyFromString( String str ) {
		String[] stateActionPairs = StringUtils.split( str.trim(), ',' );
		String[] sap_elts;
		for ( int k = 0; k < stateActionPairs.length; k++ ) {
			sap_elts = StringUtils.split( stateActionPairs[k] );
			int ai = Integer.parseInt( sap_elts[0] );
			int nv = Integer.parseInt( sap_elts[1] );
			double v = Double.parseDouble( sap_elts[2] );
			addActionEntry( ai, nv, v );
		}

	}
	
//	public PolicyValue getPolicyValueAt( int a ) {
//		for ( PolicyValue val : values )
//			if ( val.action_index == a )
//				return val;
//		return null;
//	}

	public double getValueAt( int a ) {
		//PolicyValue val = getPolicyValueAt( a );
		PolicyValue val = values.get( a );
		if ( val != null )
			return val.value;
		return 0;
	}

	public double getNumVisitsAt( int a ) {
		//PolicyValue val = getPolicyValueAt( a );
		PolicyValue val = values.get( a );
		if ( val != null )
			return val.num_visits;
		return 0;
	}
	
	public String toString() {
		int[] feats = mdp_model.getStateFeatures( state_index );
		String featsStr = StringUtils.join( feats, ' ' );
		String polStr = StringUtils.join( values.values(), ", " );
		return String.format( "%d | %s | %s", state_index, featsStr, polStr );
	}

}
