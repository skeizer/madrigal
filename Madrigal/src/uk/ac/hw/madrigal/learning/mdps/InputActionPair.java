/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created September 2017                                                          *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to MDPs and reinforcement learning.                           *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

public class InputActionPair {

	double[] input_features;
	
	int action_index;
	
	double reward;
	
	public InputActionPair( double[] input, int action ) {
		input_features = input;
		action_index = action;
	}
	
	public String toString() {
		String result = "Input:";
		for ( int i = 0; i < input_features.length; i++ )
			result += String.format( " %.2f", input_features[i] );
		result += String.format( "\nAction: %d", action_index );
		result += String.format( "\nReward: %.2f", reward );
		return result;
	}
	
}
