/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016 (code from 2012)                                              *
 *     ---------------------------------------------------------------------------     *
 *     MDP RL agent software package.                                                  *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;

import resources.Configuration;
import uk.ac.hw.madrigal.utils.MyLogger;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Represents an MDP policy based on a Q-value function mapping each
 * state-action pair to an estimated long term discounted reward.
 * Specific learning algorithms are implemented in subclasses.
 * 
 * @author skeizer
 *
 */
public abstract class Policy {
	
	/** logger */
	protected static MyLogger logger = AbstractMDP.logger;

	private MyLogger agt_logger; // logger for policy of specific MDP model

	/** random number generator */
	public static Random rand_num_gen; // = MDP.rand_num_gen;
	
	/** MDP model with which this policy is associated */
	TabularMDP mdp_model;
	
	/** number of states (enumerating all feature-value combinations */
	int num_states;

	/** number of possible actions for the policy to select from */
	int num_actions;
	
	private double[] ones_mask;
	
	/** New value function: 
	 * - open sets of states and actions;
	 * - contains both values and number of visits for each state-action pair
	 */
	HashMap< Integer, StatePolicy > value_function;
	
	private static boolean restricted_action_sets = false;
	
	/** list of valid action indices for each state */
	ArrayList<Integer>[] state_action_sets;
	
	ArrayList<Integer> action_set;
	    
//<<<<<<< HEAD
//	/** epsilon: rate at which the policy selects a random action (for exploration during training) */
//	public static double exploration_rate = 0d;
//	
//	private static double exploration_discount = 0d;
//	    
//	/** gamma: discount factor in cumulative reward */
//	public static double discount_factor = 1d;
//	    
//	/** alpha: learning rate for policy optimisation 
//	 * (replaced by decaying learning rate via ints lrate_c1 and lrate_c2) */
//	public static double learning_rate = 0d;
//	
//	static double learning_rate_discount = 0d;
//
//=======
//>>>>>>> refs/remotes/origin/mdp_refactoring
	/** constants used for adaptive learning rate:
	 * lrate = lrate_c1 / ( lrate_c2 + N(s,a) )
	 */
	public static int lrate_c1 = 1;
	public static int lrate_c2 = 1;

//<<<<<<< HEAD
//	/** tau: parameter controlling softmax policy */
//	public static double temperature = 0;
//	
//	public static double temperature_discount = 1;
//	
//=======
//>>>>>>> refs/remotes/origin/mdp_refactoring
	/** actions taken so far in the current episode */
	ArrayList<StateActionPair> state_action_history;
	
	/** Number of binary features (derived from tabular state-action space) */
	protected int num_binary_feats;

	//protected boolean[][] binary_feature_space;
	protected int[][] binary_feature_space;

	@SuppressWarnings("unchecked")
	public Policy( TabularMDP mdp, int ns, int na ) {
		mdp_model = mdp;
		agt_logger = mdp_model.agt_logger;
		num_states = ns;
		num_actions = na;
		ones_mask = new double[ num_actions ];
		value_function = new HashMap< Integer, StatePolicy >();
		if ( restricted_action_sets ) {
			state_action_sets = (ArrayList<Integer>[]) new ArrayList[num_states];
			for ( int i = 0; i < num_states; i++ ) {
				state_action_sets[i] = new ArrayList<Integer>();
				for ( int j = 0; j < num_actions; j++ )
					state_action_sets[i].add( j ); // initially include all actions for all states
			}
		} else {
			action_set = new ArrayList<Integer>( num_actions );
			for ( int i = 0; i < num_actions; i++ )
				action_set.add( i );
		}
		state_action_history = new ArrayList<StateActionPair>();
	}
	
	protected Map< Integer, StatePolicy > getSortedCopyOfValueFunction() {
		HashMap< Integer, StatePolicy > val_funct = new HashMap< Integer, StatePolicy >();
		System.out.println( "Copying tabular value function..." );
		for ( Map.Entry<Integer,StatePolicy> entry : value_function.entrySet() ) {
			//System.out.print( " -" );
			val_funct.put( entry.getKey().intValue(), new StatePolicy(entry.getValue()) );
		}
		System.out.println( "\n... done copying tabular value function." );
		return new TreeMap< Integer, StatePolicy >( val_funct );
	}
	
	ArrayList<Integer> getStateActionSet( int state_index ) {
		if ( restricted_action_sets ) //TODO remove this condition? (since we always first add all possible actions)
			return state_action_sets[ state_index ];
		return action_set;
	}
	
	protected PolicyValue getPolicyValueAt( int s, int a ) {
		StatePolicy vals = value_function.get( s );
		if ( vals != null )
			return vals.getPolicyValue( a );
		return null;
	}
	
	protected double getValueAt( int s, int a ) {
		PolicyValue pv = getPolicyValueAt( s, a );
		if ( pv != null )
			return pv.value;
		return 0;
	}
	
	protected boolean stateVisited( int s ) {
		if ( value_function.get(s) == null )
			return false;
		for ( int i = 0; i < num_actions; i++ )
			if ( getNumVisitsAt(s,i) > 0 )
				return true;
		return false;
	}
	
	protected int getNumVisitsAt( int s, int a ) {
		PolicyValue pv = getPolicyValueAt( s, a );
		if ( pv != null )
			return pv.num_visits;
		return 0;
	}
	
	protected void setValueAt( int s, int a, double v ) {
		PolicyValue pv = getPolicyValueAt( s, a );
		pv.setValue( v );
	}
	
	protected void addNumVisitsAt( int s, int a, int n ) {
		PolicyValue pv = getPolicyValueAt( s, a );
		pv.addNumVisits( n );
	}
	
	protected StatePolicy addStateEntry( int s ) {
		StatePolicy sp = new StatePolicy( mdp_model, s );
		value_function.put( s, sp );
		return sp;
	}

	protected void initStateEntries() {
		for ( int i = 0; i < num_states; i++ )
			addStateEntry( i );
	}
	
//	public int getNextActionIndex( int state_index ) {
//		return getNextActionIndex( state_index, AbstractMDP.ones_mask );
//	}
	
	/** 
	 * Returns the index of the next action to be taken,
	 * given the current state as specified in the state_features.
	 * A random action is taken with a small probability given by exploration_rate.
	 */
	public int getNextActionIndex( int state_index, double[] action_mask ) {
	        
		agt_logger.logf( Level.FINEST, "state index: %d\n", state_index );
		
		StatePolicy state_pol = null;
		if ( !value_function.containsKey(state_index) ) {
			state_pol = addStateEntry( state_index );
			agt_logger.logf( Level.FINEST, "Added state %d to policy -> %s\n", state_index, state_pol.toString() );
		}
		
		int action_index = -1;

		double f = rand_num_gen.nextDouble();
		if ( f < AbstractMDP.exploration_rate ) {
			int legal_action_index;
			if ( restricted_action_sets ) {
				legal_action_index = rand_num_gen.nextInt( state_action_sets[state_index].size() );
				action_index = state_action_sets[state_index].get( legal_action_index );
			} else {
				//TODO incorporate action_mask
				action_index = rand_num_gen.nextInt( num_actions );
			}
			agt_logger.logf( Level.FINEST, "policy execution (exploring action): %d\n", action_index );
		} else {
			action_index = getActionIndex( state_index, action_mask );
			agt_logger.logf( Level.FINEST, "policy execution (on-policy action): %d\n", action_index );
		}

		agt_logger.logf( Level.FINE, "Adding state-action pair to history: (%d,%d)\n", state_index, action_index );
		state_action_history.add( 0, new StateActionPair(state_index,action_index) );
	        
		return action_index;

	}
	
	/**
	 * Returns the index of the next action given the current state,
	 * according to the current policy.  If the temperature value is zero,
	 * the action with the highest value in the value function is returned,
	 * otherwise the index is sampled from the softmax function, derived from
	 * the value function with parameter temperature.
	 */
	private int getActionIndex( int state_index, double[] action_mask ) {
		if ( AbstractMDP.temperature > 0 )
			return getSoftMaxActionIndex( state_index, action_mask );
		else
			return getBestActionIndex( state_index, action_mask );
	}
	    
	int getBestActionIndex( int state_index ) {
		return getBestActionIndex( state_index, ones_mask );
	}
	
	/** 
	 * Returns the index of the best action, given the state
	 * and the value function.
	 */
	int getBestActionIndex( int state_index, double[] action_mask ) {
		//TODO test effect of action_mask
		int i = 0;
		while ( i < num_actions && action_mask[i] == 0)
			i++;
		if ( i > num_actions ) {
			System.out.println( "ERROR: all actions disabled!" );
			return -1;
		}
		double max_value = getValueAt( state_index, i );
		ArrayList<Integer> max_indices = new ArrayList<Integer>();
		max_indices.add( i );
		StatePolicy values = value_function.get( state_index );
		
		ArrayList<Integer> actions = action_set;
		if ( restricted_action_sets )
			actions = state_action_sets[ state_index ];
		for ( int j : actions ) {
			if ( action_mask[j] == 0 ) continue;  
			double val = values.getValueAt( j ); 
			if ( val > max_value ) {
				max_value = val;
				max_indices.clear();
				max_indices.add( j );
			} else if ( val == max_value ) {
				max_indices.add( j );
			}
		}
		int ri = rand_num_gen.nextInt( max_indices.size() );
		Integer rio = (Integer) max_indices.get( ri );
		return rio.intValue();

	}
	    
	/**
	 * Returns the index of the best action given the current state,
	 * according to the current policy (greedy).
	 */
	private int getSoftMaxActionIndex( int state_index, double[] action_mask ) {
		StatePolicy sp = value_function.get( state_index );
		double[] softmax_probs = new double[ sp.getNumValues() ];
		int i = 0;
		String logStr = String.format( "SoftMax: %d actions;", num_actions );
		for ( PolicyValue pv : sp.getValues() ) {
			//softmax_probs[i] = pv.prob;
			softmax_probs[i] = ( action_mask[i] == 1 ? pv.prob : 0f );
			logStr += String.format( " %.3f", softmax_probs[i] );
			i++;
		}
		int ai = Utils.sampleFromProbs( softmax_probs, num_actions, rand_num_gen );
		logStr += String.format( " --> %d\n", ai );
		agt_logger.log( Level.FINEST, logStr );
		return ai;
	}
	    
	/** Updates the softmax probabilities based on the current Q-values */
	private void updateSoftMaxProbs() {
		double sum_prob;
		for ( StatePolicy sp : value_function.values() ) {
			sum_prob = 0;
			// initialise probabilities exp(Q/tau)
			for ( PolicyValue pv : sp.getValues() ) {
				pv.prob = Math.exp( pv.value / AbstractMDP.temperature );
				sum_prob += pv.prob;
			}
			// normalise to probabilities
			for ( PolicyValue pv : sp.getValues() )
				pv.prob /= sum_prob;
		}        
	}
	    
	/** adds given reward signal to the accumulated reward */
	public void recordReward( double reward ) {
			        
		if ( state_action_history.isEmpty() )
			agt_logger.logf( Level.WARNING, "Attempt to add reward with no state-action pairs recorded: %.2f\n", reward );
		else {
			// associate reward with last visited state-action pair
			StateActionPair sap = state_action_history.get( 0 );
			if ( sap.reward != 0 )
				agt_logger.logf( Level.WARNING, "Adding additional reward to state-action pair: %.2f + %.2f", sap.reward, reward );
			agt_logger.logf( Level.FINEST, "Associating reward to state-action pair: (%d,%d,%.0f)", sap.state_index, sap.action_index, reward );
			sap.reward += reward;
		}
	        
	}
	
	/** Updates this policy based on accumulated state-action pairs and associated rewards */
	public void update() {
		
		if ( TabularMDP.POLICY_OPTIMISATION ) {
			double qval_change = updatePolicy();
			agt_logger.logf( Level.FINE, "Total change in value function: %.2f\n", qval_change );
		}
		state_action_history.clear();
		
		// update softmax probabilities
		if ( AbstractMDP.temperature > 0 )
			updateSoftMaxProbs();
	        
	}
	
	public abstract double updatePolicy();

	/**
	 * Prints the policy to the given PrintStream:
	 * each line contains the feature values constituting a single state,
	 * followed by the optimal action for that state (according to the value function).
	 */
	protected void writePolicy( PrintStream out ) {
		//System.out.println( "Creating state-sorted copy of tabular value function ..." );
		//Map< Integer, StatePolicy > map = getSortedCopyOfValueFunction();
		Map< Integer, StatePolicy > map = new TreeMap< Integer, StatePolicy >( value_function );
		//System.out.println( "Writing tabular value function ..." );
		for ( Map.Entry<Integer,StatePolicy> entry : map.entrySet() ) {
			StatePolicy sp = entry.getValue();
			int pa = getBestActionIndex( sp.state_index );
			out.println( sp.toString() + " | " + pa );
		}
		//System.out.println( "... done writing tabular value function!" );
	}
	    
	protected void writePolicy( BufferedWriter bw ) throws IOException {
		//Map< Integer, StatePolicy > map = getSortedCopyOfValueFunction();
		Map< Integer, StatePolicy > map = new TreeMap< Integer, StatePolicy >( value_function );
		for ( Map.Entry<Integer,StatePolicy> entry : map.entrySet() ) {
			StatePolicy sp = entry.getValue();
			int pa = getBestActionIndex( sp.state_index );
			bw.write(  sp.toString() + " | " + pa );
			bw.newLine();
		}
	}

	/**
	 * Prints the policy to a file with the given file name:
	 * each line contains the feature values constituting a single state,
	 * followed by the optimal action for that state (according to the value function).
	 */
	public void writePolicy( String fname ) {
		try ( BufferedWriter bw = new BufferedWriter( new FileWriter(fname) ) ) {
			writePolicy( bw );
		} catch ( FileNotFoundException fnfe ) {
			agt_logger.logf( Level.SEVERE, "File %s not found\n", fname );
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	
	public void writePolicy( String fname, boolean objects ) {
		if ( objects )
			writePolicyObjects( fname );
		else
			writePolicy( fname );
	}
	
	public void writePolicyObjects( String fname ) {
		try {
			ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream(fname) );
			writePolicy( out );
			out.close();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	
	public void writePolicy( ObjectOutputStream out ) {
		try {
			out.writeObject( value_function );
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	
	public void loadPolicy( InputStream is ) throws IOException {
		loadPolicy( is, 1 );
	}
	
	public void loadPolicy( String filename, boolean objects ) throws IOException {
		String fname = filename.replaceAll("\"", "" );
		agt_logger.logf( Level.INFO, "Loading policy from file %s ... ", fname );
//		try {
			if ( objects )
				loadPolicyObjects( filename );
			else
				loadPolicy( new File(fname) );
//		} catch ( IOException ioe ) {
//			logger.logf( Level.SEVERE, "Exception reading policy file %s\n", filename );
//		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadPolicyObjects( String fname ) {
		try {
			ObjectInputStream ois = new ObjectInputStream( new FileInputStream(fname) );
			value_function = (HashMap<Integer,StatePolicy>) ois.readObject();
			ois.close();
			agt_logger.log( Level.INFO, "Policy loaded\n" );
		} catch ( IOException | ClassNotFoundException e ) {
			e.printStackTrace();
		}
	}
	
	public void loadPolicy( File file ) throws IOException {
		loadPolicy( file, 1 );
	}

	public void loadPolicy( File file, int lineNum ) throws IOException {
		loadPolicy( new FileInputStream(file), lineNum );
	}
	
	/* 
	 * each line:
	 *
	 *    state number | feature values | action-index num-visits value prob tuples | best action index
	 * 
	 * for example:
	 * 
	 *    224 | 3 1 0 0 0 0 | comma-separated list of (action-index num-visits value prob) tuples | 5
	 * 
	 */
	//public void loadPolicy( File file, int lineNum ) {
	public void loadPolicy( InputStream is, int lineNum ) throws IOException {
        
			BufferedReader bf = new BufferedReader( new InputStreamReader(is) );
			String line = "";
			for ( int l = 0; l < lineNum; l++ ) // find first line
				line = bf.readLine();
			int i = 0;
			while ( line != null && i < num_states ) {
				if ( !line.isEmpty() ) {
					try {
						String[] state_parts = StringUtils.split(line, '|' );
						i = Integer.parseInt( state_parts[0].trim() );
						if ( i >= num_states ) {
							agt_logger.logf( Level.SEVERE, "ERROR: illegal state index %d >= %d\n", i, num_states );
							System.exit( 0 );
						}
						StatePolicy sp = new StatePolicy( mdp_model, i );
						sp.getPolicyFromString( state_parts[2].trim() );
						value_function.put( i, sp );
					} catch ( NumberFormatException nfe ) {
						agt_logger.log( Level.WARNING, "Number format exception\n" );
					}
				}
				line = bf.readLine();
			}
			if ( i < num_states )
				agt_logger.log( Level.WARNING, "Not all possible states found in policy file\n" );

			bf.close();

	}

	void initNumStateActionVisits( int num ) {
		for ( int i = 0; i < num_states; i++ ) {
			StatePolicy sp = addStateEntry( i );
			for ( int j = 0; j < num_actions; j++ )
				sp.addActionEntry( j, num, 0 );
		}
	}
	
	protected void initBinaryFeatureSpace() {
		//binary_feature_space = new boolean[ num_states ][ num_binary_feats ];
		binary_feature_space = new int[ num_states ][ num_binary_feats ];
		for ( int i = 0; i < num_states; i++ )
			binary_feature_space[i] = getBinaryFeatures( i );
	}


	//boolean[] getBinaryFeatures( int state_index ) {
	int[] getBinaryFeatures( int state_index ) {
	    
		int[] state_feats = mdp_model.getStateFeatures( state_index );
			        
		// set binary features based on state features
		//boolean[] binary_feats = new boolean[ num_binary_feats ];
		int[] binary_feats = new int[ num_binary_feats ];
		//binary_feats[0] = true;
		binary_feats[0] = 1;
		int idx_offset = 1;
		for ( int i = 0; i < mdp_model.num_state_features; i++ ) {
			//binary_feats[ idx_offset + state_feats[i] ] = true;
			binary_feats[ idx_offset + state_feats[i] ] = 1;
			idx_offset += mdp_model.state_dimensions[i];
		}
		if ( !AbstractMDP.INCLUDE_SECOND_ORDER_FEATURES )
			return binary_feats;
		
		// get second order binary features
		int numFirstOrderFeats = idx_offset - 1; // exclude first binary feature for regularisation
		for ( int k = 0; k < numFirstOrderFeats - 1; k++ ) {
			for ( int l = k + 1; l < numFirstOrderFeats; l++ ) {
				if ( binary_feats[k+1] == 1 && binary_feats[l+1] == 1 )
					binary_feats[ idx_offset ] = 1;
				idx_offset++;
			}
		}
		return binary_feats;
	}
	
	public static void loadConfig() {
		lrate_c1 = Integer.parseInt( Configuration.lrate_c1 );
		lrate_c2 = Integer.parseInt( Configuration.lrate_c2 );
		restricted_action_sets = Boolean.parseBoolean( Configuration.restricted_action_sets );
		
		StringBuffer logStr = new StringBuffer( "Policy configuration:\n" );
		logStr.append( String.format( "   Adaptive learning rate: %d %d\n", lrate_c1, lrate_c2 ) );
		logStr.append( String.format( "   Allow restricting action sets: %s\n", restricted_action_sets ) );
		logger.log( Level.INFO, logStr.toString() );
	}
	
}
