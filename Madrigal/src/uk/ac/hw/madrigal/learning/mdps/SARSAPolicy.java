/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016 (code from 2012)                                              *
 *     ---------------------------------------------------------------------------     *
 *     MDP RL agent software package.                                                  *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

import java.util.Collections;
import java.util.logging.Level;

public class SARSAPolicy extends Policy {

	public SARSAPolicy( TabularMDP mdp, int ns, int na ) {
		super( mdp, ns, na );
	}

	/**
	 * Updates the current Q-function with state-action history according to SARSA algorithm.
	 * 
	 * @return total absolute change in q-value (used for tracking convergence)
	 */
	@Override
	public double updatePolicy() {
		
		String logStr = "";
        
		double qval_change = 0;
		double qval_curr, qval_next, delta;
		double lrate;
	        
		StateActionPair curr_sapair = null;
		Collections.reverse( state_action_history );
		for ( StateActionPair next_sapair : state_action_history ) {
			
			if ( curr_sapair != null ) {
				qval_curr = getValueAt( curr_sapair.state_index, curr_sapair.action_index );
				qval_next = getValueAt( next_sapair.state_index, next_sapair.action_index );
	                
				// adaptive learning rate
				int numVis = getNumVisitsAt( curr_sapair.state_index, curr_sapair.action_index );
				lrate = (double) lrate_c1 / ( lrate_c2 + numVis );
	                
				//delta = learning_rate * ( sapair.reward + discount_factor * qval_next - qval_curr );
				delta = lrate * ( curr_sapair.reward + AbstractMDP.discount_factor * qval_next - qval_curr );
				setValueAt( curr_sapair.state_index, curr_sapair.action_index, qval_curr + delta );
				qval_change += Math.abs( delta );
				logStr += String.format( "POL> (%d,%d,%.3f): %.3f -> %.3f ; delta: %.3f, lrate: %.3f\n", curr_sapair.state_index, curr_sapair.action_index, curr_sapair.reward, qval_curr, qval_curr + delta, delta, lrate );
			}
			
			addNumVisitsAt( next_sapair.state_index, next_sapair.action_index, 1 );
			curr_sapair = next_sapair;
		}
		
		// now update Q-value for final state-action pair in this episode
		qval_curr = getValueAt( curr_sapair.state_index, curr_sapair.action_index );
		int numVis = getNumVisitsAt( curr_sapair.state_index, curr_sapair.action_index );
		lrate = (double) lrate_c1 / ( lrate_c2 + numVis );
		delta = lrate * ( curr_sapair.reward - qval_curr );
		setValueAt( curr_sapair.state_index, curr_sapair.action_index, qval_curr + delta );
		qval_change += Math.abs( delta );
		logStr += String.format( "POL> (%d,%d,%.3f): %.3f -> %.3f ; delta: %.3f, lrate: %.3f\n", curr_sapair.state_index, curr_sapair.action_index, curr_sapair.reward, qval_curr, qval_curr + delta, delta, lrate );
		
		addNumVisitsAt( curr_sapair.state_index, curr_sapair.action_index, 1 );
	        
		logger.log( Level.FINEST, mdp_model.name, logStr );
	        
		return qval_change;

	}

}
