/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016 (code from 2012)                                              *
 *     ---------------------------------------------------------------------------     *
 *     MDP RL agent software package.                                                  *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList; 
import java.util.logging.Level;

import uk.ac.hw.madrigal.utils.Utils;

/**
 * Represents a Markov Decision Process, defined by a set of state features and their dimensions, 
 * the size of the action set, and a policy.
 * 
 * @author skeizer
 */
public class TabularMDP extends AbstractMDP {
	
	/** number of possible values for each feature in the state representation */
	int[] state_dimensions;
	
	/** Policy for this MDP */
	private Policy policy;
	
	/** Constructor */
	public TabularMDP( String nm, int num_state_feats, int[] state_dims, int num_acts ) {
		super( nm, num_state_feats, num_acts );
		update_policy = false;
		state_dimensions = new int[num_state_features];
		System.arraycopy( state_dims, 0, state_dimensions, 0, num_state_feats );
		int num_states = computeNumStates();
		if ( learning_algorithm.equals(LVFA) ) {
			policy = new LVFAPolicy( this, num_states, num_actions );
			//policy.initStateEntries();
		} else if ( learning_algorithm.equals(SARSA) )
			policy = new SARSAPolicy( this, num_states, num_actions );
		else if ( learning_algorithm.equals(MCC) )
			policy = new MCCPolicy( this, num_states, num_actions );
		else
			agt_logger.log( Level.SEVERE, name, "Unknown learning algorithm\n" );
	}
	
	/** Returns the total number of (atomic) states, derived from the state features */
	private int computeNumStates() {
		int num_states = 1;
		for ( int i = 0; i < num_state_features; i++ ) {
			num_states *= state_dimensions[i];
		}
		return num_states;
	}
	
	public int getNumStates() {
		return policy.num_states;
	}
	
	public int getNumActions() {
		return num_actions;
	}
	
	public ArrayList<Integer> getStateActionSet( int state_index ) {
		//return policy.state_action_sets[ state_index ];
		return policy.getStateActionSet( state_index );
	}
	
	public void removeActions( int state_index, ArrayList<Integer> remove_list ) {
		policy.state_action_sets[state_index].removeAll( remove_list );
	}

	public void cancelLastAction() {
		policy.state_action_history.remove( 0 );
	}
	
	public int getLastAction() {
		if ( policy.state_action_history.isEmpty() )
			return -1;
		return policy.state_action_history.get( 0 ).action_index;
	}
	
	/** Returns the number of binary features derived from and describing the (state,action) space. */
	protected int getNumBinaryFeatures() {
		int num_bin_fts = 1; // includes regularisation term (theta_0)
		for ( int i = 0; i < num_state_features; i++ )
			num_bin_fts += state_dimensions[i];
		if ( !INCLUDE_SECOND_ORDER_FEATURES )
			return num_bin_fts;
		num_bin_fts += Utils.choose( num_bin_fts-1, 2 );
		return num_bin_fts;
	}
	
	/** Returns the state feature values based on the given (atomic) state index */
	public int[] getStateFeatures( int state_index ) {
		int[] state_feats = new int[num_state_features];
		int d1 = state_index;
		int d;
		for ( int i = num_state_features - 1; i >= 0 ; i-- ) {
			d = d1 / state_dimensions[i];
			state_feats[i] = d1 % state_dimensions[i];
			d1 = d;
		}
		return state_feats;
	}
	
	protected int getStateIndex( int[] state_feats ) {
		int k = state_feats.length - 1;
		int state_index = state_feats[ k ];
		int d = 1;
		for ( int i = 0; i < k; i++ ) {
			d *= state_dimensions[k-i];
			state_index += d * state_feats[k-i-1];
		}
		return state_index;
	}
	
	/** Returns the action index based on the given state index, determined by the current policy */
	public int getNextActionIndex( int state_index ) {
		return policy.getNextActionIndex( state_index, ones_mask );
	}
	
	public int getNextActionIndex( int state_index, double[] action_mask ) {
		return policy.getNextActionIndex( state_index, action_mask );
	}
	
	public int getNextActionIndex( int[] state_features ) {
		return getNextActionIndex( getStateIndex(state_features) );
	}
	
	public int getNextActionIndex( int[] state_features, double[] action_mask ) {
		return getNextActionIndex( getStateIndex(state_features), action_mask );
	}

	@Override
	public int getNextActionIndex( double[] state_features, double[] action_mask ) {
		// cast state features to integer and proceed (assuming that the double type features are actually integers) 
		int[] state_features_int = new int[num_state_features];
		for ( int i = 0; i < num_state_features; i++ )
			state_features_int[i] = (int) state_features[i];
		return getNextActionIndex( getStateIndex(state_features_int), action_mask ); //TODO include action_mask!
	}

	// NB: this method clears the state-action history!!
	public void saveStats() {
		corpus_stats.startNewEpisode();
		for ( StateActionPair sap : policy.state_action_history )
			corpus_stats.addScore( sap.reward );
		policy.state_action_history.clear();
	}
	
	public void computeStatistics() {
		if ( POLICY_OPTIMISATION )
			trimCorpusStats( AVG_SCORE_WINDOW_SIZE );
		corpus_stats.computeAvgScore();
		agt_logger.logf( Level.INFO, "Stats:\t%s\n", corpus_stats.getCompactSummary() );
	}
	
	public void trimCorpusStats( int window_size ) {
		int toremove = corpus_stats.getSize() - window_size;
		while ( toremove >= 0 ) {
			corpus_stats.removeFirst();
			toremove--;
		}
	}

	public void loadPolicy( InputStream is ) throws IOException {
		policy.loadPolicy( is );
	}
	
	public void loadPolicy( String filename, boolean objects ) throws IOException {
		policy.loadPolicy( filename, objects );
	}
	
	/** Load policy from file with given name 
	 * @throws IOException */
	public void loadPolicy( String filename ) throws IOException {
		loadPolicy( filename, false );
	}
	
	//public synchronized void writePolicy( String fname, boolean objects ) {
	public void writePolicy( String fname, boolean objects ) {
		String file_name = POLICY_SAVING_DIR + "/" + fname;
		agt_logger.logf( Level.FINEST, "Writing file: %s\n", file_name );
		policy.writePolicy( file_name, objects );
		agt_logger.logf( Level.FINEST, "Finished writing file: %s\n", file_name );
	}

	/** Write policy to file with given name */
	//public synchronized void writePolicy( String fname ) {
	public void writePolicy( String fname ) {
		writePolicy( fname, false );
	}
	
	/** Update the current policy with the current statistics (latest episode) */
	public void update( boolean success ) {
		
		corpus_stats.startNewEpisode();
		for ( StateActionPair sap : policy.state_action_history )
			corpus_stats.addScore( sap.reward );
		corpus_stats.processLatestEpisode( success );

		policy.update();
	}
	
	public void recordReward( double reward ) {
		policy.recordReward( reward );
	}

	/** Initialises the number of visits for all state-action pairs to the given number */
	void initNumStateActionVisits( int num ) {
		policy.initNumStateActionVisits( num );
	}
	
}
