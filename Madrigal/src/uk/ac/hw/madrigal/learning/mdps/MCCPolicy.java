/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016 (code from 2012)                                              *
 *     ---------------------------------------------------------------------------     *
 *     MDP RL agent software package.                                                  *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

import java.util.logging.Level;

public class MCCPolicy extends Policy {

	public MCCPolicy( TabularMDP mdp, int ns, int na ) {
		super( mdp, ns, na);
	}

	/**
	 * Updates the current Q-function with state-action history according to Monte Carlo Control algorithm.
	 * 
	 * @return total absolute change in q-value (used for tracking convergence)
	 */
	@Override
	public double updatePolicy() {
	        
		double qval_change = 0;
		double prev_qval;
	        
		double accReward = 0;
		//NOTE alternative policy updating: state-action pairs closer to successful completion will not be hurt by penalised state-action pairs encountered earlier
	        
		double qUpdateNum, qUpdateDenom, qUpdate;
	    for ( StateActionPair sapair : state_action_history ) {

			accReward += sapair.reward;

			PolicyValue pv = getPolicyValueAt(  sapair.state_index, sapair.action_index );
			prev_qval = pv.value; //getValueAt( sapair.state_index, sapair.action_index );
			qUpdateNum = prev_qval * pv.num_visits + accReward;
			qUpdateDenom = pv.num_visits + 1;
			qUpdate = qUpdateNum / qUpdateDenom;
			
//			//String logStr = "";
			StringBuilder logStr = new StringBuilder();
			logStr.append( String.format( "POLICY> sapair: (%d,%d,%.3f)\n", sapair.state_index, sapair.action_index, sapair.reward ) );
			logStr.append( String.format( "POLICY> accReward: %.5f\n", accReward ) );
			logStr.append( String.format( "POLICY> update: Q[%d][%d] = %.3f (was %.3f)\n", sapair.state_index, sapair.action_index, qUpdate, prev_qval ) );
			logger.logf( Level.FINEST, mdp_model.name, logStr.toString() );

			//setValueAt( sapair.state_index, sapair.action_index, qUpdate );
			pv.value = qUpdate; //.setValue( qUpdate );
			//addNumVisitsAt( sapair.state_index, sapair.action_index, 1 );
			pv.num_visits++; //.addNumVisits( 1 );
			logger.logf( Level.FINEST, mdp_model.name, "Updated state value function: %s\n", pv.toString() );
			accReward *= AbstractMDP.discount_factor;
	            
			qval_change += Math.abs( qUpdate - prev_qval );
	            
		}
	        
		return qval_change;
	        
	}

}
