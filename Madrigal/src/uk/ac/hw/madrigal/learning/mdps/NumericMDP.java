/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created September 2017                                                          *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to MDPs and reinforcement learning.                           *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;

import uk.ac.hw.madrigal.utils.Utils;

public class NumericMDP extends AbstractMDP {
	
	private static String FILE_PREFIX = "lvfa";

	double[][] value_function_weights;
	
	/** actions taken so far in the current episode */
	ArrayList<InputActionPair> state_action_history;
	
	/** accumulated reward in current episode */
	double accReward;

	/** Constructor */
	public NumericMDP( String nm, int num_state_feats, int num_acts ) {
		super( nm, num_state_feats, num_acts );
		update_policy = false;
		value_function_weights = new double[ num_actions ][ num_state_feats + 1 ]; // one additional weight as bias term
		ones_mask = Utils.getOnesVector( num_actions );
		rand_init_weights();
		state_action_history = new ArrayList<InputActionPair>();
	}
	
	private void rand_init_weights() {
		for ( int i = 0; i < num_actions; i++ )
			for ( int j = 0; j < num_state_features + 1; j++ )
				value_function_weights[i][j] = -1 + 2 * rand_num_gen.nextDouble(); // random number in interval (-1,+1)
	}

	public void loadPolicy( InputStream is ) throws FileNotFoundException {
		BufferedReader bf = new BufferedReader( new InputStreamReader(is) );
		loadPolicy( bf );
	}
	
	public void loadPolicy( String filename ) throws FileNotFoundException {
		String fname = filename.replaceAll("\"", "" );
		loadPolicy( new File(fname) );
	}
	
	public void loadPolicy( File file ) throws FileNotFoundException {
		//try {
			FileInputStream fis = new FileInputStream( file );
			BufferedReader in = new BufferedReader( new InputStreamReader(fis) );
			loadPolicy( in );
//		} catch ( FileNotFoundException fnfe ) {
//			agt_logger.log( Level.SEVERE, name + "> Policy file not found: " + file.getName() + "\n" );
//		}
	}
	
	/**
	 * Reads weights for linear value function approximation from first line
	 * of given input file, followed by the usual tabular value function.
	 */
	public void loadPolicy( BufferedReader in ) {
		try {
			String line = "";
			while ( line.isEmpty() )
				line = in.readLine();
			int k = 0;
			while ( line != null && line.startsWith(FILE_PREFIX) ) {
				String[] weight_strings = line.split( " " );
				for ( int i = 2; i < weight_strings.length; i++ ) // skipping prefix and action index
					value_function_weights[k][i-2] = Double.parseDouble( weight_strings[i] );
				k++;
				line = in.readLine();
			}
			in.close();
			if ( k == 0 || k > num_actions ) {
				agt_logger.log( Level.SEVERE, name + "> Policy file not in correct format!\n" );
				System.exit( 0 );
			}
		} catch ( IOException ioe ) {
			agt_logger.logf( Level.SEVERE, name + "> Exception reading input" );
		} catch ( NumberFormatException nfe ) {
			agt_logger.log( Level.SEVERE, name + "> Wrong format: expected double\n" );
		}
	}
	
	private int getActionIndex( double[] values ) {
		return getActionIndex( values, ones_mask );
	}
	
	private int getActionIndex( double[] values, double[] mask ) {
		if ( temperature > 0 )
			return getSoftMaxActionIndex( values, mask );
		else
			return getBestActionIndex( values, mask );		
	}

	public int getNextActionIndex( double[] state_features, int action_index_to_exclude ) {
		if ( action_index_to_exclude == -1 ) // not excluding an action
			return getNextActionIndex( state_features );
		
		// withdraw previous action
		InputActionPair iap = state_action_history.remove( 0 );
		agt_logger.logf( Level.FINE, name + "> Removing state-action pair from history:\n%s\n", iap.toString() );

		// now resample from other actions
		int action_index = -1;
		double[] values = getValueFunction( state_features, action_index_to_exclude ); // assign value -100 to action to be excluded
		action_index = getActionIndex( values );
		agt_logger.logf( Level.FINEST, name + "> policy execution (on-policy action): %d\n", action_index );
		
		InputActionPair iapair = new InputActionPair( state_features, action_index );
		agt_logger.logf( Level.FINE, name + "> Adding state-action pair to history:\n%s\n", iapair.toString() );
		state_action_history.add( 0, iapair );

		return action_index;
	}

	public int getNextActionIndex( double[] state_features ) {
		return getNextActionIndex( state_features, ones_mask );
	}
	
	public int getNextActionIndex( double[] state_features, double[] mask ) {
		int action_index = -1;
		double f = rand_num_gen.nextDouble();
		if ( update_policy && f < exploration_rate ) { // only explore when updating policy
			action_index = Utils.getRandInd( mask, num_actions, rand_num_gen );
			//action_index = rand_num_gen.nextInt( num_actions );
			agt_logger.logf( Level.FINEST, name + "> policy execution (exploring action): %d\n", action_index );
		} else {
			double[] values = getValueFunction( state_features );
			//action_index = getActionIndex( values );
			action_index = getActionIndex( values, mask );
			agt_logger.logf( Level.FINEST, name + "> policy execution (on-policy action): %d\n", action_index );
		}
		
		InputActionPair iapair = new InputActionPair( state_features, action_index );
		agt_logger.logf( Level.FINE, name + "> Adding state-action pair to history:\n%s\n", iapair.toString() );
		state_action_history.add( 0, iapair );
	        
		return action_index;
	}
	
	/**
	 * Returns the index of the best action given the current state,
	 * according to the current policy (greedy).
	 */
	private int getSoftMaxActionIndex( double[] values, double[] mask ) {
		double[] softmax_probs = new double[ num_actions ];
		double sum_prob = 0d;
		for ( int i = 0; i < values.length; i++ ) {
			softmax_probs[i] = ( mask[i] == 1 ? Math.exp( values[i] / temperature ) : 0f );
			sum_prob += softmax_probs[i];
		}
		for ( int i = 0; i < values.length; i++ )
			softmax_probs[i] /= sum_prob;
		return Utils.sampleFromProbs( softmax_probs, num_actions, rand_num_gen );
	}
	
	/**
	 * Returns the index of the best action, given the state
	 * and the value function.
	 */
	private int getBestActionIndex( double[] values, double[] mask ) {
		double max_value = 0;
		boolean first = true;
		ArrayList<Integer> max_indices = new ArrayList<Integer>();
		for ( int i = 0; i < num_actions; i++ ) {
			if ( mask[i] == 1 ) {
				if ( first ) {
					max_value = values[i];
					first = false;
				}
				if ( values[i] > max_value ) {
					max_value = values[i];
					max_indices.clear();
					max_indices.add( i );
				} else if ( values[i] == max_value ) {
					max_indices.add( i );
				}
			}
		}
		if ( max_indices.isEmpty() )
			System.out.println( "ERROR: No action selected!!!" );
		int ri = rand_num_gen.nextInt( max_indices.size() );
		Integer rio = (Integer) max_indices.get( ri );
		return rio.intValue();
	}
	
	/**
	 * Returns the values corresponding to the possible actions, given the specified state features.
	 */
	private double[] getValueFunction( double[] state_feats ) {
		double[] values = new double[ num_actions ];
		String logStr = name + "> val funct - feats:";
		for ( int k = 0; k < num_state_features; k++ )
			logStr += String.format( " %.2f", state_feats[k] );
		logStr += "\n";
		for ( int i = 0; i < num_actions; i++ ) {
			double val = value_function_weights[ i ][ 0 ]; // bias term
			for ( int j = 0; j < num_state_features; j++ )
				val += value_function_weights[ i ][ j + 1 ] * state_feats[ j ];
			if ( Double.isNaN(val) )
				System.out.println( "ERROR: value NaN" );
			values[ i ] = val;
		}
		logStr += name + "> val funct - action vals:";
		for ( int l = 0; l < num_actions; l++ )
			logStr += String.format( " %.2f", values[l] );
		logStr += "\n";
		agt_logger.log( Level.FINEST, logStr );
		return values;
	}

	private double[] getValueFunction( double[] state_feats, int action_index_to_exclude ) {
		double[] values = getValueFunction( state_feats );
		values[ action_index_to_exclude ] = -100;
		return values;		
	}
	
	/** Update the current policy with the current statistics (latest episode) */
	public void update( boolean success ) {
		
		corpus_stats.startNewEpisode();
		for ( InputActionPair sap : state_action_history )
			corpus_stats.addScore( sap.reward );
		corpus_stats.processLatestEpisode( success );

		if ( update_policy )
			updatePolicy(); //TODO only do this if POLICY_OPTIMISATION == true ?
		state_action_history.clear();

	}
		
	public double updatePolicy() {

		double accReward = 0;
		double total_weight_change = 0;

		double delta;
		double delta1;
		for ( InputActionPair in_act_pair : state_action_history ) {
			accReward += in_act_pair.reward;
			double[] values = getValueFunction( in_act_pair.input_features );
			double currVal = values[ in_act_pair.action_index ];
			delta1 = accReward - currVal;
			
			String logStr = "UPDATING MDP POLICY...\n";
			logStr += String.format( name + "> %s\n", in_act_pair.toString() );
			logStr += String.format( name + "> currVal: %.3e reward: %.2f accReward: %.2f --> delta1: %.2e\n", currVal, in_act_pair.reward, accReward, delta1 );
			
			double gradient = 1d;
			double currWeight = 0d;
			for ( int i = 0; i < num_state_features + 1; i++ ) {
				if ( i == 0 )
					gradient = 1d;
				else
					gradient = in_act_pair.input_features[i-1];
				delta = learning_rate * delta1 * gradient;
				currWeight = value_function_weights[ in_act_pair.action_index ][ i ];
				value_function_weights[ in_act_pair.action_index ][ i ] += delta;
				logStr += String.format( name + "> update: val_funct_weight[%d][%d]: %.3e -> %.3e (delta1: %.3e; delta: %.3e)\n", 
						in_act_pair.action_index, i, currWeight, value_function_weights[in_act_pair.action_index][i], delta1, delta );
				total_weight_change += Math.abs( delta );
				
				//DEBUG
				double[] valuesNew = getValueFunction( in_act_pair.input_features );
				double currValNew = valuesNew[ in_act_pair.action_index ];
				double delta1New = accReward - currValNew;
				double valueChange = currVal - currValNew;
				logStr += String.format( name + "> DBG accReward: %.2f; currVal: %.2e; currVal-accReward: %.2e; currValNew: %.2e; currValNew-accReward: %.2e; value change: %.2e\n", 
						accReward, currVal, delta1, currValNew, delta1New, valueChange );
			}
			agt_logger.logf( Level.FINE, logStr );
            
			//DEBUG
			double[] valuesNew = getValueFunction( in_act_pair.input_features );
			double currValNew = valuesNew[ in_act_pair.action_index ];
			double delta1New = accReward - currValNew;
			double valueChange = currVal - currValNew;
			logStr = String.format( name + "> DEBUG accReward: %.2f; currVal: %.2e; currVal-accReward: %.2e; currValNew: %.2e; currValNew-accReward: %.2e; value change: %.2e\n", 
					accReward, currVal, delta1, currValNew, delta1New, valueChange );
			agt_logger.logf( Level.FINEST, logStr );

			accReward *= discount_factor;
			
		}
		agt_logger.logf( Level.FINE, name + "> total_weight_change: %.3e\n", total_weight_change );
		
		return total_weight_change;		
	}
	
	@SuppressWarnings("unused")
	private double getWeightL2Norm( int action_index ) {
		double result = 0d;
		for ( int i = 0; i < num_state_features + 1; i++ )
			result += Math.pow( value_function_weights[action_index][i], 2 );
		result /= 2;
		return result;
	}
	
	/** adds given reward signal to the accumulated reward */
	public void recordReward( double reward ) {
		
		accReward += reward;
	        
		if ( state_action_history.isEmpty() )
			agt_logger.logf( Level.WARNING, name + "> Attempt to add reward with no state-action pairs recorded: %.2f\n", reward );
		else {
			// associate reward with last visited state-action pair
			InputActionPair iap = state_action_history.get( 0 );
			if ( iap.reward != 0 )
				agt_logger.logf( Level.FINEST, name + ">Adding additional reward to state-action pair: %.2f + %.2f\n", iap.reward, reward );
			String logStr = "";
			logStr += String.format( name + "> Associating reward %.2f to state-action pair:\n", reward );
			logStr += name + "> " + iap.toString() + "\n";
			agt_logger.logf( Level.FINEST, logStr );
			
			iap.reward += reward;
		}
	        
	}
	
	public void writePolicy( String fname ) {
		if ( update_policy ) {
			try {
				String file_name = POLICY_SAVING_DIR + "/" + fname;
				agt_logger.logf( Level.FINEST, name + "> Writing file: %s\n", file_name );
				BufferedWriter bw = new BufferedWriter( new FileWriter(file_name) );
				writePolicy( bw );
				agt_logger.logf( Level.FINEST, name + "> Finished writing file: %s\n", file_name );
				bw.close();
			} catch ( FileNotFoundException fnfe ) {
				agt_logger.logf( Level.SEVERE, name + "> File %s not found\n", fname );
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
	}
	
	protected void writePolicy( BufferedWriter bw ) throws IOException {
		for ( int k = 0; k < num_actions; k++ ) {
			bw.write( FILE_PREFIX + "> " + k + ":" );
			for ( int i = 0; i < num_state_features + 1; i++ )
				bw.write( String.format( " %.3f", value_function_weights[k][i] ) );
			bw.newLine();
		}
	}
	
}
