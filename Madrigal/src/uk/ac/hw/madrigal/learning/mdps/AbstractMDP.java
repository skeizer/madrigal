/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created September 2017                                                          *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to MDPs and reinforcement learning.                           *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.logging.Level;

import resources.Configuration;
import uk.ac.hw.madrigal.utils.CorpusStats;
import uk.ac.hw.madrigal.utils.LoggingManager;
import uk.ac.hw.madrigal.utils.MyLogger;

/**
 * Top level abstract class for an MDP model.  Subclasses will have specific implementations
 * of the abstract methods for action selection, updating, loading policies, etcetera. 
 */
public abstract class AbstractMDP {

	/** random number generator */
	public static Random rand_num_gen;

	/** static logger */
	public static MyLogger logger;

	public static boolean POLICY_OPTIMISATION;
	protected static String POLICY_SAVING_DIR;
	protected static int AVG_SCORE_WINDOW_SIZE = 50;
	
	/** epsilon: rate at which the policy selects a random action (for exploration during training) */
	protected static double exploration_rate = 0f;
	protected static double exploration_discount = 0f;

	/** gamma: discount factor in cumulative reward */
	protected static double discount_factor = 1f;

	/** tau: parameter controlling softmax policy */
	protected static double temperature = 0;
	protected static double temperature_discount = 1;
	
	/** alpha: learning rate for policy optimisation */
	protected static double learning_rate = 0f;
	protected static double learning_rate_discount = 0f;
	
	// --- Tabular MDP specific settings -----
	protected static String MCC = "MCC";
	protected static String SARSA = "SARSA";
	protected static String LVFA = "LVFA";
	
	/** RL algorithm: Monte Carlo Control (MCC), SARSA, or Linear Value Function Approximation (LVFA) */
	protected static String learning_algorithm = MCC;

	protected static boolean INCLUDE_SECOND_ORDER_FEATURES = false;
	// ---------------------------------------
	
	/** non-static logger */
	public MyLogger agt_logger; // logger for specific MDP model
	
	/** overall stats across all episodes collected */
	protected CorpusStats corpus_stats;

	protected String name;
	
	protected boolean update_policy;
	
	/** number of state features */
	protected int num_state_features;
    
	/** number of possible actions */
	protected int num_actions;
	
	protected double[] ones_mask;
	
	public AbstractMDP( String nm, int num_state_feats, int num_acts ) {
		name = nm;
		num_state_features = num_state_feats;
		num_actions = num_acts;
		corpus_stats = new CorpusStats();
		agt_logger = LoggingManager.setupLogging( Configuration.logging_directory, nm, Configuration.mdp_logging_level );
	}

	public abstract void loadPolicy( String fname ) throws IOException;

	public abstract void loadPolicy( InputStream is ) throws IOException;
	
	public abstract int getNextActionIndex( double[] state_features, double[] action_mask );

	public abstract void recordReward( double reward );
	
	public abstract void update( boolean success );
	
	public abstract void writePolicy( String fname );
	
	public void computeStatistics() {
		if ( POLICY_OPTIMISATION )
			trimCorpusStats( AVG_SCORE_WINDOW_SIZE );
		corpus_stats.computeAvgScore();
		agt_logger.logf( Level.INFO, name + "> Stats:\t%s\n", corpus_stats.getCompactSummary() );
	}
	
	public void trimCorpusStats( int window_size ) {
		int toremove = corpus_stats.getSize() - window_size;
		while ( toremove >= 0 ) {
			corpus_stats.removeFirst();
			toremove--;
		}
	}

	public void setUpdatePolicy() {
		update_policy = true;
	}
	
	public static void setPolOpt( String polDir ) {
		POLICY_OPTIMISATION = true;
		POLICY_SAVING_DIR = polDir;
		new File( POLICY_SAVING_DIR ).mkdirs();
		logger.logf( Level.INFO, "   NEW POLICY_SAVING_DIR: %s\n", POLICY_SAVING_DIR );
	}

	public static void reduceLearningRate() {
		String logStr = "";
		logStr += String.format( "=> alpha: %6.3e", learning_rate );
		learning_rate = Math.max( 0, learning_rate - learning_rate_discount );
		logStr += String.format( " --> %6.3e\n", learning_rate );
		logger.log( Level.FINEST, logStr );
	}

	public static void reduceExploration() {
		if ( POLICY_OPTIMISATION ) {
			String logStr = "";
			logStr += String.format( "=> epsilon: %.3f", exploration_rate );
			exploration_rate = Math.max( 0, exploration_rate - exploration_discount );
			logStr += String.format( " --> %.3f\n", exploration_rate );
			logStr += String.format( "=> tau: %.3f", temperature );
			temperature = Math.max( 0.2, temperature - temperature_discount );
			logStr += String.format( " --> %.3f\n", temperature );
			logger.log( Level.FINEST, logStr );
		}
	}

	public static void loadConfig() {
        POLICY_OPTIMISATION = Boolean.parseBoolean( Configuration.POLICY_OPTIMISATION );
        POLICY_SAVING_DIR = Configuration.POLICY_SAVING_DIR;
        if ( POLICY_OPTIMISATION )
    		new File( POLICY_SAVING_DIR ).mkdirs(); // ensure the directory exists
        AVG_SCORE_WINDOW_SIZE = Integer.parseInt( Configuration.AVG_SCORE_WINDOW_SIZE );

		learning_algorithm = Configuration.learning_algorithm;
        INCLUDE_SECOND_ORDER_FEATURES = Boolean.parseBoolean( Configuration.INCLUDE_SECOND_ORDER_FEATURES );

        StringBuffer logStr = new StringBuffer( "MDP configuration:\n" );
        logStr.append( String.format( "   POLICY_OPTIMISATION: %s\n", POLICY_OPTIMISATION ) );
		logStr.append( String.format( "   POLICY_SAVING_DIR: %s\n", POLICY_SAVING_DIR ) );
        logStr.append( String.format( "   AVG_SCORE_WINDOW_SIZE: %d\n", AVG_SCORE_WINDOW_SIZE ) );
		logStr.append( String.format( "   Learning algorithm: %s\n", learning_algorithm ) );
        logStr.append( String.format( "   INCLUDE_SECOND_ORDER_FEATURES: %s\n", INCLUDE_SECOND_ORDER_FEATURES ) );
        logStr.append( "\n" );
        logger.log( Level.WARNING, logStr.toString() );
        
        loadPolicyConfig();
	}
	
	private static void loadPolicyConfig() {
		discount_factor = Double.parseDouble( Configuration.discount_factor );
		temperature = Double.parseDouble( Configuration.temperature );
		temperature_discount = Double.parseDouble( Configuration.temperature_discount );
		exploration_rate = Double.parseDouble( Configuration.exploration_rate );
		exploration_discount = Double.parseDouble( Configuration.exploration_discount );
		learning_rate = Double.parseDouble( Configuration.learning_rate );
		learning_rate_discount = Double.parseDouble( Configuration.learning_rate_discount );
		
		StringBuffer logStr = new StringBuffer( "Policy configuration:\n" );
		logStr.append( String.format( "   Discount factor (gamma): %.3f\n", discount_factor ) );
		logStr.append( String.format( "   Temperature (tau): %.3f\n", temperature ) );
		logStr.append( String.format( "   Temperature discount: %.3e\n", temperature_discount ) );
		logStr.append( String.format( "   Exploration rate (epsilon): %.3f\n", exploration_rate ) );
		logStr.append( String.format( "   Exploration discount: %.3e\n", exploration_discount ) );
		logStr.append( String.format( "   Learning rate (alpha): %.3e\n", learning_rate ) );
		logStr.append( String.format( "   Learning rate discount: %.3e\n", learning_rate_discount ) );
		logger.log( Level.WARNING, logStr.toString() );

		Policy.loadConfig();
		if ( learning_algorithm.equals(LVFA) ) {
			LVFAPolicy.loadConfig();
		}
	}

}
