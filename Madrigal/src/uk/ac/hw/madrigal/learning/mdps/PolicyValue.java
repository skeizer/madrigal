/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016 (code from 2012)                                              *
 *     ---------------------------------------------------------------------------     *
 *     MDP RL agent software package.                                                  *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.learning.mdps;

public class PolicyValue {
	
	int action_index;
	
	int num_visits;
	
	double value;
	
	double prob;
	
	public PolicyValue( int a, int n, double v ) {
		action_index = a;
		num_visits = n;
		value = v;
		prob = -1;
	}

	public PolicyValue( int a ) {
		this( a, 0, 0 );
	}
	
	public PolicyValue( PolicyValue pv ) {
		action_index = pv.action_index;
		num_visits = pv.num_visits;
		value = pv.value;
		prob = pv.prob;		
	}
	
	protected void setValue( double v ) {
		value = v;
	}
	
	protected void addNumVisits( int n ) {
		num_visits += n;
	}
	
	public String toString() {
		return String.format( "%d %d %.3f %.3f", action_index, num_visits, value, prob );
	}
	
}
