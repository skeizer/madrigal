/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;

import resources.Configuration;
import resources.Resources;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.learning.mdps.AbstractMDP;
import uk.ac.hw.madrigal.learning.mdps.NumericMDP;

public abstract class AbstractMDPDialMan extends DialManAgent {

	private static int POLICY_SAVING_PERIOD = 5;
	private static String POLICY_FILENAME;
	private static boolean LOCAL_POLICY = false; // false: load policy from resources package; true: load policy from local/external file

	protected NumericMDP mdp_model;

	protected int num_state_feats; // dimensionality of state space
	protected int num_usr_acts;    // input features
	protected int num_sys_acts;    // input features
	protected int num_acts;        // output action set
	
	/** Constructor */
	public AbstractMDPDialMan() {
		super();
		initialise_MDP_model();
		load_MDP_policy();
	}
	
	/** Initialisation of dimensionality of state space and size of action space */
	abstract protected void initialise_MDP_model();

	/** definition of state space, linking information state with MDP model */
	abstract protected double[] getNumericStateFeatures();

	/** definition of action space, linking MDP summary action index with full dialogue act spec */
	abstract protected DialogueAct getDialogueAct( int action_index );

	private void load_MDP_policy() {
		if ( POLICY_FILENAME != null && !POLICY_FILENAME.isEmpty() ) {
			try {
				logger.logf( Level.INFO, "Loading MDP policy from file %s\n", POLICY_FILENAME );
				if ( LOCAL_POLICY )
					mdp_model.loadPolicy( POLICY_FILENAME );
				else
					mdp_model.loadPolicy( Resources.getResourceFileInputStream(POLICY_FILENAME) );
				logger.logf( Level.WARNING, "Successfully loaded MDP policy from file %s\n", POLICY_FILENAME );
			} catch ( FileNotFoundException fnfe ) {
				logger.logf( Level.SEVERE, "Could not load MDP policy %s\n", POLICY_FILENAME );
				fnfe.printStackTrace();
			} catch ( IOException ioe ) {
				logger.logf( Level.SEVERE, "Could not load MDP policy %s\n", POLICY_FILENAME );
				ioe.printStackTrace();
			}
		}
	}
	
	@Override
	public DialogueTurn respond() {
		// extract MDP state, select MDP action, and map back to full DialogueAct
		double[] state_features = getNumericStateFeatures();
		String logStr = "MDP features:";
		for ( int i = 0; i < num_state_feats; i++ )
			logStr += String.format( " %.2f", state_features[i] );
		logger.log( Level.INFO, logStr + "\n" );
		
		int action_index = mdp_model.getNextActionIndex( state_features );
		logger.logf( Level.INFO, "MDP action index: %d\n", action_index );
		
		DialogueAct sys_act = getDialogueAct( action_index );
    	logger.logf( Level.INFO, "Response act: %s\n", sys_act.toShortString() );
		
    	info_state.updateWithSysDialAct( sys_act );
		logger.logf( Level.FINE, "New info state:\n%s\n", info_state.toString() );
    	turn_num++;
    	
    	DialogueTurn sys_turn = new DialogueTurn( AgentName.system, AgentName.user );
    	sys_turn.setDialAct( sys_act );
    	sys_turn.setProcLevel( ProcLevel.INTERPRETATION );
		return sys_turn;
	}

	@Override
	public void observeReward( double reward ) {
		logger.logf( Level.INFO, "Observing reward: %.2f\n", reward );
		mdp_model.recordReward( reward );
	}

	@Override
	public void update( boolean success ) {
		logger.logf( Level.INFO, "Updating policy (success: %s)\n", success );
		mdp_model.update( success );
		savePolicies( "mdp" );
	}
	
	private void savePolicies( String name_prefix ) {
		if ( NumericMDP.POLICY_OPTIMISATION ) {
			if ( dial_num % POLICY_SAVING_PERIOD == 0 ) {
				String pol_name = name_prefix + "_" + dial_num + ".pcy";
				logger.logf( Level.FINE, "Saving policy (%d dialogues):\n\t%s\n", dial_num, pol_name );
	    		mdp_model.writePolicy( pol_name );
			}
			AbstractMDP.reduceExploration();
    		AbstractMDP.reduceLearningRate();
		}
	}
	
	public void computeStatistics() {
		mdp_model.computeStatistics();
	}
	
	public static void setPolicyFileName( String fname ) {
		POLICY_FILENAME = fname;
		LOCAL_POLICY = true;
		logger.logf( Level.INFO, "   NEW POLICY_FILENAME: %s\n", POLICY_FILENAME );
	}
	
	public static void loadConfig() {
		POLICY_SAVING_PERIOD = Integer.parseInt( Configuration.POLICY_SAVING_PERIOD );
		POLICY_FILENAME = Configuration.POLICY_FILE;

        StringBuffer logStr = new StringBuffer( "DeepMDP-based DM configuration:\n" );
        logStr.append( String.format( "   POLICY_SAVING_PERIOD: %d\n", POLICY_SAVING_PERIOD ) );
		logStr.append( String.format( "   POLICY_FILENAME: %s\n", POLICY_FILENAME ) );
        logStr.append( "\n" );
        logger.log( Level.WARNING, logStr.toString() );
        
		NumericMDP.loadConfig();
	}

}
