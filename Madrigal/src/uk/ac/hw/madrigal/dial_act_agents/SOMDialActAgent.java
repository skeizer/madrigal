/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created November 2016                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to the MaDrIgAL dialogue act agents.                          *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dial_act_agents;

import uk.ac.hw.madrigal.InfoState;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.learning.mdps.NumericMDP;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Dialogue Act Agent dedicated to the SOM dimension (using Dimension.socialObligationsManagement).
 */
public class SOMDialActAgent extends AbstractMDPDialActAgent {
	
	/** Constructor */
	public SOMDialActAgent( InfoState is ) {
		super( Dimension.socialObligationsManagement, is );
	}
	
	@Override
	protected void initialise_MDP_model( String name ) {
		num_usr_acts = 3;
		num_state_feats = num_usr_acts + 1;
		num_acts = 2;
		mdp_model = new NumericMDP( name, num_state_feats, num_acts );
		if ( UPDATE_POLICY_SOM )
			mdp_model.setUpdatePolicy();
	}

	@Override
	protected void setStateFeatures() {
		state_features = new double[ num_state_feats ];
		action_mask = Utils.getOnesVector( num_acts );
		int state_feat_ind = 0;
		
		// check any recorded user input processing problems
		if ( info_state.getProcProblem() != ProcLevel.NONE ) {
			state_features[ state_feat_ind ] = 1d;
			action_mask[1] = 0d;
		}
		state_feat_ind++;

		// user input
		DialogueAct lastUserAct = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		
		getUserActIndex( lastUserAct, state_features, state_feat_ind );
		state_feat_ind += num_usr_acts;
		
//		state_features[ state_feat_ind++ ] = ( lastUserAct == null ? 0d : lastUserAct.getConf() );
		
	}

	protected void getUserActIndex( DialogueAct usr_act, double[] result, int start_index ) {
		if ( usr_act == null )
			result[ start_index ] = 1d;
//		else if ( usr_act.getDimension() != Dimension.socialObligationsManagement )
//			result[ start_index + 1 ] = 1d;
		else {
			CommFunction cf = usr_act.getCommFunction();
			double conf = usr_act.getConf();
			if ( cf == CommFunction.initialGoodbye )
				result[ start_index + 1 ] = conf;
			else
				result[ start_index + 2 ] = conf;
		}
	}
	
	protected void getSysActIndex( DialogueAct sys_act, double[] result, int start_index ) {
		if ( sys_act == null )
			result[ start_index ] = 1d;
		else {
			CommFunction cf = sys_act.getCommFunction();
			if ( cf == CommFunction.inform ) //NOTE split into the two kinds of inform generated?
				result[ start_index + 1 ] = 1d;
			else if ( cf == CommFunction.propQuestion )
				result[ start_index + 2 ] = 1d;
			else if ( cf == CommFunction.setQuestion )
				result[ start_index + 3 ] = 1d;
			else if ( cf == CommFunction.autoNegative )
				result[ start_index + 4 ] = 1d;
			else if ( cf == CommFunction.confirm || cf == CommFunction.disconfirm )
				result[ start_index + 5 ] = 1d;
			else
				result[ start_index + 6 ] = 1d; //NOTE currently does not occur
		}
	}

	@Override
	protected DialogueAct getDialogueAct( int action_index ) {
		if ( action_index == 0 )
			return null;
		
		if ( action_index == 1 )
			return DialActUtils.newSystemAct( CommFunction.returnGoodbye );
		
		return null;
	}

	@Override
	public DialogueTurn getDialogueTurn() {
		// TODO Auto-generated method stub
		return null;
	}
		
}
