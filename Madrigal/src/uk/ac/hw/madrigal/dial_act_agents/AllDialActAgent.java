/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created November 2016                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to the MaDrIgAL dialogue act agents.                          *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dial_act_agents;

import java.util.ArrayList;
import java.util.logging.Level;

import uk.ac.hw.madrigal.InfoState;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.SemanticContent;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.domain.DBEntity;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.learning.mdps.NumericMDP;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Agent for the one-dimensional version of the DM (using Dimension.all).
 */
public class AllDialActAgent extends AbstractMDPDialActAgent {
	
	/** Constructor */
	public AllDialActAgent( InfoState is ) {
		super( Dimension.all, is );
	}
	
	@Override
	protected void initialise_MDP_model( String name ) {
		num_usr_acts = 9;
		num_sys_acts = 0; //7;
		num_state_feats = num_sys_acts + num_usr_acts + 9; //10;
		num_acts = 7;
		mdp_model = new NumericMDP( name, num_state_feats, num_acts );
		if ( UPDATE_POLICY_ALL )
			mdp_model.setUpdatePolicy();
	}

	@Override
	protected void setStateFeatures() {
		state_features = new double[ num_state_feats ];
		action_mask = Utils.getOnesVector( num_acts );
		int state_feat_ind = 0;
		
		//getSysActIndex( info_state.last_system_act, features, 0 );
		//state_feat_ind += num_sys_acts;
		
		// check any recorded user input processing problems
		if ( info_state.getProcProblem() != ProcLevel.NONE ) {
			state_features[ state_feat_ind ] = 1d;
//			action_mask[2] = 0d;
//			action_mask[3] = 0d;
//			action_mask[4] = 0d;
			action_mask[5] = 0d;
		} else
			action_mask[0] = 0d;
		state_feat_ind++;
		
		// user input
		DialogueAct lastUserAct = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		if ( lastUserAct != null ) {
			SemanticContent sem_content = lastUserAct.getSemContent();
			if ( sem_content.isEmpty() ) {
				action_mask[2] = 0d;
				action_mask[3] = 0d;
			} else {
				if ( lastUserAct.getCommFunction() != CommFunction.setQuestion )
					action_mask[2] = 0d;
				if ( lastUserAct.getCommFunction() != CommFunction.propQuestion )
					action_mask[3] = 0d;
			}
		}
		
		getUserActIndex( lastUserAct, state_features, state_feat_ind );
		state_feat_ind += num_usr_acts;
		
//		state_features[ state_feat_ind++ ] = ( lastUserAct == null ? 0d : lastUserAct.getConf() );
		
		state_features[state_feat_ind++] = ( info_state.last_uact_hyps.isEmpty() ? 1d : Utils.getEntropy( info_state.last_uact_hyps, true ) );

		// user goal
		state_features[ state_feat_ind ] = (double) info_state.ugoal_top_hyps.size() / info_state.num_user_goal_items;
		if ( info_state.ugoal_top_hyps.isEmpty() )
			action_mask[4] = 0d;
		state_feat_ind++;

		if ( info_state.minConfUG == null ) {
			state_features[ state_feat_ind ] = 0d;
			action_mask[1] = 0d;
		} else
			state_features[ state_feat_ind ] = info_state.minConfUG.getConf();
		state_feat_ind++;

		double maxEnt = 0d;
		for ( Double f: info_state.ug_item_entropies )
			if ( f > maxEnt )
				maxEnt = f;
		state_features[state_feat_ind++] = maxEnt;
		
		state_features[ state_feat_ind++ ] = ( info_state.current_entity == null ? 0d : 1d );
		if ( info_state.current_entity == null ) {
			// no entity to answer a setQ or propQ about 
			action_mask[2] = 0d;
			action_mask[3] = 0d;
//		} else {
//			action_mask[1] = 0d;
//			action_mask[6] = 0d;
		}
		
		state_features[ state_feat_ind++ ] = (double) info_state.matching_entities.size() / InfoState.database.entities.size();
		
		//state_features[ state_feat_ind++ ] = info_state.requested_slots.size();
		if ( info_state.requested_slots.isEmpty() || info_state.max_prob_req_slot == null )
			action_mask[2] = 0d; // no sign of a slot being requested
//		else
//			state_features[ state_feat_ind ] = 1d;
//		state_feat_ind++;
		
		state_features[ state_feat_ind++ ] = info_state.requested_slots_max_prob;
		
		// binary feature: all user goal item top hypotheses grounded
		state_features[ state_feat_ind++ ] = ( info_state.ugoal_top_hyps_grounded() ? 1d : 0d );
				
		//return features;
	}
	
	protected void getSysActIndex( DialogueAct sys_act, double[] result, int start_index ) {
		if ( sys_act == null )
			result[ start_index ] = 1d;
		else {
			CommFunction cf = sys_act.getCommFunction();
			if ( cf == CommFunction.inform ) //NOTE split into the two kinds of inform generated?
				result[ start_index + 1 ] = 1d;
			else if ( cf == CommFunction.propQuestion )
				result[ start_index + 2 ] = 1d;
			else if ( cf == CommFunction.setQuestion )
				result[ start_index + 3 ] = 1d;
			else if ( cf == CommFunction.autoNegative )
				result[ start_index + 4 ] = 1d;
			else if ( cf == CommFunction.confirm || cf == CommFunction.disconfirm )
				result[ start_index + 5 ] = 1d;
			else
				result[ start_index + 6 ] = 1d; //NOTE currently does not occur
		}
	}

	protected void getUserActIndex( DialogueAct usr_act, double[] result, int start_index ) {
		if ( usr_act == null )
			result[ start_index ] = 1d;
		else {
			CommFunction cf = usr_act.getCommFunction();
			double conf = usr_act.getConf();
			if ( cf == CommFunction.inform )
				result[ start_index + 1 ] = conf;
			else if ( cf == CommFunction.propQuestion )
				result[ start_index + 2 ] = conf;
			else if ( cf == CommFunction.setQuestion )
				result[ start_index + 3 ] = conf;
			else if ( cf == CommFunction.confirm )
				result[ start_index + 4 ] = conf;
			else if ( cf == CommFunction.disconfirm )
				result[ start_index + 5 ] = conf;
			else if ( cf == CommFunction.autoNegative )
				result[ start_index + 6 ] = conf;
			else if ( cf == CommFunction.initialGoodbye )
				result[ start_index + 7 ] = conf;
			else
				result[ start_index + 8 ] = conf;
		}
	}

	@Override
	protected DialogueAct getDialogueAct( int action_index ) {
		DialogueAct sysAct;
		boolean backoff = false;

		if ( action_index == 0 ) {
			if ( info_state.getProcProblem() == ProcLevel.PERCEPTION )
				return DialActUtils.newSystemAct( CommFunction.autoNegative_perc );
			if ( info_state.getProcProblem() == ProcLevel.INTERPRETATION )
				return DialActUtils.newSystemAct( CommFunction.autoNegative_int );
			logger.log( Level.FINE, "No processing problem recorded in info state: back off" );
			//return DialActUtils.newSystemAct( CommFunction.autoNegative );
			backoff = true;
		}
		
		if ( action_index == 1 ) {
			sysAct = DialActUtils.newSystemAct( CommFunction.propQuestion );
			UserGoalSVP minConfUG = info_state.minConfUG; //.getMinConfUsrGoalHyp();
			UserGoalItem ugConfItem = new UserGoalItem();
			//if ( info_state.ugoal_top_hyps.isEmpty() )
			if ( minConfUG == null ) {
				logger.log( Level.FINE, "No minConfUG in info state: back off" );
				//return DialActUtils.newSystemAct( CommFunction.autoNegative ); // back off
				backoff = true;
			} else {
				ugConfItem.addUGoalSVP( minConfUG );
				SemanticItem confItem = new SemanticItem( ugConfItem );
				sysAct.addItem( confItem );
				return sysAct;
			}
		}
		
		if ( action_index == 2 ) { // answer question about current entity (setQ)
			if ( info_state.current_entity == null || info_state.requested_slots.isEmpty() //) { 
					|| info_state.max_prob_req_slot == null ) {
				logger.log( Level.FINE, "No current entity or known requested slots: back off" );
				//return DialActUtils.newSystemAct( CommFunction.autoNegative );
				backoff = true;
			} else {
				sysAct = DialActUtils.newSystemAct( CommFunction.inform );
				//String primDescrStr = domain.getPrimarySlot();
				String val = info_state.current_entity.getAttribute( primeSlot );
				SemanticItem sem_item = new SemanticItem();
				sem_item.addSlotValuePair( new SlotValuePair(primeSlot,val) );
				// select slots believed to be requested
				if ( info_state.max_prob_req_slot != null ) { //NOTE is always the case: see previous if statement!
					SlotValuePair reqSVP = info_state.max_prob_req_slot.getSlotValuePair();
					val = info_state.current_entity.getAttribute( reqSVP.slot );
					sem_item.addSlotValuePair( new SlotValuePair(reqSVP.slot,val) );
				}
				sysAct.addItem( sem_item );
				return sysAct;
			}
		}
		
		if ( action_index == 3 ) { // answer question about current entity (propQ)
			if ( !info_state.last_uact_hyps.isEmpty() && info_state.current_entity != null ) {
				DialogueAct uactTopHyp = info_state.last_uact_hyps.get( 0 );
				SemanticContent sem_content = uactTopHyp.getSemContent();
				if ( uactTopHyp.getCommFunction() == CommFunction.propQuestion && !sem_content.isEmpty() ) {
					SemanticItem item = sem_content.getFirstItem();
					String val;
					for ( SlotValuePair svp: item.svps ) {
						if ( !svp.value.isEmpty() ) {
							val = info_state.current_entity.getAttribute( svp.slot );
							if ( svp.value.equals(val) )
								return DialActUtils.newSystemAct( CommFunction.confirm );
							else { // value incorrect
								DialogueAct da = DialActUtils.newSystemAct( CommFunction.disconfirm );
								SemanticItem it = new SemanticItem();
								SlotValuePair name_svp = new SlotValuePair( primeSlot, info_state.current_entity.getAttribute(primeSlot) );
								it.addSlotValuePair( name_svp );
								SlotValuePair corr_svp = new SlotValuePair( svp.slot, val );
								it.addSlotValuePair( corr_svp );
								da.addItem( it );
								return da;
							}
						}
					}
				}
			}
			logger.log( Level.FINE, "No entity to inform about: back off" );
			//return DialActUtils.newSystemAct( CommFunction.autoNegative );
			backoff = true;
		}
		
		if ( action_index == 4 ) { // give recommendation based on current user goal belief
			sysAct = DialActUtils.newSystemAct( CommFunction.inform );
			DBEntity entity = info_state.getRecommendation();
			SemanticItem sem_item = new SemanticItem();
			if ( entity == null ) { // no matching entities found in database
				String noneStr = SlotValuePair.getString( SlotValuePair.SpecialValue.NONE );
				sem_item.addSlotValuePair( new SlotValuePair(primeSlot,noneStr) );
			} else {
				SlotValuePair name_svp = new SlotValuePair( primeSlot, entity.getAttribute(primeSlot) );
				sem_item.addSlotValuePair( name_svp );
			}
			// including user goal slot value pairs, which were used for the database query
			// TODO only include those which have an 'actual' value ...
			for ( UserGoalSVP ug_svp: info_state.ugoal_top_hyps ) { // info_state.user_goal_items ) {
				//if ( entity == null || ug_svp.getGroundStatus() != GroundStatus.GROUNDED ) {
					SlotValuePair svp = ug_svp.getSlotValuePair();
					if ( svp.isConstraint() && !svp.value.equals(SlotValuePair.getString(SlotValuePair.SpecialValue.DONTCARE)) )
						sem_item.addSlotValuePair( new SlotValuePair(svp) );
				//}
			}
			//TODO handle cases where content is empty?
			
			//TODO including attributes should be handled by feedback agent; task agent would simply say "<name> matches your constraints",
			// or possibly include new attributes to help the user in their search 
			sysAct.addItem( sem_item );
		
			info_state.current_entity = entity;
			//TODO make current_entity keep track of which information has been conveyed about this entity	
			return sysAct;
		}

		if ( action_index == 5 )
			return DialActUtils.newSystemAct( CommFunction.returnGoodbye );
		
		if ( action_index == 6 ) {
			sysAct = DialActUtils.newSystemAct( CommFunction.setQuestion );
			int randInt;
			SlotValuePair randSVP = null;
			if ( info_state.slots_to_ask.isEmpty() ) {
				if ( info_state.minConfUG == null ) // can never happen?
					backoff = true;
				else {
					ArrayList<UserGoalSVP> ng_ug_hyps = info_state.getNonGroundedUGoalTopHyps();
					if ( ng_ug_hyps.isEmpty() )
						backoff = true;
					else {
						randInt = rand_num_gen.nextInt( ng_ug_hyps.size() );
						randSVP = new SlotValuePair( ng_ug_hyps.get(randInt).getSlotValuePair() );
					}
				}
			} else {
				randInt = rand_num_gen.nextInt( info_state.slots_to_ask.size() );
				randSVP = new SlotValuePair( info_state.slots_to_ask.get(randInt) );
			}
			if ( randSVP != null ) {
				randSVP.value = ""; // not necessary in case only items with grounding state INIT are selected
				SemanticItem newItem = new SemanticItem();
				newItem.addSlotValuePair( randSVP );
				sysAct.addItem( newItem );
				return sysAct;
			}
		}
		
		if ( backoff == true )
			return DialActUtils.newSystemAct( CommFunction.autoNegative ); //TODO N-best action selection?

		return null;
	}

	@Override
	public DialogueTurn getDialogueTurn() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
