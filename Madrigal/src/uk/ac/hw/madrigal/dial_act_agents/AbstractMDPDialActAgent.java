/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created November 2016                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to the MaDrIgAL dialogue act agents.                          *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dial_act_agents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;

import resources.Configuration;
import resources.Resources;
import uk.ac.hw.madrigal.InfoState;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.learning.mdps.AbstractMDP;
import uk.ac.hw.madrigal.utils.MyLogger;

/**
 * Abstract class for MDP based dialogue act agents.
 */
public abstract class AbstractMDPDialActAgent extends DialogueActAgent {

	private static int POLICY_SAVING_PERIOD = 5;
	private static String POLICY_FILENAME;
	private static String[] POL_FNAMES;
	private static String POLICY_FILENAME_TASK;
	private static String[] POL_FNAMES_TASK;
	private static String POLICY_FILENAME_AUTO_FB;
	private static String[] POL_FNAMES_AUTO_FB;
	private static String POLICY_FILENAME_SOM;
	private static String[] POL_FNAMES_SOM;
	private static String POLICY_FILENAME_EVAL;
	private static String[] POL_FNAMES_EVAL;
	
	private static int POLICY_POOL_SIZE = 5; // size of policy pool to sample from
	private static int POLICY_RAND_INDEX;    // random index for current policy to use
	
	private static boolean LOCAL_POLICIES = false; // false: load policy from resources package; true: load policy from local/external file
	protected static boolean UPDATE_POLICY_ALL;
	protected static boolean UPDATE_POLICY_TASK;
	protected static boolean UPDATE_POLICY_AUTO_FB;
	protected static boolean UPDATE_POLICY_SOM;
	protected static boolean UPDATE_POLICY_EVAL;
	
	private static int MAX_NUM_RESAMPLE_ATTEMPTS = 3;
	private static double PROC_PROBLEM_PENALTY = -1;
	private static double BACKOFF_PENALTY = -1; // system autoNegative act
	
	public static MyLogger main_logger; // = LoggingManager.getLogger( "main" );

	protected AbstractMDP mdp_model;

	protected int num_state_feats; // dimensionality of state space
	protected int num_usr_acts;    // input features
	protected int num_sys_acts;    // input features
	protected int num_acts;        // output action set
	
	protected double[] state_features;
	protected double[] action_mask; //TODO change to int type?

	/** Constructor */
	public AbstractMDPDialActAgent( Dimension dim, InfoState is ) {
		super( dim, is );
		initialise_MDP_model( dimension + "_mdp" );
		load_MDP_policy();
	}
	
	/** Initialisation of dimensionality of state space and size of action space */
	abstract protected void initialise_MDP_model( String name );

	/** definition of state space, linking information state with MDP model */
	abstract protected void setStateFeatures();
	
	/** definition of action space, linking MDP summary action index with full dialogue act spec */
	abstract protected DialogueAct getDialogueAct( int action_index );

	public abstract DialogueTurn getDialogueTurn();

	public void load_MDP_policy() {
		if ( dimension == Dimension.all )
			//load_MDP_policy( POLICY_FILENAME );
			load_MDP_policy( POL_FNAMES );
		else if ( dimension == Dimension.task )
			//load_MDP_policy( POLICY_FILENAME_TASK );
			load_MDP_policy( POL_FNAMES_TASK );
		else if ( dimension == Dimension.autoFeedback )
			//load_MDP_policy( POLICY_FILENAME_AUTO_FB );
			load_MDP_policy( POL_FNAMES_AUTO_FB );
		else if ( dimension == Dimension.socialObligationsManagement )
			//load_MDP_policy( POLICY_FILENAME_SOM );
			load_MDP_policy( POL_FNAMES_SOM );
		else if ( dimension == Dimension.eval )
			//load_MDP_policy( POLICY_FILENAME_EVAL );
			load_MDP_policy( POL_FNAMES_EVAL );
	}
	
	private void load_MDP_policy( String[] fnames ) {
		if ( fnames.length == 0 ) {
			agt_logger.logf( Level.INFO, "No policy files available" );
			return;
		}
//		int rand_pol_index = rand_num_gen.nextInt( fnames.length );
//		String rand_fname = fnames[rand_pol_index];
		String rand_fname = fnames[ POLICY_RAND_INDEX ];
		agt_logger.logf( Level.INFO, "Random policy file: %s\n", rand_fname );
		load_MDP_policy( rand_fname );
	}
	
	private void load_MDP_policy( String fname ) {
		if ( fname != null && !fname.isEmpty() ) {
			try {
				agt_logger.logf( Level.INFO, "Loading MDP policy from file %s\n", fname );
				if ( LOCAL_POLICIES )
					mdp_model.loadPolicy( fname );
				else
					mdp_model.loadPolicy( Resources.getResourceFileInputStream(fname) );
				agt_logger.logf( Level.WARNING, "Successfully loaded MDP policy from file %s\n", fname );
			} catch ( FileNotFoundException fnfe ) {
				agt_logger.logf( Level.SEVERE, "Could not load MDP policy %s\n", fname );
				//fnfe.printStackTrace();
			} catch ( IOException ioe ) {
				agt_logger.logf( Level.SEVERE, "Could not load MDP policy %s\n", fname );
				//ioe.printStackTrace();
			}
		} else System.out.println( "ERROR: empty file name" );	
	}
	
	@Override
	//public DialogueAct generateCandidate1() {
	public void generateCandidate() {
		agt_logger.log( Level.INFO, "Generating dialogue act candidate\n" );
		
		// extract MDP state, select MDP action, and map back to full DialogueAct
		setStateFeatures();
		String logStr = dimension + "> MDP features:";
		for ( int i = 0; i < num_state_feats; i++ )
			logStr += String.format( " %.2f", state_features[i] );
		agt_logger.log( Level.INFO, logStr + "\n" );
		logStr = dimension + "> MDP action mask:";
		for ( int i = 0; i < num_acts; i++ )
			logStr += String.format( " %.2f", action_mask[i] );
		agt_logger.log( Level.INFO, logStr + "\n" );
		
		// get response action from MDP using action mask
		int action_index = mdp_model.getNextActionIndex( state_features, action_mask );
		agt_logger.logf( Level.INFO, "   action_index: %d\n", action_index );
		DialogueAct sys_act = getDialogueAct( action_index );

		// store dial act agent action index in InfoState
		info_state.dial_act_candidates.add( new DialogueActCandidate(dimension,action_index,sys_act) );
		
		// handle internal shaping reward
		double shapingReward = getShapingReward( sys_act );
		if ( shapingReward != 0 ) {
			logger.logf( Level.INFO, "%s> Shaping reward: %.0f\n", dimension, shapingReward );
			main_logger.logf( Level.INFO, "IntRew> %.0f (%s)\n", shapingReward, dimension );
			observeShapingReward( shapingReward );
			//mdp_model.recordReward( shapingReward );
		}
		
		if ( sys_act != null ) {
			sys_act.setDimension( dimension );
			agt_logger.logf( Level.INFO, "%s> Candidate act: %s\n", dimension, sys_act.toShortString() );
		} else
			agt_logger.logf( Level.INFO, "%s> Candidate act: none\n", dimension );
		
		//return sys_act;
	}

	private double getShapingReward( DialogueAct sys_act ) {
//		if ( sys_act == null )
//			return 0d;
		
		CommFunction cf = ( sys_act == null ? null : sys_act.getCommFunction() );
		
		if ( dimension == Dimension.all || dimension == Dimension.autoFeedback ) {
			if ( cf == CommFunction.autoNegative )
				return BACKOFF_PENALTY;
			
			ProcLevel lvl = info_state.getProcProblem();
			if ( lvl == ProcLevel.PERCEPTION )
				if ( cf != CommFunction.autoNegative_perc )
					return PROC_PROBLEM_PENALTY;
				else
					return 0d;
			else
				if ( cf == CommFunction.autoNegative_perc )
					return PROC_PROBLEM_PENALTY;
			
			if ( lvl == ProcLevel.INTERPRETATION )
				if ( cf != CommFunction.autoNegative_int )
					return PROC_PROBLEM_PENALTY;
				else
					return 0d;
			else
				if ( cf == CommFunction.autoNegative_int )
					return PROC_PROBLEM_PENALTY;
			
		}
		
		return 0d;
	}

	@Override
	public void observeReward( double reward ) {
		agt_logger.logf( Level.INFO, "%s> Observing reward: %.2f\n", dimension, reward );
		mdp_model.recordReward( reward );
	}
	
	public void observeShapingReward( double reward ) {
		agt_logger.logf( Level.INFO, "%s> Observing shaping reward: %.2f\n", dimension, reward );
		mdp_model.recordReward( reward );		
	}
	
	public void sendBackoffPenalty() {
		agt_logger.logf( Level.INFO, "%s> Observing backoff penalty: %.2f\n", dimension, BACKOFF_PENALTY );
		main_logger.logf( Level.INFO, "IntRew> %.0f (%s)\n", BACKOFF_PENALTY, dimension );
		mdp_model.recordReward( BACKOFF_PENALTY );
	}

	@Override
	public void update( boolean success, int dial_num ) {
		agt_logger.logf( Level.INFO, "%s> Updating policy (success: %s)\n", dimension, success );
		mdp_model.update( success );
		savePolicy( dial_num );
	}
	
	private void savePolicy( int dial_num ) {
		if ( AbstractMDP.POLICY_OPTIMISATION ) {
			if ( dial_num % POLICY_SAVING_PERIOD == 0 || (dial_num < 1000 && dial_num % 200 == 0) ) {
				String pol_name = dimension + "_" + dial_num + ".pcy";
				agt_logger.logf( Level.FINE, "%s> Saving policy (%d dialogues):\n\t%s\n", dimension, dial_num, pol_name );
	    		mdp_model.writePolicy( pol_name );
			}
		}
	}
	
	public void computeStatistics() {
		mdp_model.computeStatistics();
	}
	
	public static void setPolicyFileName( String path_to_file ) {
		POLICY_FILENAME = path_to_file;
		LOCAL_POLICIES = true;
		logger.logf( Level.INFO, "   NEW POLICY_FILENAME: %s\n", POLICY_FILENAME );
	}
	
	public static void setPolicyFileName( String path, String affix ) {
		POLICY_FILENAME = path + "/" + Dimension.all + "_" + affix;
		POL_FNAMES = getPolicyFileNames( POLICY_FILENAME );		
		POLICY_FILENAME_TASK = path + "/" + Dimension.task + "_" + affix;
		POL_FNAMES_TASK = getPolicyFileNames( POLICY_FILENAME_TASK );		
		POLICY_FILENAME_AUTO_FB = path + "/" + Dimension.autoFeedback + "_" + affix;
		POL_FNAMES_AUTO_FB = getPolicyFileNames( POLICY_FILENAME_AUTO_FB );		
		POLICY_FILENAME_SOM = path + "/" + Dimension.socialObligationsManagement + "_" + affix;
		POL_FNAMES_SOM = getPolicyFileNames( POLICY_FILENAME_SOM );		
		POLICY_FILENAME_EVAL = path + "/eval" + "_" + affix;
		POL_FNAMES_EVAL = getPolicyFileNames( POLICY_FILENAME_EVAL );		
		LOCAL_POLICIES = true;
		POLICY_POOL_SIZE = 1;
        StringBuffer logStr = new StringBuffer( "DialActAgent new policies:\n" );
        logStr.append( String.format( "   NEW POLICY_FILENAME: %s\n", POLICY_FILENAME ) );
        logStr.append( String.format( "   NEW POLICY_FILENAME_TASK: %s\n", POLICY_FILENAME_TASK ) );
        logStr.append( String.format( "   NEW POLICY_FILENAME_AUTO_FB: %s\n", POLICY_FILENAME_AUTO_FB ) );
        logStr.append( String.format( "   NEW POLICY_FILENAME_SOM: %s\n", POLICY_FILENAME_SOM ) );
        logStr.append( String.format( "   NEW POLICY_FILENAME_EVAL: %s\n", POLICY_FILENAME_EVAL ) );
        logStr.append( "\n" );
		logger.log( Level.WARNING, logStr.toString() );

	}
	
	private static String[] getPolicyFileNames( String pol_fnames_str ) {
		String[] fnames = StringUtils.split( pol_fnames_str, ", " );
		StringBuffer logStr = new StringBuffer( "Policy file names:\n" );
		for ( int i = 0; i < fnames.length; i++ )
			logStr.append( String.format( "  %s\n", fnames[i] ) );
		logger.log( Level.WARNING, logStr.toString() );
		return fnames;
	}
	
	public static void resetPolicyIndex() {
		POLICY_RAND_INDEX = rand_num_gen.nextInt( POLICY_POOL_SIZE );
	}
	
	public static void loadConfig() {
		POLICY_SAVING_PERIOD = Integer.parseInt( Configuration.POLICY_SAVING_PERIOD );

		POLICY_FILENAME = Configuration.POLICY_FILE;
		POLICY_FILENAME_TASK = Configuration.POLICY_FILE_TASK;
		POLICY_FILENAME_AUTO_FB = Configuration.POLICY_FILE_AUTO_FB;
		POLICY_FILENAME_SOM = Configuration.POLICY_FILE_SOM;
		POLICY_FILENAME_EVAL = Configuration.POLICY_FILE_EVAL;

		POLICY_POOL_SIZE = Integer.parseInt( Configuration.POLICY_POOL_SIZE );
		
		POL_FNAMES = getPolicyFileNames( POLICY_FILENAME );
		POL_FNAMES_TASK = getPolicyFileNames( POLICY_FILENAME_TASK );
		POL_FNAMES_AUTO_FB = getPolicyFileNames( POLICY_FILENAME_AUTO_FB );
		POL_FNAMES_SOM = getPolicyFileNames( POLICY_FILENAME_SOM );
		POL_FNAMES_EVAL = getPolicyFileNames( POLICY_FILENAME_EVAL );
		
		LOCAL_POLICIES = Boolean.parseBoolean( Configuration.LOCAL_POLICIES );
		UPDATE_POLICY_ALL = Boolean.parseBoolean( Configuration.UPDATE_POLICY_ALL );
		UPDATE_POLICY_TASK = Boolean.parseBoolean( Configuration.UPDATE_POLICY_TASK );
		UPDATE_POLICY_AUTO_FB = Boolean.parseBoolean( Configuration.UPDATE_POLICY_AUTO_FB );
		UPDATE_POLICY_SOM = Boolean.parseBoolean( Configuration.UPDATE_POLICY_SOM );
		UPDATE_POLICY_EVAL = Boolean.parseBoolean( Configuration.UPDATE_POLICY_EVAL );
		MAX_NUM_RESAMPLE_ATTEMPTS = Integer.parseInt( Configuration.MAX_NUM_RESAMPLE_ATTEMPTS );
		PROC_PROBLEM_PENALTY = Double.parseDouble( Configuration.PROC_PROBLEM_PENALTY );
		BACKOFF_PENALTY = Double.parseDouble( Configuration.BACKOFF_PENALTY );

        StringBuffer logStr = new StringBuffer( "DeepMDP-based DM configuration:\n" );
        logStr.append( String.format( "   POLICY_SAVING_PERIOD: %d\n", POLICY_SAVING_PERIOD ) );
		logStr.append( String.format( "   POLICY_FILENAME:         %s\n", POLICY_FILENAME ) );
		logStr.append( String.format( "   POLICY_FILENAME_TASK:    %s\n", POLICY_FILENAME_TASK ) );
		logStr.append( String.format( "   POLICY_FILENAME_AUTO_FB: %s\n", POLICY_FILENAME_AUTO_FB ) );
		logStr.append( String.format( "   POLICY_FILENAME_SOM:     %s\n", POLICY_FILENAME_SOM ) );
		logStr.append( String.format( "   POLICY_FILENAME_EVAL:    %s\n", POLICY_FILENAME_EVAL ) );
		logStr.append( String.format( "   POLICY_POOL_SIZE:        %d\n", POLICY_POOL_SIZE ) );		
		logStr.append( String.format( "   LOCAL_POLICIES:          %s\n", LOCAL_POLICIES ) );
		logStr.append( String.format( "   UPDATE_POLICY_TASK:      %s\n", UPDATE_POLICY_ALL ) );
		logStr.append( String.format( "   UPDATE_POLICY_TASK:      %s\n", UPDATE_POLICY_TASK ) );
		logStr.append( String.format( "   UPDATE_POLICY_AUTO_FB:   %s\n", UPDATE_POLICY_AUTO_FB ) );
		logStr.append( String.format( "   UPDATE_POLICY_SOM:       %s\n", UPDATE_POLICY_SOM ) );
		logStr.append( String.format( "   UPDATE_POLICY_EVAL:      %s\n", UPDATE_POLICY_EVAL ) );
		logStr.append( String.format( "   MAX_NUM_RESAMPLE_ATTEMPTS: %d\n", MAX_NUM_RESAMPLE_ATTEMPTS ) );
		logStr.append( String.format( "   PROC_PROBLEM_PENALTY: %.0f\n", PROC_PROBLEM_PENALTY ) );
		logStr.append( String.format( "   BACKOFF_PENALTY:      %.2f\n", BACKOFF_PENALTY ) );
        logStr.append( "\n" );
        logger.log( Level.WARNING, logStr.toString() );
        
		AbstractMDP.loadConfig();
	}

}
