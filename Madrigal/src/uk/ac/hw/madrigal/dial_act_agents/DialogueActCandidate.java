package uk.ac.hw.madrigal.dial_act_agents;

import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;

public class DialogueActCandidate {
	
	private Dimension dimension;
	
	private int action_index;
	
	private DialogueAct sys_act;
	
	public DialogueActCandidate( Dimension dim, int act_ind, DialogueAct sact ) {
		dimension = dim;
		action_index = act_ind;
		sys_act = sact;
	}
	
	public Dimension getDimension() { return dimension; }
	public int getActionIndex() { return action_index; }
	public DialogueAct getSysAct() { return sys_act; }

}
