/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created November 2016                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to the MaDrIgAL dialogue act agents.                          *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dial_act_agents;

import java.util.ArrayList;
import java.util.logging.Level;

import uk.ac.hw.madrigal.InfoState;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.SemanticContent;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.domain.DBEntity;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.learning.mdps.NumericMDP;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Dialogue Act Agent dedicated to the task dimension (using Dimension.task).
 */
public class TaskDialActAgent extends AbstractMDPDialActAgent {
	
	/** Constructor */
	public TaskDialActAgent( InfoState is ) {
		super( Dimension.task, is );
	}
	
	@Override
	protected void initialise_MDP_model( String name ) {
		num_usr_acts = 9;
		num_state_feats = num_usr_acts + 8;
		num_acts = 5;
		mdp_model = new NumericMDP( name, num_state_feats, num_acts );
		if ( UPDATE_POLICY_TASK )
			mdp_model.setUpdatePolicy();
	}

	@Override
	protected void setStateFeatures() {
		state_features = new double[ num_state_feats ];
		action_mask = Utils.getOnesVector( num_acts );
		int state_feat_ind = 0;
		
		// check any recorded user input processing problems
		if ( info_state.getProcProblem() != ProcLevel.NONE ) {
			state_features[ state_feat_ind ] = 1d;
//			action_mask[1] = 0d;
//			action_mask[2] = 0d;
//			action_mask[3] = 0d;
		}
		state_feat_ind++;
		
		// user input
		DialogueAct lastUserAct = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		if ( lastUserAct == null ) {
			action_mask[1] = 0d;
			action_mask[2] = 0d;
		} else {			
			SemanticContent sem_content = lastUserAct.getSemContent();
			if ( sem_content.isEmpty() ) {
				action_mask[1] = 0d;
				action_mask[2] = 0d;
			} else {
				if ( lastUserAct.getCommFunction() != CommFunction.setQuestion )
					action_mask[1] = 0d;
				if ( lastUserAct.getCommFunction() != CommFunction.propQuestion )
					action_mask[2] = 0d;
			}
		}
		
		getUserActIndex( lastUserAct, state_features, state_feat_ind );
		state_feat_ind += num_usr_acts;
		
//		state_features[ state_feat_ind++ ] = ( lastUserAct == null ? 0d : lastUserAct.getConf() );
		
//		state_features[state_feat_ind++] = ( info_state.last_uact_hyps.isEmpty() ? 1d : Utils.getEntropy( info_state.last_uact_hyps, true ) );

		// user goal
		state_features[ state_feat_ind ] = (double) info_state.ugoal_top_hyps.size() / info_state.num_user_goal_items;
		if ( info_state.ugoal_top_hyps.isEmpty() )
			action_mask[3] = 0d;
		state_feat_ind++;
		
		if ( info_state.minConfUG == null )
			state_features[ state_feat_ind ] = 0d;
		else
			state_features[ state_feat_ind ] = info_state.minConfUG.getConf();
		state_feat_ind++;

//		double maxEnt = 0d;
//		for ( Double f: info_state.ug_item_entropies )
//			if ( f > maxEnt )
//				maxEnt = f;
//		state_features[state_feat_ind++] = maxEnt;
		
		state_features[ state_feat_ind++ ] = ( info_state.current_entity == null ? 0d : 1d );
		if ( info_state.current_entity == null ) {
			// no entity to answer a setQ or propQ about 
			action_mask[1] = 0d;
			action_mask[2] = 0d;
//		} else {
//			action_mask[4] = 0d;
		}
		
		state_features[ state_feat_ind++ ] = (double) info_state.matching_entities.size() / InfoState.database.entities.size();
		
		//state_features[ state_feat_ind++ ] = info_state.requested_slots.size();
		if ( info_state.requested_slots.isEmpty() || info_state.max_prob_req_slot == null )
			action_mask[1] = 0d; // no sign of a slot being requested
//		else
//			state_features[ state_feat_ind ] = 1d;
//		state_feat_ind++;
		
		state_features[ state_feat_ind++ ] = info_state.requested_slots_max_prob;
		
		// binary feature: all user goal item top hypotheses grounded
		state_features[ state_feat_ind++ ] = ( info_state.ugoal_top_hyps_grounded() ? 1d : 0d );
		
		if ( info_state.slots_to_ask.isEmpty() )
			action_mask[4] = 0d;
		else
			state_features[ state_feat_ind ] = 1d;
		state_feat_ind++;
		
	}
	
	protected void getUserActIndex( DialogueAct usr_act, double[] result, int start_index ) {
		if ( usr_act == null )
			result[ start_index ] = 1d;
		else {
			CommFunction cf = usr_act.getCommFunction();
			double conf = usr_act.getConf();
			if ( cf == CommFunction.inform )
				result[ start_index + 1 ] = conf;
			else if ( cf == CommFunction.propQuestion )
				result[ start_index + 2 ] = conf;
			else if ( cf == CommFunction.setQuestion )
				result[ start_index + 3 ] = conf;
			else if ( cf == CommFunction.confirm )
				result[ start_index + 4 ] = conf;
			else if ( cf == CommFunction.disconfirm )
				result[ start_index + 5 ] = conf;
			else if ( cf == CommFunction.autoNegative )
				result[ start_index + 6 ] = conf;
			else if ( cf == CommFunction.initialGoodbye )
				result[ start_index + 7 ] = conf;
			else
				result[ start_index + 8 ] = conf;
		}
	}
			
	@Override
	protected DialogueAct getDialogueAct( int action_index ) {
		DialogueAct sysAct;

		if ( action_index == 0 ) // do nothing
			return null;

		if ( action_index == 1 ) { // answer question about current entity (setQ)
			
			// check if we can answer a setQuestion about the current entity
			if ( info_state.current_entity == null || info_state.requested_slots.isEmpty()
					|| info_state.max_prob_req_slot == null ) {
				agt_logger.log( Level.INFO, "Task DBG> action_index == 1 : no current entity | no requested slots | no max prob req slot\n" );
				return null;
			}

			// create inform act with reference to current entity
			sysAct = DialActUtils.newSystemAct( CommFunction.inform );
			String val = info_state.current_entity.getAttribute( primeSlot );
			SemanticItem sem_item = new SemanticItem();
			sem_item.addSlotValuePair( new SlotValuePair(primeSlot,val) );
			
			// select slot believed to be requested and get value for current entity
			SlotValuePair reqSVP = info_state.max_prob_req_slot.getSlotValuePair();
			val = info_state.current_entity.getAttribute( reqSVP.slot );
			
			// include slot-value pair in semantic content of inform act
			sem_item.addSlotValuePair( new SlotValuePair(reqSVP.slot,val) );
			
			sysAct.addItem( sem_item );
			return sysAct;
		}
		
		if ( action_index == 2 ) { // answer question about current entity (propQ)
			
			// check if we can answer a propQuestion about the current entity
			if ( info_state.last_uact_hyps.isEmpty() || info_state.current_entity == null ) {
				agt_logger.log( Level.INFO, "Task DBG> action_index == 2 : no user act top hypothesis available | no current entity\n" );
				return null;
			}
			
			// retrieve slot-value pair from last user act to verify
			DialogueAct uactTopHyp = info_state.last_uact_hyps.get( 0 );
			SemanticContent sem_content = uactTopHyp.getSemContent();
			if ( uactTopHyp.getCommFunction() == CommFunction.propQuestion && !sem_content.isEmpty() ) {
				SemanticItem item = sem_content.getFirstItem();
//				String val;
				ArrayList<String> vals;
				for ( SlotValuePair svp: item.svps ) {
					if ( !svp.value.isEmpty() ) {
						// get correct value for current entity
//						val = info_state.current_entity.getAttribute( svp.slot ); // what about multiple values?
						vals = info_state.current_entity.getSlotValues( svp.slot );
						if ( vals.contains(svp.value) ) {
						//if ( svp.value.equals(val) ) // value correct
							DialogueAct da = DialActUtils.newSystemAct( CommFunction.confirm );
							//TODO include svp
							SemanticItem it = new SemanticItem();
							SlotValuePair name_svp = new SlotValuePair( primeSlot, info_state.current_entity.getAttribute(primeSlot) );
							it.addSlotValuePair( name_svp );
							SlotValuePair corr_svp = new SlotValuePair( svp.slot, svp.value );
							it.addSlotValuePair( corr_svp );
							da.addItem( it );
							return da;
						} else { // value incorrect
							DialogueAct da = DialActUtils.newSystemAct( CommFunction.disconfirm );
							if ( !vals.isEmpty() ) { // e.g. this place is not near anything - for now, just say no
								SemanticItem it = new SemanticItem();
								SlotValuePair name_svp = new SlotValuePair( primeSlot, info_state.current_entity.getAttribute(primeSlot) );
								it.addSlotValuePair( name_svp );
								int rnd_ind = rand_num_gen.nextInt( vals.size() ); //TODO present multiple vals?
								SlotValuePair corr_svp = new SlotValuePair( svp.slot, vals.get(rnd_ind) );
								it.addSlotValuePair( corr_svp );
								da.addItem( it );
							}
							return da;
						}
					}
				}
				
			}

		}
		
		if ( action_index == 3 ) { // give recommendation based on current user goal belief
			
			// check if we have any user goal information with which to search the database and make a recommendation
			if ( info_state.ugoal_top_hyps.isEmpty() ) {
				agt_logger.log( Level.INFO, "Task DBG> action_index == 3 : no user goal top hypotheses\n" );
				return null;
			}
			
			// create inform act presenting an entity matching the user goal top hypothesis
			sysAct = DialActUtils.newSystemAct( CommFunction.inform );
			DBEntity entity = info_state.getRecommendation();
			SemanticItem sem_item = new SemanticItem();
			if ( entity == null ) { // no matching entities found in database
				String noneStr = SlotValuePair.getString( SlotValuePair.SpecialValue.NONE );
				sem_item.addSlotValuePair( new SlotValuePair(primeSlot,noneStr) );
			} else {
				SlotValuePair name_svp = new SlotValuePair( primeSlot, entity.getAttribute(primeSlot) );
				sem_item.addSlotValuePair( name_svp );
			}
			// including user goal slot value pairs, which were used for the database query
			// TODO only include those which have an 'actual' value ...
			for ( UserGoalSVP ug_svp: info_state.ugoal_top_hyps ) { // info_state.user_goal_items ) {
				//if ( entity == null || ug_svp.getGroundStatus() != GroundStatus.GROUNDED ) {
					SlotValuePair svp = ug_svp.getSlotValuePair();
					if ( svp.isConstraint() && !svp.value.equals(SlotValuePair.getString(SlotValuePair.SpecialValue.DONTCARE)) )
						sem_item.addSlotValuePair( new SlotValuePair(svp) );
				//}
			}
			// handle cases where content is empty
			if ( sem_item.svps.isEmpty() ) {
				agt_logger.log( Level.INFO, "Task DBG> action_index == 3 : inform act with empty content\n" );
				return null;
			}

			//TODO including attributes should be handled by feedback agent; task agent would simply say "<name> matches your constraints",
			// or possibly include new attributes to help the user in their search 
			sysAct.addItem( sem_item );
		
			info_state.current_entity = entity;
			//TODO make current_entity keep track of which information has been conveyed about this entity
			
			return sysAct;
		}

		if ( action_index == 4 ) { // ask user for their preference w.r.t. a slot
			
			// create setQuestion and determine which slot to ask for
			sysAct = DialActUtils.newSystemAct( CommFunction.setQuestion );
			SlotValuePair randSVP;
			if ( info_state.slots_to_ask.isEmpty() ) { // no slots that have not been discussed yet: back off to requesting uncertain slot
				if ( info_state.minConfUG == null ) { // no uncertain user goal slot available: 
					int bound = info_state.ugoal_top_hyps.size();
					if ( bound == 0 ) {
						String randSlot = info_state.getRandomUGoalSlot();
						randSVP = new SlotValuePair( randSlot, "" );
					} else {
						int randInd = rand_num_gen.nextInt( bound );
						randSVP = new SlotValuePair( info_state.ugoal_top_hyps.get(randInd).getSlotValuePair() );
					}
				} else // select most uncertain user goal slot
					randSVP = new SlotValuePair( info_state.minConfUG.getSlotValuePair() );
			} else { // select random slot from list of slots that have not yet been discussed
				SlotValuePair rand_svp = info_state.slots_to_ask.get( rand_num_gen.nextInt(info_state.slots_to_ask.size()) ); 
				randSVP = new SlotValuePair( rand_svp ); // copy this svp
			}
			randSVP.value = ""; // not necessary in case only items with grounding state INIT are selected
			SemanticItem newItem = new SemanticItem();
			newItem.addSlotValuePair( randSVP );
			sysAct.addItem( newItem );
			
			return sysAct;
		}
		
		return null;
	}

	@Override
	public DialogueTurn getDialogueTurn() {
		// TODO Auto-generated method stub
		return null;
	}

}
