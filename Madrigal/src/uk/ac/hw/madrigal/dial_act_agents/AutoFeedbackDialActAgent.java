/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created November 2016                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to the MaDrIgAL dialogue act agents.                          *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dial_act_agents;

import java.util.logging.Level;

import uk.ac.hw.madrigal.InfoState;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.learning.mdps.NumericMDP;
import uk.ac.hw.madrigal.utils.Utils;

/**
 * Dialogue Act Agent dedicated to the auto-feedback dimension (using Dimension.autoFeedback).
 */
public class AutoFeedbackDialActAgent extends AbstractMDPDialActAgent {
	
	/** Constructor */
	public AutoFeedbackDialActAgent( InfoState is ) {
		super( Dimension.autoFeedback, is );
	}
	
	@Override
	protected void initialise_MDP_model( String name ) {
		num_usr_acts = 3;
		num_state_feats = num_usr_acts + 7;
		num_acts = 3;
		mdp_model = new NumericMDP( name, num_state_feats, num_acts );
		if ( UPDATE_POLICY_AUTO_FB )
			mdp_model.setUpdatePolicy();
	}

	@Override
	protected void setStateFeatures() {
		state_features = new double[ num_state_feats ];
		action_mask = Utils.getOnesVector( num_acts );
		int state_feat_ind = 0;
		
		// check any recorded user input processing problems
		if ( info_state.getProcProblem() != ProcLevel.NONE ) {
			state_features[ state_feat_ind ] = 1d;
		} else
			action_mask[1] = 0d;
		state_feat_ind++;
		
		// user input
		DialogueAct lastUserAct = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		
		getUserActIndex( lastUserAct, state_features, state_feat_ind );
		state_feat_ind += num_usr_acts;
		
//		state_features[ state_feat_ind++ ] = ( lastUserAct == null ? 0d : lastUserAct.getConf() );
		
		state_features[state_feat_ind++] = ( info_state.last_uact_hyps.isEmpty() ? 1d : Utils.getEntropy( info_state.last_uact_hyps, true ) );

		// user goal
		state_features[ state_feat_ind ] = (double) info_state.ugoal_top_hyps.size() / info_state.num_user_goal_items;
//		if ( info_state.ugoal_top_hyps.isEmpty() )
//			action_mask[2] = 0d;
		state_feat_ind++;

		if ( info_state.minConfUG == null ) {
			state_features[ state_feat_ind ] = 0d;
			action_mask[2] = 0d;
		} else {
			state_features[ state_feat_ind ] = info_state.minConfUG.getConf();
			if ( info_state.ugoal_top_hyps_grounded() )
				action_mask[2] = 0d;
		}
		state_feat_ind++;

		double maxEnt = 0d;
		for ( Double f: info_state.ug_item_entropies )
			if ( f > maxEnt )
				maxEnt = f;
		state_features[state_feat_ind++] = maxEnt;
		
//		state_features[ state_feat_ind++ ] = ( info_state.current_entity == null ? 0d : 1d );
//		if ( info_state.current_entity == null ) {
//		} else {
//			action_mask[1] = 0d;
//		}
		
		state_features[ state_feat_ind++ ] = info_state.requested_slots_max_prob;
		
		// binary feature: all user goal item top hypotheses grounded
		state_features[ state_feat_ind++ ] = ( info_state.ugoal_top_hyps_grounded() ? 1d : 0d );
				
	}

	protected void getUserActIndex( DialogueAct usr_act, double[] result, int start_index ) {
		if ( usr_act == null )
			result[ start_index ] = 1d;
		else {
			CommFunction cf = usr_act.getCommFunction();
			double conf = usr_act.getConf();
			if ( cf == CommFunction.initialGoodbye )
				result[ start_index + 1 ] = conf;
			else
				result[ start_index + 2 ] = conf;
		}
	}
	
	protected void getSysActIndex( DialogueAct sys_act, double[] result, int start_index ) {
		if ( sys_act == null )
			result[ start_index ] = 1d;
		else {
			CommFunction cf = sys_act.getCommFunction();
			if ( cf == CommFunction.inform ) //NOTE split into the two kinds of inform generated?
				result[ start_index + 1 ] = 1d;
			else if ( cf == CommFunction.propQuestion )
				result[ start_index + 2 ] = 1d;
			else if ( cf == CommFunction.setQuestion )
				result[ start_index + 3 ] = 1d;
			else if ( cf == CommFunction.autoNegative )
				result[ start_index + 4 ] = 1d;
			else if ( cf == CommFunction.confirm || cf == CommFunction.disconfirm )
				result[ start_index + 5 ] = 1d;
			else
				result[ start_index + 6 ] = 1d; //NOTE currently does not occur
		}
	}

	@Override
	protected DialogueAct getDialogueAct( int action_index ) {
		DialogueAct sysAct;
		boolean backoff = false;

		if ( action_index == 0 ) {
			return null; //DialActUtils.newSystemAct( CommFunction.autoPositive );
		}
		
		if ( action_index == 1 ) {
			if ( info_state.getProcProblem() == ProcLevel.PERCEPTION )
				return DialActUtils.newSystemAct( CommFunction.autoNegative_perc );
			if ( info_state.getProcProblem() == ProcLevel.INTERPRETATION )
				return DialActUtils.newSystemAct( CommFunction.autoNegative_int );
			if ( info_state.getProcProblem() == ProcLevel.EXECUTION )
				return DialActUtils.newSystemAct( CommFunction.autoNegative ); //TODO create execution level act
			//return DialActUtils.newSystemAct( CommFunction.autoNegative );
			backoff = true;
		}
		
		if ( action_index == 2 ) {
			sysAct = DialActUtils.newSystemAct( CommFunction.propQuestion );
			UserGoalSVP minConfUG = info_state.minConfUG;
			UserGoalItem ugConfItem = new UserGoalItem();
			if ( minConfUG == null ) {
				logger.log( Level.FINE, "No minConfUG in info state: back off" );
				//return DialActUtils.newSystemAct( CommFunction.autoNegative );
				backoff = true;
			} else {
				ugConfItem.addUGoalSVP( minConfUG );
				SemanticItem confItem = new SemanticItem( ugConfItem );
				sysAct.addItem( confItem );
				return sysAct;
			}
		}
		
		if ( backoff == true ) //NOTE should never happen with proper use of action mask
			return DialActUtils.newSystemAct( CommFunction.autoNegative );

		return null; //NOTE should never be reached
	}

	@Override
	public DialogueTurn getDialogueTurn() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
