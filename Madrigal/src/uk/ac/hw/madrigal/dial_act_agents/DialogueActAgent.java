/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created November 2016                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Package dedicated to the MaDrIgAL dialogue act agents.                          *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal.dial_act_agents;

import java.util.Random;

import resources.Configuration;
import uk.ac.hw.madrigal.InfoState;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.Dimension;
import uk.ac.hw.madrigal.domain.Ontology;
import uk.ac.hw.madrigal.utils.LoggingManager;
import uk.ac.hw.madrigal.utils.MyLogger;

/**
 * Represents a dialogue act agent, dedicated to generating dialogue act
 * candidates from one of the dimensions of the taxonomy.
 */
public abstract class DialogueActAgent {
	
	public static MyLogger logger; // general logger (e.g. for config settings)

	protected MyLogger agt_logger; // logger for specific DA Agent

	public static Random rand_num_gen;

	public static Ontology domain;
	
	protected String primeSlot = domain.primarySlot();
	
	/** Dimension of the taxonomy this DA agent is dedicated to. */
	public Dimension dimension;
	
	/** Information state this DA agent operates on. */
	public InfoState info_state;
	
	/** Constructor */
	public DialogueActAgent( Dimension dim, InfoState is ) {
		dimension = dim;
		info_state = is;
		agt_logger = LoggingManager.setupLogging( Configuration.logging_directory, dimension + "_agt", Configuration.dman_logging_level ); //.logging_level );
	}
	
	/** 
	 * Given the current information state, generate a candidate dialogue act 
	 * from the dimension associated with this agent
	 */
	public abstract void generateCandidate();
	
	public abstract void observeReward( double reward );

	public abstract void sendBackoffPenalty();

	public abstract void update( boolean success, int dial_num );

	public abstract void computeStatistics();

	public void savePolicy() {}

	public void set_policy_filename( String affix ) {}
	
}
