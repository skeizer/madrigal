/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created September 2016                                                          *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;

import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.SemanticItem;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.domain.DBEntity;
import uk.ac.hw.madrigal.domain.SlotValuePair;
import uk.ac.hw.madrigal.domain.UserGoalItem;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.learning.mdps.TabularMDP;

public class TabularMDPDialMan extends AbstractTabularMDPDialMan {
	
	@Override
	protected void initialise_MDP_model() {
		num_state_feats = 6;
		state_dims = new int[ num_state_feats ];
		state_dims[0] = 6; // see getSysActIndex
		state_dims[1] = 6; // top usr act hyp; see getUserActIndex
		state_dims[2] = 5; // SLU conf bins
		state_dims[3] = 6; // UG hyp min belief conf bins
		state_dims[4] = 4; // number of matching entities: 0, 1, 2, >2
		state_dims[5] = 5; // Req Slots max belief conf bins
		num_acts = 6; // autoNegative, propQuestion, answer, recommend, bye, setQuestion
		mdp_model = new TabularMDP( "dial_man_mdp", num_state_feats, state_dims, num_acts );
	}

	@Override
	/** definition of state space, linking information state with MDP model */
	protected int[] getStateFeatures() {
		int[] features = new int[ num_state_feats ];
		DialogueAct lastSysAct = info_state.last_system_act;
		features[0] = getSysActIndex( lastSysAct );
		DialogueAct lastUserAct = ( info_state.last_uact_hyps.isEmpty() ? null : info_state.last_uact_hyps.get(0) );
		features[1] = getUserActIndex( lastUserAct );
		double SLU_conf = lastUserAct.getConf();
		features[2] = getConfBin( SLU_conf, state_dims[2] );
		if ( info_state.minConfUG == null )
			features[3] = state_dims[3] - 1; // reserve last value to indicate there is no min conf UG hyp
		else {
			double minConf = info_state.minConfUG.getConf();
			logger.logf( Level.FINE, "minConfUG: %.2f\n", minConf );
			features[3] = getConfBin( minConf, state_dims[3] - 1 );
		}
		int numMatchEnts = info_state.matching_entities.size();
		features[4] = getNumIndex( numMatchEnts, state_dims[4] );
		
		reqSlots = info_state.requested_slots; // info_state.getReqSlots();
		logger.logf( Level.FINE, "reqSlots: %s\n", StringUtils.join(reqSlots, ",") );
		//TODO replace with info_state members
		double maxProb = 0d;
		UserGoalSVP ug_svp_max = null;
		for ( UserGoalSVP ug_svp: reqSlots ) {
			if ( ug_svp.getConf() > maxProb ) {
				maxProb = ug_svp.getConf();
				ug_svp_max = ug_svp;
			}
		}
		logger.logf( Level.FINE, "max prob reqSlot: %s (%.3f)\n", ( ug_svp_max == null ? "NONE" : ug_svp_max.toString() ), maxProb );
		//features[5] = ( reqSlots.isEmpty() ? 0 : 1 );
		if ( ug_svp_max == null )
			features[5] = state_dims[5] - 1; // reserve last value to indicate there is no evidence for any requested slots
		else
			features[5] = getConfBin( maxProb, state_dims[5] - 1 );
		
		//features[6] = ( (info_state.react_pressure == ReactivePressure.BYE) ? 1 : 0 );
		//features[6] = ( lastUserAct.getCommFunction() == CommFunction.initialGoodbye ? 1 : 0 );

		String logStr = "MDP features:";
		for ( int i = 0; i < num_state_feats; i++ )
			logStr += " " + features[i];
		logger.log( Level.FINE, logStr + "\n" );

		return features;
	}
	
	protected DialogueAct getDialogueAct( int action_index ) {
		DialogueAct sysAct;
		
		if ( action_index == 0 )
			return DialActUtils.newSystemAct( CommFunction.autoNegative );
		
		if ( action_index == 1 ) {
			sysAct = DialActUtils.newSystemAct( CommFunction.propQuestion );
			UserGoalSVP minConfUG = info_state.minConfUG; //.getMinConfUsrGoalHyp();
			UserGoalItem ugConfItem = new UserGoalItem();
			//if ( info_state.ugoal_top_hyps.isEmpty() )
			if ( minConfUG == null ) {
				logger.log( Level.FINE, "No minConfUG in info state: backing off to autoNegative" );
				return DialActUtils.newSystemAct( CommFunction.autoNegative ); // back off
				//TODO correct policy execution? (action index 1 -> 0) 
			} else {
				ugConfItem.addUGoalSVP( minConfUG );
				SemanticItem confItem = new SemanticItem( ugConfItem );
				sysAct.addItem( confItem );
				return sysAct;
			}
		}
		
		if ( action_index == 2 ) { // answer question about current entity
			if ( info_state.current_entity == null || reqSlots.isEmpty() ) {
				logger.log( Level.FINE, "No current entity or known requested slots: backing off to autoNegative" );
				return DialActUtils.newSystemAct( CommFunction.autoNegative ); // backoff (no entity to answer questions about)
			}
			sysAct = DialActUtils.newSystemAct( CommFunction.inform );
			//String primDescrStr = domain.getPrimarySlot();
			String val = info_state.current_entity.getAttribute( primeSlot );
			SemanticItem sem_item = new SemanticItem();
			sem_item.addSlotValuePair( new SlotValuePair(primeSlot,val) );
			// select slots believed to be requested
			String req_slot;
			for ( UserGoalSVP req_ug_svp: reqSlots ) {
				if ( req_ug_svp.getConf() > 0.01 ) {
					req_slot = req_ug_svp.getSlotValuePair().slot;
					val = info_state.current_entity.getAttribute( req_slot );
					sem_item.addSlotValuePair( new SlotValuePair(req_slot,val) );
				}
			}
			sysAct.addItem( sem_item );
			return sysAct;
		}
		
		if ( action_index == 3 ) { // give recommendation based on current user goal belief
			sysAct = DialActUtils.newSystemAct( CommFunction.inform );
//			String primDescrStr = SlotValuePair.getPrimDescrStr();
			//String primDescrStr = domain.getPrimarySlot();
			DBEntity entity = info_state.getRecommendation();
			SemanticItem sem_item = new SemanticItem();
			if ( entity == null ) { // no matching entities found in database
				String noneStr = SlotValuePair.getString( SlotValuePair.SpecialValue.NONE );
				sem_item.addSlotValuePair( new SlotValuePair(primeSlot,noneStr) );
			} else {
				SlotValuePair name_svp = new SlotValuePair( primeSlot, entity.getAttribute(primeSlot) );
				sem_item.addSlotValuePair( name_svp );
			}
				// including user goal slot value pairs, which were used for the database query
				// TODO only include those which have an 'actual' value ...
				for ( UserGoalSVP ug_svp: info_state.ugoal_top_hyps ) { // info_state.user_goal_items ) {  
					SlotValuePair svp = ug_svp.getSlotValuePair();
					if ( svp.isConstraint() && !svp.value.equals(SlotValuePair.getString(SlotValuePair.SpecialValue.DONTCARE)) )
						sem_item.addSlotValuePair( new SlotValuePair(svp) );
				}
				//TODO including attributes should be handled by feedback agent; task agent would simply say "<name> matches your constraints",
				// or possibly include new attributes to help the user in their search 
	//			for ( SlotValuePair svp: entity.getAttributes() )
	//				sem_item.addSlotValuePair( svp );
			//}
			sysAct.addItem( sem_item );
		
			info_state.current_entity = entity;
			//TODO make current_entity keep track of which information has been conveyed about this entity	
			return sysAct;
		}

		if ( action_index == 4 )
			return DialActUtils.newSystemAct( CommFunction.returnGoodbye );
		
		if ( action_index == 5 ) {
			sysAct = DialActUtils.newSystemAct( CommFunction.setQuestion );
			SlotValuePair randSVP;
			if ( info_state.slots_to_ask.isEmpty() ) // no slots that have not been discussed yet: back off to requesting uncertain slot
				if ( info_state.minConfUG == null ) {
					int bound = info_state.ugoal_top_hyps.size();
					if ( bound == 0 ) {
						String randSlot = info_state.getRandomUGoalSlot();
						randSVP = new SlotValuePair( randSlot, "" );
					} else
						randSVP = new SlotValuePair( info_state.ugoal_top_hyps.get(bound).getSlotValuePair() );
				} else
					randSVP = new SlotValuePair( info_state.minConfUG.getSlotValuePair() );
			else
				randSVP = info_state.slots_to_ask.get( rand_num_gen.nextInt(info_state.slots_to_ask.size()) );
			randSVP.value = ""; // not necessary in case only items with grounding state INIT are selected
			SemanticItem newItem = new SemanticItem();
			newItem.addSlotValuePair( randSVP );
			sysAct.addItem( newItem );
			return sysAct;
		}
		
		return null;
	}
	
}
