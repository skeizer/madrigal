/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created November 2016                                                           *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;

import resources.Configuration;
import uk.ac.hw.madrigal.dial_act_agents.AbstractMDPDialActAgent;
import uk.ac.hw.madrigal.dial_act_agents.AllDialActAgent;
import uk.ac.hw.madrigal.dial_act_agents.AutoFeedbackDialActAgent;
import uk.ac.hw.madrigal.dial_act_agents.DialogueActAgent;
import uk.ac.hw.madrigal.dial_act_agents.DialogueActCandidate;
import uk.ac.hw.madrigal.dial_act_agents.EvalDialActAgent;
import uk.ac.hw.madrigal.dial_act_agents.SOMDialActAgent;
import uk.ac.hw.madrigal.dial_act_agents.TabularEvalDialActAgent;
import uk.ac.hw.madrigal.dial_act_agents.TaskDialActAgent;
import uk.ac.hw.madrigal.dialogue_acts.DialActUtils;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.ProcLevel;
import uk.ac.hw.madrigal.learning.mdps.AbstractMDP;
import uk.ac.hw.madrigal.utils.LoggingManager;

/**
 * Dialogue manager which generates combinations of dialogue acts
 * in response to user input.
 */
public class MultiDimDialMan extends DialManAgent {
	
	private static boolean ONE_DIMENSION_MODE = true;
	private static boolean USE_TASK = false;
	private static boolean USE_AUTO_FEEDBACK = false;
	private static boolean USE_SOM = false;
	private static boolean USE_EVAL = false;
	
	/** Dialogue act agents contributing to this DMs responses */
	ArrayList<DialogueActAgent> dial_act_agents;
	
	//EvalDialActAgent
	AbstractMDPDialActAgent eval_dial_act_agent;
	private boolean tabular_eval = false; // temporary flag
	private boolean reloadPolicy;
	
	/** Constructor */
	public MultiDimDialMan() {
		super();
		dial_act_agents = new ArrayList<DialogueActAgent>();
		reloadPolicy = false;
		if ( ONE_DIMENSION_MODE ) 
			dial_act_agents.add( new AllDialActAgent(info_state) );
		else { // ensures system cannot be both one- and multi-dimensional
			// determine index for multiple policies
			AbstractMDPDialActAgent.resetPolicyIndex();
			if ( USE_TASK ) dial_act_agents.add( new TaskDialActAgent(info_state) );
			if ( USE_AUTO_FEEDBACK ) dial_act_agents.add( new AutoFeedbackDialActAgent(info_state) );
			if ( USE_SOM ) dial_act_agents.add( new SOMDialActAgent(info_state) );
			if ( USE_EVAL ) {
				if ( tabular_eval )
					eval_dial_act_agent = new TabularEvalDialActAgent( info_state );
				else
					eval_dial_act_agent = new EvalDialActAgent( info_state );
			}
		}
	}
	
	public void setReloadPolicy( boolean reload ) {
		reloadPolicy = reload;
	}
	
	public void reset() {
		super.reset();
		if ( reloadPolicy ) { // re-load dial act agent policies
			AbstractMDPDialActAgent.resetPolicyIndex();
			for ( DialogueActAgent da_agt: dial_act_agents )
				((AbstractMDPDialActAgent) da_agt).load_MDP_policy();
		}
	}

	@Override
	public DialogueTurn respond() {
		DialogueTurn sys_turn = new DialogueTurn( AgentName.system, AgentName.user );
		//DialogueAct sys_act;
		logger.log( Level.INFO, "Collecting dialogue act candidates\n" );
		info_state.dial_act_candidates.clear();
		for ( DialogueActAgent dact_agt: dial_act_agents ) {
			dact_agt.generateCandidate();
		}
		// logging the DA candidates
		String logStr = "Candidates:\n";
		DialogueAct cand_sys_act;
		for ( DialogueActCandidate da_cand: info_state.dial_act_candidates ) {
			cand_sys_act = da_cand.getSysAct();
			logStr += "    " + da_cand.getDimension() + ": " + (cand_sys_act==null?"null":cand_sys_act.toShortString()) + "\n";
		}
		logger.log( Level.INFO, logStr );
		
		if ( USE_EVAL )
			if ( tabular_eval )
				sys_turn = eval_dial_act_agent.getDialogueTurn();
			else
				sys_turn = eval_dial_act_agent.getDialogueTurn();
		else
			for ( DialogueActCandidate da_cand: info_state.dial_act_candidates )
				sys_turn.setDialAct( da_cand.getSysAct() );

		if ( sys_turn.dact_all == null ) { //NOTE should not occur if USE_EVAL
			logger.logf( Level.WARNING, "System turn dact_all == null :  back off\n" );
			sys_turn.setDialAct( DialActUtils.newSystemAct(CommFunction.autoNegative) ); // back-off to negative feedback
			logger.log( Level.INFO, "Penalising all DA agents for back off action" );
			sendBackoffPenalty();
			
		} else {
			logger.logf( Level.INFO, "Final dial acts:\n%s\n", sys_turn.toString() );
			info_state.updateWithSysDialAct( sys_turn.dact_all ); //NOTE currently only supporting update with one-dimensional sys turns
			logger.logf( Level.FINE, "New info state:\n%s\n", info_state.toString() );
		}
    	turn_num++;
		
    	// reset processing problem
		info_state.setProcProblem( ProcLevel.NONE );
		    	
		return sys_turn;
	}

	@Override
	public void observeReward( double reward ) {
		logger.logf( Level.INFO, "Observing reward: %.2f\n", reward );
		for ( DialogueActAgent dact_agt: dial_act_agents )
			dact_agt.observeReward( reward );
		if ( USE_EVAL ) eval_dial_act_agent.observeReward( reward );
	}
	
	private void sendBackoffPenalty() {
		for ( DialogueActAgent dact_agt: dial_act_agents )
			dact_agt.sendBackoffPenalty();
		if ( USE_EVAL ) eval_dial_act_agent.sendBackoffPenalty();		
	}

	@Override
	public void update( boolean success ) {
		logger.logf( Level.INFO, "Updating policy (success: %s)\n", success );
		for ( DialogueActAgent dact_agt: dial_act_agents ) {
			dact_agt.update( success, dial_num );
			//dact_agt.savePolicy();
		}
		if ( USE_EVAL ) eval_dial_act_agent.update( success, dial_num );
		AbstractMDP.reduceExploration();
		AbstractMDP.reduceLearningRate();
	}
	
	@Override
	public void computeStatistics() {
		for ( DialogueActAgent dact_agt: dial_act_agents )
			dact_agt.computeStatistics();
		if ( USE_EVAL ) eval_dial_act_agent.computeStatistics();
	}
	
	public static void setPolicyFilenames( String path_to_file ) {
		// split into path and policy filename affix:
		String[] parts = path_to_file.split( File.separator );
		String path = StringUtils.join( parts, File.separator, 0, parts.length - 1 );
		String[] fnparts = parts[ parts.length - 1 ].split( "_" );
		String affix = fnparts[ fnparts.length - 1 ];
		AbstractMDPDialActAgent.setPolicyFileName( path, affix );
	}
	
	public static void loadConfig() {
		ONE_DIMENSION_MODE = Boolean.parseBoolean( Configuration.ONE_DIMENSION_MODE );
		USE_TASK = Boolean.parseBoolean( Configuration.USE_TASK );
		USE_AUTO_FEEDBACK = Boolean.parseBoolean( Configuration.USE_AUTO_FEEDBACK );
		USE_SOM = Boolean.parseBoolean( Configuration.USE_SOM );
		USE_EVAL = Boolean.parseBoolean( Configuration.USE_EVAL );

        StringBuffer logStr = new StringBuffer( "DeepMDP-based DM configuration:\n" );
        logStr.append( String.format( "   ONE_DIMENSION_MODE: %s\n", ONE_DIMENSION_MODE ) );
        logStr.append( String.format( "   USE_TASK:           %s\n", USE_TASK ) );
        logStr.append( String.format( "   USE_AUTO_FEEDBACK:  %s\n", USE_AUTO_FEEDBACK ) );
        logStr.append( String.format( "   USE_SOM:            %s\n", USE_SOM ) );
        logStr.append( String.format( "   USE_EVAL:           %s\n", USE_EVAL ) );
        logStr.append( "\n" );
        logger.log( Level.WARNING, logStr.toString() );
        
        AbstractMDPDialActAgent.main_logger = LoggingManager.getLogger( "main" );
        AbstractMDPDialActAgent.loadConfig();
	}

}
