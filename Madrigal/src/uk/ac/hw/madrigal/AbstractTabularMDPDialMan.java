/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created June 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Main package of the MaDrIgAL dialogue manager software.                         *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package uk.ac.hw.madrigal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

import resources.Configuration;
import resources.Resources;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn;
import uk.ac.hw.madrigal.dialogue_acts.DialogueAct.CommFunction;
import uk.ac.hw.madrigal.dialogue_acts.DialogueTurn.AgentName;
import uk.ac.hw.madrigal.domain.UserGoalSVP;
import uk.ac.hw.madrigal.learning.mdps.AbstractMDP;
import uk.ac.hw.madrigal.learning.mdps.TabularMDP;

/**
 * This class represents a dialogue manager that selects actions using a trainable MDP model.
 */
public abstract class AbstractTabularMDPDialMan extends DialManAgent {
		
	private static int POLICY_SAVING_PERIOD = 5;
	private static boolean USE_POLICY_OBJECTS = false;
	private static String POLICY_FILENAME = "";
	private static boolean LOCAL_POLICY = false; // false: load policy from resources package; true: load policy from local/external file
	
	protected TabularMDP mdp_model;
	protected ArrayList< UserGoalSVP > reqSlots;
	
	protected int num_state_feats;
	protected int[] state_dims;
	protected int num_acts;

	/** Constructor */
	public AbstractTabularMDPDialMan() {
		super(); // initialises info_state
		initialise_MDP_model();
		load_MDP_policy();
	}
	
	abstract protected void initialise_MDP_model();

	private void load_MDP_policy() {
		if ( POLICY_FILENAME != null && !POLICY_FILENAME.isEmpty() ) {
			try {
				logger.logf( Level.INFO, "Loading MDP policy from file %s\n", POLICY_FILENAME );
				if ( LOCAL_POLICY )
					mdp_model.loadPolicy( POLICY_FILENAME, USE_POLICY_OBJECTS );
				else
					mdp_model.loadPolicy( Resources.getResourceFileInputStream(POLICY_FILENAME) );
			} catch ( IOException ioe ) {
				logger.logf( Level.SEVERE, "Could not load MDP policy %s\n", POLICY_FILENAME );
				ioe.printStackTrace();
			}
			logger.logf( Level.INFO, "Successfully loaded MDP policy from file %s\n", POLICY_FILENAME );
		}
	}
	
	public DialogueTurn respond() {
		// extract MDP state, select MDP action, and map back to full DialogueAct
		int[] state_features = getStateFeatures();
		int action_index = mdp_model.getNextActionIndex( state_features );
		logger.logf( Level.INFO, "MDP action index: %d\n", action_index );
		DialogueAct sys_act = getDialogueAct( action_index );
    	logger.logf( Level.INFO, "Response act: %s\n", sys_act.toShortString() );
		info_state.updateWithSysDialAct( sys_act );
    	turn_num++;
    	
    	DialogueTurn sys_turn = new DialogueTurn( AgentName.system, AgentName.user );
    	sys_turn.setDialAct( sys_act );
		return sys_turn;
	}
	
	/** definition of state space, linking information state with MDP model */
	abstract protected int[] getStateFeatures();

	protected int getNumIndex( int num, int numCats ) {
		if ( num < numCats )
			return num;
		return numCats;
	}
	
	protected int getConfBin( double conf, int numBins ) {
		for ( int i = 0; i < numBins - 1; i++ )
			if ( conf < ((double) i + 1 ) / numBins )
				return i;
		return numBins - 1;
	}
	
	protected int getSysActIndex( DialogueAct sys_act ) {
		if ( sys_act == null )
			return 0;
		CommFunction cf = sys_act.getCommFunction();
		if ( cf == CommFunction.inform )
			return 1;
		if ( cf == CommFunction.propQuestion )
			return 2;
		if ( cf == CommFunction.setQuestion )
			return 3;
		if ( cf == CommFunction.autoNegative )
			return 4;
		if ( cf == CommFunction.autoPositive )
			return 5;
		return 0;
	}

	protected int getUserActIndex( DialogueAct usr_act ) {
		if ( usr_act == null )
			return 0;
		CommFunction cf = usr_act.getCommFunction();
		if ( cf == CommFunction.inform )
			return 1;
		if ( cf == CommFunction.propQuestion )
			return 2;
		if ( cf == CommFunction.setQuestion )
			return 3;
		if ( cf == CommFunction.initialGreeting )
			return 4;
		if ( cf == CommFunction.initialGoodbye )
			return 5;
		return 0;
	}

	/** definition of action space, linking MDP summary action index with full dialogue act spec */
	abstract protected DialogueAct getDialogueAct( int action_index );

	private void savePolicies( String name_prefix ) {
		if ( TabularMDP.POLICY_OPTIMISATION ) {
			if ( dial_num % POLICY_SAVING_PERIOD == 0 ) {
				String pol_name = name_prefix + "_" + dial_num + (USE_POLICY_OBJECTS?".obj":".pcy");
				logger.logf( Level.FINE, "Saving policy (%d dialogues):\n\t%s\n", dial_num, pol_name );
	    		mdp_model.writePolicy( pol_name, USE_POLICY_OBJECTS );
	    		//Policy.reduceExploration();
	    		AbstractMDP.reduceExploration();
			}
    		//Policy.reduceLearningRate();
			AbstractMDP.reduceLearningRate();
		}
	}
	
	public void computeStatistics() {
		mdp_model.computeStatistics();
	}
	
	@Override
	public void observeReward( double reward ) {
		logger.logf( Level.INFO, "Observing reward: %.2f\n", reward );
		mdp_model.recordReward( reward );
	}

	@Override
	public void update( boolean success ) {
		logger.logf( Level.INFO, "Updating policy (success: %s)\n", success );
		mdp_model.update( success );
		savePolicies( "mdp" );
	}
	
	public static void setPolicyFileName( String fname ) {
		POLICY_FILENAME = fname;
		LOCAL_POLICY = true;
		logger.logf( Level.INFO, "   NEW POLICY_FILENAME: %s\n", POLICY_FILENAME );
	}
	
	public static void loadConfig() {
		POLICY_SAVING_PERIOD = Integer.parseInt( Configuration.POLICY_SAVING_PERIOD );
		USE_POLICY_OBJECTS = Boolean.parseBoolean( Configuration.USE_POLICY_OBJECTS );
		POLICY_FILENAME = Configuration.POLICY_FILE;
		
        StringBuffer logStr = new StringBuffer( "MDP-based DM configuration:\n" );
		logStr.append( String.format( "   POLICY_SAVING_PERIOD: %d\n", POLICY_SAVING_PERIOD ) );
		logStr.append( String.format( "   USE_POLICY_OBJECTS: %s\n", USE_POLICY_OBJECTS ) );
		logStr.append( String.format( "   POLICY_FILENAME: %s\n", POLICY_FILENAME ) );
        logStr.append( "\n" );
        logger.log( Level.INFO, logStr.toString() );
		TabularMDP.loadConfig();
	}
	
}
