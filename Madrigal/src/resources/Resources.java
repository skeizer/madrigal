/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created July 2016                                                               *
 *     ---------------------------------------------------------------------------     *
 *     Package for storing and loading resources.                                      *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

public class Resources {
	
	private static String getResourcePath( String file ) {
		String path = "/resources/";
		return path + file;
	}
	public static Properties readProperties( String file ) throws IOException {
		String resPath = getResourcePath( file );
		//System.out.printf( "Loading resource path: %s\n", resPath );
		URL url = Resources.class.getResource( resPath );
		if ( url == null )
			System.out.println( "ERROR: resource URL null!!!" );
		Properties props = new Properties();
		props.load( url.openStream() );
		return props;
	}
	
	public static InputStream getResourceFileInputStream( String file ) throws IOException {
		String resPath = getResourcePath( file );
		//System.out.printf( "Loading resource: %s\n", resPath );
		URL url = Resources.class.getResource( resPath );
		if ( url == null )
			System.out.println( "ERROR: resource URL null!!!" );
		return url.openStream();
	}
	
	public static BufferedReader getResourceFileReader( String file ) throws IOException {
		return new BufferedReader( new InputStreamReader(getResourceFileInputStream(file)) );
	}
	
    public static String readResourceFile( String file ) throws IOException {
		BufferedReader reader = getResourceFileReader( file );
		return read( reader );
    }
    
    private static String read( BufferedReader reader ) throws IOException  {
	    String line = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    String ls = System.getProperty( "line.separator" );

	    while( ( line = reader.readLine() ) != null ) {
	        stringBuilder.append( line );
	        stringBuilder.append( ls );
	    }
	    reader.close();

	    return stringBuilder.toString();
    }
	   
}
