/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        _     _        _      _ _      _ _     _ _      _ _        _     _           *
 *      /   \ /   \    /   \   |    \   |    \    |     /     \    /   \   |           *
 *     |     |     |  |_ _ _|  |     |  |_ _ /    |    |  _ _     |_ _ _|  |           *
 *     |     |     |  |     |  |     |  |   \     |    |      \   |     |  |           *
 *     |     |     |  |     |  |_ _ /   |    \   _|_    \ _ _ /   |     |  |_ _ _      *
 *                                                                                     *
 *     ===========================================================================     *
 *                                                                                     *
 *                    Copyright (C) 2016 Interaction Lab                               *
 *                                                                                     *
 *                           All rights reserved                                       *
 *                                                                                     *
 *     Simon Keizer                                                                    *
 *     Interaction Lab, MACS, Heriot-Watt University, Edinburgh, UK                    *
 *     Created October 2016                                                            *
 *     ---------------------------------------------------------------------------     *
 *     Package for storing and loading resources.                                      *
 *                                                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

package resources;

import java.util.Properties;

public class Configuration {

	public static String core_settings_file = "CoreSettings.cfg";
	
	public static String config_file = "config.cfg";
	public static String logging_directory = "logs";
	public static String logging_level = "INFO";
	public static String usim_logging_level = "INFO";
	public static String emod_logging_level = "INFO";
	public static String dman_logging_level = "INFO";
	public static String mdp_logging_level = "INFO";
	public static String nlu_logging_level = "INFO";
	public static String nlg_logging_level = "INFO";

	public static String domain_file;
	public static String lexicon_file;
	public static String dbase_file;
	public static String nlu_system;
	
	public static String RUN_SERVER = "false";
	public static String RUN_WEBSOCKET_SERVER = "false"; 
	public static String PORT_NUMBER = "8000";
	public static String TOKEN_MANAGER_URL = "http://www.macs.hw.ac.uk/cgi-bin/cgiwrap/sk382/get_token.cgi";
	
	public static String ASR_NBEST_SIZE = "1";
	public static String ASR_CONF_THRESHOLD = "0.7";
	public static String MIN_NUM_NONTRIVIAL_TURNS = "3";

	public static String num_dialogues = "25";
	public static String start_dialogue = "-1";
	public static String max_num_turns = "15";

	public static String use_mdp = "false";
	public static String use_deep_mdp = "false";
	public static String use_md_dm = "false";

	// user simulator settings
	
	public static String GOAL_COMPLETION_REWARD = "20";
	public static String SOCIAL_PENALTY = "-2";
	public static String SOCIAL_REWARD = "2";
	public static String TURN_PENALTY = "-1";
	public static String SYS_TURN_ITEM_PENALTY = "-1";
	public static String PATIENCE_PENALTY = "-1";
	
	public static String PROC_PROBLEM_PENALTY = "-1"; // internal penalty
	public static String BACKOFF_PENALTY = "-2";      // internal penalty

	public static String CONSTR_ADD_PROB = "0.2";
	public static String DONTCARE_PROB = "0.5";
	public static String MAX_NUM_REPEATED_SYS_ACTS = "2";
	public static String EMPTY_INFORM_PROB = "0.08";
	public static String SETQUESTION_PROB = "0.07";

	// goal generator settings
	
	public static String START_NUM_GOALS;
	public static String START_NUM_ZERO_SOL_GOALS;

	public static String MIN_VENUES_PER_GOAL = "1";
	public static String MAX_VENUES_PER_GOAL = "3";
	public static String ZERO_SOLUTION_TASK_RATIO = "0";
	
	public static String NUM_REQS_MEAN = "1";
	public static String NUM_INFS_MEAN = "2";
	
	public static String MAX_NUM_ATTEMPTS = "15";
	public static String NUM_ATTEMPTS_BEFORE_RELAX = "5";
	
	public static String MAX_ENTS_DISPL = "5";

	// error model settings
	
	public static String PERC_PROBLEM_PROB = "0.1";
	public static String INT_PROBLEM_PROB = "0.1";
	
	public static String MAX_NUM_CONF_ATTEMPTS = "5";
	public static String nbest_size = "1";
	public static String confusion_rate = "0";
	public static String oracle_exp_param = "1";	
	public static String variability = "32"; // default value adopted from (Thomson et al, SLT 2012)
	public static String cf_confusion_rate = "0";
	public static String item_deletion_rate = "0";
	public static String item_insertion_rate = "0";
	public static String slot_confusion_rate = "0";
	public static String value_confusion_rate = "0";
	
	// MDP Dialogue Manager settings
	
	public static String POLICY_SAVING_PERIOD = "5";
	public static String USE_POLICY_OBJECTS = "false";
	public static String POLICY_FILE = "";
	public static String POLICY_FILE_TASK = "";
	public static String POLICY_FILE_AUTO_FB = "";
	public static String POLICY_FILE_SOM = "";
	public static String POLICY_FILE_EVAL = "";
	public static String POLICY_POOL_SIZE = "1";
	public static String LOCAL_POLICIES = "false";

	public static String learning_algorithm = "MCC";
	public static String POLICY_OPTIMISATION = "true";
	public static String POLICY_SAVING_DIR = "training";
	public static String AVG_SCORE_WINDOW_SIZE = "50";
	public static String INCLUDE_SECOND_ORDER_FEATURES = "false";

	public static String restricted_action_sets = "false";
	public static String exploration_rate = "0";
	public static String exploration_discount = "0";
	public static String discount_factor = "1";
	public static String learning_rate = "0";
	public static String learning_rate_discount = "0";
	public static String lrate_c1 = "1";
	public static String lrate_c2 = "1";
	public static String temperature = "0";
	public static String temperature_discount = "1";
	
	public static String update_tabular_values = "true";
	public static String full_state_exploration = "true";

	// Handcrafted Dialogue Manager settings
	
	public static String SLU_CONF_THRESHOLD = ".5";
	public static String CONF_THRESHOLD = ".6";
	public static String NUM_ENTITIES_THRESHOLD = "3";
	
	// Multi-dimensional Dialogue Manager settings
	
	public static String ONE_DIMENSION_MODE = "true";
	public static String USE_TASK = "false";
	public static String USE_AUTO_FEEDBACK = "false";
	public static String USE_SOM = "false";
	public static String USE_EVAL = "false";
	public static String UPDATE_POLICY_ALL = "false";
	public static String UPDATE_POLICY_TASK = "false";
	public static String UPDATE_POLICY_AUTO_FB = "false";
	public static String UPDATE_POLICY_SOM = "false";
	public static String UPDATE_POLICY_EVAL = "false";
	public static String MAX_NUM_RESAMPLE_ATTEMPTS = "1";
	
	// NLU settings
	public static String LUIS_APP = "";
	public static String LUIS_KEY = "";
	
	// NLG settings
	public static String NLG_TEMPLATES;

	public static void loadConfig( Properties config ) {

		logging_directory = config.getProperty( "LOGGING_DIR", "logs" );
		logging_level = config.getProperty( "LOGGING_LEVEL", logging_level );
		usim_logging_level = config.getProperty( "USIM_LOGGING_LEVEL", usim_logging_level );
		emod_logging_level = config.getProperty( "EMOD_LOGGING_LEVEL", emod_logging_level );
		dman_logging_level = config.getProperty( "DMAN_LOGGING_LEVEL", dman_logging_level );
		mdp_logging_level = config.getProperty( "MDP_LOGGING_LEVEL", mdp_logging_level );
		nlu_logging_level = config.getProperty( "NLU_LOGGING_LEVEL", nlu_logging_level );
		nlg_logging_level = config.getProperty( "NLG_LOGGING_LEVEL", nlg_logging_level );

        num_dialogues = config.getProperty( "NUM_DIALOGUES", num_dialogues );
        start_dialogue = config.getProperty( "START_DIALOGUE", start_dialogue );
        max_num_turns = config.getProperty( "MAX_NUM_TURNS", max_num_turns );
        use_mdp = config.getProperty( "USE_MDP", use_mdp );
        use_deep_mdp = config.getProperty( "USE_DEEP_MDP", use_deep_mdp );
        use_md_dm = config.getProperty( "USE_MD_DM", use_md_dm );

		domain_file = config.getProperty( "DOMAIN", domain_file );
		lexicon_file = config.getProperty( "LEXICON", lexicon_file );
		dbase_file = config.getProperty( "DATABASE", dbase_file );
		nlu_system = config.getProperty( "NLU_SYSTEM", dbase_file );
		
		RUN_SERVER = config.getProperty( "RUN_SERVER", RUN_SERVER );
		RUN_WEBSOCKET_SERVER = config.getProperty( "RUN_WEBSOCKET_SERVER", RUN_WEBSOCKET_SERVER );
		PORT_NUMBER = config.getProperty( "PORT_NUMBER", PORT_NUMBER );
		TOKEN_MANAGER_URL = config.getProperty( "TOKEN_MANAGER_URL", TOKEN_MANAGER_URL );
		
		ASR_NBEST_SIZE = config.getProperty( "ASR_NBEST_SIZE", ASR_NBEST_SIZE );
		ASR_CONF_THRESHOLD = config.getProperty( "ASR_CONF_THRESHOLD", ASR_CONF_THRESHOLD );
		MIN_NUM_NONTRIVIAL_TURNS = config.getProperty( "MIN_NUM_NONTRIVIAL_TURNS", MIN_NUM_NONTRIVIAL_TURNS );
        
		// user simulator settings		
		GOAL_COMPLETION_REWARD = config.getProperty( "GOAL_COMPLETION_REWARD", GOAL_COMPLETION_REWARD );
		TURN_PENALTY = config.getProperty( "TURN_PENALTY", TURN_PENALTY );
		SOCIAL_PENALTY = config.getProperty( "SOCIAL_PENALTY", SOCIAL_PENALTY );
		SOCIAL_REWARD = config.getProperty( "SOCIAL_REWARD", SOCIAL_REWARD );
		BACKOFF_PENALTY = config.getProperty( "BACKOFF_PENALTY", BACKOFF_PENALTY );
		SYS_TURN_ITEM_PENALTY = config.getProperty( "SYS_TURN_ITEM_PENALTY", SYS_TURN_ITEM_PENALTY );
		PATIENCE_PENALTY = config.getProperty( "PATIENCE_PENALTY", PATIENCE_PENALTY );
		CONSTR_ADD_PROB = config.getProperty( "CONSTR_ADD_PROB", CONSTR_ADD_PROB );
		DONTCARE_PROB = config.getProperty( "DONTCARE_PROB", DONTCARE_PROB );
		MAX_NUM_REPEATED_SYS_ACTS = config.getProperty( "MAX_NUM_REPEATED_SYS_ACTS", MAX_NUM_REPEATED_SYS_ACTS );
		EMPTY_INFORM_PROB = config.getProperty( "EMPTY_INFORM_PROB", EMPTY_INFORM_PROB );
		SETQUESTION_PROB = config.getProperty( "SETQUESTION_PROB", SETQUESTION_PROB );

		// goal generator settings
		START_NUM_GOALS = config.getProperty( "START_NUM_GOALS", START_NUM_GOALS );
		START_NUM_ZERO_SOL_GOALS = config.getProperty( "START_NUM_ZERO_SOL_GOALS", START_NUM_ZERO_SOL_GOALS );
		MIN_VENUES_PER_GOAL = config.getProperty( "MIN_VENUES_PER_GOAL", MIN_VENUES_PER_GOAL );
		MAX_VENUES_PER_GOAL = config.getProperty( "MAX_VENUES_PER_GOAL", MAX_VENUES_PER_GOAL );
		ZERO_SOLUTION_TASK_RATIO = config.getProperty( "ZERO_SOLUTION_TASK_RATIO", ZERO_SOLUTION_TASK_RATIO );
		NUM_REQS_MEAN = config.getProperty( "NUM_REQS_MEAN", NUM_REQS_MEAN );
		NUM_INFS_MEAN = config.getProperty( "NUM_INFS_MEAN", NUM_INFS_MEAN );
		MAX_NUM_ATTEMPTS = config.getProperty( "MAX_NUM_ATTEMPTS", MAX_NUM_ATTEMPTS );
		NUM_ATTEMPTS_BEFORE_RELAX = config.getProperty( "NUM_ATTEMPTS_BEFORE_RELAX", NUM_ATTEMPTS_BEFORE_RELAX );

		// error model settings
		PERC_PROBLEM_PROB = config.getProperty( "PERC_PROBLEM_PROB", PERC_PROBLEM_PROB );
		INT_PROBLEM_PROB = config.getProperty( "INT_PROBLEM_PROB", INT_PROBLEM_PROB );

		MAX_NUM_CONF_ATTEMPTS = config.getProperty( "MAX_NUM_CONF_ATTEMPTS", MAX_NUM_CONF_ATTEMPTS );
		nbest_size = config.getProperty( "NBEST_SIZE", nbest_size );
		confusion_rate = config.getProperty( "CONFUSION_RATE", confusion_rate );
		oracle_exp_param = config.getProperty( "ORACLE_EXP_PARAM", oracle_exp_param );
		variability = config.getProperty( "VARIABILITY", variability );
		
		// confusion model settings
		cf_confusion_rate = config.getProperty( "CF_CONFUSION_RATE", cf_confusion_rate );
		item_deletion_rate = config.getProperty( "ITEM_DELETION_RATE", item_deletion_rate );
		item_insertion_rate = config.getProperty( "ITEM_INSERTION_RATE", item_insertion_rate );
		slot_confusion_rate = config.getProperty( "SLOT_CONFUSION_RATE", slot_confusion_rate );
		value_confusion_rate = config.getProperty( "VALUE_CONFUSION_RATE", value_confusion_rate );

		// MDP Dialogue Manager settings
		POLICY_SAVING_PERIOD = config.getProperty( "POLICY_SAVING_PERIOD", POLICY_SAVING_PERIOD );
		USE_POLICY_OBJECTS = config.getProperty( "USE_POLICY_OBJECTS", USE_POLICY_OBJECTS );
		POLICY_FILE = config.getProperty( "POLICY_FILE", POLICY_FILE );
		POLICY_FILE_TASK = config.getProperty( "POLICY_FILE_TASK", POLICY_FILE_TASK );
		POLICY_FILE_AUTO_FB = config.getProperty( "POLICY_FILE_AUTO_FB", POLICY_FILE_AUTO_FB );
		POLICY_FILE_SOM = config.getProperty( "POLICY_FILE_SOM", POLICY_FILE_SOM );
		POLICY_FILE_EVAL = config.getProperty( "POLICY_FILE_EVAL", POLICY_FILE_EVAL );
		POLICY_POOL_SIZE = config.getProperty( "POLICY_POOL_SIZE", POLICY_POOL_SIZE );
		LOCAL_POLICIES = config.getProperty( "LOCAL_POLICIES", LOCAL_POLICIES );
		
		learning_algorithm = config.getProperty( "LEARNING_ALGORITHM", learning_algorithm );		
        POLICY_OPTIMISATION = config.getProperty( "POLICY_OPTIMISATION", POLICY_OPTIMISATION );
        POLICY_SAVING_DIR = config.getProperty( "POLICY_SAVING_DIR", POLICY_SAVING_DIR );
        
        AVG_SCORE_WINDOW_SIZE = config.getProperty( "AVG_SCORE_WINDOW_SIZE", AVG_SCORE_WINDOW_SIZE );
        INCLUDE_SECOND_ORDER_FEATURES = config.getProperty( "INCLUDE_SECOND_ORDER_FEATURES", INCLUDE_SECOND_ORDER_FEATURES );

		discount_factor = config.getProperty( "DISCOUNT_FACTOR", discount_factor );
		temperature = config.getProperty( "TEMPERATURE", temperature );
		temperature_discount = config.getProperty( "TEMPERATURE_DISCOUNT", temperature_discount );
		exploration_rate = config.getProperty( "EXPLORATION_RATE", exploration_rate );
		exploration_discount = config.getProperty( "EXPLORATION_DISCOUNT", exploration_discount );
		learning_rate = config.getProperty( "LEARNING_RATE", learning_rate );
		learning_rate_discount = config.getProperty( "LEARNING_RATE_DISCOUNT", learning_rate_discount );
		lrate_c1 = config.getProperty( "LRATE_C1", lrate_c1 );
		lrate_c2 = config.getProperty( "LRATE_C2", lrate_c2 );
		restricted_action_sets = config.getProperty( "RESTRICTED_ACTION_SETS", restricted_action_sets );

		update_tabular_values = config.getProperty( "UPDATE_TABULAR_VALUES", update_tabular_values );
		full_state_exploration = config.getProperty( "FULL_STATE_EXPLORATION", full_state_exploration );

		SLU_CONF_THRESHOLD = config.getProperty( "SLU_CONF_THRESHOLD", SLU_CONF_THRESHOLD );
		CONF_THRESHOLD = config.getProperty( "CONF_THRESHOLD", CONF_THRESHOLD );
		NUM_ENTITIES_THRESHOLD = config.getProperty( "NUM_ENTITIES_THRESHOLD", NUM_ENTITIES_THRESHOLD );
		
		ONE_DIMENSION_MODE = config.getProperty( "ONE_DIMENSION_MODE", ONE_DIMENSION_MODE );
		USE_TASK = config.getProperty( "USE_TASK", USE_TASK );
		USE_AUTO_FEEDBACK = config.getProperty( "USE_AUTO_FEEDBACK", USE_AUTO_FEEDBACK );
		USE_SOM = config.getProperty( "USE_SOM", USE_SOM );
		USE_EVAL = config.getProperty( "USE_EVAL", USE_EVAL );
		UPDATE_POLICY_ALL = config.getProperty( "UPDATE_POLICY_ALL", UPDATE_POLICY_ALL );
		UPDATE_POLICY_TASK = config.getProperty( "UPDATE_POLICY_TASK", UPDATE_POLICY_TASK );
		UPDATE_POLICY_AUTO_FB = config.getProperty( "UPDATE_POLICY_AUTO_FB", UPDATE_POLICY_AUTO_FB );
		UPDATE_POLICY_SOM = config.getProperty( "UPDATE_POLICY_SOM", UPDATE_POLICY_SOM );
		UPDATE_POLICY_EVAL = config.getProperty( "UPDATE_POLICY_EVAL", UPDATE_POLICY_EVAL );
		MAX_NUM_RESAMPLE_ATTEMPTS = config.getProperty( "MAX_NUM_RESAMPLE_ATTEMPTS", MAX_NUM_RESAMPLE_ATTEMPTS );
		PROC_PROBLEM_PENALTY = config.getProperty( "PROC_PROBLEM_PENALTY", PROC_PROBLEM_PENALTY );
		
		LUIS_APP = config.getProperty( "LUIS_APP", LUIS_APP );
		LUIS_KEY = config.getProperty( "LUIS_KEY", LUIS_KEY );
		
		NLG_TEMPLATES = config.getProperty( "NLG_TEMPLATES", NLG_TEMPLATES );

	}
}
