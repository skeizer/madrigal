# =======================================================================================
# NLG templates for CamInfo domain
# Created: March 3, 2017 (SK)
# Change log
# May 23, 2017: added templates for confirm/disconfirm (SK)
# Oct 25, 2017: added templates for non-empty disconfirm acts (SK)
#
# --------------- SUB-RULES -------------------------------------------------------------

%q($X){
    food : "What food would you like";
    area : "What area would you like";
    near : "Where would you like it to be near to";
    pricerange : "What price range would you like";
}

%food_inf($food) {
  "Seafood" : "it serves sea food";
  "Pub food" : "it serves pub food";
  "Fastfood" : "it serves fast food";
  "Cafe food" : "it serves cafe food";
  $food : "it serves $food food";
}

%area_inf($area) {
  "city centre" : "it is in the $area";
  $area : "it is in the $area area";
}

%near_inf($near) {
    $near : "it is near $near";
}

%pricerange_inf($price) {
  "moderate" : "it is moderately priced";
  $price : "it is $price"; 
  $price : "it is in the $price price range";
}

%food_str($food) {
  dontcare : "if you don't care about the food";
  $food : "serving $food food";
}

%area_str($area) {
  dontcare : "in all parts of town";
  "city centre" : "in the $area";
  $area : "in the $area area";
  $area : "in the area of $area";
}

%near_str($near) {
    dontcare : "if you don't care where it is near to";
    $near : "near $near";
}

%pricerange_str($price) {
  dontcare : "if you don't care about the price range";
  $price : "in the $price price range";
}

%area_pricerange_str($area, $price) {
  dontcare, dontcare : "if you don't care about the area or the price range";
  dontcare, $price : "in the $price range and any part of town";
  $area, dontcare : "in the $area area if you don't care about the price range";
  "city centre", $price : "it is in the $price price range and in the city centre";
  $area, $price : "in the $price price range and the $area area";
}

%area_near_str($area, $near) {
  dontcare, dontcare : "if you don't care about the area or where it is near to";
  dontcare, $near : "near $near";
  $area, dontcare : "in the area of $area if you don't care where it is near to";
  $area, $near : "near $near and in the area of $area";
}

%area_food_str($area, $food) {
  dontcare, dontcare : "if you don't care about the area or the type of food";
  dontcare, $food : "serving $food food in any part of town";
  $area, dontcare : "in the area of $area serving any kind of food";
  $area, $food : "serving $food in the area of $area";
}

%food_pricerange_str($food, $price) {
  dontcare, dontcare : "if you don't care about the price range or the type of food";
  dontcare, $price : "serving any kind of food in the $price price range";
  $food, dontcare : "serving $food in any price range";
  $food, $price : "serving $food in the $price price range";
}

%addr_str($addr) {
  none : "I don't know their address";	
  $addr : "Their address is $addr";
}

%phone_str($phone) {
  none : "I don't know their phone number";	
  $phone : "Their phone number is $phone";
}

%postcode_str($postcode) {
  none : "I don't know their post code";	
  $postcode : "Their postcode is $postcode";
}

%addr_inf($addr) {
  none : "I don't know their address";	
  $addr : "Their address is $addr";
}

%phone_inf($phone) {
  none : "I don't know their phone number";	
  $phone : "Their phone number is $phone";
}

%postcode_inf($postcode) {
  none : "I don't know their post code";	
  $postcode : "Their postcode is $postcode";
}

%price_inf($price) {
  none : "I don't know their prices";	
  $price : "Their prices are $price";
}

%addr_phone_str($addr, $phone) {
  none, none : "I don't know their address or phone number";
  none, $phone : "I don't know their address but their phone number is $phone";
  $addr, none : "I don't know there phone number but their address is $addr";
  $addr, $phone : "Their address is $addr and their phone number is $phone";
}

%phone_postcode_str($phone, $postcode) {
  none, none : "I don't know the phone number or post code.";
  none, $postcode : "I don't know their phone number but the post code is $postcode .";
  $phone, none : "I don't know the post code but the phone number is $phone .";			   
  $phone, $postcode : "Their phone number is $phone and their post code is $postcode .";
}

%addr_postcode_str($addr, $postcode) {
  none, none : "I don't know the address or the post code";
  none, $postcode : "I don't know the address but the post code is $postcode";
  $addr, none : "I don't know their post code but the address is $addr";
  $addr, $postcode : "Their address is $addr , $postcode";
}

# --------------- RULES -----------------------------------------------------------------

autoNegative() : "Could you please repeat that?";
autoPositive() : "Okay";
autoNegative_perc() : "I can't hear you";
autoNegative_int() : "Could you please rephrase that?";

returnGoodbye() : "Goodbye";

confirm() : "Yes";
confirm(name=$X,pricerange=$Y) : "Yes, $X is in the $Y price range";
confirm(name=$X,area=$Y) : "Yes, $X is in the $Y area";
confirm(name=$X,near=$Y) : "Yes, $X is near $Y";
confirm(name=$X,food=Cafe food) : "Yes, $X serves Cafe food";
confirm(name=$X,food=Drinks and snacks only) : "Yes, $X serves Drinks and snacks only";
confirm(name=$X,food=Fastfood) : "Yes, $X serves fast food";
confirm(name=$X,food=Pub food) : "Yes, $X serves pub food";
confirm(name=$X,food=Seafood) : "Yes, $X serves sea food";
confirm(name=$X,food=$Y) : "Yes, $X serves $Y food";

disconfirm() : "No";
disconfirm(name=$X,pricerange=$Y) : "No, $X is in the $Y price range";
disconfirm(name=$X,area=$Y) : "No, $X is in the $Y area";
disconfirm(name=$X,near=$Y) : "No, $X is near $Y";
disconfirm(name=$X,food=Cafe food) : "No, $X serves Cafe food";
disconfirm(name=$X,food=Drinks and snacks only) : "No, $X serves Drinks and snacks only";
disconfirm(name=$X,food=Fastfood) : "No, $X serves fast food";
disconfirm(name=$X,food=Pub food) : "No, $X serves pub food";
disconfirm(name=$X,food=Seafood) : "No, $X serves sea food";
disconfirm(name=$X,food=$Y) : "No, $X serves $Y food";

setQuestion($A) : "What kind of $A would you like?";
setQuestion(area) : "What part of town do you have in mind?";
setQuestion(pricerange) : "Would you like something in the cheap, moderate, or expensive price range?";
setQuestion(food) : "What kind of food would you like?";
setQuestion(near) : "Where would you like it to be near to?";

propQuestion(area=dontcare) : "Okay, a restaurant in any part of town is that right?";
propQuestion(area=$V) : "Did you say you are looking for a restaurant in the area of $V?";
propQuestion(food=dontcare) : "You are looking for a restaurant serving any kind of food right?";
propQuestion(food=$V) : "You are looking for a $V restaurant right?";
propQuestion(pricerange=dontcare) : "Let me confirm, You are looking for a restaurant and you dont care about the price range right?";
propQuestion(pricerange=$V) : "Let me confirm, You are looking for a restaurant in the $V price range right?";

inform(name=none) : "I'm sorry but I cannot find anything that matches your request";

inform(name=none,$Y=$O) : "I am sorry but there is no restaurant %$Y_str($O).";
inform(name=none,$Y=$O,$Z=$P) : "I am sorry but there is no restaurant %$Y_str($O), and %$Z_str($P).";
inform(name=none,$Y=$O,$Z=$P,$W=$R) : "I am sorry but there is no restaurant %$Y_str($O), %$Z_str($P), and %$W_str($R).";

inform(name=none,$Y=$O) : "I am sorry but there is no place %$Y_str($O).";
inform(name=none,$Y=$O,$Z=$P) : "I am sorry but there is no place, %$Y_str($O), and %$Z_str($P).";
inform(name=none,$Y=$O,$Z=$P,$W=$R) : "I am sorry but there is no place, %$Y_str($O), %$Z_str($P), and %$W_str($R).";

inform(name=$X) : "$X is a nice restaurant.";
inform(name=$X,$Y=$O) : "$X is a nice restaurant, %$Y_inf($O).";
inform(name=$X,$Y=$O,$Z=$P) : "$X is a nice restaurant, %$Y_inf($O), and %$Z_inf($P).";
inform(name=$X,$Y=$O,$Z=$P,$W=$R) : "$X is a nice restaurant, %$Y_inf($O), %$Z_inf($P), and %$W_inf($R).";
inform(name=$X,$Y=$O,$Z=$P,$W=$R,$V=$Q) : "$X is a nice restaurant, %$Y_inf($O), %$Z_inf($P), %$W_inf($R), and %$V_inf($Q).";

inform(name=$X) : "$X is a nice place.";
inform(name=$X,$Y=$O) : "$X is a nice place, %$Y_inf($O).";
inform(name=$X,$Y=$O,$Z=$P) : "$X is a nice place, %$Y_inf($O), and %$Z_inf($P).";
inform(name=$X,$Y=$O,$Z=$P,$W=$R) : "$X is a nice place, %$Y_inf($O), %$Z_inf($P), and %$W_inf($R).";
inform(name=$X,$Y=$O,$Z=$P,$W=$R,$V=$Q) : "$X is a nice place, %$Y_inf($O), %$Z_inf($P), %$W_inf($R), and %$V_inf($Q).";

inform(name=$X,phone=$Y) : "the phone number for $X is $Y";
inform(name=$X,addr=$Y) : "the address for $X is $Y";
inform(name=$X,postcode=$Y) : "the post code for $X is $Y";
inform(name=$X,price=$Y) : "For $X, $Y";
inform(name=$X,phone=$Y, postcode=$Z) : "the phone number for $X is $Y, and the post code is $Z";
inform(name=$X,phone=$Y, addr=$Z) : "the phone number for $X is $Y, and the address is $Z";
inform(name=$X,postcode=$Y, addr=$Z) : "the post code for $X is $Y, and the address is $Z";

# --------------- END OF RULES ----------------------------------------------------------
