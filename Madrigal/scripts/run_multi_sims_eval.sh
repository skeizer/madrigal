#!/bin/sh

#echo $@

td=""
nd=10000
while [[ "$1" != "" && "$1" != "-"* ]]; do
   if [[ ! "$1" =~ ^[0-9]+ ]]; then
      td="$1/"
   else
      nd=$1
   fi
   shift
done
echo "Training dir: $td"
echo "Evaluating policies trained on $nd dialogues"

n=5
echo "Running $n simulation batches"

wdir=${0%/*}
for (( i=0; i<$n; i++ ))
do
   tdir="${td}tra_$i"
   echo "${wdir}/run_simulations.sh -p ${tdir}/mdp_${nd}.pcy $@"
   ${wdir}/run_simulations.sh -p ${tdir}/mdp_${nd}.pcy $@
   #echo "${wdir}/run_simulations_jar.sh -p ${tdir}/mdp_${nd}.pcy $@"
   #${wdir}/run_simulations_jar.sh -p ${tdir}/mdp_${nd}.pcy $@
done

