#!/bin/sh

if [ $# -ne 2 ]
then
  echo "USAGE: ./eval_learn_curve.sh <logdir> <config>"
  exit
fi

dir=`basename "$1"`
conf=$2
tdir="training/${dir}"
logdir="logs/eval/${dir}"

n=60000
rates=( "0" "0.1" "0.2" "0.3" "0.4" "0.5" )
rateNames=( "00" "10" "20" "30" "40" "50" )

wdir=${0%/*}
for (( i=0; i<${#rates[@]}; i++ ))
do
   r=${rates[$i]}
   ld=${logdir}/eval_r${rateNames[$i]}
   echo "${wdir}/run_multi_sims_eval.sh $n ${tdir} -c ${conf} -r $r -l ${ld}"
   ${wdir}/run_multi_sims_eval.sh $n ${tdir} -c ${conf} -r $r -l ${ld}
   echo "perl $MADRIGAL_HOME/scripts/get_results.pl ${ld}/main_*"
   perl $MADRIGAL_HOME/scripts/get_results.pl ${ld}/main_*
done

