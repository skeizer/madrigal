#!/usr/bin/env python
# -"- coding: utf-8 -"-


import json
import codecs
from argparse import ArgumentParser
import numpy as np
import re
import os
import os.path
import sys
import datetime


def filter_nonempty(transcriptions, recs):
    nonempty = []
    for rec in recs:
        if rec not in transcriptions:
            nonempty.append(rec)
        transcription = transcriptions[rec]
        transcription = re.sub(r'\((sil|noise)\)', '', transcription)  # ignore noises
        transcription = transcription.strip()
        if transcription:
            nonempty.append(rec)
    return nonempty


def filter_turns(dialogue):
    valid = []
    for turn in dialogue:
        if turn['sys_turn']['utterance'] == 'You have ended the dialogue without trying to complete the task.':
            valid = []
            continue  # weird dialogue reset
        if (turn['sys_turn']['sys_act'] == 'returnGoodbye()' and
            (turn['usr_turn']['asr_hyps'][0]['utterance'] == valid[-1]['usr_turn']['asr_hyps'][0]['utterance'])):
            continue  # pressed End dialogue
        if not turn['usr_turn']['asr_hyps']:
            continue # empty ASR for some reason

        valid.append(turn)

    return valid


def filename_to_turn_number(filename):
    turn = re.sub('^.*Turn_', '', filename)
    turn = re.sub('_.*', '', turn)
    return int(turn)


def get_dialogue_dir(user_dirs, transcriptions, dialogue):

    start_date = datetime.datetime.strptime(dialogue['CF_data']['_started_at'], '%m/%d/%Y %H:%M:%S')
    user_id = dialogue['userID']
    user_id = re.sub(r'^FAKE_', '', user_id)
    if user_id not in user_dirs:
        user_dirs[user_id] = sorted(os.listdir(os.path.join(args.audio_dir, 'User-' + user_id)))

    dialogue_dir, audio_recs, valid_turns = None, None, None

    while user_dirs[user_id] and not dialogue_dir:
        dialogue_dir = user_dirs[user_id].pop(0)
        dir_date = re.sub('^Dialogue-', '', dialogue_dir)
        dir_date = re.sub('_[^_]+$', '', dir_date)
        dir_date = datetime.datetime.strptime(dir_date, '%Y-%m-%d_%H-%M-%S')
        dir_date -= datetime.timedelta(hours=args.timezone)  # normalize to UTC
        if dir_date < start_date:
            print >> sys.stderr, 'Skipping directory %s %s (based on date)' % (user_id, dialogue_dir)
            dialogue_dir = None
            continue
        elif (dir_date - start_date).total_seconds() > 300:
            print >> sys.stderr, 'No appropriate audio directory found for %s / %s' % (user_id, str(start_date))
            return None, None, None

        dialogue_dir = os.path.join('User-' + user_id, dialogue_dir)
        audio_recs = [os.path.join(dialogue_dir, rec_file)
                      for rec_file in os.listdir(os.path.join(args.audio_dir, dialogue_dir))]
        audio_recs.sort(key=filename_to_turn_number)
        # ignore empty audio files / clicks for end dialogue
        total_recs = len(audio_recs)
        audio_recs = filter_nonempty(transcriptions, audio_recs)
        valid_turns = filter_turns(dialogue['dialogue'])
        # 1 shorter == user pushed "End Dialogue"
        if len(audio_recs) != len(valid_turns):
            if total_recs >= len(valid_turns):
                print >> sys.stderr, 'Probably can\'t align %s' % (dialogue_dir)
                break
            print >> sys.stderr, 'Skipping directory %s (based on length: %d vs. %d)' % (dialogue_dir, len(audio_recs), len(valid_turns))
            dialogue_dir = None
            continue

    if not dialogue_dir:
        print >> sys.stderr, 'Not enough audio directories for user %s (%s)' % (user_id, str(start_date))
        return None, None, None

    return dialogue_dir, audio_recs, valid_turns



def main(args):
    with codecs.open(args.logfile, 'rb', 'UTF-8') as fh:
        data = json.load(fh)
    transcriptions = {}
    if args.transcriptions:
        with codecs.open(args.transcriptions, 'rb', 'UTF-8') as fh:
            transcriptions = json.load(fh)

    # XXX wouldn't work if multiple years are involved
    data.sort(key=lambda item: item['CF_data']['_started_at'])

    user_dirs = {}
    processed = 0
    errors = 0
    for dialogue in data:
        dialogue_dir, audio_recs, valid_turns = get_dialogue_dir(user_dirs, transcriptions, dialogue)
        if not dialogue_dir:
            errors += 1
            continue
        if len(audio_recs) != len(valid_turns):
            errors += 1
            dialogue['TRANSCRIPTIONS-MANUAL'] = [
                {'audio_file': rec, 'transcription': transcriptions.get(rec, '')}
                for rec in audio_recs
            ]
            continue

        print >> sys.stderr, 'Processing %s %s / %s' % (dialogue['userID'], dialogue['CF_data']['_started_at'], dialogue_dir)
        for turn, rec in zip(valid_turns, audio_recs):
            turn['usr_turn']['audio_file'] = rec
            turn['usr_turn']['transcription'] = transcriptions.get(rec, '')
        processed += 1

    print >> sys.stderr, 'Processed: %d, errors: %d' % (processed, errors)
    with codecs.open(args.out_file, 'w', 'UTF-8') as fh:
        json_text = json.dumps(data, ensure_ascii=False, indent=4, sort_keys=True)
        fh.write(json_text)


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('--timezone', '-t', type=int, help='Webserver logs timezone (in hours +- UTC)', default=0)
    ap.add_argument('--transcriptions', '-a', type=str, help='JSON with ASR transcriptions')
    ap.add_argument('logfile', type=str, help='JSON with joint logs')
    ap.add_argument('audio_dir', type=str, help='Directory for audio files')
    ap.add_argument('out_file', type=str, help='Output file')

    args = ap.parse_args()
    main(args)
