#
#
# Simon Keizer, 30/06/'17
# 

if ( $#ARGV < 0 ) {
    print "USAGE: perl get_sys_act_stats.pl <log-files>\n";
    exit;
}

my $trace = 0;

my @sysActs = ( "inform", "propQuestion", "setQuestion", "confirm", "disconfirm", "autoNegative", "autoNegative_perc", "autoNegative_int", "returnGoodbye" );
my %sysActStats = ();

# initialisation:
foreach $sa ( @sysActs ) {
    $sysActStats{$sa} = 0;
}
my $totalNumSysActs = 0;

my $line;
my $sysAct = "";

foreach $file ( @ARGV ) {

    if ( $file =~ /.*\/main_\S+\.log/ ) {
	
	if ( $trace ) { print "opening log file $file\n"; }
	open ( FILE, $file ) or die "Cannot open log file $file\n";

	while ( <FILE> ) {
	    $line = $_;
	    if ( $line =~ /Sact> (\S+)\(/ ) {
		$totalNumSysActs++;
		$sysAct = $1;
		foreach $sa ( @sysActs ) {
		    if ( $sa eq $sysAct ) {
			$sysActStats{$sa}++;
		    }
		}
	    }
	}

	close( FILE );

    } else {
	if ( $trace ) { print "skipping file $file\n"; }
    }

}

print "SysAct  NumSysAct  percSysAct\n";
foreach $sa ( @sysActs ) {
    $percSysAct{$sa} = 100 * $sysActStats{$sa} / $totalNumSysActs;
    printf "$sa  $sysActStats{$sa}  %.1f\%\n", $percSysAct{$sa};
}
print "Total  $totalNumSysActs  100\%\n";
