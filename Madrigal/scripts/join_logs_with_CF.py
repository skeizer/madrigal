#!/usr/bin/env python
# -"- coding: utf-8 -"-


from argparse import ArgumentParser
import pandas as pd
import os
import re
import datetime
import json
import sys
import codecs

def get_ip_to_userid(webserver_log_dir, timezone):
    """Obtain a mapping of IP -> userID, based on webserver logs."""
    data = os.popen('grep -R "A dialogue started" "%s"' % webserver_log_dir, 'r').readlines()
    mapping = {}
    for line in data:
        line = line.strip()
        date, ip, userid = re.search(r'.*\[([^\]]*)\] \(dm_server_handler\) .* userIP=([^,]*), userID=(.*)$', line).groups()
        date = datetime.datetime.strptime(date, '%a %b %d %H:%M:%S %Y')
        date -= datetime.timedelta(hours=timezone)  # normalize to UTC
        mapping[ip] = mapping.get(ip, []) + [(date, userid)]
    return mapping


def find_dm_log(dmserver_log_dir, viable_userids, token):
    """Find the DM logfile based on viable userIDs (computed based on time & IP address) + token value."""
    logs_for_id = []
    for userid in viable_userids:
        logs_for_id.extend(os.popen('grep -R \'"userID": "\(FAKE_\)\?%s"\' "%s"' % (userid, dmserver_log_dir), 'r').readlines())
    logs_for_token = os.popen('grep -R \'"token": "%s"\' "%s"' % (token, dmserver_log_dir), 'r').readlines()
    if not logs_for_id:
        raise Exception('No logs for ids: %s' % str(viable_userids))
    if not logs_for_token:
        raise Exception('No logs for token: %s' % str(token))
    logs_for_token = [re.sub(':.*', '', line).strip() for line in logs_for_token]
    logs_for_id = [re.sub(':.*', '', line).strip() for line in logs_for_id]
    match = set(logs_for_id) & set(logs_for_token)
    if len(match) != 1:
        raise Exception('Non-unique / non-match %s %s (%d matches: %s)' % (str(viable_userids), str(token), len(match), str(match)))
    return match.pop()


def read_task_descs(task_descs_file):
    """Read task descriptions from the Caminfo_tasks file."""
    task_descs = {}
    cur_data = {}
    with codecs.open(task_descs_file, 'r', encoding='UTF-8') as fh:
        for line in fh.readlines():
            line = line.strip()
            if line.startswith('==='):  # new dialogue
                cur_data = {}
                task_id = re.search(r'=== Dialogue ([^ ]*) ===', line).group(1)
                task_descs[task_id] = cur_data
            elif line.startswith('Requests'):
                cur_data['requests'] = line[len('Requests: '):].split(', ')
            elif line.startswith('Constraints'):
                cur_data['constraints'] = line[len('Constraints: '):].split(', ')
            elif line.startswith('GoalGen'):
                line = re.sub('.* matching entities: ', '', line)
                cur_data['matching'] = line.split(',') if line != 'NONE' else []
    return task_descs


def main(args):
    """Collect the data & write the output into one JSON."""

    cf_data = pd.read_csv(args.cf_data, encoding='UTF-8')
    ip_to_userid = get_ip_to_userid(args.webserver_log_dir, args.timezone)
    task_descs = read_task_descs(args.tasks) if args.tasks else {}
    out_data = []

    for inst_no, inst in cf_data.iterrows():
        try:
            print >> sys.stderr, 'Processing %d %s %s' % (inst_no, inst['_ip'], inst['_started_at'])
            cur_date = datetime.datetime.strptime(inst['_started_at'], '%m/%d/%Y %H:%M:%S')
            cur_ip = inst['_ip']
            if cur_ip not in ip_to_userid:
                raise Exception('IP not in IP -> user ID mapping: %s' % cur_ip)
            viable_userids = set([userid for (date, userid) in ip_to_userid[cur_ip]
                                  if abs((date - cur_date).total_seconds()) < 300])
            assert viable_userids
            if not viable_userids:
                raise Exception('No viable user IDs for IP: %s, %s' % (cur_ip, inst['_started_at']))
            dm_log = find_dm_log(args.dmserver_log_dir, viable_userids, inst['auto_token'])
            with codecs.open(dm_log, 'r', 'UTF-8') as fh:
                log_data = json.load(fh)
                log_data['filename'] = dm_log
                log_data['CF_data'] = {k: v for (k, v) in inst.iteritems() if not k.endswith('_gold')}
                if log_data['taskID'] in task_descs:
                    log_data['task'] = task_descs[log_data['taskID']]
                out_data.append(log_data)

        except Exception as e:
            # skip all erroneous lines in the CF data
            print >> sys.stderr, e.message

    with codecs.open(args.out_file, 'w', 'UTF-8') as fh:
        json_text = json.dumps(out_data, ensure_ascii=False, indent=4, sort_keys=True)
        fh.write(json_text)


if __name__ == '__main__':

    ap = ArgumentParser()
    ap.add_argument('--timezone', '-t', type=int, help='Webserver logs timezone (in hours +- UTC)', default=0)
    ap.add_argument('--tasks', '-T', type=str, help='Path to CamInfo_tasks file')
    ap.add_argument('cf_data', type=str, help='CF data CSV file')
    ap.add_argument('webserver_log_dir', type=str, help='Webserver log directory')
    ap.add_argument('dmserver_log_dir', type=str, help='DM server log directory')
    ap.add_argument('out_file', type=str, help='Output JSON file')

    args = ap.parse_args()
    main(args)



