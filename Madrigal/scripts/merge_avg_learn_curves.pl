#
# Prints the average learning curves corresponding to the final column in the specified data files to stdout.
#
# Simon Keizer, 7/08/'17
#

if ( $#ARGV < 0 ) {
    print "USAGE: perl merge_avg_learn_curves.pl <data-files>\n";
    exit;
}

my $trace = 0;

my %avgScoreData = ();

foreach $file ( @ARGV ) {

    if ( $file =~ /\S+\.txt/ ) {
	
	if ( $trace ) { print "opening data file $file\n"; }
	open ( FILE, $file ) or die "Cannot open data file $file\n";

	while ( <FILE> ) {
	    @data_entry = split /\s+/;
	    push @{$avgScoreData{$data_entry[0]}}, $data_entry[$#data_entry]; # retain final column (average over avgScores)
	    $size = scalar @{$avgScoreData{$data_entry[0]}};
	    if ( $trace ) { print "n:$data_entry[0]; i:$#data_entry; s:$data_entry[$#data_entry]; N:$size\n"; }
	}
	close( FILE );
	
    } else {
	if ( $trace ) { print "skipping file $file\n"; }
    }

}

foreach $key ( sort {$a<=>$b} keys %avgScoreData ) {
    @avgScores = @{ $avgScoreData{$key} };
    print " " . $key . "\t" . join( "\t", @avgScores ) . "\n";
}
