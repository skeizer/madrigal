#!/bin/sh

pd=""
nd=0
while [[ "$1" != "" && "$1" != "-"* ]]; do
   if [[ ! "$1" =~ ^[0-9]+ ]]; then
      pd="$1"
   else
      nd=$1
   fi
   shift
done
echo "Policy dir: $pd"
echo "Starting with policies trained on $nd dialogues"

n=5
echo "Running $n simulation batches:"

wdir=${0%/*}
for (( i=0; i<$n; i++ ))
do
    tdir="tra_$i"
    if [[ ${pd} == "" ]]; then
	echo "${wdir}/run_simulations.sh -t ${tdir} $*"
	${wdir}/run_simulations.sh -t ${tdir} $*
	#echo "${wdir}/run_simulations_jar.sh -t ${tdir} $*"
	#${wdir}/run_simulations_jar.sh -t ${tdir} $*
    else
	pdir="${pd}/tra_$i"
	echo "${wdir}/run_simulations.sh -p ${pdir}/mdp_${nd}.pcy -t ${tdir} $*"
	${wdir}/run_simulations.sh -p ${pdir}/mdp_${nd}.pcy -t ${tdir} $*
	#echo "${wdir}/run_simulations_jar.sh -p ${pdir}/mdp_${nd}.pcy -t ${tdir} $*"
	#${wdir}/run_simulations_jar.sh -p ${pdir}/mdp_${nd}.pcy -t ${tdir} $*
    fi
done

