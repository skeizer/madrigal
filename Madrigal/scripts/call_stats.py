#!/usr/bin/env python
# -"- coding: utf-8 -"-


import json
import codecs
from argparse import ArgumentParser
from collections import OrderedDict
import numpy as np
import scipy.stats
import re
import sys


class Dir:
    """\
    Direction to traverse diff matrixes (can be combined with "or").
    """
    UP = 1
    LEFT = 2
    DIAG = 4

class DA(object):

    def __init__(self, text=None):
        self.act_type = None
        self.slots = set()
        if text is not None:
            text = text.strip()
            text = re.sub(r'u?\'', '', text)
            text = re.sub(',\s*\]', ']', text)
            self.act_type = re.sub(r'\(.*', '', text)
            slots = re.split(',\s*', re.sub('\]?\)$', '',  re.sub(r'^[^(]*\(\[?', '', text)))
            self.slots.update(slots)

    def __str__(self):
        return str(self.act_type) + '(' + ', '.join(sorted(list(self.slots))) + ')'

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return str(self) == str(other)

    def __ne__(self, other):
        return str(self) != str(other)

    def __hash__(self):
        return hash(str(self))

def da_diff(transcr_da, hyp_da):
    """Return length and # of errors (substitutions -- same slot with a different value, missing, added).
    Act type is considered one of the slots"""
    if not isinstance(hyp_da, DA):
        if transcr_da is None:
            return int(hyp_da is None), 1
        if isinstance(transcr_da, DA):
            return len(transcr_da.slots) + 1, len(transcr_da.slots) + 1
        return transcr_da == hyp_da, 1  # strings
    elif not isinstance(transcr_da, DA):
        return 1, 1
    subst = 0 if transcr_da.act_type == hyp_da.act_type else 1
    transcr_da_map = dict(slot.split(':', 1) if ':' in slot else (slot, None) for slot in transcr_da.slots)
    hyp_da_map = dict(slot.split(':', 1) if ':' in slot else (slot, None) for slot in hyp_da.slots)
    miss = sum(0 if slot in hyp_da_map else 1 for slot in transcr_da_map.keys())
    add = sum(0 if slot in transcr_da_map else 1 for slot in hyp_da_map.keys())
    subst += sum(0 if transcr_da_map[slot] == hyp_da_map[slot] else 1 for slot in set(transcr_da_map.keys()) & set(hyp_da_map.keys()))
    return subst + miss + add, len(transcr_da.slots) + 1


def ztost(x, y, epsilon):
    def ztest(x, y, epsilon):
        """P-value of a z-test (for H0: x >= y + epsilon).
        Using z-test pooled with continuity correction from:
        https://ncss-wpengine.netdna-ssl.com/wp-content/themes/ncss/pdf/Procedures/PASS/Equivalence_Tests_for_the_Difference_Between_Two_Proportions.pdf
        """
        px = np.mean(x)
        nx = len(x)
        py = np.mean(y)
        ny = len(y)
        p = (nx * px + ny * py) / (nx + ny)
        sigma = np.sqrt(p * (1. - p) * ((1. / nx) + (1. / ny)))
        cc =  -0.5*((1. / nx) + (1. / ny))  # continuity correction for lower-tailed test
        zt = (px - py - epsilon + cc) / sigma
        return scipy.stats.norm.sf(abs(zt))  # p-value for the z-test

    # two one-sided tests
    z1 = ztest(x, y, epsilon)  # x >= y + epsilon
    z2 = ztest(y, x, epsilon)  # y >= x + epsilon ~ x <= y - epsilon

    # basically reject H0 (y - epsilon <= x <= y + epsilon) only if both one-sided tests reject
    return max(z1, z2)

def measure_wer_values(s, t):
    """Measure insertions, deletions, substitutions. s = reference, t = hypothesis.
    """
    H = np.zeros((len(s) + 1, len(t) + 1))
    path = np.zeros((len(s) + 1, len(t) + 1), dtype=int)
    for (j, tj) in enumerate([''] + t):
        for (i, si) in enumerate(['_'] + s):
            # compute score for all directions
            (left, up, diag) = (float('-Inf'), float('-Inf'), float('-Inf'))
            if i > 0:  # look up
                gap_cont = path[i - 1, j] & Dir.UP != 0
                up = H[i - 1, j]
            if j > 0:  # look left
                gap_cont = path[i, j - 1] & Dir.LEFT != 0
                left = H[i, j - 1]
            if i > 0 and j > 0:  # look diagonally
                diag = H[i - 1, j - 1] + (1 if s[i-1] == t[j-1] else 0)
            # find the best score and remember it along with the direction
            best_score = max(left, up, diag)
            if best_score > float('-Inf'):  # i.e. not upper-left corner
                H[i, j] = best_score
                if up == best_score:
                    path[i, j] |= Dir.UP
                if left == best_score:
                    path[i, j] |= Dir.LEFT
                if diag == best_score:
                    path[i, j] |= Dir.DIAG

    # backtrack path, H & count
    # change in H & diag = good, no change in H & diag = subst, left = insertion, top = deletion
    i, j = len(s), len(t)
    deletions, insertions, substitutions, missed_words, correct_words = 0, 0, 0, [], []
    while i > 0 or j > 0:
        if path[i, j] & Dir.DIAG:
            if H[i, j] == H[i - 1, j - 1]:  # substiution
                missed_words.append(s[i - 1])
                substitutions += 1
            else:
                correct_words.append(s[i - 1])
            i -= 1
            j -= 1
        elif path[i, j] & Dir.LEFT:
            insertions += 1
            j -= 1
            correct_words.append(s[i - 1])
        elif path[i, j] & Dir.UP:
            missed_words.append(s[i - 1])
            deletions += 1
            i -= 1

    return deletions, insertions, substitutions, len(s), missed_words, correct_words


def measure_wer(asr, trans):

    def normalize(text):
        text = text.lower()
        text = re.sub(r' +', ' ', text)
        text = re.sub(r'\([a-z]+\)', '', text)  # (sil), (noise) etc. are ignored
        text = re.sub(r'\bok\b', r'okay', text)
        text = re.sub(r'\bpost code\b', r'postcode', text)
        return text.split(' ')

    return measure_wer_values(normalize(trans), normalize(asr))


def get_asr_with_trans(call):
    return [(turn['usr_turn']['asr_hyps'][0]['utterance'], turn['usr_turn']['transcription'])
            for turn in call['dialogue']
            if (turn['usr_turn']['transcription'] and
                "HANGUP :" not in turn['usr_turn']['transcription'] and
                len(turn['usr_turn']['asr_hyps']) > 0 )]


def wer_stats(calls, output_misheard=False):

    stats = OrderedDict()
    raw = OrderedDict()
    turns, transcribed_turns = 0, 0
    missed, inserted, substituted, total_words = 0, 0, 0, 0
    misheard = {}
    correct = {}
    perfect_turns = 0
    raw['call_wer'] = []

    for call in calls:
        asr_with_trans = get_asr_with_trans(call)
        turns += len(call['dialogue'])
        transcribed_turns += len(asr_with_trans)

        if not asr_with_trans:  # skip calls that weren't transcribed
            continue

        call_m, call_i, call_s, call_w = 0, 0, 0, 0

        for asr, trans in asr_with_trans:
            m, i, s, w, misses, corrs = measure_wer(asr, trans)
            if m == 0 and i == 0 and s == 0:
                perfect_turns += 1
            for miss in misses:
                misheard[miss] = misheard.get(miss, 0) + 1
            for corr in corrs:
                correct[corr] = correct.get(corr, 0) + 1
            call_m += m
            call_i += i
            call_s += s
            call_w += w

        raw['call_wer'].append((call_m + call_i + call_s) / np.float64(call_w))
        missed += call_m
        inserted += call_i
        substituted += call_s
        total_words += call_w

    stats['total_calls'] = len(calls)
    stats['wer'] = (missed + inserted + substituted) / np.float64(total_words)
    stats['transcribed_turns'] = transcribed_turns
    stats['transcribed_turns_rate'] = float(transcribed_turns) / turns
    stats['total_turns'] = turns
    stats['perfectly_recognized_turns_rate'] = np.float64(perfect_turns) / transcribed_turns

    if output_misheard:
        for pos, (word, times) in sorted(misheard.items(), key=lambda x: x[1], reverse=True)[:20]:
            stats['misheard_top-%02d' % pos] = '%s:%d' % (word, times)

        mishear_freq = {w: mis/float(mis + correct.get(w, 0)) for w, mis in misheard.iteritems() if mis >= 5}
        for pos, (word, freq) in sorted(mishear_freq.items(), key=lambda x: x[1], reverse=True)[:20]:
            stats['mishear_freq_top-%02d' % pos] = '%s:%.3f' % (word, freq)

    return stats, raw


def objective_success(calls):
    stats = OrderedDict()
    raw = OrderedDict()

    # this only uses 1-best NLU
    def get_user_das(call):
        return [turn['usr_turn']['sem_hyps'][0]['dial_act'][0]
                for turn in call['dialogue'] if turn['usr_turn']['sem_hyps']]

    def get_sys_das(call):
        return [turn['sys_turn']['sys_act'] for turn in call['dialogue']]
    # check if the user mentioned all requirements
    cstr_mentioned = []
    for call in calls:
        user_nlu = '\n'.join(get_user_das(call))
        for constraint in call['task']['constraints']:
            if user_nlu.find(constraint) == -1:
                cstr_mentioned.append(0)
                break
        else:
            cstr_mentioned.append(1)
    stats['constr_mentioned'] = np.mean(cstr_mentioned)
    raw['constr_mentioned'] = cstr_mentioned

    # check if the system provided one of the matching entities
    ent_mentioned = []  # system mentioned match (1-0 vector, 1 number per call)
    cstr_confirmed = [] # system confirmed all constraints of a match (1-0, 1 number per call)
    req_provided = []  # system mentioned match & all its requirements (1-0, 1 number per call)
    for call in calls:
        cur_ents = {}
        # look into system DAs
        for sys_da in get_sys_das(call):
            m = re.search(r'inform\(\[([^\]]*)\]\)', sys_da)
            if m:  # only use informs
                # split by slots
                slots = re.split(r'(?:^|, )([a-z_]+):', m.group(1))[1:]
                slots = dict(zip(slots[::2], slots[1::2]))
                if 'name' not in slots:  # ignore informs not about a restaurant
                    continue
                # store a union of all slots ever mentioned for a given restaurant
                cur_ents[slots['name']] = cur_ents.get(slots['name'], {})
                cur_ents[slots['name']].update(slots)

        # shouldn't find anything and complained -- inform(name=none) should have been issued
        if not call['task']['matching']:
            ent_mentioned.append(1 if 'none' in cur_ents else 0)
            req_provided.append(1 if 'none' in cur_ents else 0)
        # found one of the correct ones?
        else:
            # just check if the system ever mentioned one of the correct restaurants
            matching_mentioned = set(cur_ents.keys()) & set(call['task']['matching'])
            ent_mentioned.append(1 if matching_mentioned else 0)
            # check if the system confirmed all the constraints the user was supposed to ask
            # also check if it explicitly mentioned all the slots the user should have requested
            cstrs = set(call['task']['constraints'])
            cstr_confirmed.append(1 if any((set([':'.join(i) for i in cur_ents[ent].items()]) & cstrs == cstrs)
                                           for ent in matching_mentioned)
                                  else 0)
            # for at least one of the correct restaurants
            to_req = set(call['task']['requests'])  # list of slots that should be requested
            req_provided.append(1 if any((set(cur_ents[ent]) & to_req == to_req)
                                         for ent in matching_mentioned)
                                else 0)
    stats['entity_provided'] = np.mean(ent_mentioned)
    stats['constr_confirmed'] = np.mean(cstr_confirmed)
    raw['entity_provided'] = ent_mentioned
    raw['constr_confirmed'] = cstr_confirmed

    # check if the user asked for all the stuff they should
    req_asked = []
    for call in calls:
        reqs = set()
        for user_da in get_user_das(call):
            m = re.search(r'setQuestion\(\[([^\]]+)\]\)', user_da)
            if m:
                reqs.update(m.group(1).split(','))
        to_req = set(call['task']['requests'])
        to_req.discard('name')  # name is always provided, no need to ask for it
        req_asked.append(1 if (reqs & to_req) == to_req else 0)

    stats['all_info_requested'] = np.mean(req_asked)
    stats['all_info_provided'] = np.mean(req_provided)
    raw['all_info_requested'] = req_asked
    raw['all_info_provided'] = req_provided
    return stats, raw


def questionnaire_stats(calls):
    ret = OrderedDict()

    stats = OrderedDict()
    for key in ['q1', 'q2', 'q3', 'q4', 'q5']:
        stats[key] = []
    for call in calls:
        for q in stats.keys():
            val = call['CF_data'][q]
            val = {'Yes': 1, 'No': 0}.get(val, val)  # replace Yes/No with 1/0
            stats[q].append(val)

    for q, vals in sorted(stats.items()):
        ret['%s_mean' % q] = np.mean(vals)
        ret['%s_std' % q] = np.std(vals)
        for val, count in enumerate(np.bincount(vals)):
            if q != 'q1' and val == 0:  # 0 is not a legal value, skip it
                continue
            ret['%s_count_%d' % (q, val)] = count

    return ret, stats


def turn_stats(calls):
    stats = OrderedDict()
    raw = OrderedDict()

    turns = []
    transcr_turns = []
    for call in calls:
        turns.append(len(call['dialogue']))
    stats['turns_mean'] = np.mean(turns)
    stats['turns_std'] = np.std(turns)
    raw['turns'] = turns
    raw['asr_sem_err'] = []
    raw['asr_sem_fine_err'] = []

    null_turns = []
    empty_req_turns = []
    asr_sem_err = []
    da_fine_errs, da_fine_len = 0, 0
    user_acts = {}
    transcr_user_acts = {}
    sys_acts = {}
    for call in calls:
        user_das = [DA(turn['usr_turn']['sem_hyps'][0]['dial_act'][0])
                    if turn['usr_turn']['sem_hyps'] else (
                        'HANGUP' if turn['usr_turn']['transcription'].startswith('HANGUP') else None
                    )
                    for turn in call['dialogue']]
        transcr_user_das = [DA(turn['usr_turn']['transcription_sem']['dial_act'])
                            if 'transcription_sem' in turn['usr_turn'] else (
                                "" if turn['usr_turn']['transcription'] == "" else (
                                    'HANGUP' if turn['usr_turn']['transcription'].startswith('HANGUP') else None
                                )
                            )
                            for turn in call['dialogue']]
        sys_das = [DA(turn['sys_turn']['sys_act']) for turn in call['dialogue']]
        null_turns.append(len([d for d in user_das if d is None]))
        empty_req_turns.append(len([d for d in user_das if d == 'setQuestion([])']))
        call_asr_sem_errs = sum([h != t for h, t in zip(user_das, transcr_user_das) if t not in set(["", "HANGUP"])])
        call_transcr_turns = len([t for t in transcr_user_das if t not in set(["", "HANGUP"])])
        if call_transcr_turns:
            transcr_turns.append(call_transcr_turns)
            asr_sem_err.append(call_asr_sem_errs)
            raw['asr_sem_err'].append(call_asr_sem_errs / np.float64(call_transcr_turns))

        call_e, call_l = 0, 0
        for h, t in zip(user_das, transcr_user_das):
            if t in set(["", "HANGUP"]):
                continue  # ignore non-transcribed turns
            e, l = da_diff(t, h)
            call_e += e
            call_l += l
        da_fine_errs += call_e
        da_fine_len += call_l
        if call_l:
            raw['asr_sem_fine_err'].append(call_e / np.float64(call_l))

        usr_act_types = [d.act_type if isinstance(d, DA) else d for d in user_das]
        for act_type in usr_act_types:
            user_acts[act_type] = user_acts.get(act_type, 0) + 1
        transcr_usr_act_types = [d.act_type if isinstance(d, DA) else d for d in transcr_user_das
                                 if d not in set(["", "HANGUP"])]
        for act_type in transcr_usr_act_types:
            transcr_user_acts[act_type] = transcr_user_acts.get(act_type, 0) + 1
        sys_act_types = [d.act_type if isinstance(d, DA) else d for d in sys_das]
        for act_type in sys_act_types:
            sys_acts[act_type] = sys_acts.get(act_type, 0) + 1

    stats['turns_null_rate'] = np.sum(null_turns) / float(np.sum(turns))
    stats['empty_reqs_rate'] = np.sum(empty_req_turns) / float(np.sum(turns))
    stats['asr_sem_err_rate'] = np.sum(asr_sem_err) / float(np.sum(turns))
    stats['asr_sem_fine_err_rate'] = da_fine_errs / float(da_fine_len)
    for act_type in sorted(user_acts.keys()):
        # stats['user_da-%s' % act_type] = user_acts[act_type]
        stats['user_da_rate-%s' % act_type] = user_acts[act_type] / float(np.sum(turns))
    for act_type in sorted(transcr_user_acts.keys()):
        # stats['transcr_user_da-%s' % act_type] = transcr_user_acts[act_type]
        stats['transcr_user_da_rate-%s' % act_type] = transcr_user_acts[act_type] / float(np.sum(transcr_turns))
    for act_type in sorted(sys_acts.keys()):
        # stats['sys_da-%s' % act_type] = sys_acts[act_type]
        stats['sys_da_rate-%s' % act_type] = sys_acts[act_type] / float(np.sum(turns))

    return stats, raw


def split_data_by_key(calls, key_prefix, key_func):
    """Split the data by a given key function that gets the system ID / task ID etc."""
    calls_by_key = {}

    for call in calls:
        key_val = key_func(call)
        calls_by_key[key_prefix + key_val] = calls_by_key.get(key_prefix + key_val, []) + [call]

    return calls_by_key


def per_key_stats(data, output_misheard=False):

    stats = OrderedDict()
    raw = OrderedDict()
    for key_val in sorted(data.keys()):
        key_data = data[key_val]
        w_stats, w_raw = wer_stats(key_data, output_misheard)
        stats[key_val] = w_stats
        raw[key_val] = w_raw
        o_stats, o_raw = objective_success(key_data)
        stats[key_val].update(o_stats)
        raw[key_val].update(o_raw)
        q_stats, q_raw = questionnaire_stats(key_data)
        stats[key_val].update(q_stats)
        raw[key_val].update(q_raw)
        t_stats, t_raw =turn_stats(key_data)
        stats[key_val].update(t_stats)
        raw[key_val].update(t_raw)

    return stats, raw


def print_stats(data, as_csv):
    if as_csv:
        val_types = data.itervalues().next().keys()
        keys = data.keys()
        print "\t".join([''] + keys)
        for val_type in val_types:
            values = [data[key].get(val_type, 0) for key in keys]
            if isinstance(values[0], int):
                values = ['%d' % val for val in values]
            elif isinstance(values[0], basestring):
                values = [val for val in values]
            else:
                values = ['%.3f' % val for val in values]
            print "\t".join([val_type] + values)
    else:
        for data_key, data_sub in data.iteritems():
            print
            print 'STATS FOR %s' % data_key
            for key, val in data_sub.iteritems():
                if isinstance(val, int):
                    print '%s:\t%d' % (key, val)
                else:
                    print '%s:\t%.3f' % (key, val)


def add_signif_tests(stats, raw, signif_level=0.05,
                     run_tost=False, eq_rel_epsilon=0.05):
    """Compute significance tests & add the result (list of sign. different systems from this one)
    to the data."""
    if run_tost:
        from rpy2.robjects import FloatVector
        from rpy2.robjects.packages import importr
        from rpy2.rinterface import RRuntimeError
        r_eq = importr('equivalence')

    for val_type in raw.itervalues().next().keys():
        for sys_type in raw.iterkeys():

            sign_mwu = []
            sign_fisher = []
            sign_chisq = []
            sign_rtost = []
            sign_ztost = []

            x = raw[sys_type][val_type]

            for other_sys_type in raw.iterkeys():
                if sys_type == other_sys_type:
                    continue
                y = raw[other_sys_type][val_type]
                side = 'greater' if np.mean(x) > np.mean(y) else 'less'

                # Mann-Whitney U
                try:
                    U, p = scipy.stats.mannwhitneyu(x, y, use_continuity=True, alternative=side)
                    if p < signif_level:
                        sign_mwu.append('%s=%.3f' % (re.sub('.*=', '', other_sys_type), p))
                except ValueError as ex:
                    print >> sys.stderr, str(ex), '(%s for %s and %s)' % (val_type, sys_type, other_sys_type)

                # Fisher's exact test + ztost for binary only
                if all([i in set([0, 1]) for i in x]) and all([i in set([0, 1]) for i in y]):
                    cont_table = [[len(y) - sum(y), len(x) - sum(x)], # num. of 0's in sys Y and X
                                  [sum(y), sum(x)]]  # num. of 1's in sys Y and X
                    _, p = scipy.stats.fisher_exact(cont_table, side)
                    if p < signif_level:
                        sign_fisher.append('%s=%.3f' % (re.sub('.*=', '', other_sys_type), p))

                    p = ztost(x, y, eq_rel_epsilon)  # eq_rel_epsilon = epsilon in binary case
                    if p < signif_level:
                        sign_ztost.append('%s=%.3f' % (re.sub('.*=', '', other_sys_type), p))

                # Chi-square test for categorical values
                if (all([isinstance(i, int) for i in x]) and all([isinstance(i, int) for i in y]) and
                        len(set(x)) <= 6 and len(set(y)) <= 6):
                    cont_table = np.transpose([np.bincount(y), np.bincount(x)])
                    _, p, _, _ = scipy.stats.chi2_contingency(cont_table[np.any(cont_table != 0, axis=1)])
                    if p < signif_level:
                        sign_chisq.append('%s=%.3f' % (re.sub('.*=', '', other_sys_type), p))

                # R-TOST
                if run_tost:
                    try:
                        epsilon = eq_rel_epsilon * (np.max(x + y) - np.min(x + y))
                        res = r_eq.rtost(FloatVector(x), FloatVector(y), signif_level, epsilon)
                        if res[7][0] < signif_level:  # res[7] is p-value
                            sign_rtost.append('%s=%.3f' % (re.sub('.*=', '', other_sys_type), res[7][0]))
                    except RRuntimeError:
                        print >> sys.stderr, (
                            '!! Undefined values in equivalence test for %s between %s and %s' %
                            (val_type, sys_type, other_sys_type))

            stats[sys_type][val_type + '-sign_mwU'] = ','.join(sign_mwu)
            if all([i in set([0, 1]) for i in x]):
                stats[sys_type][val_type + '-sign_Fisher'] = ','.join(sign_fisher)
                stats[sys_type][val_type + '-sign_ztost'] = ','.join(sign_ztost)
            if all([isinstance(i, int) for i in x]) and len(set(x)) <= 6:
                stats[sys_type][val_type + '-sign_chisq'] = ','.join(sign_chisq)
            if run_tost:
                stats[sys_type][val_type + '-sign_rtost'] = ','.join(sign_rtost)

    return stats



def main(args):
    with codecs.open(args.logfile, 'rb', 'UTF-8') as fh:
        data = json.load(fh)

    # to store averaged stats; keys sys_id -> data_type
    stats = OrderedDict()
    # to raw data for significance testing; keys sys_id -> data_type
    raw = OrderedDict()

    # overall stats
    all_stats, all_raw = per_key_stats({'*': data}, args.misheard)
    stats.update(all_stats)
    raw.update(all_raw)

    # split stats
    if args.per_task:
        split_data = split_data_by_key(data, 'taskID=', lambda call: '%03d' % int(call['taskID']))
        split_stats, split_raw = per_key_stats(split_data, args.misheard)
        stats.update(split_stats)
        raw.update(split_raw)

    if args.per_system:
        split_data = split_data_by_key(data, 'sysName=', lambda call: call['sysName'])
        split_stats, split_raw = per_key_stats(split_data, args.misheard)
        stats.update(split_stats)
        raw.update(split_raw)

    if args.per_sys_letter:
        split_data = split_data_by_key(data, 'sysLetter=', lambda call: call['sysName'][:-1])
        split_stats, split_raw = per_key_stats(split_data, args.misheard)
        stats.update(split_stats)
        raw.update(split_raw)

    if args.per_sys_number:
        split_data = split_data_by_key(data, 'sysNumber=', lambda call: call['sysName'][-1])
        split_stats, split_raw = per_key_stats(split_data, args.misheard)
        stats.update(split_stats)
        raw.update(split_raw)

    stats = add_signif_tests(stats, raw, args.p_level, args.tost, args.epsilon)

    print_stats(stats, args.csv)


if __name__ == '__main__':
    ap = ArgumentParser(description='Compute dialogue system call statistics')
    ap.add_argument('--csv', '-c', action='store_true', help='Output CSV instead of text')
    ap.add_argument('--misheard', '-m', action='store_true', help='Print frequently misheard words')
    ap.add_argument('--per-task', '-t', action='store_true', help='Split data by task')
    ap.add_argument('--per-system', '-s', action='store_true', help='Split data by system ID')
    ap.add_argument('--per-sys-letter', '-l', action='store_true', help='Split data by system letter only')
    ap.add_argument('--per-sys-number', '-n', action='store_true', help='Split data by system number only')
    ap.add_argument('--p-level', '-p', type=float, help='P-value level to use for signif test', default=0.05)
    ap.add_argument('--epsilon', '-e', type=float, help='Relative epsilon value used for equivalence test', default=0.05)
    ap.add_argument('--tost', '-T', action='store_true', help='Run R-based TOST')
    ap.add_argument('logfile', type=str, help='JSON with joint logs')

    args = ap.parse_args()
    main(args)
