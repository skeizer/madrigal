#
# script to merge average learning curves for multi-dim, trans-adapt, and trans-fixed
# policy optimisation experiments into a single data file and plot
#

logdir=$1
metric=$2

ofname="${logdir}_transLearnCurves_${metric}.txt"
pfname="${logdir}_transLearnCurves_${metric}.png"

wdir=${0%/*}

echo "perl ${wdir}/merge_avg_learn_curves.pl ${logdir}*/task_merged_${metric}.txt > ${ofname}"
perl ${wdir}/merge_avg_learn_curves.pl ${logdir}*/task_merged_${metric}.txt > ${ofname}

echo "gnuplot -persist -e \"filename='${ofname}'; ttl='${logdir} transfer learning curves (${metric})'\" ${wdir}/plot_avg_learning_curves.gpl > ${pfname}"
gnuplot -persist -e "filename='${ofname}'; ttl='${logdir} transfer learning curves (${metric})'" ${wdir}/plot_avg_learning_curves.gpl > ${pfname}

