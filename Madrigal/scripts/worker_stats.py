#!/usr/bin/env python2
# -"- coding: utf-8 -"-

from __future__ import print_function

import json
import codecs
from argparse import ArgumentParser
from collections import defaultdict

import numpy as np

from call_stats import split_data_by_key


def analyze_workers(label, data):

    channel = defaultdict(int)
    tasks_per_worker = defaultdict(int)
    trust_per_worker = {}
    trust_abs = []
    countries = defaultdict(int)

    for call in data:
        cfdata = call['CF_data']
        channel[cfdata['_channel']] += 1
        tasks_per_worker[cfdata['_worker_id']] += 1
        trust_per_worker[cfdata['_worker_id']] = cfdata['_trust']
        trust_abs.append(cfdata['_trust'])
        countries[cfdata['_country']] += 1

    print("Worker stats for %s" % label)
    print("------------------------")
    print("Channels distribution:")
    print("\n".join(["  %-15s%4d" % (channel, num_calls)
                     for channel, num_calls in sorted(channel.items(), key=lambda item: item[1], reverse=True)]))
    print()

    print("Countries distribution:")
    print("\n".join(["  %-8s%4d" % (country, num_calls)
                     for country, num_calls in sorted(countries.items(), key=lambda item: item[1], reverse=True)]))
    print()

    print("Workers who completed the given number of tasks:")
    workers = np.bincount(tasks_per_worker.values())
    print("\n".join(["  %2d:  %4d" % (num_tasks, num_workers)
                     for num_tasks, num_workers in reversed(list(enumerate(workers)))
                     if num_workers > 0]))
    print()


    print("Workers with given trust:")
    workers, bins = np.histogram(trust_per_worker.values(), bins=[0.0, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.001])
    print("\n".join(["  [%.2f-%.2f)  %4d" % (bin_lo, bin_hi, num_calls)
                     for bin_lo, bin_hi, num_calls in zip(bins[:-1], bins[1:], workers)]))

    print("Average trust (over calls):   %.3f" % np.mean(trust_abs))
    print("Average trust (over workers): %.3f\n" % np.mean(trust_per_worker.values()))
    print()



def main(args):
    with codecs.open(args.logfile, 'rb', 'UTF-8') as fh:
        data = json.load(fh)

    split_data = {}
    if args.per_system:
        split_data = split_data_by_key(data, 'sysName=', lambda call: call['sysName'])

    elif args.per_sys_letter:
        split_data = split_data_by_key(data, 'sysLetter=', lambda call: call['sysName'][:-1])

    elif args.per_sys_number:
        split_data = split_data_by_key(data, 'sysNumber=', lambda call: call['sysName'][-1])

    split_data['*'] = data

    for label, data in sorted(split_data.items()):
        analyze_workers(label, data)


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('--per-system', '-s', action='store_true', help='Split data by system ID')
    ap.add_argument('--per-sys-letter', '-l', action='store_true', help='Split data by system letter only')
    ap.add_argument('--per-sys-number', '-n', action='store_true', help='Split data by system number only')
    ap.add_argument('logfile', type=str, help='JSON with joint logs')

    args = ap.parse_args()
    main(args)
