#!/bin/sh

#NOTE: change error rate variables (errRt and errNm) as appropriate!

if [ $# -ne 2 ]
then
  echo "USAGE: ./eval_learn_curve.sh <logdir> <config>"
  exit
fi

dir=`basename "$1"`
#dir="tra_mdim_r20_n40k"
#dir="tra_mdim_r20_n40k_transfer-adapt"
#dir="tra_mdim_r20_n40k_transfer-fixed"

#conf="config_mdp_eval.cfg"
#conf="config_mdp_eval_1dim.cfg"
conf=$2

tdir="training/${dir}"
errRt=0.3
errNm="30"
logdir="logs/eval/${dir}"

n=60
k=5

wdir=${0%/*}
for (( i=0; i<=$n/$k; i++ ))
do
   ndd=$(( $n-$k*$i ))
   nd=$(( $ndd*1000 ))
   ld=${logdir}/p${ndd}k/eval_r${errNm}
   echo "${wdir}/run_multi_sims_eval.sh ${nd} ${tdir} -c ${conf} -r ${errRt} -l ${ld}"
   ${wdir}/run_multi_sims_eval.sh ${nd} ${tdir} -c ${conf} -r ${errRt} -l ${ld}
   echo "perl $MADRIGAL_HOME/scripts/get_results.pl ${ld}/main_*"
   perl $MADRIGAL_HOME/scripts/get_results.pl ${ld}/main_*
done

ld=${logdir}/p2k/eval_r${errNm}
echo "${wdir}/run_multi_sims_eval.sh 2000 ${tdir} -c ${conf} -r ${errRt} -l ${ld}"
${wdir}/run_multi_sims_eval.sh 2000 ${tdir} -c ${conf} -r ${errRt} -l ${ld}
echo "perl $MADRIGAL_HOME/scripts/get_results.pl ${ld}/main_*"
perl $MADRIGAL_HOME/scripts/get_results.pl ${ld}/main_*

ld=${logdir}/p1k/eval_r${errNm}
echo "${wdir}/run_multi_sims_eval.sh 1000 ${tdir} -c ${conf} -r ${errRt} -l ${ld}"
${wdir}/run_multi_sims_eval.sh 1000 ${tdir} -c ${conf} -r ${errRt} -l ${ld}
echo "perl $MADRIGAL_HOME/scripts/get_results.pl ${ld}/main_*"
perl $MADRIGAL_HOME/scripts/get_results.pl ${ld}/main_*

