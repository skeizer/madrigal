#
#
# Simon Keizer, 01/09/'16
# 

if ( $#ARGV < 0 ) {
    print "USAGE: perl get_results.pl <log-files>\n";
    exit;
}

my $trace = 0;

my $dialsPerPoint = 10; # results printed only after every X entries

my $line;
my $numDials;
my $avgScore, $avgLen, $succRate;

foreach $file ( @ARGV ) {

    if ( $file =~ /\S+\.log/ ) {
	
	if ( $trace ) { print "opening log file $file\n"; }
	open ( FILE, $file ) or die "Cannot open log file $file\n";
	my $ofile = ( substr $file, 0, -3 ) . "txt";
	print "output file: $ofile\n";
	open ( OFILE, ">", $ofile ) or die "Cannot open output file $ofile\n";

	$numDials = 0;
	while ( <FILE> ) {
	    $line = $_;
	    if ( $line =~ /Stats\:/ ) {
		#( $w, $n, $avgLen, $avgScore ) = split /\s+/, $line;
		@parts = split /\s+/, $line;
		$avgLen = $parts[ $#parts-2 ];
		$avgScore = $parts[ $#parts-1 ];
		$succRate = $parts[ $#parts ];
		$numDials++;
		if ( $trace ) { print join( ', ', ($numDials,$avgLen,$avgScore,$succRate) ) . "\n"; }
		if ( $numDials % $dialsPerPoint == 0 ) {
		    if ( $trace ) { print "printing results\n"; }
		    print ( OFILE join( "\t", ($numDials,$avgLen,$avgScore,$succRate) ) . "\n" );
		} else {
		    if ( $trace ) { print "skipping entry\n"; }
		}
	    }
	}
	close( FILE );
	close( OFILE );

    } else {
	if ( $trace ) { print "skipping file $file\n"; }
    }

}

