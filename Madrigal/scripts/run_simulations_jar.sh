#!/bin/bash

# Script for running simulated dialogues, for testing/evaluating a dialogue manager,
# or for training an MDP dialogue manager policy.
# 
# Simon Keizer, 12/07/'16
# Last modified: 15/07/'16

wdir=${0%/*}/..
jdir=${wdir}/dist
echo "java -jar ${jdir}/Madrigal.jar $*"
java -jar ${jdir}/Madrigal.jar $*

