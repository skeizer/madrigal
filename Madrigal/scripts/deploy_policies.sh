#!/bin/sh

# Script to copy trained policies from multiple runs in a given directory to a target directory for deployment,
# in particular, a directory in the Madrigal resources directory.  NOTE: move any existing resource policy directory elsewhere as backup.
#
# USAGE: ./deploy_policies.sh <training directory> <number of training dialogues (x 1000)> <num-training-runs> <target_directory>
#
# EXAMPLE: ./deploy_policies.sh training/tra_r30_n60k 60 5 $MADRIGAL_HOME/src/resources/policies/mdp-mcc-lvfa_caminfo_r30_n60k_3best 
# 
#    copies trained policies for subdirs tra_0 to tra_4 and all dimensions <dimension>_60000.pcy from directory
#    training/tra_r30_n60k to the given target directory.
#

if [ $# -ne 4 ]
then
  echo "USAGE: ./deploy_policies.sh <training directory> <number of training dialogues (x1000)> <number of training runs> <target_directory>"
  exit
fi

td=$1
nd=$2
ndk=$(( ${nd}*1000 ))
nr=$3
rd=$4

# show input arguments:
echo "source training directory: ${td}"
echo "number of training dialogues final policies: ${ndk}"
echo "number of training run subdirectories to copy from: ${nr}"
echo "target resource directory: ${rd}"

# dimensions to be included:
dims=(task autoFeedback socialObligationsManagement)
#dims=(all)
echo "policies to copy from ${nr} subdirectories:"
for d in ${dims[@]}
do
   echo "     ${d}_${ndk}.pcy"
done

# create target directory
echo "  mkdir ${rd}"
mkdir ${rd}

# loop over training runs
for (( i=0; i<${nr}; i++ ))
do
   tdir="${td}/tra_$i"
   
   # create new training run subdir in target directory 
   rd2="${rd}/tra_$i"
   echo "  mkdir ${rd2}"
   mkdir ${rd2}

   # copy trained policies to target directory
   # loop over the dimensions
   for dim in ${dims[@]}
   do
      # copy trained source policy for this dimension to the target directory
      echo "cp ${tdir}/${dim}_${ndk}.pcy ${rd2}/${dim}_${ndk}.pcy"
      cp ${tdir}/${dim}_${ndk}.pcy ${rd2}/${dim}_${ndk}.pcy
   done
done

