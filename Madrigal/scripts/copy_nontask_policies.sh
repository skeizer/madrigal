#!/bin/sh

#script to create new directory with pre-trained policies for transfer experiments
#
# USAGE: ./copy_nontask_policies.sh <training directory> <number of training dialogues (x 1000)> <num-training-runs>
#
# EXAMPLE: ./copy_nontask_policies.sh training/tra_r20_n40k 40 10
# 
#    copies trained policies for dimensions to transfer <dimension>_40000.pcy from directory
#    training/tra_r20_n20k to training/tra_r20_n20k_new
#
#    performs this copy operation for each training stage, i.e., the new directory will have
#    multiple copies of <dimension>_40000.pcy, named <dimension>_1000.pcy to <dimension>_40000.pcy
#

if [ $# -ne 3 ]
then
  echo "USAGE: ./copy_nontask_policies.sh <training directory> <number of training dialogues (x1000)> <number of training runs>"
  exit
fi


td=$1
echo "training directory: $td"
nd=$2
ndk=$(( ${nd}*1000 ))

# number of training runs (to be matched with the one specified in run_multi_sims_tra.sh)
n=$3
echo "number of policy subdirs: $n"
echo

# dimensions to be transferred
dims=(autoFeedback socialObligationsManagement)
echo "policies to copy:"
for d in ${dims[@]}
do
   echo "     ${d}_${ndk}.pcy"
done

# create new training directory
tdir1="${td}_new"
echo "  mkdir ${tdir1}"
mkdir ${tdir1}

# loop over training runs
for (( i=0; i<$n; i++ ))
do
   tdir="${td}/tra_$i"
   # create new training run subdir in new training dir
   tdir2="${tdir1}/tra_$i"
   echo "  mkdir ${tdir2}"
   mkdir ${tdir2}
   # copy trained source policies to new directory (starting policies for all training stages in new training)
   # loop over all training stages (for which we have saved the trained policies)
   for (( k=0; k<${nd}; k++ ))
   do
      kk=$(( $k+1 ))
      nk=$(( $kk*1000 ))
      # loop over the transferred dimensions
      for d in ${dims[@]}
      do
         # copy trained source policy for this dimension to new directory
         echo "cp ${tdir}/${d}_${ndk}.pcy ${tdir2}/${d}_${nk}.pcy"
         cp ${tdir}/${d}_${ndk}.pcy ${tdir2}/${d}_${nk}.pcy
      done
   done
done

