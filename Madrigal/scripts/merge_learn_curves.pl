#
#
# Simon Keizer, 11/07/'17
# 

if ( $#ARGV < 1 ) {
    print "USAGE: perl merge_learn_curves.pl [l|r|s] <data-files>\n"
	. "              l: average length\n"
	. "              r: average reward\n"
	. "              s: success rate\n";
    exit;
}

my $trace = 0;

my $inclAvg = 1;

my $datTpStr = shift;
if ( $datTpStr eq "l" ) {
    $datTpInd = 1;
} elsif ( $datTpStr eq "r" ) {
    $datTpInd = 2;
} elsif ( $datTpStr eq "s" ) {
    $datTpInd = 3;
} else {
    print "illegal argument: " . $datTpStr;
    exit;
}

my %data = ();

foreach $file ( @ARGV ) {

    if ( $file =~ /\S+\.txt/ ) {
	
	if ( $trace ) { print "opening data file $file\n"; }
	open ( FILE, $file ) or die "Cannot open data file $file\n";

	while ( <FILE> ) {
	    @data_entry = split /\s+/;
	    push @{$data{$data_entry[0]}}, $data_entry[$datTpInd];
	    $size = scalar @{$data{$data_entry[0]}};
	    if ( $trace ) { print "n:$data_entry[0]; s:$data_entry[$datTpInd]; N:$size\n"; }
	}
	close( FILE );
	
    } else {
	if ( $trace ) { print "skipping file $file\n"; }
    }

}

foreach $key ( sort {$a<=>$b} keys %data ) {
    @scores = @{ $data{$key} };
    if ( $inclAvg ) {
        my $avg = 0;
        my $num = scalar @scores;
        foreach $score ( @scores ) {
	    $avg += $score;
    }
    $avg /= $num;
    push @scores, $avg;
    }
    print $key . "\t" . join( "\t", @scores ) . "\n";
}


