#!/bin/bash

# Script for running simulated dialogues, for testing/evaluating a dialogue manager,
# or for training an MDP dialogue manager policy.
# 
# Simon Keizer, 12/07/'16
# Last modified: 13/07/'16

wdir=${0%/*}/..
ldir=${wdir}/lib
tdir=${wdir}/target
echo "java -cp ${ldir}/mallet.jar:${ldir}/java-json.jar:${ldir}/commons-lang3-3.4.jar:${ldir}/commons-cli-1.3.1.jar:${tdir}/classes/. uk.ac.hw.madrigal.Madrigal $*"
java -cp ${ldir}/mallet.jar:${ldir}/java-json.jar:${ldir}/commons-lang3-3.4.jar:${ldir}/commons-cli-1.3.1.jar:${tdir}/classes/. uk.ac.hw.madrigal.Madrigal $*
