#
#
# Simon Keizer, 31/08/'16
# 

if ( $#ARGV < 0 ) {
    print "USAGE: perl get_results.pl <log-files>\n";
    exit;
}

my $trace = 0;

my $line;
my $succRate, $avgScore, $avgLen;
my $succRate_avg, $avgScore_avg, $avgLen_avg;
my $numRuns = 0;

foreach $file ( @ARGV ) {

    if ( $file =~ /\S+\.log/ ) {

	if ( $trace ) { print "opening log file $file\n"; }
	open ( FILE, $file ) or die "Cannot open policy file $file\n";

	while ( <FILE> ) {
	    $line = $_;
	    if ( $line =~ /Success rate\: (\S+)/ ) {
		$succRate = $1;
		if ( $trace ) { print "succRate: $succRate\n"; }
	    }
	    if ( $line =~ /Average score\: (\S+) / ) {
		$avgScore = $1;
		if ( $trace ) { print "avgScore: $avgScore\n"; }
	    }
	    if ( $line =~ /Average length\: (\S+) / ) {
		$avgLen = $1;
		if ( $trace ) { print "avgLen: $avgLen\n"; }
	    }
	}
	print join( "\t", ($succRate, $avgLen, $avgScore) ) . "\n";

	close( FILE );

	# update overall stats:
	$succRate_avg += $succRate;
	$avgScore_avg += $avgScore;
	$avgLen_avg += $avgLen;
	$numRuns++;
	
    } else {
	if ( $trace ) { print "skipping file $file\n"; }
    }

}

# compute averages:
$succRate_avg /= $numRuns;
$avgScore_avg /= $numRuns;
$avgLen_avg /= $numRuns;
print "----------------------\n";
print join( "\t", ($succRate_avg, $avgLen_avg, $avgScore_avg) ) . "\n";
