#!/bin/sh

#script to create new directory with pre-trained policies for transfer experiments
#
# USAGE: ./copy_nontask_policies_foreval.sh <source directory> <target directory> <num-training-runs>
#

source=$1
echo "source policy directory: $source"
target=$2
echo "target policy directory: $target"
echo

# number of training runs (to be matched with the one specified in run_multi_sims_tra.sh)
n=$3
echo "number of policy subdirs: $n"
echo

# loop over training runs
for (( i=0; i<$n; i++ ))
do
   sdir="${source}/tra_$i"
   tdir="${target}/tra_$i"
   echo "cp ${sdir}/*.pcy ${tdir}/"
   cp ${sdir}/*.pcy ${tdir}/
done

