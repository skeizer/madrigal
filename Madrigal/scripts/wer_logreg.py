#!/usr/bin/env python2
# -"- coding: utf-8 -"-

import json
import random
import codecs
import re
import yaml
from argparse import ArgumentParser

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors

from sklearn.linear_model import LogisticRegression

from call_stats import measure_wer, get_asr_with_trans, split_data_by_key

random.seed(1206)

def call_wer(call):

    asr_with_trans = get_asr_with_trans(call)
    if not asr_with_trans:
        return None

    missed = 0
    inserted = 0
    substituted = 0
    total_words = 0
    for asr, trans in asr_with_trans:
        m, i, s, w, _, _ = measure_wer(asr, trans)
        missed += m
        inserted += i
        substituted += s
        total_words += w

    return (missed + inserted + substituted) / np.float64(total_words)


def call_success(call, success_type):
    # this only uses 1-best NLU
    def get_user_das(call):
        return [turn['usr_turn']['sem_hyps'][0]['dial_act'][0]
                for turn in call['dialogue'] if turn['usr_turn']['sem_hyps']]

    def get_sys_das(call):
        return [turn['sys_turn']['sys_act'] for turn in call['dialogue']]

    if success_type == 'constr_mentioned':
        user_nlu = '\n'.join(get_user_das(call))
        for constraint in call['task']['constraints']:
            if user_nlu.find(constraint) == -1:
                return 0
        return 1

    elif success_type in ['entity_provided', 'all_info_provided', 'constr_confirmed']:

        cur_ents = {}
        # look into system DAs
        for sys_da in get_sys_das(call):
            m = re.search(r'inform\(\[([^\]]*)\]\)', sys_da)
            if m:  # only use informs
                # split by slots
                slots = re.split(r'(?:^|, )([a-z_]+):', m.group(1))[1:]
                slots = dict(zip(slots[::2], slots[1::2]))
                if 'name' not in slots:  # ignore informs not about a restaurant
                    continue
                # store a union of all slots ever mentioned for a given restaurant
                cur_ents[slots['name']] = cur_ents.get(slots['name'], {})
                cur_ents[slots['name']].update(slots)

        # shouldn't find anything and complained -- inform(name=none) should have been issued
        if not call['task']['matching']:
            return 1 if 'none' in cur_ents else 0
        # found one of the correct ones?
        else:
            matching_mentioned = set(cur_ents.keys()) & set(call['task']['matching'])
            # just check if the system ever mentioned one of the correct restaurants
            if success_type == 'entity_provided':
                return (1 if matching_mentioned else 0)
            # check if the system confirmed all the constraints the user was supposed to ask
            # also check if it explicitly mentioned all the slots the user should have requested
            elif success_type == 'constr_confirmed':
                cstrs = set(call['task']['constraints'])
                return (1 if any((set([':'.join(i) for i in cur_ents[ent].items()]) & cstrs == cstrs)
                                 for ent in matching_mentioned)
                        else 0)
            # for at least one of the correct restaurants
            elif success_type == 'all_info_provided':
                to_req = set(call['task']['requests'])  # list of slots that should be requested
                return (1 if any((set(cur_ents[ent]) & to_req == to_req)
                                 for ent in matching_mentioned)
                        else 0)

    elif success_type == 'all_info_requested':
        reqs = set()
        for user_da in get_user_das(call):
            m = re.search(r'setQuestion\(\[([^\]]+)\]\)', user_da)
            if m:
                reqs.update(m.group(1).split(','))
        to_req = set(call['task']['requests'])
        to_req.discard('name')  # name is always provided, no need to ask for it
        return (1 if (reqs & to_req) == to_req else 0)

    elif success_type == 'q1':
        val = call['CF_data']['q1']
        val = {'Yes': 1, 'No': 0}.get(val, val)  # replace Yes/No with 1/0
        return val


def build_logreg(calls, success_type):
    def sigmoid(x):
        return 1 / (1 + np.exp(-x))

    X = []
    y = []
    for call in calls:
        wer = call_wer(call)
        if wer is None:
            continue
        X.append([wer])
        y.append(call_success(call, success_type))

    model = LogisticRegression(tol=1e-6, max_iter=10000, C=1000000, penalty='l2')
    model.fit(X, y)
    wer_test = np.linspace(0, 1, 100)
    succ_pred = sigmoid(wer_test * model.coef_[0][0] + model.intercept_[0])

    return (model.coef_[0][0], model.intercept_[0], X, y, wer_test, succ_pred)



def main(args):
    with codecs.open(args.logfile, 'rb', 'UTF-8') as fh:
        data = json.load(fh)

    label_override = {}
    if args.label_override:
        with codecs.open(args.label_override, 'rb', 'UTF-8') as fh:
            label_override = yaml.load(fh)

    if args.per_system:
        split_data = split_data_by_key(data, 'sysName=', lambda call: call['sysName'])

    elif args.per_sys_letter:
        split_data = split_data_by_key(data, 'sysLetter=', lambda call: call['sysName'][:-1])

    elif args.per_sys_number:
        split_data = split_data_by_key(data, 'sysNumber=', lambda call: call['sysName'][-1])

    split_data['*'] = data
    labels = []
    used_colors = []
    plt.figure(1, figsize=(5, 5))
    avail_colors = list(matplotlib.colors.CSS4_COLORS.keys())
    random.shuffle(avail_colors)

    for label, data in sorted(split_data.items()):
        coef, bias, train_xs, y, test_xs, pred = build_logreg(data, args.success_type)
        print label, coef, bias
        color = avail_colors.pop()
        if label != '*':
            plt.scatter(train_xs, y, color=color, marker='.')
        plt.plot(test_xs, pred, color=color, linestyle=('--' if label == '*' else '-'))
        labels.append(label_override.get(label, label))
        used_colors.append(color)
    plt.xlabel('WER')
    plt.ylabel(label_override.get(args.success_type, args.success_type))
    plt.legend(labels, loc="upper right", bbox_to_anchor=(1, 0.9), fontsize='xx-small')
    plt.savefig(args.output, dpi=200)


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('--per-system', '-s', action='store_true', help='Split data by system ID')
    ap.add_argument('--per-sys-letter', '-l', action='store_true', help='Split data by system letter only')
    ap.add_argument('--per-sys-number', '-n', action='store_true', help='Split data by system number only')
    ap.add_argument('--success-type', '-t', type=str,
                    choices=['q1', 'constr_confirmed', 'entity_provided', 'all_info_requested',
                             'all_info_provided', 'constr_mentioned'],
                    help='What to use as "task success"')
    ap.add_argument('--label-override', '-o', type=str, help='key-value label override YAML')
    ap.add_argument('logfile', type=str, help='JSON with joint logs')
    ap.add_argument('output', type=str, help='output graph file')

    args = ap.parse_args()
    main(args)
