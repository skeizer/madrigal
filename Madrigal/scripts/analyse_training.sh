#!/bin/sh

if [ $# -ne 2 ]
then
  echo "USAGE: ./analyse_training.sh <logdir> <dimension>"
  exit
fi

logdir=$1
logdirNm=$logdir # older versions of gnuplot
#logdirNm="${logdir//_/\_}"
wdir=${0%/*}

dim=$2
#dim="all"
#dim="task"

# generate data files from log

echo "perl $MADRIGAL_HOME/scripts/get_curves.pl ${logdir}/${dim}_mdp_*.log"
perl $MADRIGAL_HOME/scripts/get_curves.pl ${logdir}/${dim}_mdp_*.log

# generate learning curves for average length

echo "perl $MADRIGAL_HOME/scripts/merge_learn_curves.pl l ${logdir}/${dim}_mdp_*.txt > ${logdir}/${dim}_merged_avgLen.txt"
perl $MADRIGAL_HOME/scripts/merge_learn_curves.pl l ${logdir}/${dim}_mdp_*.txt > ${logdir}/${dim}_merged_avgLen.txt

echo "gnuplot -persist -e \"filename='${logdir}/${dim}_merged_avgLen.txt';ttl='${logdirNm}'\" ${wdir}/plot_learning_curves.gpl"
gnuplot -persist -e "filename='${logdir}/${dim}_merged_avgLen.txt';ttl='${logdirNm}'" ${wdir}/plot_learning_curves.gpl > ${logdir}/${dim}_merged_avgLen.png

# generate learning curves for average reward

echo "perl $MADRIGAL_HOME/scripts/merge_learn_curves.pl r ${logdir}/${dim}_mdp_*.txt > ${logdir}/${dim}_merged_avgRew.txt"
perl $MADRIGAL_HOME/scripts/merge_learn_curves.pl r ${logdir}/${dim}_mdp_*.txt > ${logdir}/${dim}_merged_avgRew.txt

echo "gnuplot -persist -e \"filename='${logdir}/${dim}_merged_avgRew.txt';ttl='${logdirNm}'\" ${wdir}/plot_learning_curves.gpl"
gnuplot -persist -e "filename='${logdir}/${dim}_merged_avgRew.txt';ttl='${logdirNm}'" ${wdir}/plot_learning_curves.gpl > ${logdir}/${dim}_merged_avgRew.png

# generate learning curves for success rate

echo "perl $MADRIGAL_HOME/scripts/merge_learn_curves.pl s ${logdir}/${dim}_mdp_*.txt > ${logdir}/${dim}_merged_succRate.txt"
perl $MADRIGAL_HOME/scripts/merge_learn_curves.pl s ${logdir}/${dim}_mdp_*.txt > ${logdir}/${dim}_merged_succRate.txt

echo "gnuplot -persist -e \"filename='${logdir}/${dim}_merged_succRate.txt';ttl='${logdirNm}'\" ${wdir}/plot_learning_curves.gpl"
gnuplot -persist -e "filename='${logdir}/${dim}_merged_succRate.txt';ttl='${logdirNm}'" ${wdir}/plot_learning_curves.gpl > ${logdir}/${dim}_merged_succRate.png

