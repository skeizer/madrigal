#!/usr/bin/env python
# -"- coding: utf-8 -"-
#
# X.Liu propareTranscriptionAudioData.py
# 29.05.2018
#

from argparse import ArgumentParser
import pandas as pd
import os
import re
import datetime
import json
import sys
import codecs
import csv

# get file names list in current directory
def getFiles_old(indir):
    crntDirFiles = []
    for dirName, subdirList, fileList in os.walk(indir):
        #print dirName
        if(dirName == indir):
            crntDirFiles = fileList
            break
    print str(crntDirFiles)
    return crntDirFiles
    
def getFiles(indir):
    print os.listdir(indir)
    print indir    
    f = []
    for (dirpath, dirnames, filenames) in os.walk(indir):
        f.extend(filenames)
        break
    #print str(f)            
    return f
    
def get_audio_files(calls,audio_root_dir):

    def find_turn_audio(allfiles, turntime):
        for afile in allfiles:
            if turntime in afile:
            #if afile.find(turntime) != -1:
               #print "\n Found!"
               return afile
        # not found, return empty
        return ""
        
    all_files = []
    numAudioFound = 0
    numNoAudioFound = 0  
    numNoAudioFound_n = 0
    no_audio_turns = []
    no_audio_turns_n = []         
    for call in calls:
        dial_id = call['dialogueID']
        user_id = call['userID']
        if user_id.startswith('a_'):
            user_id = user_id[2:]
        #print "userid: " +    user_id
        #print "dial_id: " + dial_id
        
        audio_dir = audio_root_dir + "/User-" + user_id + "/" + dial_id
        #a_files = getFiles(audio_dir) # Turn_1_2018_05_17-18_36_12-034.opus
        a_files = os.listdir(audio_dir)
        for turn in call['dialogue']:
            pre_msg = turn['usr_turn']['transcription'] 
            if "HANGUP :" in pre_msg: # this check is added after the transcription, so no use for now though.
                continue # do not use its timestr to find the audio. 
                # Possible there is an audio corresponding to the timestr due to user click-end-button
                # issue or network transport issue (not sync..)
                
            turn_time = turn['usr_turn']['crntTurnTimeStr']
            aturnfile = find_turn_audio(a_files, turn_time)
            #print "aturnfile:" + aturnfile
            
            # possible: "transcription": "HANGUP : 'End dialogue' button clicked."
            if aturnfile == "":
                numNoAudioFound += 1
                no_audio_turns.append("Couldn't find audio file! Index: " + str(numNoAudioFound))
                no_audio_turns.append("    userID:" + user_id)
                no_audio_turns.append("    dialougeID:" + dial_id)
                no_audio_turns.append("    turn time:" + turn_time)
                no_audio_turns.append("    transcipt:" + turn['usr_turn']['transcription'] + "\n")
                if "HANGUP :" not in turn['usr_turn']['transcription']:
                    numNoAudioFound_n += 1
                    no_audio_turns_n.append("Couldn't find audio file! Index: " + str(numNoAudioFound_n))
                    no_audio_turns_n.append("    userID:" + user_id)
                    no_audio_turns_n.append("    dialougeID:" + dial_id)
                    no_audio_turns_n.append("    turn time:" + turn_time)
                    no_audio_turns_n.append("    transcipt:" + turn['usr_turn']['transcription'] + "\n")
                
            else:
                numAudioFound += 1
                all_files.append(audio_dir + "/" + aturnfile)

    print "Num of processed dialogues:" + str(len(calls))
    print "numNoAudioFound: " + str(numNoAudioFound)
    print "numNoAudioFoundNoTrans: " + str(numNoAudioFound_n)
    print "numAudioFound: " + str(numAudioFound)

    # save no_audio_turns to file
    outfile =  args.outdir + "/no_audio_turns_info.txt" # clicked StopButton plus Missing Audio files.
    with open(outfile, 'w') as f:
        f.write("\n".join(no_audio_turns))

    outfile =  args.outdir + "/no_audio_turns_info_notrans.txt" # emty Transcription if it is not Click End dialogue button, so this is the missing file only.
    with open(outfile, 'w') as f:
        f.write("\n".join(no_audio_turns_n))
        
    return all_files
               
def main(args):
    """ prepare audio files for transcriptions"""
    
    audio_web_root = "https://www.macs.hw.ac.uk/ilabarchive/madrigal/userAudio-FinalFull-21052018"
    
    with codecs.open(args.logfile, 'rb', 'UTF-8') as fh:
        data = json.load(fh)

    audio_files0 = get_audio_files(data,args.audio_root_dir)

    audio_files = [_file.replace(args.audio_root_dir, audio_web_root) for _file in audio_files0]

    # write list to csv file
    audio_files.insert(0, "url")
    
    outfile = args.outdir + "/audioFileNames.csv"
    
    with open(outfile, 'w') as f:
        f.write("\n".join(audio_files))
    
    #afile = open("audio_file.csv", "w")
    #writer = csv.writer(afile, delimiter = ",")
    #for list_ in audio_files:
    #     writer.writerow(list_)
    #afile.close()

def test(args):

    with codecs.open(args.logfile, 'rb', 'UTF-8') as fh:
        calls = json.load(fh)
    num_dm = 0;
    for call in calls:
        #dial_id = call['dialogueID']
        #user_id = call['userID']
        num_dm += 1
        
    print "Total num DM logs in merged logs: " + str(num_dm)
    
    
if __name__ == '__main__':

    ap = ArgumentParser()
    ap.add_argument('logfile', type=str, help='the joint json log file')
    ap.add_argument('audio_root_dir', type=str, help='audio root dir')
    ap.add_argument('outdir', type=str, help='output dir')
    
    args = ap.parse_args()
    main(args)
    #test(args)


