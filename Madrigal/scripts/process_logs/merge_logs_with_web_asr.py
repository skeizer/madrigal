#!/usr/bin/env python
# -"- coding: utf-8 -"-
#
# merge_logs_with_web_asr.py:
#
# to merge the merged CF-DM evaluation logs with the web log json data which contains 
# ASR full list of the hypotheses that were not logged anywhere else.
#        
# X.Liu, 01.06.2018
#
# Useage example:
#
#  python merge_logs_with_web_asr.py mergedLogsAll_CF_Trsc WebLogs_Json mergedLogsAll_CF_Trsc_Asr
#
#

import argparse
import json
import os
import sys
import codecs
import itertools
import pandas as pd
from collections import Counter
from difflib import SequenceMatcher


def get_full_asr_hyps(weblog, turntime):
    fullasr = {}
    #print weblog["dialogueID"]

    for turn in weblog["turns"]:
        #print str(turn)
        #print turn["userTurn"]
        #break
        #if turn["stopButtonClicked"] == "true":
        #   continue
        if "userTurn" not in turn:
            continue
            
        if turn["userTurn"]["crntTurnTimeStr"] != turntime:
            continue
        fullasr["asr_full_hyps"] = turn["userTurn"]["asrhyps"]
        fullasr["top_hyp"] = turn["topHyp"]
        return fullasr
    # not found the turn, return empty string
    return ""
    
def merge_dmlogs_with_web_asr(args, dmfile):

    dmfile_uri = args.dm_log_dir + "/" + dmfile
    m_logs = json.load(codecs.open(dmfile_uri, 'r', 'UTF-8'))
    merging_msg = []
    msg = "\nLog File:" + dmfile_uri
    merging_msg.append(msg)
    print msg
    
    for dial in m_logs: # json array
        dial_id = dial["dialogueID"]
        user_id = dial["userID"]
        if user_id.startswith("a_"): # DM added "a_" for userID starting with "0"
            user_id = user_id[2:]
            
        # find the web log json file
        weblogfile = args.web_log_dir + "/" + user_id + "/" + dial_id + ".json"
        
        #if(not os.path.exists(weblogfile)):
        if(not os.path.isfile(weblogfile)): # same as os.path.exists
            msg = "\n    WARNING: web log file not exist!" + weblogfile
            merging_msg.append(msg)
            print msg
            continue
        weblog = json.load(codecs.open(weblogfile, 'r', 'UTF-8'))
        
        for turn in dial["dialogue"]:
            if "HANGUP :" in turn["usr_turn"]["transcription"]:
                continue # no need to look for full asr since it is clicking the button
            turn_time = turn["usr_turn"]["crntTurnTimeStr"]
            fullasr = get_full_asr_hyps(weblog, turn_time)
            if fullasr == "":
                msg = "    WARNING: Could not find full ASR from web log!"
                merging_msg.append(msg)
                print msg

                msg = "    weblog:" + weblogfile
                merging_msg.append(msg)
                print msg

                msg = "    turn time:" + turn_time
                merging_msg.append(msg)
                print msg
                
                continue
            # merge user_turn with fullasr which has key: topHyp and asrhps
            merged_usrturn = {key: value for (key, value) in (turn["usr_turn"].items() + fullasr.items())}
            turn["usr_turn"] = merged_usrturn
            
    # save changes
    if(not os.path.exists(args.output_dir)):
        os.mkdir(args.output_dir)

    # dmfile: mergedLogsWithTasks_Transc_A1.json
    filep1 = dmfile[:dmfile.rfind('_')]
    filep2 = dmfile[dmfile.rfind('_')+1:dmfile.rfind('.')] # e.g. A1
    
    outfile = args.output_dir + "/" + filep1 + "_FullAsr_" + filep2 + ".json" 
   
    if outfile.find('/') != -1:
        destdir = outfile[:outfile.rfind('/')]
        if(not os.path.exists(destdir)):
            os.mkdir(destdir)
    
    with codecs.open(outfile, 'w', 'UTF-8') as fh:
        json_text = json.dumps(m_logs, ensure_ascii=False, indent=4, sort_keys=True)
        fh.write(json_text)
        fh.close()
        
    return merging_msg
    
# get file names list in current directory
def get_files(indir):
    crntDirFiles = []
    for dirName, subdirList, fileList in os.walk(indir):
        #print dirName
        if(dirName == indir):
            crntDirFiles = fileList
            break

    return crntDirFiles
                                            
def main(argv):
    # See the top of the file for usage example.
    parser = argparse.ArgumentParser(description='merge DM log file with web log full ASR')
    parser.add_argument('dm_log_dir', type=str, help='dir for dm logs merged with CF eval reports, and/or transc reports')
    parser.add_argument('web_log_dir', type=str, help='preprocessed web json log root dir')
    parser.add_argument('output_dir', type=str, help='results output dir')
    
    args = parser.parse_args()

    dmfiles = get_files(args.dm_log_dir)
    all_msg = []
    for dmfile in dmfiles:
        dmfilefull = args.dm_log_dir + "/" + dmfile
        msg = merge_dmlogs_with_web_asr(args, dmfile)
        all_msg += msg  # merge two lists or use all_msg.extend(msg)

    output_dir = "Tmp_Process_Msg"
    if(not os.path.exists(output_dir)):
        os.mkdir(output_dir)
        
    outfile_uri = output_dir + '/Merge_Msg_DM_With_WebAsr.txt'
    outfile = open(outfile_uri, 'w')
    outfile.write("\n".join(all_msg))
    outfile.write("\n")
    outfile.close()
    
    print "\n\nTerminal output messages saved in: " + outfile_uri + "\n\n"
        
if __name__ == "__main__":
    main(sys.argv[1:])
    
    


