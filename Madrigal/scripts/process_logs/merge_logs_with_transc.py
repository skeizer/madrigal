#!/usr/bin/env python
# -"- coding: utf-8 -"-
#
# merge_logs_with_transc.py:
#
# to merge the merged CF-DM evaluation logs with the ASR transcritpion
#
# X.Liu, 01.06.2018
#         ver2: 13.06.2018, first use process_asr_cf_results.py to generate top_voted and disputed
#               transcription files, then manually resolved disputed ones, merge both to one file
#               as the transcription input to this script.
                
# Useage example:
#
# python merge_logs_with_transc.py mergedLogsAll_CF Results_Transcription_Resolved/transc_top_voted_with_disputeresolved.json mergedLogsAll_CF_Trsc
#
#
 
import argparse
import os
import re
import json
import sys
import codecs
import itertools
import pandas as pd
from collections import Counter
from difflib import SequenceMatcher

to_remove_dm_ids = [
'Dial_2018-05-17_17-00-58_583885', 'Dial_2018-05-17_17-06-20_394387', 'Dial_2018-05-17_17-03-27_592169',
'Dial_2018-05-17_17-11-12_278894', 'Dial_2018-05-17_17-12-15_160131',
'Dial_2018-05-17_11-41-58_820647',
'Dial_2018-05-17_18-03-24_829288', 'Dial_2018-05-18_01-20-21_587395','Dial_2018-05-17_19-17-43_103846',
'Dial_2018-05-17_15-58-46_490685', 'Dial_2018-05-17_18-03-32_546869', 'Dial_2018-05-18_02-03-25_808409',
'Dial_2018-05-18_03-57-03_614818'
]
                            
def merge_dmlogs_asr_transcription(args, dmfile,cf_trans):
    '''
    This is to loop over the merged DM logs, then find the right transcription in CF csv
    and then update the DM logs, since we want to update merged DM logs of different 
    system variants: A, A1, A2, B, B1, B2, C, C1, C2, D, D1, D2, S1, S2.
    '''
    # dmfile: e.g. mergedLogsWithTasks_A1.json
    dmfile_uri = args.dm_log_dir + "/" + dmfile
    m_logs = json.load(codecs.open(dmfile_uri, 'r', 'UTF-8'))

    msg =  "\nNum of transcription from CF:" + str(len(cf_trans)) + "\n"
    merging_msg = []
    print msg

    msg = "\nLog File:" + dmfile_uri
    merging_msg.append(msg)
    print msg
    
    num_utt = 0
    num_missed = 0
    
    # url_prefix = "https://www.macs.hw.ac.uk/ilabarchive/madrigal/userAudio-FinalFull-21052018/User-"
    for dial in m_logs: # json array
        user_id = dial['userID']
        dial_id = dial['dialogueID']
        if dial_id in to_remove_dm_ids: 
            # shouldn't be the case as it should already be removed in the merged DM logs.
            msg = "Removed dial_id. Not need to update!" + dial_id
            merging_msg.append(msg)
            print msg
            continue
            
        for turn in dial["dialogue"]:
            if "HANGUP :" in turn["usr_turn"]['transcription']:
                continue  # user clicked End dialogue button, no spoken utterance.
            turn_time = turn["usr_turn"]['crntTurnTimeStr']
            # the CF transc url: url_prefix/user_id/dial_id/Turn_X_turn_time.opus
            updated = False
            for url, utt in cf_trans.iteritems(): # key-value : url-utt
                if dial_id in url and turn_time in url:
                    turn["usr_turn"]['transcription'] = utt # update the transcription
                    turn["usr_turn"]["audio_file"] = url
                    updated = True
                    num_utt += 1
                    break
            if not updated:
                num_missed += 1
                msg = "\n    WARNING: Couldn't find the transcription from CF reports!"
                merging_msg.append(msg)
                print msg
                
                msg = "    user ID:" + user_id
                merging_msg.append(msg)
                print msg
                
                msg = "    dial ID:" + dial_id
                merging_msg.append(msg)
                print msg
                
                msg = "    turn time:" + turn_time
                merging_msg.append(msg)
                print msg

    msg = "\nSummary Info for : " + dmfile_uri
    merging_msg.append(msg)
                
    msg = "\n    Total num of transcription merged:" + str(num_utt) 
    merging_msg.append(msg)
    print msg
    msg = "    Total num of missed trans/aduio:" + str(num_missed) 
    merging_msg.append(msg)
    print msg
    
    # save the changes:
    if(not os.path.exists(args.output_dir)):
        os.mkdir(args.output_dir)

    # dmfile: mergedLogsWithTasks_A1.json
    filep1 = dmfile[:dmfile.rfind('_')]
    filep2 = dmfile[dmfile.rfind('_')+1:dmfile.rfind('.')] # e.g. A1
    
    outfile = args.output_dir + "/" + filep1 + "_Transc_" + filep2 + ".json" 
    with codecs.open(outfile, 'w', 'UTF-8') as fh:
        json_text = json.dumps(m_logs, ensure_ascii=False, indent=4, sort_keys=True)
        fh.write(json_text)
        fh.close()
        
    return merging_msg
    
# get file names list in current directory
def get_files(indir):
    crntDirFiles = []
    for dirName, subdirList, fileList in os.walk(indir):
        #print dirName
        if(dirName == indir):
            crntDirFiles = fileList
            break

    return crntDirFiles

def main(argv):
    # See the top of the file for usage example.
    parser = argparse.ArgumentParser(description='merge DM log file with CF ASR transcription')
    parser.add_argument('dm_log_dir', type=str, help='dir for dm logs merged with CF eval reports')
    parser.add_argument('cf_transc_file', type=str, help='all resolved transcription json file')
    parser.add_argument('output_dir', type=str, help='results output dir')
    
    args = parser.parse_args()
    
    cf_trans = json.load(codecs.open(args.cf_transc_file, 'r', 'UTF-8'))
    dmfiles = get_files(args.dm_log_dir)
    all_msg = []
    for dmfile in dmfiles:
        dmfilefull = args.dm_log_dir + "/" + dmfile
        msg = merge_dmlogs_asr_transcription(args, dmfile, cf_trans)
        all_msg += msg # merge two lists or use all_msg.extend(msg)
 
    output_dir = "Tmp_Process_Msg"
    if(not os.path.exists(output_dir)):
        os.mkdir(output_dir)
        
    outfile_uri = output_dir + '/Merge_Msg_DM_With_Transc.txt'
    outfile = open(outfile_uri, 'w')
    outfile.write("\n".join(all_msg))
    outfile.write("\n")
    outfile.close()
   
    print "\n\nTerminal output messages saved in: " + outfile_uri + "\n\n"
    
if __name__ == "__main__":
    main(sys.argv[1:])
    
    


