#!/usr/bin/env python

import re
import codecs
import json
import pandas as pd
from collections import defaultdict
from argparse import ArgumentParser


def main(args):
    data = pd.read_csv(args.input_file, encoding='UTF-8')

    transcriptions = {}
    for _, inst in data.iterrows():
        trans_id = re.sub(r'^https:\/\/.*\/madrigal\/', '', inst.url)
        transcriptions[trans_id] = transcriptions.get(trans_id, []) + [inst.transcribed_text]

    uniq_trans = {}
    disputed_trans = {}
    for url, trans in transcriptions.iteritems():
        occs = defaultdict(int)
        for tran in trans:
            occs[tran] += 1
        m = max(occs.values()); val = [i[0] for i in occs.iteritems() if i[1] == m]
        if len(val) == 1:
            uniq_trans[url] = val[0]
        else:
            disputed_trans[url] = trans

    with codecs.open(args.resolved_file, 'wb', 'UTF-8') as fh:
        fh.write(json.dumps(uniq_trans, ensure_ascii=False, indent=4, sort_keys=True))
    with codecs.open(args.disputed_file, 'wb', 'UTF-8') as fh:
        fh.write(json.dumps(disputed_trans, ensure_ascii=False, indent=4, sort_keys=True))


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('input_file', type=str, help='Input CSV from CrowdFlower')
    ap.add_argument('resolved_file', type=str,
                    help='Output JSON with successfuly resolved transcriptions (majority vote works)')
    ap.add_argument('disputed_file', type=str,
                    help='Output JSON with no clear majority among workers -- to resolve manualy')
    args = ap.parse_args()

    main(args)

