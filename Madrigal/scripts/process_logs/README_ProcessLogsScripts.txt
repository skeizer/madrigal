
 Scripts for preprocessing various logs, usage example.
 X.Liu @ HWU, 11 June 2018
  
 ##########################################################################
   All scripts can be re-run!
   but strongly suggest to copy existing NLU_Cache/NLU_Logs_Cache_Ext.json 
   from HW MACS server space: Madrigal/FigureEight-Evaluation/ProcessLogs/
   for running get_nlu_semantics.py to reduce number of hits for LUIS
 ###########################################################################

1. Generate NLU Json file from original DM logs as NLU Cache:
python create_nlu_cache_from_eval_logs.py ../../FinalResults-21May2018-Unzip/DMLogs NLU_Cache

2. Generate Json files in ./WebLogs_Json
python preprocessWebLogs.py ../../FinalResults-21May2018-Unzip/webLogs/logs WebLogs_Json
  (It contains a method validateTokenCsv() which can be run manually and separately)
  
3. merge original DM logs with CF evaluation info, saved in ./mergedLogsAll_CF
python merge_logs_dm_with_cf.py --tasks CamInfo_Resources/CamInfo_tasks.txt ../../FinalResults-21May2018-Unzip/CF-Reports/f1268344.csv ../../FinalResults-21May2018-Unzip/Tokens/tokens-FullEval-Valiated2.csv ../../FinalResults-21May2018-Unzip/DMLogs mergedLogsAll_CF/mergedLogsWithTasks

4. merge with Transcriptions, saved in mergedLogsAll_CF_Trsc

python merge_logs_with_transc.py mergedLogsAll_CF ../../Transcriptions_CF/TranscForFinalRun-FullMerged/f1271784_f1273086_merged.csv mergedLogsAll_CF_Trsc

5. merge with ASR full list of hypotheses, saved in mergedLogsAll_CF_Trsc_Asr
 python merge_logs_with_web_asr.py mergedLogsAll_CF_Trsc WebLogs_Json mergedLogsAll_CF_Trsc_Asr
 

6. get NLU semantics for the transcriptions: query NLU Cache file or real LUIS app and map to 
   ontology, saved in ./mergedLogsAll_CF_Trsc_Asr_Sem
   
  python get_nlu_semantics.py mergedLogsAll_CF_Trsc_Asr CamInfo_Resources/CamInfo_ontology.json CamInfo_Resources/CamInfo_lexicon.json NLU_Cache/NLU_Logs_Cache.json mergedLogsAll_CF_Trsc_Asr_Sem
    # 
    # It will check there is file NLU_Cache/NLU_Logs_Cache_Ext.json 
    # if exists, use it. Or you can just specify NLU_Logs_Cache_Ext.json 

 =============================================
 NB:
 
 1) prepareTranscriptionAudioData.py was used to generate audio files list for CF transcriptops.
 
 2) CF transcriptions have at least 3 Judgements for each audio. When merging with DM logs,
    top voted one is used. if the vote count is the same, choose the most similar one.
    
 3) The transcriptions contain the required annotations such (hum), (sil), (noise) etc.
    when calculating the semantics, (hum) is replaced with hum, all others are just removed.
    
================================================

  


