#
# This is to post-process the web php text logs to generate json format
# of dialogue info, mainly to recall the full list ASR hypotheses 
# which should but haven't logged in the demosys_xxx.json files.
# 
# The intention is for checking the effects of alternative ASR hypotheses 
# to the semantic parsing.
#
# There is a method here validateTokenCsv() which can be used to validate the token csv file
# against the CF report. It just adds more info from CF reports to it.
#
#  X. Liu @ HWU, 23.05.2018
#
# Useage example:
#
# python preprocessWebLogs.py ../../FinalResults-21May2018-Unzip/webLogs/logs WebLogs_Json
#
import os.path
import sys, getopt
import argparse
import json
import pandas as pd
#import datetime
#import time
#from decimal import Decimal
 
def getDirectories(indir):
    dirs = []
    for dirName, subdirList, fileList in os.walk(indir):
        #print dirName
        if(dirName != indir):
            dirs.append(dirName)
    return dirs

def readFile2List(infile):
    lines = []
    with open(infile) as file:
        for line in file:
            line = line.strip() #or strip the '\n'
            lines.append(line)
    return lines

def saveJson2File(dirNameInCrntDir, fileName, jdata):
    #
    path = os.getcwd()
    dirname = os.path.join(path, dirNameInCrntDir)
    if(not os.path.exists(dirname)):
        os.mkdir(dirname)

    fileName = fileName + ".json"
    with open(os.path.join(dirname, fileName), 'w') as f:
        f.write(json.dumps(jdata, indent=4, sort_keys=True))

def getColumnFromCsvFile(csvfile, colName):
    
    df = pd.read_csv(csvfile, encoding='UTF-8')   # use pandas
    col = []
    for row_index, row in df.iterrows():
        #print row_index, row['auto_token']    
        col.append(row[colName])

    return col

def validateTokenCsv():
    origcsv = "tokens-FullEval-21May2018-EvalOnly.csv"
    df = pd.read_csv(origcsv, encoding='UTF-8')
    cf_csv = "f1268344.csv"
    cf_col_name = "auto_token"
    cf_tokens = getColumnFromCsvFile(cf_csv, cf_col_name)
    print "num of cf_tokens:" + str(len(cf_tokens))
    
    num = 0
    for row_index, row in df.iterrows():
        if(row['token'] in cf_tokens):
            df.at[row_index, 'validated'] = "Yes"
            num += 1
            
    print "Num of Validated:" + str(num)
             
    # index=False: do not store the preceding indices of each row of the DataFrame object.
    dest_file = "tokens-FullEval-Valiated.csv"
    df.to_csv(dest_file, sep=',', encoding='utf-8', index=False)


def findTopHypothesis(jUsrTurn):
    # from ASR list to get the one with highest confidence score
    # and return a json str with utt and conf score.
    asr = jUsrTurn["asrhyps"]
    #maxConf = Decimal(asr[0]["conf"])
    maxConf = asr[0]["conf"]
    maxUtt = asr[0]["utt"]
    
    for item in asr:
       utt = item["utt"]
       conf = item["conf"]
       if(conf > maxConf) :
           maxConf = conf
           maxUtt = utt
    res = {"utt":maxUtt, "conf":maxConf}
    #print res
    return res
# 
def processOneLogFile(fileUri, destBaseDir):
    if(not os.path.exists(destBaseDir)):
        os.mkdir(destBaseDir)

    dialStartPref = "A dialogue started.";
    lines = readFile2List(fileUri)
    i = 0;
    # find the start point of a dialogue
    while i < len(lines) and dialStartPref not in lines[i]:
        i += 1
    if(i == len(lines)-1):
        print "No dialogue in this logfile!"
        return
        
    # each user has one web log file: userID/logfile.txt
    # which contains all the dialogues from that user.
    userTurnPref="Start speaking. User:"
    sysTurnPref="Final to user:"
    #stopBtnClickPref = "Event: StopButtonClicked, == End dialogue button"
    stopBtnClickPref = "StopButtonClicked"
    numDial = 0
    numClickStop = 0
    
    dialInfo = {"dialogues":[]}
    while i < len(lines):
        line = lines[i]
        dialJson = {}
        # A dialogue started. userIP=73.xxx, userID=xxx, dialogueID=Dial_xxx
        userIP=line[line.index("userIP=")+7:line.index("userID=")-2]
        userID=line[line.index("userID=")+7:line.index("dialogueID=")-2]
        dialogueID=line[line.index("dialogueID=")+11:]
        dialJson['userIP'] = userIP
        dialJson['userID'] = userID
        dialJson['dialogueID'] = dialogueID
        dialJson['turns'] = []
        numDial += 1
        #print "Processing dialogue:" + str(numDial) + ":" + dialogueID
        numTurn = 0
        ip=""
        port=""
        sysNum=""
        i += 1
        while i < len(lines) and dialStartPref not in lines[i]:
            line = lines[i]
            aturn = {}
            aturn["stopButtonClicked"] = "false"
            #while i < len(lines) and dialStartPref not in lines[i] and sysTurnPref not in lines[i]:
            while i < len(lines) and dialStartPref not in lines[i]:

                line = lines[i]
                # connected:137.195.27.135, port:8083, sysNum:1
                if("connected:" in line):
                    ip = line[line.index("connected:")+10:line.index("port:")-2]
                    port = line[line.index("port:")+5:line.index("port:")+9]
                    sysNum = line[line.index("sysNum:")+7:]
                   
                if(stopBtnClickPref in line):  # StopButtonClicked
                    aturn["stopButtonClicked"] = "true"
                    numClickStop += 1
                    # the next line is the message sent to DM as the user turn info
                    i += 1
                    line = lines[i]
                    userTurn = line[line.index("{"):]
                    #print userTurn
                    ustr = json.loads(userTurn)
                    aturn["userTurn"] = ustr
                    
                   
                if(sysTurnPref in line):
                    sysTurn = line[line.index("{"):]
                    #print sysTurn
                    aturn["sysTurn"] = json.loads(sysTurn)
                    if aturn["stopButtonClicked"] == "true":
                        # there is another "Final to user:" after it, which is for "Event: RELOAD", need to skip it.
                        # otherwise its sysTurn info will overwrite the real sysTurn info.
                        # loop next lines to find the next dialogue start point.
                        while i < len(lines) and dialStartPref not in lines[i]:
                            i += 1
                        break # now lines[i] contains dialStartPref, break to save turn info, continue for next Dialogue.
                            
                if(userTurnPref in line):    
                    userTurn = line[line.index("{"):]
                    #print userTurn
                    ustr = json.loads(userTurn)
                    aturn["userTurn"] = ustr
                    topHyp = findTopHypothesis(ustr)
                    aturn["topHyp"] = topHyp
                    numTurn += 1
                    i += 1
                    break
                i += 1
                   
            dialJson["turns"].append(aturn)
           
        dialJson["serverIP"] = ip
        dialJson["serverPort"] = port
        dialJson["sysNum"] = sysNum
                   
        # save a dialogue to a file. userID as subDir name, 
        #destBaseDir = "./weblog-json"
        destDir = destBaseDir + "/" + userID
        saveJson2File(destDir, dialogueID, dialJson)
        dialInfo1 ={}
        dialInfo1["dialogueID"] = dialogueID
        dialInfo1["numOfTurns"] = numTurn
        dialInfo["dialogues"].append(dialInfo1)
    
    dialInfo["numDial"] = numDial
    dialInfo["numClickStop"] = numClickStop
    dialInfo["user"] = fileUri
    
    #print "    numDial=" + str(numDial)
    #print "    numClickStop=" + str(numClickStop)
    #print "    ------"
    #print dialInfo
    
    return dialInfo
    
    # end of web log file for the user.
    
def singlTest():
    logdir = "./logs/User-0e99086f-6605-4129-84f9-3eff8b27e1b2"
    logfile = "logfile.txt"
    destBaseDir = "./WebLogs-Json1"
    
    processOneLogFile(logdir + "/" + logfile, destBaseDir)
    
    print "==== finished ===="
    
def processAll(srcBaseDir, destBaseDir):
    #srcBaseDir = "./WebLogs-OrigText"
    logfile = "logfile.txt"
    #destBaseDir = "./WebLogs-Json"
    users =  getDirectories(srcBaseDir)
    if len(users) < 1:
       print "No files found in " + srcBaseDir
       return
       
    allInfo = {"usersDialInfo":[]}
    allInfo["numOfLoggedUsers"] = len(users)
    totalDials = 0
    totalTurns = 0
    totalClickStop = 0
    zeroTurnDials = 0
    loadPageOnlyUsers = 0
    for userDir in users:
        # userDir: ./WebLogs-OrigText/User-7d1e95c1-6bf6-4752-93a3-53c9db6587cf
        print "processing for user:" + userDir
        info = processOneLogFile(userDir + "/" + logfile, destBaseDir)
        allInfo["usersDialInfo"].append(info)
        if info["numDial"] == 0:
            loadPageOnlyUsers += 1
        else:
            totalDials += info["numDial"]
        totalClickStop += info["numClickStop"]

        for item in info["dialogues"]:
            if item["numOfTurns"] == 0:
                zeroTurnDials += 1
            else:
                totalTurns += item["numOfTurns"]
        
    saveJson2File("./Tmp_Process_Msg", "WebLogs-AllUserDialInfo", allInfo)

    nonZeroTurnDials = totalDials-zeroTurnDials
    aveTurnsPerDial = totalTurns *1.0 / nonZeroTurnDials
    
    print "  ---------Summary Info ------------"
    print "totalWebLoggedUsers=" + str(len(users)) # all the logged users
    print "loadPageOnlyUsers=" + str(loadPageOnlyUsers)  # the users only loaded page, not start a dialogue
    print "totalDials=" + str(totalDials) # all num of dialogue including ones with zero turn
    print "zeroTurnDials=" + str(zeroTurnDials)  # After connected, sys greeting, reloaded page
    print "nonZeroTurnDials=" + str(nonZeroTurnDials)            
    print "totalTurns=" + str(totalTurns)
    print "totalClickStop=" + str(totalClickStop)
    print "aveTurnsPerDial=" + str(aveTurnsPerDial)
    
    summ = {}
    summ["totalWebLoggedUsers"] = len(users)
    summ["loadPageOnlyUsers"] = loadPageOnlyUsers
    summ["totalDials"] = totalDials
    summ["zeroTurnDials"] = zeroTurnDials
    summ["nonZeroTurnDials"] = nonZeroTurnDials
    summ["totalTurns"] = totalTurns
    summ["totalClickStop"] = totalClickStop
    summ["aveTurnsPerDial"] = aveTurnsPerDial

    saveJson2File("./Tmp_Process_Msg", "WebLogs-AllDialogueSummaryInfo", summ)
    
def main(argv):
    parser = argparse.ArgumentParser(description='process web text logs to json format.')

    parser.add_argument('weblogs_dir', type=str, help='original web text logs dir')
    parser.add_argument('output_dir', type=str, help='results output dir')

    args = parser.parse_args()

    if(not os.path.exists(args.output_dir)):
        os.mkdir(args.output_dir)
    
    # validateTokenCsv() # need to manually provide the dir and file info

    processAll(args.weblogs_dir, args.output_dir)

       
if __name__ == "__main__":
    main(sys.argv[1:])
    
    
    
    
    
    
    
