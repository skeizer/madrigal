#
# This is to post-process NLU Logs to create NLU Cashe files for calculating NLU semantics error rate
# 
#  X. Liu @ HWU, 08.06.2018
#
# Useage example:
#
# python create_nlu_cache_from_eval_logs.py ../../FinalResults-21May2018-Unzip/DMLogs NLU_Cache
#
import os.path
import sys, getopt
import argparse
import json
import pandas as pd
import codecs

def readFile2List(infile):
    lines = []
    with open(infile) as file:
        for line in file:
            line = line.strip() #or strip the '\n'
            lines.append(line)
    return lines
    
def process_all_nlu(args):
    # e.g.     args.dmlogs_dir = "../../FinalResults-21May2018-Unzip/DMLogs"
    dmlogs_dir = args.dmlogs_dir
    output_dir = args.output_dir
    all_subdirs = [d for d in os.listdir(dmlogs_dir) if os.path.isdir(os.path.join(dmlogs_dir, d))]
    nlu_all = {}
    for adir in os.listdir(dmlogs_dir):
        nlufiles = [f for f in os.listdir(os.path.join(dmlogs_dir, adir)) if f.startswith("nlu_") and not f.endswith(".lck") ]    
        for afile in nlufiles:
            nlufile = os.path.join(dmlogs_dir, adir, afile)
            print nlufile
            print "----"
            nluout = process_one_nlu_logfile(nlufile, output_dir)
            nlu_all.update(nluout)
    outfile = args.output_dir + "/NLU_Logs_Cache.json"
    with codecs.open(outfile, 'w', 'UTF-8') as fh:
        json_text = json.dumps(nlu_all, ensure_ascii=False, indent=4, sort_keys=True)
        fh.write(json_text)
        fh.close()
            
        
def process_one_nlu_logfile(fileuri, output_dir):
    lines = readFile2List(fileuri)
    i = 0
    utt_sem_all = {}
    num_utt = 0
    utt_set = set()
    utt_list = []
    while i < len(lines):
        while i < len(lines) and "utterance:" not in lines[i]:
            i += 1
        if i >= len(lines):
            break
            
        #print "utt line:" + lines[i]
        utt = (lines[i][lines[i].index(":") +1:]).strip()
        i = i + 3 # to just after line: "LUIS output:"
        luis = ""
        while i < len(lines) and "Recognised user act:" not in lines[i] and "user turn:" not in lines[i]:
            luis += " " + lines[i]
            i += 1
        #print "luis:" + luis
        luis_j = json.loads(luis)
        user_sem = "None"  # user turn: UsrTurn> PERCEPTION
        user_sem_score = 0
        if "Recognised user act:" in lines[i]:
            # Recognised user act: setQuestion([addr]) [0.972]
            user_sem = lines[i][lines[i].index(':')+1:lines[i].index(')')+1]
            user_sem_score = lines[i][lines[i].rfind('[')+1:len(lines[i])-1]
        utt_sem = {}
        #utt_sem['utterance'] = utt
        utt_sem['luis_out'] = luis_j
        utt_sem['user_sem'] = user_sem
        utt_sem['user_sem_score'] = user_sem_score
        utt_sem_all[utt] = utt_sem
        num_utt += 1
        utt_set.add(utt)
        utt_list.append(utt)
        
    print "num_utt:" + str(num_utt)
    print "num set size:" + str(len(utt_set))

    outfile = open(output_dir + '/tmp_utt_list_all.txt', 'a')
    outfile.write("\n".join(utt_list))
    outfile.write("\n")
    outfile.close()
    
    outfile = open(output_dir + '/tmp_utt_set_all.txt', 'a')
    outfile.write("\n".join(utt_set))
    outfile.write("\n")
    outfile.close()

    return utt_sem_all

def main(argv):
    parser = argparse.ArgumentParser(description='process web text logs to json format.')

    parser.add_argument('dmlogs_dir', type=str, help='original dm logs logs dir')
    parser.add_argument('output_dir', type=str, help='results output dir')

    args = parser.parse_args()

    if(not os.path.exists(args.output_dir)):
        os.mkdir(args.output_dir)

    process_all_nlu(args)
      
if __name__ == "__main__":
    main(sys.argv[1:])
    
    
    
    
    
    
    
