#!/usr/bin/env python
# -"- coding: utf-8 -"-
#
# X.Liu modified Ondrej's pilot-run codes to merge all related logs based on the new log formats for the final run.
# 29.05.2018
#
# Usage example:
#
# python merge_logs_dm_with_cf.py --tasks CamInfo_Resources/CamInfo_tasks.txt ../../FinalResults-21May2018-Unzip/CF-Reports/f1268344.csv ../../FinalResults-21May2018-Unzip/Tokens/tokens-FullEval-Valiated2.csv ../../FinalResults-21May2018-Unzip/DMLogs mergedLogsAll_CF/mergedLogsWithTasks
#

from argparse import ArgumentParser
import pandas as pd
import os
import re
import datetime
import json
import sys
import codecs
import time

# To remove user's dialogues identified as invalid. 
#
# e.g. all dialogues from the userID: ff04addb-edb6-4205-bce2-4813840788cd
#      (IP: 104.129.18.9, CF-WorkerID: 44491851).
# Recorded dialogues for him:
# 'Dial_2018-05-17_17-00-58_583885',  'Dial_2018-05-17_17-06-20_394387',  'Dial_2018-05-17_17-08-42_684856',
# 'Dial_2018-05-17_17-03-27_592169',  'Dial_2018-05-17_17-07-31_852621',  'Dial_2018-05-17_17-11-12_278894',
# 'Dial_2018-05-17_17-05-03_60189',   'Dial_2018-05-17_17-08-07_949826',  'Dial_2018-05-17_17-12-15_160131',
#
# corresponding Tokens (so just remove the dialogues with CF tokens)
# [8495, 8709, NoToken, 2005, NoToken, 8653, NoToken, NoToken, 5157]
#
# X Liu's initial random test 
# 647d6eb7-4473-4f36-bb68-cf4ffe118afa/Dial_2018-05-17_11-41-58_820647
# Token: 8843
#
# DM logs inconsistent with web logs and audio logs:
#
# e188b282-fae6-4598-86be-f81d031978d1/Dial_2018-05-17_18-03-24_829288
# b2b54866-a352-4177-ba0a-f401aba86dbb/Dial_2018-05-18_01-20-21_587395
# 0e99086f-6605-4129-84f9-3eff8b27e1b2/Dial_2018-05-17_19-17-43_103846
# 325dfdcd-d49d-4dbe-a7bc-bd7c19e734ad/Dial_2018-05-17_15-58-46_490685
# 07e11690-5141-49f7-ab0d-d15ab99dc880/Dial_2018-05-17_18-03-32_546869
# 82f82eef-27d9-4205-b5cf-e3a3377000cc/Dial_2018-05-18_02-03-25_808409
# 82f82eef-27d9-4205-b5cf-e3a3377000cc/Dial_2018-05-18_03-57-03_614818
#
to_remove_dm_ids = [
'Dial_2018-05-17_17-00-58_583885', 'Dial_2018-05-17_17-06-20_394387', 'Dial_2018-05-17_17-03-27_592169',
'Dial_2018-05-17_17-11-12_278894', 'Dial_2018-05-17_17-12-15_160131',
'Dial_2018-05-17_11-41-58_820647',
'Dial_2018-05-17_18-03-24_829288', 'Dial_2018-05-18_01-20-21_587395','Dial_2018-05-17_19-17-43_103846',
'Dial_2018-05-17_15-58-46_490685', 'Dial_2018-05-17_18-03-32_546869', 'Dial_2018-05-18_02-03-25_808409',
'Dial_2018-05-18_03-57-03_614818'
]

m1 = {"8080":"logs-A-1","8081":"logs-A-2","8082":"logs-A-3","8083":"logs-A-4"} 
m2 = {"8090":"logs-B-1","8091":"logs-B-2","8092":"logs-B-3","8093":"logs-B-4"}
m3 = {"8060":"logs-C-1","8061":"logs-C-2","8062":"logs-C-3","8063":"logs-C-4"} 
m4 = {"8070":"logs-D-1","8071":"logs-D-2","8072":"logs-D-3","8073":"logs-D-4"}  
port_folder_map = m1.copy()
port_folder_map.update(m2)
port_folder_map.update(m3)
port_folder_map.update(m4)

def find_dm_log(tokens_df, token):
    
    for row_index, row in tokens_df.iterrows():
        if(row['token'] == token):
            port = str(row['sys_port']) 
            if port.endswith('.0'): # port 8080 from csv may be 8080.0
                port = port[:-2]
        
            if row["validated"] != "Yes":
                raise Exception('Token not validated!: %s' % token)
            if row['dialogue_id'] in to_remove_dm_ids:
                print "Removed dialogueID: " + row['dialogue_id']
                dm_file = "REMOVE"
            else:
                dm_file = port_folder_map[port] +"/demosys_" +  row['dialogue_id'] + ".json"
            return dm_file
    raise Exception('Token not found in the tokens csv file!: %s' % token)        

def find_sys_variant(tokens_df, token):
    # Level1: A1, A2, B1, B2, C1, C2, D1, D2
    # Level2: A,B,C,D
    # Level3: S1 (all A1, B1,C1,D1), S2 (all A2,B2,C2,D2)
    
    for row_index, row in tokens_df.iterrows():
        if(row['token'] == token):
            return row['sys_name'][-2:]  # e.g. sysA1, return A1
    
    raise Exception('Token not found in find_sys_variant!: %s' % token)
    
def read_task_descs(task_descs_file):
    """Read task descriptions from the Caminfo_tasks file."""
    task_descs = {}
    cur_data = {}
    with codecs.open(task_descs_file, 'r', encoding='UTF-8') as fh:
        for line in fh.readlines():
            line = line.strip()
            if line.startswith('==='):  # new dialogue
                cur_data = {}
                task_id = re.search(r'=== Dialogue ([^ ]*) ===', line).group(1)
                task_descs[task_id] = cur_data
            elif line.startswith('Requests'):
                cur_data['requests'] = line[len('Requests: '):].split(', ')
            elif line.startswith('Constraints'):
                cur_data['constraints'] = line[len('Constraints: '):].split(', ')
            elif line.startswith('GoalGen'):
                line = re.sub('.* matching entities: ', '', line)
                cur_data['matching'] = line.split(',') if line != 'NONE' else []
    return task_descs


def main(args):
    """Collect the data & write the output into one JSON."""
    start = time.clock()
    tokens_df = pd.read_csv(args.tokens_csv, encoding='UTF-8') 
    
    cf_data = pd.read_csv(args.cf_data, encoding='UTF-8')
    #ip_to_userid = get_ip_to_userid(args.webserver_log_dir, args.timezone)
    task_descs = read_task_descs(args.tasks) if args.tasks else {}
    out_data = []
    out_var = {"A1":[],"A2":[],"B1":[], "B2":[],"C1":[],"C2":[],"D1":[],"D2":[],"A":[],"B":[],"C":[],"D":[],
           "S1":[], "S2":[]}
    #out_a1,out_a2,out_b1,out_b2,out_c1,out_c2,out_d1,out_d2,out_a,out_b,out_c, our_d = []
    missed_dm_logs = []
    num_missedlogs = 0
    num_addedlogs = 0
    for inst_no, inst in cf_data.iterrows():
        #print "Processing token: "+ str(inst['auto_token'])
        try:
            dm_log = find_dm_log(tokens_df, inst['auto_token'])
            if dm_log == "REMOVE":
                continue
                
            dm_log = args.dmserver_log_dir + "/" + dm_log
            #print "dm_log:" + dm_log
            if(not os.path.exists(dm_log)): # somehow the demosysxxx log was not logged. e.g. due to crash
                #print "dm_log not exist:" + dm_log
                num_missedlogs += 1
                missed_dm_logs.append(str(inst['auto_token']) + "," + dm_log)
                
            variant = find_sys_variant(tokens_df, inst['auto_token']) # A1, A2, B1,...
            
            with codecs.open(dm_log, 'r', 'UTF-8') as fh:
                log_data = json.load(fh)
                log_data['filename'] = dm_log
                log_data['CF_data'] = {k: v for (k, v) in inst.iteritems() if not k.endswith('_gold')}
                if log_data['taskID'] in task_descs:
                    log_data['task'] = task_descs[log_data['taskID']]
                out_data.append(log_data)
                out_var[variant].append(log_data) # append lower level: A1, A2,B1,..
                out_var[variant[0:1]].append(log_data) # append to higher level: A, B. C, D
                if variant[1:2] == "1":
                   out_var['S1'].append(log_data)
                else:  # it is for S2
                   out_var['S2'].append(log_data)
                   
                num_addedlogs += 1

        except Exception as e:
            # skip all erroneous lines in the CF data
            # print >> sys.stderr, e.message
            print('Caught error: ' + repr(e))

    print "missed DM logs num:" + str(num_missedlogs)            
    print "merged DM logs num:" + str(num_addedlogs)

    if args.outfile_prefix.find('/') != -1:
        destdir = args.outfile_prefix[:args.outfile_prefix.rfind('/')]
        if(not os.path.exists(destdir)):
            os.mkdir(destdir)

    outfile = args.outfile_prefix + "_Full.json"
    with codecs.open(outfile, 'w', 'UTF-8') as fh:
        json_text = json.dumps(out_data, ensure_ascii=False, indent=4, sort_keys=True)
        fh.write(json_text)
        fh.close()
        
    # save all sys variant logs to files
    for key in out_var:
        values = out_var[key]
        outfile = args.outfile_prefix + "_" + key + ".json"
        with codecs.open(outfile, 'w', 'UTF-8') as fh:
            json_text = json.dumps(values, ensure_ascii=False, indent=4, sort_keys=True)
            fh.write(json_text)
            fh.close()
            
    # write missed DM log file list to file
    if len(missed_dm_logs) > 0:
        outfile = open('missedDialogueLogFiles.txt', 'w')
        outfile.write("\n".join(missed_dm_logs))
        outfile.close()
    # time token
    print "time elapsed:"
    #print time.clock() - start

    etime = time.clock() - start
    timestamp = datetime.datetime.fromtimestamp(etime)
    print timestamp.strftime('%Y-%m-%d %H:%M:%S_%f') # HH:MM:SS.microseconds like this e.g 14:38:19.425961
    
if __name__ == '__main__':

    ap = ArgumentParser()
    ap.add_argument('--tasks', '-T', type=str, help='Path to CamInfo_tasks file')
    ap.add_argument('cf_data', type=str, help='CF data CSV file')
    ap.add_argument('tokens_csv', type=str, help='validated tokens CSV file')
    #ap.add_argument('webserver_log_dir', type=str, help='Webserver log directory')
    ap.add_argument('dmserver_log_dir', type=str, help='DM server log directory')
    ap.add_argument('outfile_prefix', type=str, help='Output file prefix e.g. mergedLogsAll/mergedLogs')

    args = ap.parse_args()
    main(args)



