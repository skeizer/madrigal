#
# This is to query LUIS and map LUIS results to the semantic acts for calculating NLU semantics error rate
# 
#  X. Liu @ HWU, 08.06.2018
#
# Useage example:
#
# python get_nlu_semantics.py mergedLogsAll_CF_Trsc_Asr CamInfo_Resources/CamInfo_ontology.json CamInfo_Resources/CamInfo_lexicon.json NLU_Cache/NLU_Logs_Cache.json mergedLogsAll_CF_Trsc_Asr_Sem
# 
# It will check there is file NLU_Cache/NLU_Logs_Cache_Ext.json 
# if exists, use it. Or you can just specify NLU_Logs_Cache_Ext.json 
#
import os.path
import re
import sys, getopt
import argparse
import json
import pandas as pd
import codecs
from datetime import datetime
#import datetime
#import time
#from decimal import Decimal
from luis_sdk import LUISClient

# https://github.com/Microsoft/Cognitive-LUIS-Python 

LUIS_APP = "d307e155-e159-4007-a969-da80f2fba5e2"
LUIS_KEY = "85753b9a0c0346bab3acad681a0ecf88"
CLIENT = LUISClient(LUIS_APP, LUIS_KEY, True)

def getDirectories(indir):
    dirs = []
    for dirName, subdirList, fileList in os.walk(indir):
        #print dirName
        if(dirName != indir):
            dirs.append(dirName)
    return dirs

# get file names list in current directory
def get_files(indir):
    crntDirFiles = []
    for dirName, subdirList, fileList in os.walk(indir):
        #print dirName
        if(dirName == indir):
            crntDirFiles = fileList
            break

    return crntDirFiles

def readFile2List(infile):
    lines = []
    with open(infile) as file:
        for line in file:
            line = line.strip() #or strip the '\n'
            lines.append(line)
    return lines

        
def process_luis_results(res):
    '''
     XL: process Luis query results and return a json object
    '''
    text = res.get_query() # the query utterance
    int_name_top = res.get_top_intent().get_name()
    int_score_top = res.get_top_intent().get_score() # confidence of intent

    intents = []
    for intent in res.get_intents():
        int_name = intent.get_name()
        int_score = intent.get_score()
        intents.append({"intent":int_name,"score":int_score})
        
    entities = []
    for entity in res.get_entities():
        #print(u'Type: %s, Score: %s' % (entity.get_type(), entity.get_score()))
        e_type = entity.get_type()
        e_value = entity.get_name()
        e_score = entity.get_score()
        e_start = entity.get_start_idx()
        e_end = entity.get_end_idx()
        entityJO = {"type": e_type, "entity": e_value, "score": e_score, "startIndex": e_start, "endIndex": e_end}
        entities.append(entityJO)
    
    # comply with Madrigal NLU log formats:
    res_j = {"query": text, "intents": intents, "entities": entities,
              "topScoringIntent":{"intent":int_name_top, "score":int_score_top} }
              
    return res_j
    
# CommFunction Not Used for now.
CommFunction = [  # from madrigal.dialogue_acts.DialogueAct.CommFunction
 "inform", "setQuestion", "propQuestion","choiceQuestion", "confirm", "disconfirm",
 "instruct", "request", "acceptRequest", "declineRequest", "autoPositive",
 "autoNegative", "autoNegative_perc", "autoNegative_int", "attentionAutoPositive", "alloPositive",
 "alloNegative", "feedbackElicit", "attentionFeedbackElicit", "stalling", "pausing",
 "initialGreeting", "returnGreeting", "apology", "acceptApology", "thanking",
 "acceptThanking", "initialGoodbye", "returnGoodbye", "other"]


def map_luis_to_dm_ontology(luis_res, ontology, lexicon):
    '''
    Translated from LUIS_NLU.java
    
    DialogueTurn getTurnFromLUISobj( JSONObject luis_root )
    
    result example:
    "user_sem": " inform([food:Mexican/tex mex, pricerange:expensive])"
    
    Java defined in SlotValuePair.java:
        String[] SPECIAL_SLOTS = { "name" };
        String[] SPECIAL_VALUES = { "none", "dontcare" };

    CamInfo_LUIS.json defined:
      "confirm", "disconfirm", "inform","initialGoodbye","initialGreeting", "None", "propQuestion"
       "setQuestion", "thanking"
            
    '''
    
    luis_dontcare_entity_str = "don ' t care" # returned by LUIS when serving as entity
    dontcare_value = "dontcare"
 
    dial_act_conf = float(luis_res["topScoringIntent"]["score"])
    top_intent = luis_res["topScoringIntent"]["intent"]
    cf = top_intent  # CommFunction
    #print "top intent:" + cf
    if top_intent not in CommFunction: # CommFunction.other
        proc_level = "PERCEPTION" # proc_level not used for now
        sem ={}
        sem["dial_act"] = cf # e.g. top_intent = None
        sem["confidence"] = 0
        return sem
    else:
        proc_level = "INTERPRETATION" # proc_level not used for now
    	
        sem_item = []
        
        for entity in luis_res["entities"]:
            slot = entity["type"]
		    
            # primary slot "name" defined in CamInfo_ontology.json, get from Ontology.java
            if slot == "name" and top_intent == "inform":
                continue # skip item name=...
                
            dial_act_conf *= float(entity["score"])
            
            if  slot == "dontcare":
                # "slot" is the type in the entity, entity value e.g. don't mind, don't need.
                svp = ":dontcare" # comply with Java version. Here DM slot = ""
            else:
                value = entity["entity"]
                if  slot == "requestedSlot":
                    corr_slot = get_domain_slot(value, ontology, lexicon); # value: "phone number", "address", "postcode"
                    svp = corr_slot # empty value, for phone, addr, etc
                    
                else:
                    if value == luis_dontcare_entity_str:
				        value = dontcare_value
						
                    corr_val = get_domain_value(slot, value, ontology, lexicon)
					
                    if corr_val != "":
                        svp = slot +":" + corr_val
                    else:
                        svp = ""
            if svp != "":
                sem_item.append(svp) 
                 
        # "user_sem": " inform([food:Mexican/tex mex, pricerange:expensive])"
        usr_act = cf + "(" + str(sem_item) + ")"
        print "usr_act:" + usr_act
        sem ={}
        sem["dial_act"] = usr_act.strip()
        sem["confidence"] = dial_act_conf            

	return sem

  
 
def get_domain_slot(value, ontology, lexicon):
    '''
    Ontology.java: getDomainSlot( String slot )
    
    check if 'value' is in domain slotNames, if yes, return it.
    else getValue( String slot ) in Lexicon.java
    
    getValue from Lexicon:
    if phrase: p.value.isEmpty() && p.phrases.contains(slot)
       return p.slot
    '''
    
    # For now get all slotnames, not considering requestable or informable.
    all_ontol_slotnames = [ item["slotname"] for item in ontology] 
    #print "all_ontol_slotnames" + Str(all_ontol_slotnames)
    
    if value in all_ontol_slotnames:
        return value
        
    # else check the Lexicon
    lex_arr = lexicon['lexicon']
    for lex in lex_arr:
        if value in lex['phrases']: # Java check  p.value.isEmpty(). so only for requestable slot like postcode
            return lex['slotname']
    # end of loop but not found the value in any phrases            
    
    return "" # ERROR unknown slot

def get_domain_value(slot, value, ontology, lexicon):
    # get correct domain value
    # first get domain values for the slot
    values = []
    for item in ontology:
        if item['slotname'] == slot:
            values = item['values']
    # loop over values, find it and return it with ontology's captioning
    for ont_val in values:
        if ont_val.lower() == value.lower():
            return ont_val
    # not in ontology, search Lexicon
    lex_arr = lexicon['lexicon']
    for lex in lex_arr:
        if slot == lex['slotname'] and value in lex['phrases']:
            return lex['valuename'] # the unified lexicon value
        
    # Not found at all, return ""
    return ""
    

def get_user_semantics_for_turn_trans(utt, ontology, lexicon, luis_cache):
    #utt = "I'm looking for traditional food"
    print "\n get Sem for utt:" + utt
    cache_utts = luis_cache.keys()
    user_sem = {}
    if utt in cache_utts:
       print "Found utt in cache!"
       #print luis_cache[utt]["luis_out"]
       #print "------------------_"
       #print luis_cache[utt]["user_sem"]
       #print luis_cache[utt]["user_sem_score"]
       user_sem["dial_act"] = luis_cache[utt]["user_sem"]
       user_sem["confidence"] = luis_cache[utt]["user_sem_score"]
           
       return user_sem
       
    
    #utt = "I'm looking for traditional food"
    res = CLIENT.predict(utt)
    res_j = process_luis_results(res)
    user_sem = map_luis_to_dm_ontology(res_j, ontology, lexicon)
    
    print "New query user_sem:" + str(user_sem)
    print "New luis:" + str(res_j)
    
    #save the new query to cache
    luis_new_query = {}
    luis_new_query["luis_out"] = res_j
    luis_new_query["user_sem"] = user_sem["dial_act"]
    luis_new_query["user_sem_score"] = user_sem["confidence"]
    
    luis_cache[utt] = luis_new_query
    
    print "User Sem From LUIS query:" + str(user_sem)
    return user_sem

def get_user_semantics_for_dial(dmfile_uri,ontology, lexicon, luis_cache, output_dir):

    m_logs = json.load(codecs.open(dmfile_uri, 'r', 'UTF-8'))

    for dial in m_logs: # json array
        dial_id = dial["dialogueID"]
        user_id = dial["userID"]
        for turn in dial["dialogue"]:
            if "HANGUP :" in turn["usr_turn"]["transcription"]:
                continue # no need to look for full asr since it is clicking the button
            turn_time = turn["usr_turn"]["crntTurnTimeStr"]
            trans_utt = turn["usr_turn"]["transcription"]
            #trans_utt = preprocess_transc_utt(trans_utt) # this has been moved to merge_logs_with_transc.py
            if trans_utt == "":
                continue
            trans_sem = get_user_semantics_for_turn_trans(trans_utt, ontology, lexicon, luis_cache)
            turn["usr_turn"]["transcription_sem"] = trans_sem
            
    # save changes
    if(not os.path.exists(output_dir)):
        os.mkdir(output_dir)

    # dmfile: mergedLogsWithTasks_Transc_A1.json
    dmfile = dmfile_uri
    if "/" in dmfile_uri:
        dmfile = dmfile_uri[dmfile_uri.rfind('/')+1:]
        
    filep1 = dmfile[:dmfile.rfind('_')]
    filep2 = dmfile[dmfile.rfind('_')+1:dmfile.rfind('.')] # e.g. A1
    
    outfile = output_dir + "/" + filep1 + "_SemTrsc_" + filep2 + ".json" 
   
    if outfile.find('/') != -1:
        destdir = outfile[:outfile.rfind('/')]
        if(not os.path.exists(destdir)):
            os.mkdir(destdir)
    
    with codecs.open(outfile, 'w', 'UTF-8') as fh:
        json_text = json.dumps(m_logs, ensure_ascii=False, indent=4, sort_keys=True)
        fh.write(json_text)
        fh.close()

def main(argv):
    parser = argparse.ArgumentParser(description='process web text logs to json format.')

    parser.add_argument('dm_log_dir', type=str, help='dir for dm logs merged with CF eval reports, and/or transc reports')
    parser.add_argument('ontology_file', type=str, help='DM ontology file')
    parser.add_argument('lexicon_file', type=str, help='DM lexicon file')
    parser.add_argument('nlu_cache_file', type=str, help='NLU cache file')            
    parser.add_argument('output_dir', type=str, help='results output dir')

    args = parser.parse_args()

    if(not os.path.exists(args.output_dir)):
        os.mkdir(args.output_dir)

    #ontology_file = "CamInfo_Resources/CamInfo_ontology.json"
    #lexicon_file = "CamInfo_Resources/CamInfo_lexicon.json"
    # NLU_Cache/NLU_Logs_Cache.json
    ontology = json.load(codecs.open(args.ontology_file, 'r', 'UTF-8'))
    ontology = ontology["ontology"]["restaurant"]
    # e.g.: [ {"slotname":"food", "informable":"true", "requestable":"true", "primary":"false",
    #  "multival":"true", "values":[ "American", "Cafe food", "Chinese",...] } ...]
    
    lexicon = json.load(codecs.open(args.lexicon_file, 'r', 'UTF-8'))
    # e.g. { "lexicon" : [  {"slotname":"pricerange", "valuename":"moderate", 
    #                        "phrases":["moderate", "moderately priced"] }, ....] }
    #print str(lexicon['lexicon'][0])
    
    luis_cache_file = args.nlu_cache_file
    if "_Ext" not in luis_cache_file:
        ext_file = luis_cache_file[:len(luis_cache_file)-5] + "_Ext.json"
        if os.path.exists(ext_file):
            luis_cache_file = ext_file
            
    print "Opend LUIS cache file:" + luis_cache_file        
    luis_cache = json.load(codecs.open(luis_cache_file, 'r', 'UTF-8'))
    
    dmfiles = get_files(args.dm_log_dir)
    all_msg = []
    msg = "\nRunning get_nlu_semantics.py"
    all_msg.append(msg)
    all_msg.append("    started at " + str(datetime.now()))
    all_msg.append("Opened LUIS cache file:" + luis_cache_file)
    
    for dmfile in dmfiles:
        dmfile_uri = args.dm_log_dir + "/" + dmfile
        #Test: dmfile_uri = args.dm_log_dir + "/" + "mergedLogsWithTasks_Transc_FullAsr_A2.json"
        print "processing: " + dmfile_uri
        get_user_semantics_for_dial(dmfile_uri,ontology, lexicon, luis_cache, args.output_dir)
        all_msg.append("processed: " + dmfile_uri)
        
    # save extended LUIS_Cache with new query results
    new_cache_file = luis_cache_file
    if "_Ext" not in luis_cache_file:
        new_cache_file = luis_cache_file[:len(luis_cache_file)-5] + "_Ext.json"
        
    with codecs.open(new_cache_file, 'w', 'UTF-8') as fh:
        json_text = json.dumps(luis_cache, ensure_ascii=False, indent=4, sort_keys=True)
        fh.write(json_text)
        fh.close()
    
    all_msg.append("\nFinished at " + str(datetime.now()))
    all_msg.append("\nExtented LUIS Cache file saved in " + new_cache_file)
    all_msg.append("  ")
    
    cache_dir = args.nlu_cache_file[:args.nlu_cache_file.rfind("/")]
    outfile_uri = cache_dir + '/Info_For_NLU_Cache_Processing.txt'
    outfile = open(outfile_uri, 'a')
    outfile.write("\n".join(all_msg))
    outfile.write("\n")
    outfile.close()
   
if __name__ == "__main__":
    main(sys.argv[1:])
    

