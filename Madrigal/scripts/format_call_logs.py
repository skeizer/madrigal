#!/usr/bin/env python2
# -"- coding: utf-8 -"-

from __future__ import print_function
import json
import codecs
from argparse import ArgumentParser

from wer_logreg import call_wer, call_success


def process_call(call):
    print("%s  Task: %2d  System: %s" % (call['dialogueID'], int(call['taskID']), call['sysName']))
    print("----------------------------------------------")
    print("Task constraints: %s" % ', '.join(call['task']['constraints']))
    print("To request: %s" % ', '.join(call['task']['requests']))
    print("Matching venues: %s" % ', '.join(call['task']['matching']))
    print("Worker country %s  trust %.3f  channel %s" %
          (call['CF_data']['_country'], call['CF_data']['_trust'], call['CF_data']['_channel']))
    print()
    wer = call_wer(call)
    if wer is None:
        wer = float('nan')
    print("%-18s: %.3f" % ('WER', wer))
    for success_type in ['constr_mentioned', 'constr_confirmed',
                         'entity_provided',
                         'all_info_requested', 'all_info_provided',
                         'q1']:
        print("%-18s: %s" % (success_type, 'Y' if call_success(call, success_type) else 'N'))
    print()
    for turn_no, turn in enumerate(call['dialogue'], start=1):
        usr = turn['usr_turn']
        print("%2d Usr transc  : %s" % (turn_no, usr['transcription']))
        if 'asr_full_hyps' in usr:
            if isinstance(usr['asr_full_hyps'][0]['conf'], basestring):
                usr['asr_full_hyps'][0]['conf'] = float(usr['asr_full_hyps'][0]['conf'])
            print("       best ASR: %s (%.3f)" %
                  (usr['asr_full_hyps'][0]['utt'], usr['asr_full_hyps'][0]['conf']))
        if 'sem_hyps' in usr and usr['sem_hyps']:
            print("       DA      : %s (%.3f)" %
                  (usr['sem_hyps'][0]['dial_act'][0], usr['sem_hyps'][0]['confidence'][0]))
        if 'transcription_sem' in usr:
            print("       ref-DA  : %s (%.3f)" %
                  (usr['transcription_sem']['dial_act'].strip(),
                   float(usr['transcription_sem']['confidence'])))

        sys = turn['sys_turn']
        print("   Sys DA      : %s" % sys['sys_act'])
        print("       out     : %s" % sys['utterance'])
        print()
    print("\n\n")


def main(args):
    with codecs.open(args.logfile, 'rb', 'UTF-8') as fh:
        calls = json.load(fh)

    calls = sorted(calls, key=lambda call: (int(call['taskID']), call['sysName']))
    for call in calls:
        process_call(call)


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('logfile', type=str, help='JSON with joint logs')

    args = ap.parse_args()
    main(args)
