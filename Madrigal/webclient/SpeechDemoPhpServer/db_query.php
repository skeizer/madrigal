<?php

/****************************************************************************
 *   Madrigal project, HW, iLab, by X.Liu , 31/102017
 *
 ****************************************************************************/ 


  include("db_configuration.php");

  function connectToDB() {
    $link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die("Couldn't make connection to database.");
    //mysql_set_charset('utf8');
    $db = mysql_select_db(DB_NAME, $link) or die("Couldn't select database");
  }
  
   /*
      Use Round Robin approach ( actually a simple loop over the systems)
      to get the next available DM systems for the user to connect to.
      
      We have multiple DM systems, each of which will be running on different ports 
      as different instances. The DM systems need to be equally distributed across
      the user calls ( no need to across the instances as they may be from the same DM.)
      
      DM systems status are stored in the external MySQL database, which allows to be accessed
      across users.
      
    */
  function getNextAvailSys($lastSysNum, $numAllSys) {
    $nonAvail = 0;
    // loop over all systems, get next available one
    while (true) {
      $checkNum = $lastSysNum + 1;
      if($checkNum == $numAllSys) {
          // last time has checked the last sys. NB: sysNum = 0,.., $numAllSys-1.
          // so go to the first one to check
          $checkNum = 0;
      }
      $sysinfo = getAvailSysInfo($checkNum); // check if $checkNum avail.
      if( $sysinfo != -1 ) {
        // results have: $sysinfo['dmsysnum'], $sysinfo['instancehost'], $sysinfo['instanceport']
        return $sysinfo; 
      } else {
        $nonAvail += 1;
        if($nonAvail == $numAllSys ) {
          // all systems are checked, no one available.
          return "ALL_BUSY";
        }
      }
    }
  }
  
  // check if locked by this particular user
  function releaseLockIfOccupied($userid) {
      // check if the user previously locked an instance due to refreshing browser(re-starting a task)
      // or browser time out or error etc. Each Start Dialogue will call this method first
      $info = "";
      $q = "select `dmsysnum`, `instancehost`,`instanceport` from `".TABLE_NAME."` where `lastaccessuser` ='$userid' and `occupied` = 1";      
      $result = mysql_query($q) or die(mysql_error());
      
      if(mysql_num_rows($result) > 0) {
         // this user previously occupied port (instance)
         // get a row: $row['dmsysnum'], $row['instancehost'], $row['instanceport']
         $row = mysql_fetch_assoc($result); 
$info .= "Port Released for StartDialogue, RELOAD, TIMEOUT: \n" . ", host=" . $row['instancehost']. ", port="
                  .$row['instanceport'].", sysnum=".$row['dmsysnum']. ", userid=". $userid ;         
         // release it
         updateDB($row['instancehost'],$row['instanceport'],$row['dmsysnum'],$userid,0);
      }
      
      return $info;
  }
  
  // possible locked by others, check all occupied users.
  function getOverOccupiedAndReleaseLock($max, $log) {
      // check all occupied users. release ports if over occupied, return string info for logging.
      $info = "";
      $q = "select `dmsysnum`, `instancehost`,`instanceport`,`lastaccessuser`,`lastaccesstime` from `".TABLE_NAME."` where `occupied` = 1";
      $result = mysql_query($q) or die(mysql_error());
      $numRow = mysql_num_rows($result);
      //$log->lwrite("DB:".$numRow);
      if($numRow > 0) {
      //TODO check if MySql lastaccesstime field needs to use string, and change the use of MySql now()
      // when populating the table
          $crntTime = date('Y-m-d H:i:s');   
          while ($row = mysql_fetch_assoc($result)) { // Loops N times if there are N returned rows
              // loop check for each occupied user if they occupied over the predefined threshold.
              if($row['lastaccessuser'] == "DM_ERROR")
                 continue;
             
              $diff = timeDiff($row['lastaccesstime'], $crntTime); // return time diff in seconds
              //$log->lwrite("timeDiff:" . $diff . ", max: " . $max);
              if($diff > $max) {
                  // over occupied, release the lock.
                  $info .= " OverOccupied and port Released for : " . ", numRow=" . $numRow . ", timeDiff=" . $diff . ", maxTime=" . $max . ", host=" . $row['instancehost'] . ", port=" . $row['instanceport'] . ", sysnum=".$row['dmsysnum'] . ", userid=".$row['lastaccessuser'] . " ROW_END";
                  
                  updateDB($row['instancehost'],$row['instanceport'],$row['dmsysnum'],$row['lastaccessuser'],0);
              }
          } 
    }  
    return $info; 
  }

  function getAllSysNum() {
     // each system has a name and a number which is from 0 to N. each system may have multiple
     // instance (different port) but has the same number.
     // these names/numbers are fixed once the DB is deployed.
    $q = "select distinct `dmsysnum` from `".TABLE_NAME."`";
    
    // mysql_query($q) will use default last opended link from mysql_connect.
    // could also use: mysql_query($q, $link)
    $result = mysql_query($q) or die(mysql_error());
    $resArray = Array();
    while ($row = mysql_fetch_assoc($result)) {
        //$t = $row['dmsysnum']; 
        //echo "num:$t <br/>"; 
        $resArray[] =  $row['dmsysnum'];  
    }
    return $resArray;
  }
  
  function getLastChosenSys() {
      $q = "select distinct `dmsysnum` from `".TABLE_NAME."` where `lastchosensys` = 1";
      $result = mysql_query($q) or die(mysql_error()); 
      $sysnum = -1;
      if(mysql_num_rows($result) > 0) {
         $row = mysql_fetch_assoc($result); // get a row
         $sysnum = $row['dmsysnum'];      
      }  
      return $sysnum;
  }
  
  function getAvailSysInfo($sysnum) { 
      // check if the system $sysnum available, if yes return the port, otherwise return -1.
      $q = "select `dmsysname`, `dmsysnum`, `instancehost`,`instanceport` from `".TABLE_NAME."` where `dmsysnum` ='$sysnum' and `occupied` = 0";
      $result = mysql_query($q) or die(mysql_error());
      
      if(mysql_num_rows($result) > 0) {
         $row = mysql_fetch_assoc($result); // get a row
         return $row; // contains: $row['dmsysnum'], $row['instancehost'], $row['instanceport']
      } else {
         return -1;  
      }
  }
  
  function updateDB($host, $port, $sysnum, $usr, $occupy) { 
      // clear lastchosensys
      if($occupy == 1 && $usr != "DM_ERROR") { // when each dialogue starts, updateDB is called with $occupy=1
          $q = "update `".TABLE_NAME."` set `lastchosensys`= 0";
          mysql_query($q) or die(mysql_error());      
			        
          // update lastchosensys. $sysnum is unique for same sys (which may have multiple instances)
          // lastchosensys = 1 means Yes, 0 means Not last chosen.
          $q = "update `".TABLE_NAME."` set `lastchosensys`= 1 where `dmsysnum` = '$sysnum'";
          mysql_query($q) or die(mysql_error());      
      }
      
      // update DB for both $occupy=1 or 0 (finished a dialogue).
      // update occupied. remove condition of dmsysnum should be ok as the $port is unique.
      $q = "update `".TABLE_NAME."` set `lastaccessuser`= '$usr', `lastaccesstime`= now() , `occupied`= '$occupy'    where `dmsysnum` = '$sysnum' and `instancehost` = '$host' and `instanceport` = '$port' ";
      mysql_query($q) or die(mysql_error()); 
  }
  
    /*
    Usage : echo timeDiff("2002-04-16 10:00:00","2002-03-16 18:56:32");
    $difference = timeDiff("2002-03-16 10:00:00",date("Y-m-d H:i:s"));
    $years = abs(floor($difference / 31536000));
    $days = abs(floor(($difference-($years * 31536000))/86400));
    $hours = abs(floor(($difference-($years * 31536000)-($days * 86400))/3600));
    $mins = abs(floor(($difference-($years * 31536000)-($days * 86400)-($hours * 3600))/60));#floor($difference / 60);
    echo "<p>Time Passed: " . $years . " Years, " . $days . " Days, " . $hours . " Hours, " . $mins . " Minutes.</p>"  
     */
    function timeDiff($firstTime,$lastTime) {
       // convert to unix timestamps
       $firstTime=strtotime($firstTime);
       $lastTime=strtotime($lastTime);

       // perform subtraction to get the difference (in seconds) between times
       $timeDiff=$lastTime-$firstTime;

       // return the difference
       return $timeDiff;
    }

  
?>

