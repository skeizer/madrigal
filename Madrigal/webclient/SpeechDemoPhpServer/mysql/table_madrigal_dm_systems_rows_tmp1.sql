-- MySql DB/tables for Madrigal systems.
-- X.Liu @ HW, 31/10/2017
--
CREATE TABLE IF NOT EXISTS `madrigal_dm_systems` (
  `id` bigint(20) NOT NULL auto_increment,
  `dmsysname` varchar(255) NOT NULL,
  `dmsysnum` int(11) NOT NULL,    
  `instancehost` varchar(255) NOT NULL,  
  `instanceport` int(11) NOT NULL,
  `occupied` int(11) NOT NULL,
  `lastchosensys` int(11) NOT NULL,  
  `lastaccessuser` varchar(255) NOT NULL,  
  `lastaccesstime`  datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- populate the table with initila defined values:
-- command line use:
-- Delete/clear all records in the table: mysql> delete from madrigal_dm_systems;
-- if needed, you can: mysql> drop table madrigal_dm_systems;
-- mysql -u madrigal -h mysql-server-1 -p madrigal < table_madrigal_dm_systems_rows.sql

set @host = '137.195.27.220';

INSERT into `madrigal_dm_systems` (`dmsysname`,`dmsysnum`,`instancehost`, `instanceport`,`occupied`,`lastchosensys`,`lastaccessuser`,`lastaccesstime`) VALUES ('sys1',0,@host,8080,0,0, 'none', now() );

INSERT into `madrigal_dm_systems` (`dmsysname`,`dmsysnum`,`instancehost`,`instanceport`,`occupied`,`lastchosensys`,`lastaccessuser`,`lastaccesstime`) VALUES ('sys2',1,@host,8081,0,0, 'none', now() );

