-- MySql DB/tables for Madrigal systems.
-- X.Liu @ HW, 31/10/2017
--
-- Recommend: modify table_madrigal_dm_systems_rows.sql and use it.
--
CREATE TABLE IF NOT EXISTS `madrigal_dm_systems` (
  `id` bigint(20) NOT NULL auto_increment,
  `dmsysname` varchar(255) NOT NULL,
  `dmsysnum` int(11) NOT NULL,    
  `instanceport` int(11) NOT NULL,
  `occupied` int(11) NOT NULL,
  `lastchosensys` int(11) NOT NULL,  
  `lastaccessuser` varchar(255) NOT NULL,  
  `lastaccesstime`  datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

