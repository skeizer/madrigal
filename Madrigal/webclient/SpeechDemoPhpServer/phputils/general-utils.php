<?php

  function createDirIfNotExist($dir) {
      // create new directory with 744 permissions if it does not exist yet
      // owner will be the user/group the PHP script is run under
      if ( !file_exists($dir) ) {
        $oldmask = umask(0);  // helpful when used in linux server  
        if(! mkdir ($dir, 0777)) {
            die("failed to create specified folder $dir");
        }
      }
  
  }
  
  function filter($data) {
	  $data = trim(htmlentities(strip_tags($data)));
	
	  if (get_magic_quotes_gpc())
		  $data = stripslashes($data);
	
	  $data = mysql_real_escape_string($data);
	
	  return $data;
  }

?>
