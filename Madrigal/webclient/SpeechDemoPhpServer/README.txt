Brief Notes about the version  -- X.Liu@HW 03/11/2017
27/11/2017 -- Unfied version
---------------------------------

The Crowdflower (CF) Javascript (JS) codes and CF CML codes are in SpeechDemoCF.txt
in bitbucket repository.

These are the PHP server codes which is the bridge between the browser clients and 
the Dialogue Manager (DM) servers.

The connection between browsers and servers has to be turn-by-turn during one dialogue
due to this server-client architecture via the PHP server bridge (via Ajax HTTP requests
from the browser).

Some notes are in the PHP files and HTML file.

Different setups:

1. Test a single DM instance via a normal browser (mainly tested on Google Chrome).
-----------------------------------------------------

   use the speech_demo.html which has the consistent codes with CF version (in SpeechDemoCF.txt),
   but the related CML codes are commented out (find them by search "_CF_CODES_" in the speech_demo.html).
   
   e.g. use the URL
   https://www.macs.hw.ac.uk/ilabarchive/madrigal/web/speech_demo.html

   Change dm_server_handler.php:

   $runMoreDMs = false; 
   $localhost = "x.x.x.x"; // your DM server instance host
   $localport = 8080; // // your DM server instance port

   You can adjust the timeout threshold in speech_demo.html
   
       var idleTimeout = 300000; //milliseconds. it is browser page inactivity timeout. 

       // dialogue timeout timer starts whenever "Start dialogue" button is clicked.
       var dialogueMaxTimeMinutes = 6; // minutes, used when tell the user on the browser      


2. Test multiple DM instances.
-----------------------------------------------------

   In this case the exteral DB is used to keep track of the availability of all instances.
   We have set up a MySql DB in MACS server. You can run your own in your machine if you want.
   
   Change to: $runMoreDMs = true;

   Make sure in db_configuration.php it specifies the correct DB info, especially the Table name,
   e.g.
   define ("TABLE_NAME","madrigal_dm_systems"); 
   
   
3. Test the system in your local machine.
-----------------------------------------------------

   As the cases above, but you can run a PHP server in your local machine for testing purpose.
   e.g. copy all the server codes in your local directory, and inside it:

   php -S localhost:8000
   
   Test it in browser:
   https://localhost:8000/speech_demo.html
   
   Current Issue: this has been tested and working on ubuntu 14.04, but in Mac OS,
   the Javascrip complains the JSON parsing error in the DM returned json responses.
   This may be because the php server version issues in Mac OS. You may try other server
   such as Apache etc.
   
   And,in dm_server_handler, or die ("............!") may not work for php server in Mac OS.
   
   
4. Test from Crowdflower.
-----------------------------------------------------

   From SpeechDemoCF.txt, copy the JS codes to CF JS windows, the CML codes to CML windows.
   
   Make sure the correct server URLs, e.g.
   
   var urlAudio =  "https://www.macs.hw.ac.uk/ilabarchive/madrigal/web/upload_user_audio.php";
   var urlDMHandler =  "https://www.macs.hw.ac.uk/ilabarchive/madrigal/web/dm_server_handler.php";

5. Final point for local test
-----------------------------------------------------
   If you couldn't get your local setup to work, you can try the websocket connection directly from a brower
   (The initial version of html in the repositry: Madrigal/webclient/speech_demo.html)
   

--------------- END -----------------------

