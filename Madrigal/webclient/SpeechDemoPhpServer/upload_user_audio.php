<?php 

   header('Access-Control-Allow-Origin: https://www.crowdflower.com');
   header('Access-Control-Allow-Origin: https://render.crowdflower.io');
   //header('Access-Control-Allow-Origin: *'); // if set Credentials true, here * won't work, need specific URLs.
   header('Access-Control-Allow-Credentials: true'); // for keep SESSION[...] values
   //header('Content-Type: text/plain');
   header('Content-Type: application/json');

   // Above headers are for CORS requests -- cross-domain requests.
   
    /*
     X.Liu@HW: handle the ajax request from the javascript for the recorded user audio.
     and save them to the server side. Notes: the server side current directory need to
     be made writable for the PHP. For the MACS server apache server, the php user name
     is 'apache'.
        
     27/10/2017

     23/11/2017 re-organised directory structure.
          
   */
  include('phputils/general-utils.php');
  
  session_start();
  $dir = 'userAudio'; // top level dir name in the current server directory.

  createDirIfNotExist($dir);
  
  $userid = $_POST["usrID"];
  $usrdir = $dir."/User-".$userid;
  createDirIfNotExist($usrdir);
    
  if(isset($_FILES['usrAudioFile']) and !$_FILES['usrAudioFile']['error']){
    
    // get temp file name PHP used.
    $tmpfilename=$_FILES['usrAudioFile']['tmp_name'];
    //echo $tmpfilename;
    
    $time = $_SESSION['timestamp'];
    $userTurnIdx = $_POST["usrTurnIdx"];
    // audio files structure: "./userAudio/userID/dialogueID/audio-files-of-all-turns.opus"
    $usrdir = $usrdir."/Dialogue-" . $time;
    createDirIfNotExist($usrdir);
    
    //$destfilename = $userid . "_" . $time . "_" . $userTurnIdx . ".opus";
    $destfilename = "Turn_" . $userTurnIdx . "_" . $time . ".opus";
    $destfile = "$usrdir/$destfilename";
        
    // could use "copy"
    $res = "Saved";
    if( ! move_uploaded_file($tmpfilename, $destfile) ) {
        //echo "can not move uploaded file $tmpfilename";
		$res = "NotSaved";
    }

    // response with json string

	$jsonObj = new \stdClass(); // needed for testing locally in Mac machines.      
	$jsonObj->meta = "AUDIO";
	$jsonObj->text = $res;	 

	$jsonStr = json_encode($jsonObj); // JSON Object to string

    echo $jsonStr;

  }

?>

