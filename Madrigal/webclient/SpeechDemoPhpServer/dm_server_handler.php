<?php

   header('Access-Control-Allow-Origin: https://www.crowdflower.com');
   header('Access-Control-Allow-Origin: https://render.crowdflower.io');
   //header('Access-Control-Allow-Origin: *'); // if set Credentials true, here * won't work, need specific URLs.
   header('Access-Control-Allow-Credentials: true'); // for keep SESSION[...] values
   //header('Content-Type: text/plain');
   header('Content-Type: application/json');

/*
    X.Liu@HW modified Simon's madrigal_web_interface.php to handle the javascript requests
    for DM server connections.

    27/10/2017, Ver1: handle request from speech_demo.php

    31/11/2017 Ver2: handle request from speech_demo.html
       adapted the changes of speech_demo.php to speech_demo.html

    31/11/2017 Ver3: get next available DM instances from database.
     TODO consistent var names for usr -- user
     
    20/11/2017 added IDLE_TIMEOUT, DIALOGUE_TIMEOUT
    
    23/11/2017 added logging, TABLE_NAME variable, CORS requests test in CF: Crowdflower
    
    24/11/2017 added Over-occupied to DM codes, timeout events to DM, but commented for now.
               also query_db.php is changed.

    ---------------Events Handling ---------------------    
    ALL_BUSY: local event, pass to browser client

    Following events will pass to DM server in "meta" element of the JSON string.
    each DM request json will carry the userID and taskID element along with other elements.

    INIT : Start dialogue button clicked.
    HANGUP : End dialogue button clicked
    USERTURN : normal user turn, from browser client

    STOP_SPEAKING_BUTTON_CLICKED : "Stop speaking" button clicked
    IDLE_TIMEOUT : inactivity over time
    DIALOGUE_TIMEOUT : dialogue over time after starting dialogue
    RELOAD : user refresh/reload page (also occurred when first time loading page)
    PORT_TIMEOUT : some users occupied ports for too long and released by db_query  
   -------------------------------------------------------------------------------
   
   27/11/2017 make the unified version. Also see the README file.
   
   30/11/2017 added handling for checking DM server shut down, if yes, mark the port
      unavailable in DB with special name DM_ERROR in the usrID field, and try to get another one.
   
*/

include("db_query.php"); //NB: for use different DB table, modify db_configuration.php
include('phputils/logging-v2.php');
include('phputils/general-utils.php');

//$runMoreDMs = true for running multiple DM server instances which is the normal setup and 
//    will use external MySql DB for keeping track of availablility of each instances. 
//    = false for running single DM instance test, mainly used for testing, and you need to
//    specify the hostname and port below.
// To run everything locally: see the README.

$runMoreDMs = true; // see notes above

//$localhost = '127.0.0.1'; // Only needed when doing single DM test, e.g. $runMoreDMs = false;
//$localhost = "137.195.27.244"; // Only needed when doing single DM test, e.g. $runMoreDMs = false;
//$localport = "8080"; // Only needed when doing single DM test, e.g. $runMoreDMs = false;


session_start(); // without this, won't get SESSION info e.g. $_SESSION["sysHost"] 
// $_SESSION[...] are always kept for same browser even if reload page/close tab then retype URL.

$sys = ""; // final return to the client in JSON format.
$taskid = ""; 
$usrid = "";
$btnText = "";
$usrUtt = "";
$dialogueTimeoutThreshold = 300; //in seconds, will be passed from client to be consistent. 
            // Used for PORT_TIMEOUT when running multiple DM instances and query DB.


// even initial load or reload page, will receive a request, need access DB to check locked port, 
// so need query DB for each request
if($runMoreDMs) connectToDB(); 

if( $_SERVER['REQUEST_METHOD'] == 'POST') {

    if( isset($_POST['usrID']) ) $usrid = $_POST["usrID"] or die ("Not pass usrID");
    if( isset($_POST['taskID']) ) $taskid = $_POST["taskID"] or die ("Not pass taskID");    
    if( isset($_POST['usrUtt']) ) $usrUtt = $_POST["usrUtt"] or die ("Not pass userUtt") ;
    if( isset($_POST['btnText']) ) $btnText = $_POST["btnText"] or die ("Not pass btnText");
    if( isset($_POST["dialTimeoutThreshold"]) ) $dialogueTimeoutThreshold = $_POST["dialTimeoutThreshold"] or die ("Not pass dm timeout threshold");
}


$maxOccupyTime = $dialogueTimeoutThreshold + 30; // in seconds

// Logging class initialization
$log = new Logging();

$logDir = "logs/User-".$usrid;
createDirIfNotExist($logDir);

// set path and name of log file (optional, default: logs/logfile.txt)
$logFile = $logDir."/logfile.txt";
$log->lfile($logFile,10); // maximum file size x MB, The Logging seems not rollover log files for now, so better to set size bigger

if(!isset($_SESSION['DialogueStarted'])){ // so not check it during a dialgoue. But onload will check.
    $info ="";
    if($runMoreDMs)  $info = getOverOccupiedAndReleaseLock($maxOccupyTime, $log); // possible locked by others
    
    //uncomment this as DM server code updated
     if($info != "") informDMServerOverOccupied($info);
    
    // write message to the log file
    if($info != "") $log->lwrite($info);
}

if ( $btnText == "Start dialogue" ) {
    $log->lwrite("Event: Start dialogue." );
    // ------------- New Session initialsation --------------
    //$version = phpversion();
    //session_start(); // need to first start it for session_destroy(). now it started on the top of the file.
    session_unset();
    $_SESSION = array(); // Clears the $_SESSION variable
    session_destroy();

    //setcookie(); // caused issues in Mac machine locally testing.
    session_start();

    if(!isset($_SESSION['DialogueStarted'])){
        $_SESSION['DialogueStarted'] = 1;
        $info = "";
        
        if($runMoreDMs)  {
           $info = getOverOccupiedAndReleaseLock($maxOccupyTime, $log); // possible locked by others
          // write message to the log file
          if($info != "") {
             $log->lwrite($info);

             //uncomment this as DM server code updated
              $msg = informDMServerOverOccupied($info);
             if($msg != "") $log->lwrite($msg);
         }
       }
    }

    $log->lwrite("A dialogue started." . " userIP=" . $_SERVER["REMOTE_ADDR"] . ", userID=". $usrid);
    
    $info = "";
    // first check if the user previously locked an instance, if yes, release it.
    if($runMoreDMs)  $info = releaseLockIfOccupied($usrid); // check if locked by this particular user
    if($info != "") $log->lwrite($info);
   
    $timestamp = date('Y-m-d_H-i-s_') . gettimeofday()['usec'];
    $_SESSION['timestamp']= $timestamp;   // each dialogue has the same timestamp (among dialogue turns)
   
   $availSys = "NA";
   if($runMoreDMs)  {
        $allSys = getAllSysNum() ;
        $numAllSys = count($allSys);        
        // get last accessed system
        $lastSysNum = getLastChosenSys();
        $availSys = getNextAvailSys($lastSysNum, $numAllSys);
    }
    
    if($availSys == "ALL_BUSY") {
        $jsonObj->meta = "ALL_BUSY";
        $jsonObj->utt = "I am busy at the moment, please try again later!"; 
        $sys = json_encode($jsonObj); // JSON Object to string
        $info = "Tried at ". $timestamp . ", ALL_BUSY!";
        $log->lwrite($info);
    } else {
        $sysNum = "0";
        $sysName = "DefaultSys0";
        $availHost = $localhost;
        $availPort = $localport;

        if($runMoreDMs)  {
            $sysNum = $availSys['dmsysnum'];
            $sysName = $availSys['dmsysname'];            
            $availHost = $availSys['instancehost'];
            $availPort = $availSys['instanceport'];
        }
        $_SESSION["sysNum"] = $sysNum;
        $_SESSION["sysName"] = $sysName;        
        $_SESSION["sysHost"] = $availHost; //DM host
        $_SESSION["sysPort"] = $availPort; // DM port

        // send meta information (task ID, user IP, ...)
        $jsonObj = new \stdClass(); // needed for testing locally in Mac machines.          
        $jsonObj->meta = "INIT";
        $jsonObj->sysName = $sysName;
        $jsonObj->sysHost = $availHost; 
        $jsonObj->sysPort = $availPort; 
        $jsonObj->userID = $usrid;	 
        $jsonObj->taskID = $taskid;	 
        $jsonObj->userIP = $_SERVER["REMOTE_ADDR"];	 

        $jsonStr = json_encode($jsonObj); // JSON Object to string
         $log->lwrite("Try initial connection to " . $availHost . ", port:". $availPort.", sysNum:". $sysNum);
        // $jsonStr = "xliuxliutesttest";
        // loop to check if some DM server shut down, then choose another available one
        $sys = makeInitConnection($availHost,$availPort, $sysNum,$usrid, $runMoreDMs, $jsonStr, $log );

        if($sys == "ALL_BUSY") {
            $jsonObj->meta = "ALL_BUSY";
            $jsonObj->utt = "I am busy at the moment, please try again later!"; 
            $sys = json_encode($jsonObj); // JSON Object to string
            $info = "Tried at ". $timestamp . ", ALL_BUSY!";
            $log->lwrite($info);
       }        
    
        $info = "DM host=".$_SESSION["sysHost"].", DM port=" . $_SESSION["sysPort"].", SysNum=".$_SESSION["sysNum"];
        $log->lwrite($info);    
        
        $log->lwrite("INIT sent to DM server.");
    }	 

} else if ( $btnText == "StopButtonClicked" ) { // "End Dialogue" button clicked.
    $log->lwrite("Event: StopButtonClicked, == End dialogue button." );
    // send HANGUP
    $host = $_SESSION["sysHost"];
    $port = $_SESSION["sysPort"];
    $num = $_SESSION["sysNum"] ;

    $jsonObj->meta = "HANGUP"; 
    $jsonObj->userID = $usrid;	 
    $jsonObj->taskID = $taskid;	 
      
    $jsonStr = json_encode($jsonObj);

    $socket = getConnection( $_SESSION["sysHost"], $_SESSION["sysPort"] ); // See notes in the method.
    $sys = getResponse( $socket, $jsonStr );

    if($runMoreDMs) updateDB($_SESSION["sysHost"], $_SESSION["sysPort"], $_SESSION["sysNum"], $usrid, 0); // set it free
    
} else if ( $btnText == "Stop speaking" ) { // stop current turn.
    $log->lwrite("Event: Stop speaking, button clicked." );
    // Currently JS doesn't send any request to here. The JS toggle the button to "Start speaking"
    // if there is any ASR result, it will be sent from recognition.onresult as btnText "Start speaking"

    // Once clicked, html page called stopRecognizer(). TODO: check if ASR gets something or sends empty utt.
    // agreed not to pass to DM. DM will response to user based on whatever ASR gets. 
        
    
} else if ( $btnText == "Start speaking" ) { 
    $log->lwrite("Event: Start speaking." ); 
    
    $info = "DM host=".$_SESSION["sysHost"].", DM port=" . $_SESSION["sysPort"].", SysNum=".$_SESSION["sysNum"];

    $log->lwrite($info);    
    $log->lwrite("User: ".$usrUtt ); 
    
    //$usrUtt is passed as a whole JSON string, which is got above on the top.
    $socket = getConnection($_SESSION["sysHost"], $_SESSION["sysPort"] ); // See notes in the method.
    $sys = getResponse( $socket, $usrUtt ); // sends message and receives response
    $log->lwrite("Sys: ".$sys ); 
        
    // Takes a JSON encoded string and converts it into a PHP variable.
    $de = json_decode($sys);

    // Even not complete the task, meta is still CLOSE    
    // { "utt": "You have ended the dialogue without trying to complete the task.", "meta": "CLOSE" }
    if ($de->meta == "CLOSE") {
        if($runMoreDMs) updateDB($_SESSION["sysHost"], $_SESSION["sysPort"], $_SESSION["sysNum"], $usrid, 0); // set it free
    }
        
} else if ( $btnText == "IDLE_TIMEOUT" ) { // set IDEL_TIMEOUT greater than DIALOGUE_TIMEOUT. no need to pass to DM
    $log->lwrite("Event: IDLE_TIMEOUT."); 
    
    $info = "";
    if($runMoreDMs) $info = releaseLockIfOccupied($usrid); // shouldn't need this, as timeout will trigger RELOAD
    if($info != "") $log->lwrite($info);
    
    $jsonObj = new \stdClass(); // needed for testing locally in Mac machines.      
    $jsonObj->meta = "IDLE_TIMEOUT";
    $jsonObj->utt = "IDLE_TIMEOUT msg received.";	        
    $sys = json_encode($jsonObj);

    // Agreed not to pass to DM. IDEL_TIMEOUT timer starts once the page is loaded.
    
} else if ( $btnText == "RELOAD" ) { // user refresh/reload browser
    $log->lwrite("Event: RELOAD.");    
    $info = "";
    
    if($runMoreDMs) $info = releaseLockIfOccupied($usrid);
    if($info != "") $log->lwrite($info);
    
    $jsonObj = new \stdClass(); // needed for testing locally in Mac machines.      
    $jsonObj->meta = "RELOAD";
    $jsonObj->utt = "RELOAD received.";	        
    $sys = json_encode($jsonObj); // JSON Object to string
    
    //TODO uncomment below to send to DM server when server code updated
    $socket = getConnection( $_SESSION["sysHost"], $_SESSION["sysPort"] ); // See notes in the method.
    $tmp = getResponse( $socket, $sys ); //$sys also sent back to client
    $log->lwrite("DM: RELOAD, returned: " . $tmp);    
    
} else if ( $btnText == "DIALOGUE_TIMEOUT" ) { // user refresh/reload browser
    $log->lwrite("Event: DIALOGUE_TIMEOUT.");    
        
    $info = "";
    if($runMoreDMs) $info = releaseLockIfOccupied($usrid);
    if($info != "") $log->lwrite($info);
    
    $jsonObj = new \stdClass(); // needed for testing locally in Mac machines.      
    $jsonObj->meta = "DIALOGUE_TIMEOUT";
    $jsonObj->utt = "PHP: DIALOGUE_TIMEOUT received.";	        
    $sys = json_encode($jsonObj); // JSON Object to string
    
    //TODO uncomment below to send to DM server when server code updated
    $socket = getConnection( $_SESSION["sysHost"], $_SESSION["sysPort"] ); // See notes in the method.
    $tmp = getResponse( $socket, $sys ); //$sys also sent back to client
    $log->lwrite("DM: DIALOGUE_TIMEOUT, returned: " . $tmp);    
    
} else {
       $log->lwrite("Event: Unknown.");    
    // shouldn't be the case.
    //$sys = "Request parameters error! $btnText";
    $jsonObj = new \stdClass(); // needed for testing locally in Mac machines.      
    $jsonObj->meta = "SYSTEMTURN";
    $jsonObj->utt = "Request parameters error!";	        
    $sys = json_encode($jsonObj); // JSON Object to string
    
    $log->lwrite("Request parameters error!");  
} 
       
$log->lwrite("Final to user: ".$sys);    
// return the results to the Ajax request    
echo $sys;
  
  // loop to check if some DM server shut down, then choose another available one
  // mark the port unavailable in DB with special name DM_ERROR in the usrID field.
  function makeInitConnection($availHost,$availPort, $sysNum,$usrid, $runMoreDMs, $jsonStr, $log )  {  
       if(!$runMoreDMs) {
          $socket = getConnection($availHost,$availPort ); 
          $sys = getResponse( $socket, $jsonStr ); // returned JSON string by server          
          return $sys;
       } 
          $log->lwrite("First Try to connect:". $availHost . ", port:" . $availPort . ", sysNum:" . $sysNum);
          $socket = getConnection($availHost,$availPort ); 
          $sys = getResponse( $socket, $jsonStr );
          while($sys == false) { // do not check getResponse(...) for now
          while(!$socket ) {
              $log->lwrite("Connect failure:". $availHost . ", port:" . $availPort . ", sysNum:" . $sysNum);
              updateDB($availHost, $availPort, $sysNum, "DM_ERROR", 1); // set occupied
             // sleep(5);
            $allSys = getAllSysNum() ;
            $numAllSys = count($allSys);        
            // get last accessed system
            $lastSysNum = getLastChosenSys();
            $availSys = getNextAvailSys($lastSysNum, $numAllSys);
                if($availSys == "ALL_BUSY") {
                   return $availSys;
                } 
            $sysNum = $availSys['dmsysnum'];
            $availHost = $availSys['instancehost'];
            $availPort = $availSys['instanceport'];

            $_SESSION["sysNum"] = $sysNum;
            $_SESSION["sysHost"] = $availHost; //DM host
            $_SESSION["sysPort"] = $availPort; // DM port
            
            $socket = getConnection($availHost,$availPort ); 
           
           }
                      
           $sys = getResponse( $socket, $jsonStr ); // returned JSON string by server
           
        }
       
        $log->lwrite("connected:". $availHost . ", port:" . $availPort . ", sysNum:" . $sysNum);
        updateDB($_SESSION["sysHost"], $_SESSION["sysPort"], $_SESSION["sysNum"], $usrid, 1); // set occupied
        
       $log->lwrite("INIT sys response:". $sys . "END");
        return $sys;
       }
        
    
   /*
     function getConnection( $port ):
     
     PHP scripts will die after this page exits as PHP's stateless.
     There is no simple way to keep socket connection object during
     the serveral turns/requests of one dialogue. And it can't be serialised
     into $_SESSION as it is an external object, not in the control of PHP.
     
     Possilbe to serialised the socket object to external database but each time
     of serialising and deserialising are expensive/taking more time.
     
     So for now, each request will create a new socket and connection.
     The JAVA socket server has the corresponding logic for the connections. 
   */ 
  function getConnection( $hostname, $port ) { 

	  //echo "Attempting to create socket... ";
	  $socket = socket_create( AF_INET, SOCK_STREAM, 0 );
	    //  or die( "error: could not create socket!" );
     //if (!is_resource($socket)) {
     //    echo 'Unable to create socket: '. socket_strerror(socket_last_error()) . PHP_EOL;
     //}
	    
	  if(!$socket) {
	      return false;
      }
      
	  //echo "Attempting to connect to '$hostname' on port '$port'... ";
	  $result = socket_connect( $socket, $hostname, $port );
	     // or die( "error: could not connect to '$hostname' on port '$port'..." );
	  if(!$result) return false;
	      
      // Now do not read anything when connecting, server won't send any now.
	  //$response = socket_read( $socket, 10000, PHP_NORMAL_READ )
	  //    or die( "error: failed to read from socket!" );

	  return $socket;
  }

    
  // using socket connection, send utterance to and receive response from the DM
  function getResponse( $socket, $input ) {
	  $input .= "\n";
	  $response = "";

	  $succ = socket_write( $socket, $input, strlen($input) );
	     //or die( "error: failed to write to socket!" );
      // It is perfectly valid for socket_write() to return zero which means no bytes have been written. Be sure to use the === operator to check for FALSE in case of an error.
      if($succ == false) return false;
      
	  $response = socket_read( $socket, 10000, PHP_NORMAL_READ );
	     //or die( "error: failed to read from socket!" );
      return $response; // it includes FALSE on failure if any.
  }
  
  // To inform DM server that some ports are over-occupyied and released.
  // $maxOccupyTime is defined above.
  function informDMServerOverOccupied($dbInfo) {
      // $dbInfo returned by db_query is a string which separats each row by ROW_END
      // OverOccupied and port Released for : , numRow=2, timeDiff=50, maxTime=40, host=137.195.27.135, port=8071, sysnum=0, userid=user1 
      //OverOccupied and port Released for : , numRow=2, timeDiff=50, maxTime=40, host=137.195.27.135, port=8072, sysnum=0, userid=user2 
      $msg = "";
      $jstr = getJsonStr($dbInfo);
      $jobj = json_decode($jstr);
      $info = $jobj->info; // array;
      for ($i = 0; $i < count($info); $i++) { // send to each of the occupied instances
        $host = $info[$i]->host;
        $port = $info[$i]->port;        

        if($host != "" && port != "") {
            $socket = getConnection( $host, $port ); 
            $msg .= getResponse( $socket, $jstr ) . "    ";
        } else {
           // something wrong
           $msg .= "    ERROR: Something wrong! PORT_TIMEOUT but host = ".$host.", port=".$port;
        }
      }
      return $msg;
  }
  
function getJsonStr($str1) {

    $array = explode('ROW_END', $str1);
    $list=array(); //instantiate the array 
    for ($i = 0; $i < count($array); $i++) {
        if($array[$i] != "")  {
            $row = $array[$i]; 
            $items = explode(',', $row); // each item separated by comma
            // parse items
            $host="";
            $port="";
            $sysnum="";
            $userid="";
            
            for ($j = 0; $j < count($items); $j++) {
                $item = $items[$j];
                if (strpos($item, '=') !== false) { // contains "="
                  $idx = strpos($item, '=') + 1;
                  $val = substr($item, $idx);
                  if (strpos($item, 'host') !== false) $host = $val;
                  if (strpos($item, 'port') !== false) $port = $val;
                  if (strpos($item, 'sysnum') !== false) $sysnum = $val;
                  if (strpos($item, 'userid') !== false) $userid = $val;
                }
            }
            
            $rowObj = new stdClass(); // create a new object
            $rowObj->orig_str=$row;
            $rowObj->host=$host;
            $rowObj->port=$port;
            $rowObj->sysnum=$sysnum;
            $rowObj->userid=$userid;
            
            array_push($list,$rowObj); // push object to stack array
        }
    }

    $jsonObj = new \stdClass(); // needed for testing locally in Mac machines.      
    $jsonObj->meta = "PORT_TIMEOUT";
    $jsonObj->info = $list;	        
    $jsonStr = json_encode($jsonObj);
    
    return $jsonStr;
}
  
  
?>
  
