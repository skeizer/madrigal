#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import json
from BaseHTTPServer import BaseHTTPRequestHandler
import os
import SocketServer
import argparse
import random
import time
import sys
import cgi


MAX_RECV = 16384
REC_EXTENSION = 'ogg'
SDS_ADDRESS = 'localhost'
SDS_PORTS = [8886]

DEFAULT_LOG_PATH = "./log"
MYDIR = os.path.dirname(__file__)
DEFAULT_TIMEOUT = -1
DEFAULT_ALLOW_IP = '0.0.0.0-255.255.255.255'


class IPRange(object):

    def __init__(self, range_str):
        """Initialize using a range string, such as 0.0.0.0-255.255.255.255."""
        self.lo, self.hi = (self._parse_addr(addr_str) for addr_str in range_str.split('-'))

    def _parse_addr(self, addr_str):
        """Parse an IP address to an integer representation for easy comparisons."""
        addr = [int(i) for i in addr_str.split('.')]
        if len(addr) != 4 or any([i < 0 for i in addr]) or any([i > 255 for i in addr]):
            raise ValueError('Invalid IP address: %s' % addr_str)
        val = 0
        for i in addr:
            val *= 255
            val += i
        return val

    def is_in_range(self, addr_str):
        """Return True if the given address (string) is in this range."""
        addr = self._parse_addr(addr_str)
        return self.lo <= addr and addr <= self.hi


class Handler(BaseHTTPRequestHandler):

    # set a timeout so that the server never hangs (this should be vastly more than enough
    # for handling a single request, with no file access blocking etc.)
    timeout = 30

    def do_POST(self):
        ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
        if ctype == 'multipart/form-data':
            postvars = cgi.parse_multipart(self.rfile, pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers.getheader('content-length'))
            postvars = cgi.parse_qs(self.rfile.read(length), keep_blank_values=1)
        else:
            postvars = {}
        err_code, response = self.server.handle(self.client_address, postvars)
        self.send_response(err_code)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        self.wfile.write(json.dumps(response))


class SdsGateServer(SocketServer.TCPServer):

    def __init__(self, server_address, settings, bind_and_activate=True):
        """Constructor. May be extended, do not override."""

        SocketServer.TCPServer.__init__(self, server_address, Handler, False)

        self.log_path = settings['log']
        self.timeout = settings['timeout']
        self.allow_ip = IPRange(settings['allow_ip'])

        # start serving
        if bind_and_activate:
            self.server_bind()
            self.server_activate()

    def log_text(self, logdir, text):
        with codecs.open(os.path.join(logdir, 'log.txt'), "a", 'UTF-8') as fh_out:
            print >> fh_out, time.strftime('%Y-%m-%d %H:%M:%S') + "\t" + text

    def log_data(self, logdir, file_name, data):
        with open(os.path.join(logdir, file_name), "wb") as fh_out:
            fh_out.write(data)

    def is_addr_allowed(self, client_addr):
        """Return true if the given client address (IP + port) is allowed to add keys."""
        return self.allow_ip.is_in_range(client_addr[0])

    def get_session(session_id):
        if session_id not in self.sessions:
            address = (SDS_ADDRESS, random.choice(SDS_PORTS))
            sock.connect(address)
            self.sessions[session_id] = # TODO FIX HERE

    def handle(client_addr, data):
        try:
            sock, logdir = self.get_session(data['session_id'])

            if data['asr']:
                sock.send(data['asr'])
            reply = sock.recv(MAX_RECV)

            log_buf = "ASR IN:\n" + unicode(data['asr']) + "\n"

            if 'rec' in data:
                timestamp = time.strftime('%Y-%m-%d-%H_%M_%S.%f')
                rec_fname = timestamp + REC_EXTENSION
                log_buf += "RECORDING:" + rec_fname + "\n"
                self.log_data(logdir, rec_fname, data['rec'])

            log_buf += "REPLY:\n" + reply

            self.log_text(logdir, "\n" + log_buf)

            return 200, reply
        except:
            print  sys.exc_info()
            return 500, 'Server error'


def run(settings={}):

    httpd = SdsGateServer(('', settings['port']), settings, True)
    sa = httpd.socket.getsockname()

    print "Serving HTTP requests on", sa[0], "port", sa[1], "..."
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()
    print "Server Stopped - %s:%s" % (sa[0], sa[1])


if __name__ == '__main__':
    random.seed()
    ap = argparse.ArgumentParser()
    ap.add_argument('-p', '--port', type=int, default=DEFAULT_PORT)
    ap.add_argument('-l', '--log', default=DEFAULT_LOG_PATH)
    ap.add_argument('-t', '--timeout', type=int, default=DEFAULT_TIMEOUT)
    ap.add_argument('-i', '--allow-ip', default=DEFAULT_ALLOW_IP)
    args = ap.parse_args()
    run(settings=vars(args))
