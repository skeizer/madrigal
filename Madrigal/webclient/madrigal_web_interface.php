<?php
  session_start();
?>
<!DOCTYPE html>
<html
<body>

  <center>

  <h1>Dialogue System Web Interface</h1>

  <p>To start a new dialogue, click 'Start'.  Type utterances in the text field, 
    each followed by clicking 'Send'.  To end the dialogue, click 'Stop'</p>

  <?php

     if ( isset($_GET["taskID"]) ) {
        $_SESSION["taskID"] = $_GET["taskID"];
        echo "<p>Task ID: " . $_SESSION["taskID"] . "</p>";

     } else if ( $_SESSION["taskID"] == "" ) {
        $_SESSION["taskID"] = "none";
        echo "<p>Task ID: " . $_SESSION["taskID"] . "</p>";
     }

     $msg = "";
     $usr = "";
     $sys = "";
     $status = "INIT";
     if ( $_SESSION["status"] ) {
	 $status = $_SESSION["status"];
     }
     //echo "<p>Status 1: " . $status . "</p>";

     if ( isset($_POST["startBut"]) ) {
	 //echo "Start button clicked<br/>";
	 if ( $status == "INIT" ) {
	     $status = "STARTED";

	     // assign random port (each corresponding to different SDS)
	     //$systems = array( 8080, 8081, 8082 ); // three different SDSs; adjust as appropriate
	     $systems = array( 8080 );
	     $randInd = rand( 0, count($systems)-1 ); // alternatively, use rotation system
	     $sysPort = $systems[ $randInd ];
	     echo "<p>Random system port: $sysPort</p>";
	     $_SESSION["sysPort"] = $sysPort;
	
	     // send meta information (task ID, user IP, ...)
	     $metaMsg = "META:taskID=" . $_SESSION["taskID"] . ",userIP=" . $_SERVER["REMOTE_ADDR"];
	     $socket = getConnection( $sysPort ); // reads one server message
	     $msg = getResponse( $socket, $metaMsg );
	 
	 } else {
	     $msg = "ERROR: already started";
	 }

     } else if ( isset($_POST["stopBut"]) ) {
       //echo "Stop button clicked<br/>";
       if ( $status == "STARTED" ) {
          $status = "INIT";
	  // send HANGUP
	  $socket = getConnection( $_SESSION["sysPort"] ); // reads one server message
	  $msg = getResponse( $socket, "HANGUP" );
       } else {
          $msg = "ERROR: already stopped";
       }

     } else if ( isset($_POST["sendBut"]) ) {
       //echo "Send button clicked<br/>";
       if ( $_POST["usrUtt"] == "" ) {
          $msg = "ERROR: enter an utterance first";
       } else {
          if ( $status == "STARTED" ) {
             $usr = $_POST["usrUtt"];
	     $socket = getConnection( $_SESSION["sysPort"] ); // reads one server message
	     $sys = getResponse( $socket, $usr ); // sends message and receives response
          } else {
             $msg = "ERROR: start dialogue first";
          }
       }
     }

     echo '<form method="post">';
     if ( $status == "STARTED" ) {
	 echo '<input type="submit" name="startBut" value="Start" id="startID" disabled />';
	 echo '<input type="text" name="usrUtt" /><input type="submit" name="sendBut" value="Send" />';
	 echo '<input type="submit" name="stopBut" value="Stop" id="stopID" />';
     } else {
	 echo '<input type="submit" name="startBut" value="Start" id="startID" />';
	 echo '<input type="text" name="usrUtt" disabled /><input type="submit" name="sendBut" value="Send" disabled />';
	 echo '<input type="submit" name="stopBut" value="Stop" id="stopID" disabled />';
     }
     echo '</form>';

     echo "<p>Status: " . $status . "</p>";
     if ( $msg != "" ) {
        echo "<p>Message: $msg</p>";
     }
     
     if ( $_SESSION["status"] != "INIT" && $status != "INIT" ) {
        $_SESSION["history"] .= "Usr> $usr<br/>";
        $_SESSION["history"] .= "Sys> $sys<br/>";
        echo "<p>" . $_SESSION["history"];
     } else if ( $_SESSION["status"] == "STARTED" && $status == "INIT" ) {
     //} else if ( $status == "INIT" ) {
        echo "<p>" . $_SESSION["history"] . "END OF DIALOGUE<br/>";
        session_unset();
        //session_destroy();
     }

     $_SESSION["status"] = $status;

     function getConnection( $port ) {
	 $hostname = "localhost";
	 //$port = 8080;

	 //echo "Attempting to create socket... ";
	 $socket = socket_create( AF_INET, SOCK_STREAM, 0 )
	     or die( "error: could not create socket<br/>" );
	 //echo "OK.<br/>";

	 //echo "Attempting to connect to '$hostname' on port '$port'... ";
	 $result = socket_connect( $socket, $hostname, $port )
	     or die( "error: could not connect to host<br/>" );
	 //echo "OK.<br/>";

	 $response = socket_read( $socket, 10000, PHP_NORMAL_READ )
	     or die( "error: failed to read from socket<br/>" );
	 //echo "Server: $response<br/>";

	 return $socket;
     }

     // using socket connection, send utterance to and receive response from the DM
     function getResponse( $socket, $input ) {
	 $input .= "\n";
	 $response = "";
	 //echo "Sending utterance: $input<br/>";
	 socket_write( $socket, $input, strlen($input) )
	     or die( "error: failed to write to socket<br/>" );
	 //echo "Utterance sent: $input<br/>";

	 $response = socket_read( $socket, 10000, PHP_NORMAL_READ )
	     or die( "error: failed to read from socket<br/>" );
	 //echo "Response received: $response<br/>";

	 return $response;
     }

  ?>

  </center>

</body>
</html>
