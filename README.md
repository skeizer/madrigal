![MaDrIgAL logo](madrigal-logo-curves_smaller.png)

# Multi-Dimensional Interaction Management and Adaptive Learning. #

This repository contains a java implementation of a multi-dimensional dialogue manager, i.e., a dialogue manager that selects system
response actions along multiple dimensions.  The concept is based on Bunt's notion of multi-dimensionality in communication and 
multi-functional utterances in (spoken) dialogue.

Our DM is a multi-agent system that can be trained using multi-agent reinforcement learning.  Each agent is dedicated to one dimension 
from the ISO standard for dialogue act annotation (ISO 24617-2); currently, we support the dimensions Task, Auto-Feedback, and Social Obligations Management.
Further details can be found in these papers:

* Simon Keizer and Verena Rieser.  *Towards Learning Transferable Conversational Skills using Multi-dimensional Dialogue Modelling*.
[arXiv:1804.00146 [cs.CL]](https://arxiv.org/abs/1804.00146), March 2018 (extended version of SemDial 2017 short paper).

* Simon Keizer, Ondrej Dušek, Xingkun Liu, and Verena Rieser.  *User Evaluation of a Multi-dimensional Statistical Dialogue System*.
Proc. SIGdial, September 2019.
[arXiv:1909.02965 [cs.CL cs.AI]](https://arxiv.org/abs/1909.02965)

The data from this evaluation is available at [https://bitbucket.org/skeizer/madrigal-corpus/](https://bitbucket.org/skeizer/madrigal-corpus).


## Instructions ##

The subdirectory **Madrigal** contains a java project with code for the dialogue manager and user simulator.

### Eclipse ###

Import the java project in Eclipse from the git repository:

1. Select File | Import... | Git | Projects from git | Clone URI
2. Enter the URI (https://skeizer@bitbucket.org/skeizer/madrigal.git) and your credentials, and click 'Next'
3. Select branch (master) and click 'Next'
4. Specify you local git directory and click 'Next'
5. Select 'Import existing eclipse projects' and click 'Next'
6. Under 'Projects', select the project 'Madrigal' and click 'Finish'

(Note that project files for NetBeans can be found in the folder 'nbproject'.)

### Running the Dialogue Manager ###

To test the dialogue manager (DM), run the java class *Madrigal* from eclipse, or from the command line:

``java -cp lib/mallet.jar:lib/java-json.jar:lib/commons-lang3-3.4.jar:lib/commons-cli-1.3.1.jar:target/classes/. uk.ac.hw.madrigal.Madrigal``

This will create a log file in the directory *logs*, showing a number of simulated dialogues between the DM and the simulated user.
(Note that this runs an outdated rule-based DM, resulting in unsuccessful dialogues only.  See below for training, evaluating and testing DM policies.)

The default settings of the dialogue manager and simulator are specified in the *src/resources/CoreSettings.cfg*, but can be changed 
in a separate configuration file passed to the program with the option *-c config.cfg*.  For example, the number of dialogues and maximum number 
of turns per dialogue, as well as the DM policies to use, can be specified in this way.

### Training and evaluating policies in simulation ###

The recommended procedure is to run experiments from a separate working directory and create an environment variable *MADRIGAL_HOME* for easy access to 
the scripts for running training and evaluating sessions.  Some typical configuration files for training and evaluating dialogue policies are stored 
in *src/resources* and can be copied to your working directory and modified where necessary.  For some info on using the script *run_simulations.sh*, run:

``$MADRIGAL_HOME/scripts/run_simulations.sh -h``

To run multiple training sessions, use:

``$MADRIGAL_HOME/scripts/run_multi_sims_tra.sh -c config.cfg``

where the number of runs is currently still hard-coded in the script, but can be easily changed.  For running the evaluations corresponding to the above training sessions, run:

``$MADRIGAL_HOME/scripts/run_multi_sims_eval.sh <number-of-training-dialogues> <policy-directory> -c config.cfg``

where *<number-of-training-dialogues>* is the number of simulated dialogues used to train the policies, and *<policy-directory>* is the directory containing 
subdirectories (*tra_0* to *tra_<num_sessions-1>*) for each of the training sessions containing the trained policies (at different stages of training, e.g. after every 1000 dialogues).

(see also *scripts/README*)

### Running the Dialogue System ###

To run the text-in text-out dialogue system as a server, run the class *TextDialSystem* from eclipse, or from the command line:

``java -cp lib/mallet.jar:lib/java-json.jar:lib/commons-lang3-3.4.jar:lib/commons-cli-1.3.1.jar:target/classes/. uk.ac.hw.madrigal.TextDialSystem -c <config>``

You can use the configuration file *dist/config_mdp.cfg* to run the system locally.  To interact with the system, run the class TextDialSystemClient with the correct port number (8080 in case of running locally with the above-mentioned configuration).

The dialogue system server can also be run using websockets, in which case it can be tested with a web-interface, see *webclient/sds_web_interface.html*.  To do this, change the configuration as follows:

RUN_SERVER = false

RUN_WEBSOCKET_SERVER = true

To test the web interface locally, you can run an http server in the webclient folder:

``python -m SimpleHTTPServer 8000`` (for more recent versions of python, use ``python -m http.server 8000``)

and then open the interface here:

``localhost:8000/speech_demo.html``

There is also a text-only web interface in php.  To test this locally, you can run a php server in the webclient folder:

``php -S localhost:8000``

and then open the interface here:

``localhost:8000/madrigal_web_interface.php``

If both of the above server variables are set to false, you will get a simple GUI interface to test the dialogue system locally without a server.

### Using RASA NLU ###

If you have RASA-NLU installed and have trained a model, located in the folder ``./models/current/nlu``, 
you can create a local http endpoint:

``python -m rasa_nlu.server --path models/``

In the Madrigal configuration, enable these settings:

NLU_SYSTEM = RASA

RASA_HOST = http://localhost

RASA_PORT = 5000

The system can be tested as before by using the local http server as explained in the previous section.
